RewriteBase /
RewriteEngine On

# ЧПУ, исключения
RewriteRule ^modules/mapsite/sitemapxml_cron.php modules/mapsite/sitemapxml_cron.php [QSA,L]
RewriteRule ^sitemap.xml modules/mapsite/sitemapxml_cron.php [QSA,L]
RewriteRule ^modules/newsi/rss.php modules/newsi/rss.php [QSA,L]
RewriteRule ^rss.xml modules/newsi/rss.php [QSA,L]
RewriteRule ^components/captcha/captcha.php components/captcha/captcha.php [QSA,L]
RewriteRule ^components/html_editor/ckeditor/plugins/kcfinder/browse.php components/html_editor/ckeditor/plugins/kcfinder/browse.php [QSA,L]
RewriteCond %{REQUEST_URI} (components|\.css|\.scss|\.less|\.json|\.xml|\.js|\.jpg|\.jpeg|\.png|\.gif|\.doc|\.docx|\.xls|\.xlsx|\.rtf|\.txt|\.pdf|\.ttf|\.eot|\.svg|\.woff|\.woff2|\.mp4|\.mp3|\.mov|\.ogv|\.webm)
RewriteRule ^(.+) $1 [QSA,L]

# 301-редирект с добавлением слеша в конец ЧПУ
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteCond %{REQUEST_URI} !-f
RewriteCond %{REQUEST_URI} !/$
RewriteCond %{REQUEST_URI} !.html$
RewriteRule (.+) $1/ [R=301,L]

# ЧПУ, базовые правила
RewriteRule ^([a-z0-9_-]+)/page-(\d+)/ index.php?sitepage=$1&page=$2 [QSA,L]
RewriteRule ^([a-z0-9_-]+)/$ index.php?sitepage=$1 [QSA,L]

# ЧПУ для модуля "Галерея"
RewriteRule ^([a-z0-9_-]+)/([a-z0-9_-]+)-gcat(\d+)/page-(\d+)/$ index.php?sitepage=$1&gcat=$3&page=$4 [QSA,L]
RewriteRule ^([a-z0-9_-]+)/([a-z0-9_-]+)-gcat(\d+)/$ index.php?sitepage=$1&gcat=$3 [QSA,L]

# ЧПУ для модуля "Новости"
RewriteRule ^([a-z0-9_-]+)/start-([0-9-]+)/stop-([0-9-]+)/$ index.php?sitepage=$1&start=$2&stop=$3 [QSA,L]
RewriteRule ^([a-z0-9_-]+)/start-([0-9-]+)/stop-([0-9-]+)/page-(\d+)/$ index.php?sitepage=$1&start=$2&stop=$3&page=$4 [QSA,L]
RewriteRule ^([a-z0-9_-]+)/date-([0-9-]+)/page-(\d+)/$ index.php?sitepage=$1&date=$2&page=$3 [QSA,L]
RewriteRule ^([a-z0-9_-]+)/tags/([a-z0-9_-]+)-i(\d+)/page-(\d+)/$ index.php?sitepage=$1&tags=$2&page=$4 [QSA,L]
RewriteRule ^([a-z0-9_-]+)/tags/([a-z0-9_-]+)-i(\d+)/$ index.php?sitepage=$1&tags=$3 [QSA,L]
RewriteRule ^([a-z0-9_-]+)/category/([a-z0-9_-]+)-i(\d+)/page-(\d+)/$ index.php?sitepage=$1&category=$2&page=$4 [QSA,L]
RewriteRule ^([a-z0-9_-]+)/category/([a-z0-9_-]+)-i(\d+)/$ index.php?sitepage=$1&category=$3 [QSA,L]
RewriteRule ^([a-z0-9_-]+)/date-([0-9-]+)/$ index.php?sitepage=$1&date=$2 [QSA,L]
RewriteRule ^([a-z0-9_-]+)/([a-z0-9_-]+)-p(\d+)\.html$ index.php?sitepage=$1&id=$3 [QSA,L]

# ЧПУ для модуля "Каталог"
RewriteRule ^([a-z0-9_-]+)/([a-z0-9_-]+)-c(\d+)/([a-z0-9_-]+)-i(\d+)\.html$ index.php?sitepage=$1&cid=$3&itemid=$5 [QSA,L]
RewriteRule ^([a-z0-9_-]+)/([a-z0-9_-]+)-i(\d+)\.html$ index.php?sitepage=$1&itemid=$3 [QSA,L]
RewriteRule ^([a-z0-9_-]+)/([a-z0-9_-]+)-c(\d+)/page-(\d+)/$ index.php?sitepage=$1&cid=$3&page=$4 [QSA,L]
RewriteRule ^([a-z0-9_-]+)/([a-z0-9_-]+)-c(\d+)/ index.php?sitepage=$1&cid=$3 [QSA,L]

# ЧПУ для модуля "Вопросы и ответы"
RewriteRule ^([a-z0-9_-]+)/([a-z0-9_-]+)-a(\d+)/page-(\d+)/$ index.php?sitepage=$1&a=2&b=$3&page=$4 [QSA,L]
RewriteRule ^([a-z0-9_-]+)/([a-z0-9_-]+)-a(\d+)/ index.php?sitepage=$1&a=2&b=$3 [QSA,L]
RewriteRule ^([a-z0-9_-]+)/([a-z0-9_-]+)-q(\d+)/ index.php?sitepage=$1&a=3&b=$3 [QSA,L]

# ЧПУ для модуля "Поиск по сайту"
RewriteRule ^([a-z0-9_-]+)/(.+)/page-(\d+)/ index.php?sitepage=$1&search=$2&page=$3 [QSA,L]
RewriteRule ^([a-z0-9_-]+)/(.+)/ index.php?sitepage=$1&search=$2 [QSA,L]
### End of Pretty URL ###

### delete the copy of the main page ( http://your_domain.ru/index.html)
# RewriteCond %{THE_REQUEST} ^[A-Z]{3,9}\ /index\.html\ HTTP/
# RewriteRule ^index\.html$ http://your_domain.ru/ [R=301,L]
# RewriteRule ^index$ http://your_domain.ru/ [R=301,L]
# RewriteRule ^index/$ http://your_domain.ru/ [R=301,L]

RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteCond %{REQUEST_FILENAME} !-l
RewriteRule   ^[a-z0-9_-]*\.html$ index.php

AddDefaultCharset UTF-8
AddType text/x-component .htc