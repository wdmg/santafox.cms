DROP TABLE IF EXISTS `%PREFIX%_backup_rules`;
-- sqlseparator------------------------------------------------
CREATE TABLE `%PREFIX%_backup_rules` (
  `id` int(5) unsigned NOT NULL auto_increment,
  `stringid` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `ftphost` varchar(255) default NULL,
  `ftpuser` varchar(255) default NULL,
  `ftppass` varchar(255) default NULL,
  `ftpdir` varchar(255) default NULL,
  `needcontent` tinyint(1) unsigned NOT NULL default '0',
  `needsystem` tinyint(1) unsigned NOT NULL default '0',
  `needtables` tinyint(1) unsigned NOT NULL default '0',
  `needdesign` tinyint(1) unsigned NOT NULL default '0',
  `replace_every_days` smallint(3) unsigned default 1,
  `replace_every_weeks` smallint(3) unsigned default 0,
  `replace_every_months` smallint(3) unsigned default 0,
  `replace_every_years` smallint(3) unsigned default 0,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `stringid` (`stringid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Правила backup';
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_backup_rules` VALUES ('2','contab','Таблицы и контент','','','','/','1','0','1','0',1,0,0,0);