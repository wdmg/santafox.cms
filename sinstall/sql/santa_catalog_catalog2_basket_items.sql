DROP TABLE IF EXISTS `%PREFIX%_catalog_catalog2_basket_items`;
-- sqlseparator------------------------------------------------
CREATE TABLE `%PREFIX%_catalog_catalog2_basket_items` (
  `id` int(5) unsigned NOT NULL auto_increment,
  `orderid` int(5) unsigned NOT NULL,
  `sessionid` varchar(32) NOT NULL,
  `itemid` int(5) unsigned NOT NULL,
  `qty` int(5) unsigned NOT NULL,
  `option_group` int(5) unsigned DEFAULT '0',
  `choice` int(5) unsigned DEFAULT '0',
  PRIMARY KEY  (`id`),
  KEY `orderid` (`orderid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
