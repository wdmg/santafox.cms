<?php
/**
 * Основной (базовый) модуль
 *
 * @contributors Oslix <oslix@yandex.ru>, sanchez <sanchezby@gmail.com>, Alexsander Vyshnyvetskyy (alex_wdmg) <wdmg.com.ua@gmail.com>, Ivan Kudryavsky <spam@mr-god.net>, Rinat <mail.rinat@yandex.ru>
 */

abstract class BaseModule
{
    /**
     * Current module ID
     * @var string
     */
    public $module_id;

    /**
     * Current module name
     * @var string
     */
    public $module_name;

    /**
     * Parsed templates
     * @var array
     */
    public $templates = array();

    public function __construct()
    {
        global $kernel;
        $this->module_id = $kernel->pub_module_id_get();
        $this->module_name = $this->get_current_module_name();
    }

    public function get_recaptcha($template)
    {
		$public_key = null;
		if (defined("RECAPTCHA_PUBLIC_KEY") && RECAPTCHA_PUBLIC_KEY)
			$public_key = RECAPTCHA_PUBLIC_KEY;
		
		return str_replace('%recaptcha_key%', $public_key, $template);
    }
	
    public function get_captcha_img_url()
    {
        return '/components/captcha/captcha.php?'.time();
    }

    public function is_valid_captcha($captcha)
    {
        global $kernel;
		
		if (defined("CAPTCHA_SERVICE") && CAPTCHA_SERVICE == 'recaptcha') {
			
			if (!$captcha)
				return false;
			
			$private_key = null;
			$remote_addr = $kernel->pub_user_ip_get();
			if (defined("RECAPTCHA_PRIVATE_KEY") && RECAPTCHA_PRIVATE_KEY)
				$private_key = RECAPTCHA_PRIVATE_KEY;
			
			require_once(dirname(dirname(__FILE__)).'/components/recaptcha/autoload.php');
			$recaptcha = new \ReCaptcha\ReCaptcha($private_key);
			
			$resp = $recaptcha->verify($captcha, $remote_addr);
			if ($resp->isSuccess()) {
				return true;
			} else {
				/*$errors = $resp->getErrorCodes();
				$kernel->debug($errors, true);*/
				return false;
			}
		} else {
			if (!$captcha)
				return false;

			require_once(dirname(dirname(__FILE__)).'/components/captcha/php-captcha.inc.php');
			return PhpCaptcha::Validate($captcha);
		}
    }

    public function get_current_module_name() {
        global $kernel;
        $manager_modules = new manager_modules();
        $current_module = $manager_modules->return_info_modules($kernel->pub_module_id_get());
        return $kernel->pub_page_textlabel_replace($current_module['full_name']);
    }
	
    protected function get_module_prop_value($propid,$default=null)
    {
        global $kernel;
        $prop = $kernel->pub_modul_properties_get($propid);
        if (!$prop || !$prop['isset'])
            return $default;
        return $prop['value'];
    }

    protected static function process_image_settings_block($block,$settings)
    {
        global $kernel;

        $thumb_settings = isset($settings['small'])?$settings['small']:array();
        $big_settings = isset($settings['big'])?$settings['big']:array();
        $src_settings = isset($settings['source'])?$settings['source']:array();

        $block = str_replace('%pict_source_transparency%', isset($src_settings['transparency'])?$src_settings['transparency']:"", $block);
        $block = str_replace('%source_check%', (isset($src_settings['isset']) && $src_settings['isset'])?"checked":"", $block);
        $block = str_replace('%path_source_water_path%', isset($src_settings['water_path'])?$src_settings['water_path']:"", $block);
        $block = str_replace('%pict_source_width%', (isset($src_settings['width']))?$src_settings['width']:"", $block);
        $block = str_replace('%pict_source_height%', (isset($src_settings['height']))?$src_settings['height']:"", $block);
        //Отметим тек значение по добавлению водяного знака
        $wm = array("pswas0"=> "", "pswas1"=> "",  "pswas2" => "");
        if (isset($src_settings['water_add']))
            $wm['pswas'.intval($src_settings['water_add'])] = ' selected="selected"';
        $block = $kernel->pub_array_key_2_value($block, $wm);
        //список возможного расположения водяного знака
        $wm = array("pswps0"=> "","pswps1"=> "","pswps2" => "","pswps3" => "","pswps4" => "");
        if (isset($src_settings['place']))
            $wm['pswps'.intval($src_settings['place'])] = ' selected="selected"';
        $block = $kernel->pub_array_key_2_value($block, $wm);


        $block = str_replace('%pict_big_transparency%', isset($big_settings['transparency'])?$big_settings['transparency']:"", $block);
        $block = str_replace('%big_check%', (isset($big_settings['isset']) && $big_settings['isset'])?"checked":"", $block);
        $block = str_replace('%path_big_water_path%', isset($big_settings['water_path'])?$big_settings['water_path']:"", $block);
        $block = str_replace('%pict_big_width%', (isset($big_settings['width']))?$big_settings['width']:"", $block);
        $block = str_replace('%pict_big_height%', (isset($big_settings['height']))?$big_settings['height']:"", $block);
        //Отметим тек значение по добавлению водяного знака
        $wm = array("pbwas0"=> "", "pbwas1"=> "",  "pbwas2" => "");
        if (isset($big_settings['water_add']))
            $wm['pbwas'.intval($big_settings['water_add'])] = ' selected="selected"';
        $block = $kernel->pub_array_key_2_value($block, $wm);
        //список возможного расположения водяного знака
        $wm = array("pbwps0"=> "","pbwps1"=> "","pbwps2" => "","pbwps3" => "","pbwps4" => "");
        if (isset($big_settings['place']))
            $wm['pbwps'.intval($big_settings['place'])] = ' selected="selected"';
        $block = $kernel->pub_array_key_2_value($block, $wm);


        $block = str_replace('%small_check%', (isset($thumb_settings['isset']) && $thumb_settings['isset'])?"checked":"", $block);
        $block = str_replace('%pict_small_width%', (isset($thumb_settings['width']))?$thumb_settings['width']:"", $block);
        $block = str_replace('%pict_small_height%', (isset($thumb_settings['height']))?$thumb_settings['height']:"", $block);

        return $block;
    }

    public static function make_default_pict_prop_addparam()
    {
        $ret = array();
        $ret['pict_path']			     = '';

        $ret['source']['isset']          = true;
        $ret['source']['width']          = 800;
        $ret['source']['height']         = 600;
        $ret['source']['water_add']      = '0';
        $ret['source']['water_path']     = '';
        $ret['source']['water_position'] = '0';

        $ret['big']['isset']          = true;
        $ret['big']['width']          = 400;
        $ret['big']['height']         = 300;
        $ret['big']['water_add']      = '1';
        $ret['big']['water_path']     = '';
        $ret['big']['water_position'] = '3';

        // Маленькое изображение без знаков
        $ret['small']['isset']        = true;
        $ret['small']['width']        = 100;
        $ret['small']['height']       = 100;
        return $ret;
    }

    /**
	 * Функция для отображения административного интерфейса
	 *
	 * @return string
	*/
    abstract public function start_admin();

    /**
     * Функция для построения меню для административного интерфейса
     *
     * @param pub_interface $menu Обьект класса для управления построением меню
     * @return boolean true
     */
    abstract public function interface_get_menu($menu);

    /**
     * Устанавливает шаблоны
     *
     * @param array $templates Массив распаршенных шаблонов
     */
    public function set_templates($templates)
    {
        $this->templates = $templates;
    }

    /**
     * Возвращает указанный блок шаблона
     *
     * @param string $block_name Имя блока
     * @return mixed
     */
    public function get_template_block($block_name)
    {
        if(isset($this->templates[$block_name]))
            return str_replace('%submodul_name%', $this->module_name, $this->templates[$block_name]);
        else
            return null;
    }

    /**
     * Возвращает указанный блок шаблона с учётом глубины
     *
     * @param string $block_name Имя блока
     * @param integer $depth глубина
     * @return mixed
     */
    public function get_template_block_with_depth($block_name, $depth)
    {
        if (!isset($this->templates[$block_name]))
            return null;

        if (is_array($this->templates[$block_name]))
        {
            $arr_size = count($this->templates[$block_name]);
            if ($arr_size > $depth)
                return $this->templates[$block_name][$depth];
            else
                return $this->templates[$block_name][$arr_size-1];
        }
        else
            return $this->templates[$block_name];
    }


    /**
     * Удаляет оставшиеся метки %label% в тексте шаблонов
     *
     * @param string $str
     * @return string
     */
    public function clear_left_labels($str)
    {
		global $kernel;
        return $kernel->tpl_clear_labels($str, false);
    }

    /**
     *  Строит блок постраничной навигации
     * @param integer $total общее кол-во элементов в выборке
     * @param integer $offset смещение
     * @param integer $perpage кол-во элементов на страницу
     * @param string $q префикс-строка для урлов страниц
     * @param integer $maxpages макс. кол-во страниц в блоке, если 0, то нет ограничения
     * @param string $linkLabelName название метки, в которой проставляется ссылка (в разных модулях по-разному)
     * @return string
     */
    public function build_pages_nav($total, $offset, $perpage, $q, $maxpages=0, $linkLabelName="link", $offset_name='')
    {
		global $kernel;

        //Строим постраничную навигацию только тогда, когда это нужно
        if (!$perpage || $total<=$perpage)
            return $this->get_template_block('pages_null');
        $pages_count = ceil($total/$perpage);
        $currpage = ceil($offset/$perpage)+1;
        if ($currpage<1 || $currpage>$pages_count)
            $currpage=1;

        if ($maxpages)
        {
            $startBlockPage=$currpage-floor($maxpages/2);
            if ($startBlockPage<1)
                $startBlockPage=1;
            $finishBlockPage=$startBlockPage+$maxpages-1;
            if ($finishBlockPage>$pages_count)
                $finishBlockPage=$pages_count;
        }
        else
        {
            $startBlockPage = 1;
            $finishBlockPage = $pages_count;
        }

        $pblock = $this->get_template_block('pages');
        if ($currpage>1)
        {
			if (defined("USE_PRETTY_URL") && USE_PRETTY_URL && !$kernel->is_backend()) {
				$previous = str_replace('%'.$linkLabelName.'%', $q.($currpage-1)."/", $this->get_template_block('page_previous'));
				$first = str_replace('%'.$linkLabelName.'%', $q.'0/', str_replace('%page_num%',1,$this->get_template_block('page_first')));
			} else {
				$previous = str_replace('%'.$linkLabelName.'%', $q.($currpage-2)*$perpage, $this->get_template_block('page_previous'));
				$first = str_replace('%'.$linkLabelName.'%', $q.'0', str_replace('%page_num%',1,$this->get_template_block('page_first')));
			}

			//переходная, пока не адаптировал под все модули
			if (empty($offset_name))
			{

				$first = str_replace('%page_num%',1,$this->get_template_block('page_first'));
				$first = str_replace('%'.$linkLabelName.'%', $q.'0',$first);

			}
			else
			{

				//для первого - вырезаем
				$first = str_replace('%page_num%',1,$this->get_template_block('page_first'));

				$query = parse_url($q);
				$query = $query['query'];
				$query = explode('&', $query);
				$key = array_search($offset_name.'=', $query);

				unset($query[$key]);

				$qf = '?'.join('&', $query);

				$first = str_replace('%'.$linkLabelName.'%', $qf, $first);
			}

        }
        else
        {
            $previous = $this->get_template_block('page_previous_disabled');
            $first = $this->get_template_block('page_first_disabled');
        }
		
        $pblock = str_replace('%first%', $first, $pblock);
        $pblock = str_replace('%previous%', $previous, $pblock);

        if ($startBlockPage>1)
			if (defined("USE_PRETTY_URL") && USE_PRETTY_URL && !$kernel->is_backend())
				$backward = str_replace('%'.$linkLabelName.'%', $q.($startBlockPage-1)."/", $this->get_template_block('page_backward'));
			else
				$backward = str_replace('%'.$linkLabelName.'%', $q.(($startBlockPage-2)*$perpage), $this->get_template_block('page_backward'));
        else
            $backward = $this->get_template_block('page_backward_disabled');
		
		$pblock = str_replace('%backward%', $backward, $pblock);

        if ($currpage<$pages_count) //есть ли страницы дальше?
        {
			if (defined("USE_PRETTY_URL") && USE_PRETTY_URL && !$kernel->is_backend()) {
				$next = str_replace('%'.$linkLabelName.'%', $q.($currpage+1)."/", $this->get_template_block('page_next'));
				$last = str_replace('%'.$linkLabelName.'%', $q.($pages_count)."/", str_replace('%page_num%',$pages_count,$this->get_template_block('page_last')));
			} else {
				$next = str_replace('%'.$linkLabelName.'%', $q.($currpage*$perpage), $this->get_template_block('page_next'));
				$last = str_replace('%'.$linkLabelName.'%', $q.(($pages_count-1)*$perpage), str_replace('%page_num%',$pages_count,$this->get_template_block('page_last')));
			}
        }
        else
        {
            $next = $this->get_template_block('page_next_disabled');
            $last = $this->get_template_block('page_last_disabled');
        }
        $pblock = str_replace('%last%', $last, $pblock);
        $pblock = str_replace('%next%', $next, $pblock);

        if ($finishBlockPage<$pages_count)
			if (defined("USE_PRETTY_URL") && USE_PRETTY_URL && !$kernel->is_backend())
				$forward = str_replace('%'.$linkLabelName.'%',  $q.($finishBlockPage+1)."/", $this->get_template_block('page_forward'));
			else
				$forward = str_replace('%'.$linkLabelName.'%',  $q.($finishBlockPage*$perpage), $this->get_template_block('page_forward'));
        else
            $forward = $this->get_template_block('page_forward_disabled');
        
		$pblock = str_replace('%forward%', $forward, $pblock);
		
        $pages = array();
        for ($p=$startBlockPage;$p<=$finishBlockPage;$p++)
        {
            $currOffset=($p-1)*$perpage;
            if ($currOffset == $offset)
                $page = $this->get_template_block('page_passive');
            else
                $page = $this->get_template_block('page_active');

			if (defined("USE_PRETTY_URL") && USE_PRETTY_URL && !$kernel->is_backend())
				$link = $q.(ceil($currOffset/$perpage)+1)."/";
			else
				$link = $q.$currOffset;
				
            $page = str_replace('%'.$linkLabelName.'%', $link, $page);

            $page = str_replace('%page%', $p, $page);
            $pages[] = $page;
        }
		
		
		$pblock = str_replace('%pages_block%', implode($this->get_template_block('page_delimeter'), $pages), $pblock);
		
		// Уберём &offset=0, page-1 и page-0 для первых страниц
        if (defined("USE_PRETTY_URL") && USE_PRETTY_URL && !$kernel->is_backend()) {
			$pblock = str_replace(array('page-0/', 'page-1/', '?page=0', '?page=1/'), '', $pblock);
			$pblock = str_replace('//','/',$pblock);
		} else {
			$pblock = str_replace('&offset=0','',$pblock);
            $pblock = preg_replace('~\?offset=0(["\'])~ui','$1',$pblock);
		}
		
        $pblock = str_replace('%total_pages_count%',$pages_count,$pblock);
        return $pblock;
    }
}