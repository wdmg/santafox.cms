<?php

/*
 * Some PHP variables
 *
 * Current support variables:
 *
 *  {{$date.year}} - return string like `2019`
 *  {{$date.month}} - return string like `02`
 *  {{$date.day}} - return string like `07`
 *  {{$date.datetime}} - return string like `07.02.2019 11:37:59`
 *  {{$date.date}} - return string like `07.02.2019`
 *  {{$date.time}} - return string like `11:37:59`
 *  {{$date.timestamp}} - return string like `1549532279`
 *  {{$_get}} - return string like `Array ( [sitepage] => index )`
 *  {{$_post}} - return string like `Array ( [search] => example )`
 *  {{$_request}} - return string like `Array ( [PHPSESSID] => 519...0af )`
 *
 * */

class variables extends postprocessor
{
    public function do_postprocessing($html, $label)
    {
        $dateTime = new DateTime('NOW');
        $replaces = array(
            'date.year' => $dateTime->format('Y'),
            'date.month' => $dateTime->format('m'),
            'date.day' => $dateTime->format('d'),
            'date.datetime' => $dateTime->format('d.m.Y h:i:s'),
            'date.date' => $dateTime->format('d.m.Y'),
            'date.time' => $dateTime->format('h:i:s'),
            'date.timestamp' => $dateTime->getTimestamp(),
            '_get' => print_r($_GET, true),
            '_post' => print_r($_POST, true),
            '_request' => print_r($_REQUEST, true)
        );

        $html = preg_replace_callback('#{{\$([^}]+)}}#', function($match) use ($replaces){
            if(isset($replaces[$match[1]]))
            {
                return $replaces[$match[1]];
            }
            else
            {
                $match[0] = str_replace('{{$', '', $match[0]);
                $match[0] = str_replace('}}', '', $match[0]);
                return $match[0];
            }
        }, $html);

        return $html;
    }

    public function get_name($lang)
    {
        return "PHP variables";
    }

    public function get_description($lang)
    {
        return "Вызов variables на содержимом";
    }

}