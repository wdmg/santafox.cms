<?php
class caching extends postprocessor
{
    public function do_postprocessing($s, $label)
    {
		$cache = $_SESSION['caching'][$label] = isset($_SESSION['caching'][$label]) ? $_SESSION['caching'][$label] : base64_encode($s);
		if(!empty($cache))
			return base64_decode($cache);
		else
			return $s;
    }

    public function get_name($lang)
    {
		if($lang=='EN')
			return "Cache label";
		elseif($lang=='UA')
			return "Кешувати мітку";
		else
			return "Кешировать метку";
    }

    public function get_description($lang)
    {
		if($lang=='EN')
			return "Caching content labels for the lifetime of the session";
		elseif($lang=='UA')
			return "Кешування вмісту мітки на час життя сесії";
		else
			return "Кеширование содержимого метки на время жизни сессии";
    }

}