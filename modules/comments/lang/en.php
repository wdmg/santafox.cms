<?php

$type_langauge = 'en';

$il['comments_base_name'] = 'Comments and reviews';
$il['comments_modul_base_name1'] = 'Comments and reviews';

$il['comments_property_admin_email'] = 'Email admin';
$il['comments_property_premod'] = 'Moderated?';
$il['comments_property_showcaptcha'] = 'Show captcha?';
$il['comments_property_comments_per_page_admin'] = 'Comments page for AI';
$il['comments_property_news_date_format'] = 'Date format';

$il['comments_property_allow_upload'] = 'Upload photo for unauthorized users';
$il['comments_property_use_gravatar'] = 'Use "Gravatar" to display the photo';

$il['comments_pub_show_comments'] = 'View comments';
$il['comments_pub_show_comments_template'] = 'Template';
$il['comments_pub_show_comments_httpparams'] = 'http-parameters';
$il['comments_pub_show_comments_limit'] = 'Comments per page';
$il['comments_pub_show_comments_type'] = 'Sort';
$il['comments_pub_show_comments_type_default'] = 'Default';
$il['comments_pub_show_comments_type_new_at_top'] = 'New on the top';
$il['comments_pub_show_comments_type_new_at_bottom'] = 'New at the bottom';
$il['comments_pub_show_page_reviews'] = 'Show page reviews';
$il['comments_pub_show_comments_pagereviews'] = 'Page with reviews';
$il['comments_pages_in_block']    = 'Number of pages in block (N)';

$il['comments_pub_show_sorting'] = 'Show Sort';
$il['comments_pub_show_selection'] = 'Show selection';
$il['comments_pub_show_selection_template'] = 'Template';

$il['comments_pub_show_comments_header']            = 'List header';
$il['comments_pub_show_comments_header_defaul']     = 'Comments';
$il['comments_pub_show_reviews_header']            = 'List header';
$il['comments_pub_show_reviews_header_defaul']     = 'Reviews';

$il['comments_menu_label'] = 'Comments';
$il['comments_menu_show_list'] = 'View';
$il['comments_menu_between'] = 'During the period';
$il['comments_menu_notmoderated'] = 'Not Moderated';
$il['comments_menu_add_new'] = 'Add';

$il['comments_property_photo_label'] = 'Photo of the author';
$il['comments_property_no_photo'] = 'No photos';

$il['comments_property_img_source_width'] = 'The width of the original image';
$il['comments_property_img_source_height'] = 'The height of the source image';
$il['comments_property_img_big_width'] = 'Width of the big picture, in pixels';
$il['Comments_property_img_big_height'] = 'The height of the largest image in pixels';
$il['comments_property_img_small_width'] = 'The width of the small picture, in pixels';
$il['comments_property_img_small_height'] = 'The height of the small picture, in pixels';

$il['comments_show_list_action_available_on'] = 'Mark Moderated';
$il['comments_show_list_action_available_off'] = 'Mark nemoderirovannye';
$il['comments_show_list_action_delete'] = 'Delete';
$il['comments_show_list_action_move'] = 'Move';

// The headers in show_list
$il['comments_item_id'] = 'ID';
$il['comments_item_date'] = 'Date';
$il['comments_item_pages'] = 'Page'; // added aim
$il['comments_item_txt'] = 'Text';
$il['comments_item_available'] = 'Available';
$il['comments_item_author'] = 'Author';
$il['comments_item_actions'] = 'Actions';

$il['comments_property_date_label'] = 'Date';
$il['comments_property_time_label'] = 'Time (HH:MM:SS)';
$il['comments_property_pages_label'] = 'Add page'; // added aim
$il['comments_property_edit_label'] = 'Edit Page'; // added aim
$il['comments_property_available_label'] = 'Comment enabled';
$il['comments_property_txt_label'] = 'Comment Text';
$il['comments_property_author_label'] = 'Author';

$il['comments_item_action_edit'] = 'Edit';
$il['comments_item_action_remove'] = 'Delete';
$il['comments_show_list_submit'] = 'OK';
$il['comments_actions_with_selected'] = 'Options marked with the comment:';
$il['comments_submit_label'] = 'Save';
$il['comments_actions_simple'] = 'Actions';
$il['comments_actions_advanced'] = 'Move to:';
$il['comments_item_number'] = '#';
$il['comments_menu_label1'] = 'The selection by date';
$il['comments_select_between_label'] = 'Indicate the desired date range';
$il['comments_start_date'] = 'Indicate the desired date range';
$il['comments_end_date'] = 'Indicate the desired date range';
$il['comments_button_show'] = 'Show';


$il['comments_property_pages_count'] = 'number of pages per block (N)';
$il['comments_pub_pages_type'] = 'View pagination';
$il['comments_pub_pages_get_block'] = 'Blocks for N pages';
$il['comments_pub_pages_get_float'] = 'Always in the center of the block of N pages';
$il['comments_delete_confirm'] = 'Are you sure you want to delete the comment?';
$il['comments_list_header'] = 'Comments list';
$il['comments_select_range_header'] = 'Select the period';
$il['comments_select_range_from'] = 'from';
$il['comments_select_range_to'] = 'to';
$il['comments_no_data_to_show'] = 'No data to display';
$il['comments_pub_show_reviews'] = 'Show comments';
$il['comments_reviews_menu_block'] = 'Comments';
$il['comments_reviews_list_header'] = 'List of comments';
$il['comments_property_pros_label'] = 'Benefits';
$il['comments_property_cons_label'] = 'Shortcomings';
$il['comments_property_rate_label'] = 'Evaluation';
$il['comments_error_incorrect_datetime'] = 'Incorrect date or time';
$il['comments_comment_form_header'] = 'Add / Edit Comment';
$il['comments_review_form_header'] = 'Add / Edit Review';
$il['comments_pub_show_reviews_stat'] = 'User Statistics';