<?php
/**
 * Мовний файл для модуля "Питання і Відповіді"
 * @author Олександр Ільїн [Comma]
 * @copyright ArtProm © 2007
 * @version 1.0 beta
 */

$type_langauge	= 'ua';

$il['faq_import_label1'] = 'Додати питання-відповіді в розділ';
$il['faq_import_label2'] = 'з СSV-файлу';
$il['faq_import_button_label'] = 'завантажити';

$il['faq_modul_base_name'] = 'Питання і Відповіді';
$il['faq_modul_base_name1'] = 'Питання і Відповіді';
$il['faq_modul_metod_pub_faq'] = 'Показати питання';

$il['faq_modul_errore1'] = 'Не настроєно файл шаблону в налаштуваннях модуля';


$il['faq_admin_table_label'] = 'Створення нового розділу';
$il['faq_add_partition_name'] = 'Назва розділу';
$il['faq_add_partition_description'] = 'Опис розділу';
$il['faq_add_partition_meta_description'] = 'Мета-опис розділу';
$il['faq_add_partition_meta_keywords'] = 'Мета-ключові слова';
$il['faq_button_name_add_partition'] = 'Додати';
$il['faq_admin_table_label_edit'] = 'Редагування розділу';
$il['faq_button_save'] = 'Зберегти';
$il['faq_list_edit_partition_name'] = 'Редагувати розділ';
$il['faq_partition_intems'] = 'елементів';
$il['faq_list_delete_partition'] = 'Видалити розділ';
$il['faq_list_add_element'] = 'Додати нове питання';
$il['faq_list_del_element'] = 'Видалити питання розділу';
$il['faq_list_delete_partition_alert'] = 'Розділ і його вміст буде видалено. Продовжити? ';
$il['faq_list_delete_element_alert'] = 'Питання буде видалений. Продовжити? ';
$il['faq_list_edit_element'] = 'Редагувати питання';


$il['faq_modul_param1_name'] = 'Відображати форму для введення нового питання';
$il['faq_modul_param2_name'] = 'Адреса відсилання повідомлень про нові питаннях користувачів';
$il['faq_modul_param_list_pagename'] = 'Сторінка для виведення питань-відповідей';
$il['faq_modul_param_answer_mail_subj'] = 'Тема листа для відповіді користувачам';


$il['faq_menu_users_questions_nonew'] = 'Немає питань від відвідувачів';
$il['faq_menu_users_questions'] = 'Питання відвідувачів';
$il['faq_module_label_user_question'] = 'Питання відвідувачів сайту';


$il['faq_menu_caption'] = 'Управління';
$il['faq_menu_list'] = 'Всі питання';
$il['faq_list_header'] = 'Розділи і елементи';
$il['faq_list_manage'] = 'Управління';

$il['faq_edit_categoty'] = 'Розділ';
$il['faq_edit_question'] = 'Заголовок питання';
$il['faq_edit_content'] = 'Редагування елемента';
$il['faq_edit_description'] = 'Текст питання';
$il['faq_edit_editor'] = 'Відповідь на питання';
$il['faq_edit_meta_description'] = 'Мета-опис відповіді';
$il['faq_edit_meta_keywords'] = 'Мета-ключові слова відповіді';

$il['faq_button_name'] = 'Зберегти';
$il['faq_module_label_propertes1'] = 'Файл шаблону';
$il['faq_module_method_name1'] = 'Повертає список категорій';

// new
$il['faq_modul_metod_pub_form'] = 'Висновок форми для відправки питання';
$il['faq_modul_metod_show_partitions'] = 'Показати розділи';
$il['faq_modul_param_new_question_email_subj'] = 'Тема листа для адміна про новий питанні';
$il['faq_list_datetime'] = 'Коли';
$il['faq_list_partition'] = 'Розділ';
$il['faq_list_question'] = 'Питання';
$il['faq_menu_partitions'] = 'Розділи';
$il['faq_list_show_partition_questions'] = 'Показати питання розділу';
$il['faq_modul_show_counters'] = 'Показувати лічильник питань';
$il['faq_list_has_answer'] = 'Є відповідь?';
$il['faq_date_block'] = 'Відбір за датою';
$il['faq_limit_questions'] = 'Кількість питань на сторінку';
$il['faq_pages_questions'] = 'Макс. кількість сторінок в блоці ';
$il['faq_sortorder_questions'] = 'Порядок сортування';
$il['faq_sortorder_questions_default'] = 'За id у порядку додавання';
$il['faq_sortorder_questions_desc'] = 'За id у порядку убування';
$il['faq_sortorder_questions_new_at_top'] = 'Спочатку нові';
$il['faq_sortorder_questions_new_at_bottom'] = 'Спочатку старі';
$il['pub_show_title'] = 'Показати заголовок title';
$il['pub_show_title_def'] = 'Заголовок за замовчуванням';
$il['pub_show_meta_keywords'] = 'Показати meta-keywords';
$il['pub_show_meta_keywords_def'] = 'Meta-keywords за замовчуванням';
$il['pub_show_meta_description'] = 'Показати meta-description';
$il['pub_show_meta_description_def'] = 'Meta-description за замовчуванням';


?>