<?php
/**
 * Модуль "Каталог товаров"
 *
 * @author s@nchez s@nchez.me
 * @copyright ArtProm (с) 2001-2019
 * @name catalog
 * @contributors Oslix <oslix@yandex.ru>, Alexsander Vyshnyvetskyy (alex_wdmg) <wdmg.com.ua@gmail.com>, sanchez <sanchezby@gmail.com>, Rinat <mail.rinat@yandex.ru>, rinatr <mail.rinat@yandex.ru>, sanchez <s@nchez.me>, Ivan Kudryavsky <spam@mr-god.net>, bubek <bvvtut@tut.by>, wdmg <wdmg.com.ua@gmail.com>, alex-wdmg <wdmg.com.ua@gmail.com>, devsanchez <s@nchez.me>, Alexsander Vyshnyvetskyy <wdmg.com.ua@gmail.com>, arteshuk <arteshuk@gmail.com>, Ivan Kudryavsky (wollk) <iwan.vk@gmail.com>, paranoik-via <paranoik.via@gmail.com>
 * @version 2.0
 *
 */

require_once realpath(dirname(__FILE__)."/../../")."/include/basemodule.class.php";
require 'catalog.commons.class.php';

class catalog extends BaseModule
{
    /**
     * Название параметра с id-шником категории для фронтэнда
     *
     * @var string
     */
    private $frontend_param_cat_id_name = "cid";

    /**
     * Название параметра с id-шником товара для фронтэнда
     *
     * @var string
     */
    private $frontend_param_item_id_name = "itemid";

    /**
     * Название параметра с лимитом на страницу для фронтэнда
     *
     * @var string
     */
    private $frontend_param_limit_name = "limit";

    /**
     * Название параметра со сдвигом для фронтэнда
     *
     * @var string
     */
    private $frontend_param_offset_name = "offset";

    /**
     * Название параметра кол-ва на страницу
     *
     * @var string
     */
    private $frontend_param_perpage_name = "perpage";

    /**
     * Название параметра со сдвигом для админки
     *
     * @var string
     */
    private $admin_param_offset_name = "offset";


    /**
     * На сколько увеличиваем порядок в списках
     *
     * @var integer
     */
    private $order_inc = 5;

    /**
     * Название cookie, где храним товары корзины
     *
     * @var string
     */
    private $basketid_cookie_name = "basket_";


    /**
     * Сколько дней хранить cookie
     *
     * @var integer
     */
    private $basketid_cookie_days = 7;


    /**
     * Запись текущего заказа для фронтенд
     *
     * @var array
     */
    private $current_basket_order = null;

    /**
     * Текущие товары в корзине
     *
     * @var array
     */
    private $current_basket_items = false;


    /**
     * Дорога добавлена?
     * @var boolean
     */
    private $is_way_set = false;


    /**
     * Корзина инициализирована?
     * @var boolean
     */
    private $is_basket_inited = false;

    private $structure_cookie_name;

    /** @var array айдишники текущих категорий, moduleid=>catid */
    private $current_cat_IDs = array();

    /**
     *  Конструктор класса модуля
     * @return catalog
     */
    public function __construct()
    {
        parent::__construct();
        global $kernel;
        if ($kernel->pub_httpget_get('flush'))
            $kernel->pub_session_unset();
        $this->structure_cookie_name = "tree_".$kernel->pub_module_id_get();
    }
    /**
     * Редирект страницы назад при добавлении в корзину, сравнении и т.д.
     *
     * @return string
     */
    private function redirURL()
    {
        global $kernel;

        if (isset($_REQUEST['redir2']) && strlen($_REQUEST['redir2']))
        {
            $redirURL = frontoffice_manager::sanitize_redir_url($_REQUEST['redir2']);
            $redirURL = '/'.ltrim($redirURL,'/');
        }
        else
        {
            if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                $redirURL = "/".$kernel->pub_page_current_get()."/";
            else
                $redirURL = "/".$kernel->pub_page_current_get().".html";
        }
        $kernel->pub_redirect_refresh_global($redirURL);
    }

    private function basket_init()
    {

        global $kernel;
        $ajax = false;

        if ($this->is_basket_inited)
            return;

        $moduleid = $kernel->pub_module_id_get();

        if (rand(1,1000)==1) //чистим старые корзины с вероятностью 1 из 1000
            CatalogCommons::clean_old_baskets($moduleid);

        $this->is_basket_inited = true;

        //устанавливаем уникальное имя cookie для этого экземпляра модуля
        $this->basketid_cookie_name .= $moduleid;

        if (isset($_COOKIE[$this->basketid_cookie_name])) {
            //проверка что значение у cookie валидное
            if (preg_match("/^([a-z0-9]+)$/i", $_COOKIE[$this->basketid_cookie_name])) {
                //проверка, что запись в БД существует
                $this->current_basket_order = $this->get_basket_order_by_sid($_COOKIE[$this->basketid_cookie_name]);
            }
        }

        // Добавление товара в корзину
        if (isset($_REQUEST["catalog_basket_additemid"]) && !empty($_REQUEST["catalog_basket_additemid"]))
        {
            $addok = false;
            $add_item_id = $_REQUEST["catalog_basket_additemid"];
            $ajax = $kernel->pub_is_ajax_request();

            $basket_additem_success = $kernel->pub_modul_properties_get('catalog_property_basket_additem_success', $kernel->pub_module_id_get());
            if(empty($basket_additem_success['value']))
                $additem_success = '[#catalog_property_basket_additem_success_message#]!';
            else
                $additem_success = $basket_additem_success['value'];

            $basket_additem_error = $kernel->pub_modul_properties_get('catalog_property_basket_additem_error', $kernel->pub_module_id_get());
            if(empty($basket_additem_error['value']))
                $additem_error = '[#catalog_property_basket_additem_error_message#]';
            else
                $additem_error = $basket_additem_error['value'];

            if (is_array($add_item_id)) { //для добавления сразу группы товаров через чекбоксы
                foreach ($add_item_id as $aiid => $aiq) {
                    //если не пустой выбор варианта
                    if (!empty($_REQUEST['choice']) && (!empty($_REQUEST['option_group']))) {
                        if($this->add_basket_item(intval($aiid), $aiq, $_REQUEST['option_group'], $_REQUEST['choice'])) {
                            $addok = true;
                        } else {
                            $addok = false;
                            break;
                        }
                    } else {
                        if($this->add_basket_item(intval($aiid), $aiq)) {
                            $addok = true;
                        } else {
                            $addok = false;
                            break;
                        }
                    }
                }
            } else { //один товар
                $qty = 1;
                if (isset($_REQUEST['qty'])) {
                    $qty = intval($_REQUEST['qty']);
                    if ($qty < 1)
                        $qty = 1;
                }

                //если в карточке товара была выбрана какая либо опция (цвет, размер)
                $selected = $kernel->pub_httpget_get("selected");
                if (empty($selected)) // значит проверяем POST-запрос
                    $selected = $kernel->pub_httppost_get("selected");

                if (!empty($selected) && is_array($selected))
                {
                    if(is_array($kernel->pub_session_get('selected'))) { // значит в сесии уже есть отмеченные опции свойства других товаров
                        $old_selected = $kernel->pub_session_get('selected');
                        $kernel->pub_session_set('selected', array_merge($old_selected, array(
                            "itemid-".$add_item_id => array(
                                'fields' => $selected
                            )
                        )));
                    } else {
                        $kernel->pub_session_set('selected', array(
                            "itemid-".$add_item_id => array(
                                'fields' => $selected
                            )
                        ));
                    }
                }

                //если не пустой выбор варианта
                if (!empty($_REQUEST['choice']) && (!empty($_REQUEST['option_group']))) {
                    if($this->add_basket_item(intval($add_item_id), $qty, $_REQUEST['option_group'], $_REQUEST['choice']))
                        $addok = true;
                    else
                        $addok = false;
                } else {
                    if($this->add_basket_item(intval($add_item_id), $qty))
                        $addok = true;
                    else
                        $addok = false;
                }
            }

            if($ajax) { // если нужно отдать ответ на ajax-запрос
                if($addok)
                    echo json_encode(array("moduleid" => $moduleid, "message" => $additem_success, "error" => "false"));
                else
                    echo json_encode(array("moduleid" => $moduleid, "message" => $additem_error, "error" => "true"));
                exit;
            } else {
                $this->redirURL();
            }

        }

        // Обновление кол-ва товара в корзине
        $param = $kernel->pub_httpget_get("catalog_basket_upd_qty");
        if (empty($param)) // значит проверяем POST-запрос
            $param = $kernel->pub_httppost_get("catalog_basket_upd_qty");

        if (!empty($param))
        {

            $updok = false;
            $qties = $kernel->pub_httppost_get("basket_item_qty");
            $ajax = $kernel->pub_is_ajax_request();
            $group_option = $kernel->pub_httppost_get("basket_item_group_option");
            $basket_item_choice = $kernel->pub_httppost_get("basket_item_choice");

            $basket_upditem_success = $kernel->pub_modul_properties_get('catalog_property_basket_upditem_success', $kernel->pub_module_id_get());
            if(empty($basket_upditem_success['value']))
                $upditem_success = '[#catalog_property_basket_upditem_success_message#]!';
            else
                $upditem_success = $basket_upditem_success['value'];

            $basket_upditem_error = $kernel->pub_modul_properties_get('catalog_property_basket_upditem_error', $kernel->pub_module_id_get());
            if(empty($basket_upditem_error['value']))
                $upditem_error = '[#catalog_property_basket_upditem_error_message#]';
            else
                $upditem_error = $basket_upditem_error['value'];

            foreach ($qties as $itemid => $qty)
            {
                if(empty($qty) || empty($itemid))
                    continue;

                if (is_array($qty)) {
                    foreach($qty as $option_group => $option_group_array)
                    {
                        foreach($option_group_array as  $choice => $sum ) {
                            if($this->update_basket_item_qty($itemid, $sum, $option_group, $choice)) {
                                $updok = true;
                            } else {
                                $updok = false;
                                break;
                            }
                        }
                    }
                } else {
                    if($this->update_basket_item_qty($itemid, $qty)) {

                        //если в карточке товара была выбрана какая либо опция (цвет, размер)
                        $selected = $kernel->pub_httpget_get("selected");
                        if (empty($selected)) // значит проверяем POST-запрос
                            $selected = $kernel->pub_httppost_get("selected");

                        if (!empty($selected) && is_array($selected))
                        {
                            if(is_array($kernel->pub_session_get('selected'))) { // значит в сесии уже есть отмеченные опции свойств товара
                                $old_selected = $kernel->pub_session_get('selected');
                                $kernel->pub_session_set('selected', array_merge(array(
                                    "itemid-".$itemid => array(
                                        'fields' => $selected
                                    )
                                ), $old_selected));
                            } else {
                                $kernel->pub_session_set('selected', array(
                                    "itemid-".$itemid => array(
                                        'fields' => $selected
                                    )
                                ));
                            }
                        }

                        $updok = true;
                    }
                }
            }
            if($ajax) { // если нужно отдать ответ на ajax-запрос
                if($updok)
                    echo json_encode(array("moduleid" => $moduleid, "message" => $upditem_success, "error" => "false"));
                else
                    echo json_encode(array("moduleid" => $moduleid, "message" => $upditem_error, "error" => "true"));
                exit;
            }
        }

        // Применение скидочного купона
        $param = $kernel->pub_httpget_get("catalog_basket_promocode");
        if (empty($param)) // значит проверяем POST-запрос
            $param = $kernel->pub_httppost_get("catalog_basket_promocode");

        if (!empty($param))
        {
            $codeok = false;
            $ajax = $kernel->pub_is_ajax_request();

            $basket_promocode_success = $kernel->pub_modul_properties_get('catalog_property_basket_promocode_success', $kernel->pub_module_id_get());
            if(empty($basket_promocode_success['value']))
                $promocode_success = '[#catalog_property_basket_promocode_success_message#]!';
            else
                $promocode_success = $basket_promocode_success['value'];

            $basket_promocode_error = $kernel->pub_modul_properties_get('catalog_property_basket_promocode_error', $kernel->pub_module_id_get());
            if(empty($basket_promocode_error['value']))
                $promocode_error = '[#catalog_property_basket_promocode_error_message#]';
            else
                $promocode_error = $basket_promocode_error['value'];

            $cupon = CatalogCommons::search_cupon($param);
            if(is_array($cupon) && count($cupon) > 0) {
                $codeok = true;
                if($kernel->pub_session_set('promo_code', array(
                    'basket_order'=> $this->current_basket_order,
                    'promo_code' => array(
                        'id' => $cupon['id'],
                        'textcode' => $cupon['textcode'],
                        'type' => $cupon['type'],
                        'value' => $cupon['cupon_value']
                    )
                ))) {
                    $codeok = true;
                }
            }

            if($ajax) { // если нужно отдать ответ на ajax-запрос
                if($codeok)
                    echo json_encode(array("moduleid" => $moduleid, "message" => $promocode_success, "error" => "false"));
                else
                    echo json_encode(array("moduleid" => $moduleid, "message" => $promocode_error, "error" => "true"));
                exit;
            } else {
                $this->redirURL();
            }
        }

        // Удаление промо-кода из корзины
        $param = $kernel->pub_httpget_get("catalog_basket_remove_promocode");
        if (empty($param)) // значит проверяем POST-запрос
            $param = $kernel->pub_httppost_get("catalog_basket_remove_promocode");

        if (!empty($param))
        {
            $rempromocodeok = false;
            $ajax = $kernel->pub_is_ajax_request();

            $basket_rempromocode_success = $kernel->pub_modul_properties_get('catalog_property_basket_rempromocode_success', $kernel->pub_module_id_get());
            if(empty($basket_rempromocode_success['value']))
                $rempromocode_success = '[#catalog_property_basket_rempromocode_success_message#]!';
            else
                $rempromocode_success = $basket_rempromocode_success['value'];

            $basket_rempromocode_error = $kernel->pub_modul_properties_get('catalog_property_basket_rempromocode_error', $kernel->pub_module_id_get());
            if(empty($basket_rempromocode_error['value']))
                $rempromocode_error = '[#catalog_property_basket_rempromocode_error_message#]';
            else
                $rempromocode_error = $basket_rempromocode_error['value'];

            if($kernel->pub_session_get('promo_code')) {
                $cupon = $kernel->pub_session_get('promo_code');
                if ($cupon['promo_code']['id'] == intval($param)) {
                    $kernel->pub_session_set('promo_code', false);
                    $rempromocodeok = true;
                }
            }

            if($ajax) { // если нужно отдать ответ на ajax-запрос
                if($rempromocodeok)
                    echo json_encode(array("moduleid" => $moduleid, "message" => $rempromocode_success, "error" => "false"));
                else
                    echo json_encode(array("moduleid" => $moduleid, "message" => $rempromocode_error, "error" => "true"));
                exit;
            } else {
                $this->redirURL();
            }

        }

        // Удаление товара из корзины
        $param = $kernel->pub_httpget_get("catalog_basket_removeitemid");
        if (empty($param)) // значит проверяем POST-запрос
            $param = $kernel->pub_httppost_get("catalog_basket_removeitemid");

        if (!empty($param))
        {
            $delok = false;
            $ajax = $kernel->pub_is_ajax_request();

            $basket_removeitem_success = $kernel->pub_modul_properties_get('catalog_property_basket_removeitem_success', $kernel->pub_module_id_get());
            if(empty($basket_removeitem_success['value']))
                $removeitem_success = '[#catalog_property_basket_removeitem_success_message#]!';
            else
                $removeitem_success = $basket_removeitem_success['value'];

            $basket_removeitem_error = $kernel->pub_modul_properties_get('catalog_property_basket_removeitem_error', $kernel->pub_module_id_get());
            if(empty($basket_removeitem_error['value']))
                $removeitem_error = '[#catalog_property_basket_removeitem_error_message#]';
            else
                $removeitem_error = $basket_removeitem_error['value'];

            if (!empty($_REQUEST['choice']) && (!empty($_REQUEST['option_group']))) {
                if($this->remove_item_from_basket($param, $_REQUEST['option_group'], $_REQUEST['choice']))
                    $delok = true;
                else
                    $delok = false;
            } else {
                if($this->remove_item_from_basket($param))
                    $delok = true;
                else
                    $delok = false;
            }

            //очищаем опции товара (размер, цвет и т.д.)
            $selected = $kernel->pub_session_get('selected');
            if(is_array($selected)) {
                $kernel->pub_session_set('selected', array_merge($selected, array(
                    "itemid-".$param => array(
                        'fields' => false
                    )
                )));
            }

            if($ajax) { // если нужно отдать ответ на ajax-запрос
                if($delok)
                    echo json_encode(array("moduleid" => $moduleid, "message" => $removeitem_success, "error" => "false"));
                else
                    echo json_encode(array("moduleid" => $moduleid, "message" => $removeitem_error, "error" => "true"));
                exit;
            } else {
                $this->redirURL();
            }

        }
    }

    /**
     * Возвращает текущий заказ корзины товаров
     * при необходимости создаёт запись в БД и ставит cookie с созданным ID-шником
     *
     * @return array
     */
    private function get_current_basket_order()
    {
        if (!empty($this->current_basket_order))
            return $this->current_basket_order;
        $new_basketsid = CatalogCommons::generate_random_string(32, true);
        $new_orderid = $this->add_basket_order($new_basketsid);
        $this->current_basket_order = array("id" => $new_orderid, "sessionid" => $new_basketsid);
        setcookie($this->basketid_cookie_name, $new_basketsid, time() + $this->basketid_cookie_days * 24 * 60 * 60, '/');
        return $this->current_basket_order;
    }

    /**
     * Публичный метод для отображения названия элементов
     *
     * @param string $fields_template шаблон товара
     * @param string $category_template шаблон категории
     * @return string
     */
    public function pub_catalog_show_item_name($fields_template, $fields_default=false, $category_template, $category_default=false)
    {
        global $kernel;
        $itemid = $kernel->pub_httpget_get($this->frontend_param_item_id_name);
        if (!empty($itemid))
        { //товар - используем шаблон
            $curr_item = CatalogCommons::get_item_full_data($itemid);

            if (!$curr_item) { //не нашли товар
                if($fields_default)
                    return $fields_default;
                else
                    return '';
            }

            $props = CatalogCommons::get_props($curr_item['group_id'], true);
            foreach ($props as $prop)
            {
                if(!array_key_exists($prop['name_db'],$curr_item))
                    continue;
                $val = $this->format_value($curr_item[$prop['name_db']],$prop);
                $fields_template = str_replace("%".$prop['name_db']."%", $val, $fields_template);
            }

            $fields_template = $this->process_variables_out($fields_template);
            $fields_template = $this->clear_left_labels($fields_template);
            return $fields_template;
        }
        else
        { //значит не товар, а категория
            $curr_cid = $this->get_current_catid(true);
            if ($curr_cid == 0) //не нашли, попробуем категорию по-умолчанию
            {
                $curr_cid = $this->get_default_catid();
                if ($curr_cid == 0) { //не нашли
                    if($category_default)
                        return $category_default;
                    else
                        return '';
                }
            }

            $curr_cat = CatalogCommons::get_category($curr_cid);
            $cprops = CatalogCommons::get_cats_props();

            foreach ($cprops as $cprop)
            {
                $category_template = str_replace("%".$cprop['name_db']."%", $curr_cat[$cprop['name_db']], $category_template);
            }

            $category_template = $this->process_variables_out($category_template);
            $category_template = $this->clear_left_labels($category_template);
            return $category_template;
        }
    }

    /**
     * Сохраняет порядок свойств группы
     * @return void
     */
    private function save_gprops_order()
    {
        global $kernel;
        $porders = $kernel->pub_httppost_get("porder");
        $flprop = $kernel->pub_httppost_get("flprop");
        asort($porders);
        $order = 10;
        foreach (array_keys($porders) as $pid)
        {
            if(isset($flprop[$pid])) {
                $showinfilters = 1;
            } else {
                $showinfilters = 0;
            }

            $query = "UPDATE `".$kernel->pub_prefix_get()."_catalog_item_props` SET `order`='".($order+=10)."', `showinfilters`='".$showinfilters."' WHERE `id`='".$pid."'";
            $kernel->runSQL($query);
        }
        $groupid = $kernel->pub_httppost_get("group_id");
        //удалим все видимые, потом добавим только отмеченные
        $query = 'DELETE FROM `'.$kernel->pub_prefix_get().'_catalog_visible_gprops` WHERE group_id='.$groupid.
            " AND `module_id`='".$kernel->pub_module_id_get()."'";
        $kernel->runSQL($query);
        if (is_array($kernel->pub_httppost_get("grprop")))
        {
            $gprops = array_keys($kernel->pub_httppost_get("grprop"));
            foreach ($gprops as $propdb)
            {
                $this->add_group_visible_prop($kernel->pub_module_id_get(), $groupid, $propdb);
            }
        }
    }

    /**
     * Добавляет видимое свойство в группу
     *
     * @param string $moduleid
     * @param integer $groupid
     * @param string $propdb
     * @return void
     */
    private function add_group_visible_prop($moduleid, $groupid, $propdb)
    {
        global $kernel;
        $query = 'INSERT INTO `'.$kernel->pub_prefix_get().'_catalog_visible_gprops` (`group_id`,`module_id`,`prop`) '.
            'VALUES ('.$groupid.",'".$moduleid."','".$propdb."')";
        $kernel->runSQL($query);
    }

    /**
     * Сохраняет порядок свойств для заказа (корзины)
     *
     */
    private function save_order_fields_order()
    {
        global $kernel;
        $porders = $kernel->pub_httppost_get("porder");
        foreach ($porders as $pid => $order)
        {
            if (is_numeric($order))
            {
                $query = "UPDATE `".$kernel->pub_prefix_get()."_catalog_".$kernel->pub_module_id_get()."_basket_order_fields` SET `order`=".$order." WHERE `id`=".$pid;
                $kernel->runSQL($query);
            }
        }
    }

    /**
     * Возвращает следующий порядок (order) для товаров в категории
     *
     * @param integer $cat_id id-шник категории
     * @return integer след. порядок
     */
    private function get_next_order_in_cat($cat_id)
    {
        global $kernel;
        $query = 'SELECT `order` FROM `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_item2cat` '.
            'WHERE cat_id='.$cat_id.' ORDER BY `order` DESC LIMIT 1';
        $ret = $this->order_inc;
        $result = $kernel->runSQL($query);
        if ($row = mysqli_fetch_assoc($result))
            $ret = $row['order'] + $this->order_inc;
        mysqli_free_result($result);
        return $ret;
    }

    /**
     * Добавляет связь между товарами
     *
     * @param integer $itemid1
     * @param integer $itemid2
     * @return void
     */
    private function add_items_link($itemid1, $itemid2)
    {
        global $kernel;
        $query = 'INSERT IGNORE INTO `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_items_links` (`itemid1`,`itemid2`) '.
            ' VALUES ('.$itemid1.','.$itemid2.')';
        $kernel->runSQL($query);
    }

    /**
     * Импортирует весь CSV-файл
     *
     * @param integer $group_id id-шник тов. группы
     * @param string $file имя CSV файла
     * @param string $separator разделитель полей
     * @param integer $cat_id id-шник категории
     * @param integer $cat_id4new id-шник категории для новых товаров
     * @param integer $add_in_cat_parent - добавлять товар в родительские категории
     * @param integer $create_cat - создавать не существующие категории
     * @return void
     */
    private function make_csv_import($group_id, $file, $separator, $cat_id, $cat_id4new, $break_files, $category_auto,$add_in_cat_parent=false,$create_cat=false)
    {
        global $kernel;

        $is_category = $kernel->pub_httppost_get('is_category', false);
        $icategory = (!empty($is_category) || $is_category==="0") ? intval($is_category) : -1;

        $csv_items = $this->parse_csv_file($file, $separator);

        if (count($csv_items) == 0)
            return;

        //пропускаем первую строчку если указано
        $bypass_1st_line = $kernel->pub_httppost_get('bypass_1st_line');
        if (!empty($bypass_1st_line)) {
            array_shift($csv_items);
            if (count($csv_items) == 0)
                return;
        }
        $main_prop = $this->get_common_main_prop();


        $order = 0;
        if ($cat_id > 0)
            $order = $this->get_next_order_in_cat($cat_id);

        $order4newcat = 0;
        if ($cat_id4new > 0)
            $order4newcat = $this->get_next_order_in_cat($cat_id4new);


        $count_columns = count($csv_items[0]); //кол-во столбцов
        $uniqs = $kernel->pub_httppost_get('uniq');
        if (!is_array($uniqs))
            $uniqs = array();
        $cfields = $kernel->pub_httppost_get('elem'); //поля для столбцов
        $group = CatalogCommons::get_group($group_id);
        $group_props = CatalogCommons::get_props2($group_id);
        $common_props = CatalogCommons::get_props2(0);

        //все товары из группы, в которую готовим импорт (сначала только common-поля)
        $group_items1 = $this->get_items(0, 0, $group_id, false);
        $group_items = array();
        foreach ($group_items1 as $gi) {
            //теперь занесём в этот массив полную инфу о товарах
            $commonid = $gi['id'];
            unset($gi['id']);
            $gi['common_id'] = $commonid;
            $igf = CatalogCommons::get_item_group_fields($gi['ext_id'], $group['name_db']);


            $newitem = array_merge($gi, $igf);
            $group_items[] = $newitem;
        }

        foreach ($csv_items as $csv_item) {
            if(count($csv_item)!=$count_columns) continue; //Пропускаеам подозрительные строки, например пустую в конце
            $common_fields = array(); // значения общих свойств
            $group_fields = array(); // значения свойств группы
            $uniq_fields = array(); //уникальные столбцы
            $linked_ids = array();
            $cats = null;

            for ($i = 0; $i < $count_columns; $i++) { //цикл по столбцам

                if ($icategory == $i) { $cats =  $csv_item[$i];}

                if ($cfields[$i] == '') //значит столбец игнорируем (выбрано "Игнорировать")
                    continue;

                if ($cfields[$i] == '__linked__') {
                    //поле для связей товаров
                    $lseparator = $kernel->pub_httppost_get("separator_" . $i);
                    if (!empty($lseparator)) { //обрабатываем только если разделитель не пустой
                        $lvals = explode($lseparator, $csv_item[$i]);
                        foreach ($lvals as $lval) {

                            $lval = trim($lval);
                            if (empty($lval))
                                continue;
                            $litem = CatalogCommons::get_item_by_prop($main_prop, $lval);
                            if ($litem)
                                $linked_ids[] = $litem['id'];
                        }
                    }
                    continue;
                }
                if (mb_strpos($cfields[$i], 'group0_') === false) { //свойство тов. группы
                    $fname = $cfields[$i];
                    if (array_key_exists($fname, $group_fields)) { //значит значения надо "склеить"
                        $group_fields[$fname] .= ' ' . $csv_item[$i];
                    } else
                        $group_fields[$fname] = $csv_item[$i];
                } else {

                    //значит выбрано common-свойство
                    $fname = mb_substr($cfields[$i], 7);
                    if (array_key_exists($fname, $common_fields)) { //значит значения надо "склеить"
                        $common_fields[$fname] .= ' ' . $csv_item[$i];
                    } else
                        $common_fields[$fname] = $csv_item[$i];
                }


                if (array_key_exists($i, $uniqs)) //значит это поле-уникальное
                    $uniq_fields[$fname] = $csv_item[$i];

            }


            $found_item_id = $this->get_itemid_in_group($uniq_fields, $group_items);
            $itemid = $found_item_id;


            if ($found_item_id > 0) { //update
                //сначала обновим таблицу товаров группы, если надо
                if (count($group_fields) != 0) {
                    $uitem = CatalogCommons::get_item($found_item_id);

                    $query = 'UPDATE ' . $kernel->pub_prefix_get() . '_catalog_items_' . $kernel->pub_module_id_get() . '_' . strtolower($group['name_db']) . ' SET ';
                    $grfields_keys = array_keys($group_fields);
                    for ($j = 0; $j < count($grfields_keys); $j++) {
                        $grfields_key = $grfields_keys[$j];
                        $query .= '`' . $grfields_key . '`=' . $this->prepare_property_value2($group_fields[$grfields_key], $group_props[$grfields_key]['type']);
                        if ($j != (count($grfields_keys) - 1))
                            $query .= ', ';
                    }
                    $query .= ' WHERE `id`=' . $uitem['ext_id'];
                    $kernel->runSQL($query);
                }

                // теперь общую таблицу товаров если надо
                if (count($common_fields) != 0) {
                    $query = 'UPDATE ' . $kernel->pub_prefix_get() . '_catalog_' . $kernel->pub_module_id_get() . '_items SET ';
                    $comfields_keys = array_keys($common_fields);
                    for ($j = 0; $j < count($comfields_keys); $j++) {
                        $comfields_key = $comfields_keys[$j];
                        $query .= '`' . $comfields_key . '`=' . $this->prepare_property_value2($common_fields[$comfields_key], $common_props[$comfields_key]['type']);
                        if ($j != (count($comfields_keys) - 1))
                            $query .= ', ';
                    }
                    $query .= ' WHERE `id`=' . $found_item_id;
                    $kernel->runSQL($query);
                }


                foreach ($linked_ids as $linked_id)
                    $this->add_items_link($linked_id, $found_item_id);

            } else { //insert
                //сначала добавим в таблицу товаров группы
                $query = 'INSERT INTO ' . $kernel->pub_prefix_get() . '_catalog_items_' . $kernel->pub_module_id_get() . '_' . strtolower($group['name_db']) . ' ';
                if (count($group_fields) == 0)
                    $query .= '(`id`) VALUES (NULL)';
                else {
                    $grfields_keys = array_keys($group_fields);
                    $query .= '(`' . implode('`,`', $grfields_keys) . '`, `id`) VALUES (';
                    foreach ($grfields_keys as $grfields_key) {
                        //$query .= '`'.$grfields_key.'`='.$this->prepare_property_value2($group_fields[$grfields_key],$group_props[$grfields_key]['type']).',';
                        $query .= $this->prepare_property_value2($group_fields[$grfields_key], $group_props[$grfields_key]['type']) . ',';
                    }
                    $query .= 'NULL)';
                }
                $kernel->runSQL($query);
                $ext_id = $kernel->db_insert_id();


                $is_available = 1;
                // теперь в общую таблицу товаров
                $query = 'INSERT INTO ' . $kernel->pub_prefix_get() . '_catalog_' . $kernel->pub_module_id_get() . '_items';
                if (count($common_fields) == 0)
                    $query .= '(`group_id`,`ext_id`,`available`) VALUES (' . $group_id . ',' . $ext_id . ',' . $is_available . ')';
                else {
                    $comfields_keys = array_keys($common_fields);
                    $query .= '(`' . implode('`,`', $comfields_keys) . '`, `group_id`,`ext_id`,`available`) VALUES (';
                    foreach ($comfields_keys as $comfields_key) {
                        $query .= $this->prepare_property_value2($common_fields[$comfields_key], $common_props[$comfields_key]['type']) . ',';
                    }
                    $query .= $group_id . ', ' . $ext_id . ', ' . $is_available . ')';
                }
                $kernel->runSQL($query);
                $comm_id = $kernel->db_insert_id();
                $itemid = $comm_id;

                foreach ($linked_ids as $linked_id) {
                    $this->add_items_link($linked_id, $comm_id);
                }

                if ($cat_id4new > 0 && $cat_id != $cat_id4new && $icategory == -1) { //если указана категория для новых, то добавим и в неё
                    $tmp_cat = $cat_id4new;
                    while ($tmp_cat > 0) {
                        $cat = $kernel->db_get_record_simple("_catalog_" . $kernel->pub_module_id_get() . "_cats", "id=" . $tmp_cat);
                        if ($cat) {
                            if ($tmp_cat == $cat_id4new || $add_in_cat_parent)
                                $kernel->db_add_record("_catalog_" . $kernel->pub_module_id_get() . "_item2cat",
                                    array("cat_id" => $cat["id"], "item_id" => $itemid, "order" => $this->get_next_order_in_cat($cat["id"])), 'REPLACE');
                            if ($add_in_cat_parent)
                                $tmp_cat = $cat["parent_id"];
                            else
                                $tmp_cat = 0;
                        }else break;
                    }
                }
            }

            if ($cat_id > 0 && $icategory==-1 && $itemid>0) { //добавляем в категорию
                $tmp_cat = $cat_id;
                while($tmp_cat>0) {
                    $cat = $kernel->db_get_record_simple("_catalog_" . $kernel->pub_module_id_get() . "_cats", "id=" . $tmp_cat);
                    if ($cat) {
                        if ($tmp_cat == $cat_id || $add_in_cat_parent)
                            $kernel->db_add_record("_catalog_" . $kernel->pub_module_id_get() . "_item2cat",
                                array("cat_id" => $cat["id"], "item_id" => $itemid, "order" => $this->get_next_order_in_cat($cat["id"])), 'REPLACE');
                        if ($add_in_cat_parent)
                            $tmp_cat = $cat["parent_id"];
                        else
                            $tmp_cat = 0;
                    } else break;
                }
            }

            if($icategory>-1 && $cats && $itemid>0){
                $cats = explode('||', $cats); // Товар может быть одновременно в нескольких категориях. Проходим их все.

                foreach ($cats as $cat_name) {
                    $cats_a = explode('//', $cat_name); // Двумя слешами разделяются категории по вложенности от родительской к дочерней
                    $parent_cat = 0;
                    for ($i=0,$count_cat = count($cats_a); $i<$count_cat; $i++) {
                        $cat = $cats_a[$i];
                        if (is_numeric($cat)) {
                            $catid = $cat;
                        } else {
                            $cat_tmp = $kernel->db_get_record_simple("_catalog_" . $kernel->pub_module_id_get() . "_cats", "`name`='" . $cat . "' AND parent_id=" . $parent_cat);
                            if ($cat_tmp) {
                                $catid = $cat_tmp["id"];
                            } else {
                                if($create_cat) {
                                    $new_cat = array("parent_id" => $parent_cat, "name" => $cat);
                                    $catid = $kernel->db_add_record('_catalog_' . $kernel->pub_module_id_get() . '_cats', $new_cat);
                                }else break;
                            }
                        }
                        $parent_cat = $catid;
                        if (!empty($catid) && ($add_in_cat_parent!=false || ($add_in_cat_parent==false && $i==$count_cat-1))) {
                            $rec['item_id'] = $itemid;
                            $rec['cat_id'] = $catid;
                            $rec['order'] =  $this->get_next_order_in_cat($catid);
                            $kernel->db_add_record('_catalog_' . $kernel->pub_module_id_get() . '_item2cat', $rec, 'REPLACE');
                        }
                    }
                }
            }

        }

        @unlink($file);

        //@unlink('content/files/'.$kernel->pub_module_id_get().'/'.$file);
    }

    /**
     * Проверяет по уникальным полям, присутствует ли товар в массиве товаров группы
     *
     * @param array $uniq_fields массив назв.поля=>значение, по которым проверяем наличие
     * @param array $gitems массив товаров товарной группы
     * @return integer 0, если товар не найден в массиве, иначе его common-id
     */
    private function get_itemid_in_group($uniq_fields, $gitems)
    {
        if (count($uniq_fields) == 0 || count($gitems) == 0)
            return 0;
        $ret = 0;
        $uniq_keys = array_keys($uniq_fields);
        foreach ($gitems as $gitem)
        {
            //проходим по всем уникальным ключам
            $is_fields_eq = true;
            foreach ($uniq_keys as $uniq_key)
            {
                if ($uniq_fields[$uniq_key] != $gitem[$uniq_key])
                {
                    $is_fields_eq = false;
                    break;
                }
            }
            if ($is_fields_eq)
            {
                $ret = $gitem['common_id'];
                break;
            }
        }

        return $ret;
    }

    /**
     * Показывает таблицу импорта из CSV-файла в админке
     *
     * @param integer $group_id id-щник тов. группы для импорта
     * @param file   $file tmp-имя uploaded-файла
     * @param string $separator разделитель
     * @return string
     */
    private function show_import_csv_table($group_id, $file, $separator, $break_files, $category_auto)
    {

        global $kernel;
        $this->set_templates($kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'import_csv2.html'));
        $content = $this->get_template_block("table_header");


        if ($category_auto == 1)
        {
            $content = str_replace('%thead_category%', $this->get_template_block("thead_category"), $content);
        }
        else
        {
            $content = str_replace('%thead_category%', '', $content);
        }

        $items = $this->parse_csv_file($file, $separator, 10); //показываем первые 10 товаров
        $elems = count($items[0]);
        $props = CatalogCommons::get_props($group_id, true);



        $theads = '';


        for ($i = 0; $i < $elems; $i++)
        {
            $thead = $this->get_template_block('thead');
            $thead = str_replace('%prop%', 'elem['.$i.']', $thead);
            $thead = str_replace('%cb%', 'uniq['.$i.']', $thead);
            $thead = str_replace('%id%', $i, $thead);



            $props_select = $this->get_template_block("prop_option");
            $props_select = str_replace('%prop_name%', '', $props_select);


            $props_select = str_replace('%prop_name_full%', '[#catalog_import_ignore_column_label#]', $props_select);
            $props_select = $kernel->pub_page_textlabel_replace($props_select);



            foreach ($props as $prop)
            {

                if (($prop['type'] == 'file' || $prop['type'] == 'pict') && $break_files == 1)
                    continue; //пропускаем только в том случае, если указано
                $prop_select = $this->get_template_block("prop_option");
                $opt_name = $prop['name_db'];
                if ($prop['group_id'] == 0)
                    $opt_name = 'group0_'.$opt_name;
                $prop_select = str_replace('%prop_name%', $opt_name, $prop_select);
                $prop_select = str_replace('%prop_name_full%', $prop['name_full'], $prop_select);




                if ((!empty($items[0][$i])) &&  ($prop['name_full'] == trim($items[0][$i])))
                {


                    $prop_select = str_replace('%selected%', 'selected = "selected" ', $prop_select);

                }
                elseif((!empty($items[0][$i])) &&  ($prop['name_db'] == trim($items[0][$i])))
                {
                    $prop_select = str_replace('%selected%', 'selected = selected ', $prop_select);
                }

                else
                {
                    $prop_select = str_replace('%selected%', '', $prop_select);
                }
                $props_select .= $prop_select;

            }


            $main_prop = $this->get_common_main_prop();
            if ($main_prop)
            { //связать с другими товарами можно только если есть "главное" свойство
                $prop_select = $this->get_template_block("prop_option");
                $prop_select = str_replace('%prop_name%', '__linked__', $prop_select);
                $prop_select = str_replace('%prop_name_full%', $kernel->pub_page_textlabel_replace('[#catalog_import_linked_col#]'), $prop_select);
                $props_select .= $prop_select;
            }

            $thead = str_replace('%props%', $props_select, $thead);


            if($category_auto != 0)
            {
                $is_category_tpl =   $this->get_template_block("is_category");
                $is_category_tpl = str_replace('%i%', $i, $is_category_tpl);

                $thead = str_replace('%is_category%',  $is_category_tpl, $thead);
            }
            else
            {
                $thead = str_replace('%is_category%', '', $thead);
            }
            $theads .= $thead;
        }

        $content = str_replace('%theads%', $theads, $content);

        $tlines = '';
        foreach ($items as $item_line)
        {
            $tline = $this->get_template_block('tline');
            $tline = str_replace('%cols%', implode('</td><td>', $item_line), $tline);
            $tlines .= $tline;
        }
        $content .= $tlines;
        $content .= $this->get_template_block('table_footer');
        $content = str_replace('%form_action%', $kernel->pub_redirect_for_form('import_csv3'), $content);

        $content = $kernel->priv_page_textlabels_replace($content);

        return $content;

    }

    /**
     * Парсит загруженный CSV-файл в админке в массив
     *
     * @param string $file имя файла
     * @param string $separator разделитель полей
     * @param integer $limit макс. кол-во обрабатываемых строк
     * @return array
     */
    private function parse_csv_file($file, $separator, $limit = null)
    {
        global $kernel;

        if (!file_exists($file)) return array();
        $fc = file_get_contents($file);

        //определяем кодировку
        $charset = mb_detect_encoding($fc, mb_detect_order(), true);
        if ($charset !== 'utf-8') {
            $fc = iconv($charset, 'UTF-8//IGNORE', $fc);
        }

        $lines = explode("\n", $fc);
        $max_lc = $this->get_real_elements_from_line($lines[0], $separator);
        $max_lc = count($max_lc);
        $break_line = $kernel->pub_session_get('break_line');

        $curr = 0;
        foreach ($lines as $line) { //теперь будем "запоминать" только строки с макс. кол-вом элементов,
            //остальные - игнорировать
            $lc = $this->get_real_elements_from_line($line, $separator);

            //если количество меньше или два - то это c большой вероятностью заголовок
            if ((count($lc) >= $break_line) && ($max_lc >= $break_line)) {
                $res[] = $lc;
                $curr++;
            }
            if (!is_null($limit) && $limit == $curr)
                break;
        }


        return $res;
    }

    /**
     * Возвращает непустые элементы из строки, разделённые символом $separator
     *
     * @param string $line      строка
     * @param string $separator разделитель элементов
     * @return array
     */
    private function get_real_elements_from_line($line, $separator)
    {
        $res = array();
        $res = str_getcsv($line, $separator);

        return $res;
    }

    /**
     * Показывает форму импорта из CSV-файла в админке
     *
     * @return string
     */
    private function show_import_csv_form()
    {
        global $kernel;
        $this->set_templates($kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'import_csv.html'));
        $content = $this->get_template_block('import_form');
        $content = str_replace('%form_action%', $kernel->pub_redirect_for_form('import_csv2'), $content);

        $groups = CatalogCommons::get_groups();
        $gblock = '';
        foreach ($groups as $group)
        {
            $gblock .= $this->get_template_block('group_item');
            $gblock = str_replace('%group_id%', $group['id'], $gblock);
            $gblock = str_replace('%group_name%', htmlspecialchars($group['name_full']), $gblock);
        }
        $content = str_replace('%groups%', $gblock, $content);

        $cats = $this->get_child_categories(0, 0, array());
        $options = '';
        $cat_shift = $this->get_template_block('cat_shift');
        foreach ($cats as $cat)
        {
            $option = $this->get_template_block('cat_option');
            $option = str_replace('%cat_id%', $cat['id'], $option);
            $option = str_replace('%cat_name%', str_repeat($cat_shift, $cat['depth']).$cat['name'], $option);
            $options .= $option;
        }
        $content = str_replace('%cats_options%', $options, $content);

        return $content;
    }


    /**
     * Добавляет категории в дорогу
     * @param array $cats_way_elems категории
     * @return void
     */
    private function add_categories2waysite($cats_way_elems)
    {
        global $kernel;

        if ($this->is_way_set)
            return;

        $way_cat_tpl = $kernel->pub_modul_properties_get("catalog_property_way_cay_tpl", $kernel->pub_module_id_get());

        if (empty($way_cat_tpl) || !isset($way_cat_tpl['value']) || empty($way_cat_tpl['value']))
            $way_cat_tpl = false;
        else
            $way_cat_tpl = $way_cat_tpl['value'];

        if ($way_cat_tpl)
        {
            foreach ($cats_way_elems as $cwe)
            {
                if ($cwe['id'] == 0 || $cwe['_hide_from_waysite'] == 1 || $cwe['_hide_from_site'] == 1)
                    continue;

                $cat_label = $way_cat_tpl;
                foreach ($cwe as $cpname => $cpval)
                {
                    $cat_label = str_replace("%".$cpname."%", $cpval, $cat_label);
                }
                if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                    $kernel->pub_waysite_set(array('caption' => $cat_label, 'url' => '/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($cwe['name']).'-c'.$cwe['id'].'/'));
                else
                    $kernel->pub_waysite_set(array('caption' => $cat_label, 'url' => '/'.$kernel->pub_page_current_get().'.html?'.$this->frontend_param_cat_id_name.'='.$cwe['id']));

            }
        }
        $this->is_way_set = true;
    }


    /**
     * Публичное действие, выводит форму поиска (внешний фильтр)
     *
     * @param string $header заголовок для блока фильтров
     * @param string $template шаблон построения внешнего фильтра
     * @param boolean $counters подсчитывать кол-во товаров
     * @return string
     */
    public function pub_catalog_show_outer_filter($header, $template, $counters = false)
    {
        global $kernel;

        $list = '';
        $catid = 0; // Категория по-умолчанию
        $group = false; // Заглушка для товарной группы
        $multi = false; // Флаг мультигрупности
        $filters = $kernel->pub_httppost_get();
        $moduleid = $kernel->pub_module_id_get();
        $itemid = $kernel->pub_httpget_get($this->frontend_param_item_id_name);

        // Проверим, передан ли шаблон!?
        if (empty($template) || !file_exists($template))
            return "Template not found.";
        else
            $this->set_templates($kernel->pub_template_parse($template));

        // Если передан `itemid` с фронтенда, значит ничего не выводим
        if (!empty($itemid))
            return $this->get_template_block("no_data");

        if($kernel->pub_httpget_get($this->frontend_param_cat_id_name))
            $catid = $kernel->pub_httpget_get($this->frontend_param_cat_id_name);
        elseif (!empty($_COOKIE[$moduleid.'_last_catid']))
            $catid = CatalogCommons::get_category(intval($_COOKIE[$moduleid.'_last_catid']));

        // Если категория отличается от посещённой ранее - очистим фильтры в сессии
        if(isset($_SESSION['filters'])) {
            if(intval($catid) !== intval($_SESSION['filters']['catid']))
                unset($_SESSION['filters']);
        }

        // Запишем текущую категорию в сессию
        $_SESSION['filters']['catid'] = $catid;

        $catalog_page = $kernel->pub_modul_properties_get('catalog_property_catalog_page', $moduleid);
        if(empty($catalog_page['value']))
            $catalog_page = 'catalog';
        else
            $catalog_page = $catalog_page['value'];

        if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
            $action = $catalog_page.'/';
        else
            $action = $catalog_page.'.html';

        // Делаем выборку всех товарных групп
        $groups = CatalogCommons::get_groups($moduleid, false);

        // счётчик мультигрупности
        $countof = 0;

        // Ищем только ту товарную группу, в которой категория включена для отображения фильтров
        foreach ($groups as $ingroup)
        {
            $defcatids = explode(",", $ingroup['defcatids']);
            if(isset($ingroup['filtercats'])) {
                $filtercats = explode(",", $ingroup['filtercats']);
                if(count($filtercats) > 0) {
                    foreach ($filtercats as $cat_id) {
                        if($cat_id == $catid) {
                            if($countof == 0) {
                                $group = $ingroup;
                            } else {

                                // сначала добавим преыдущую
                                if($countof == 1)
                                    $_group[] = $group;

                                // теперь текущую группу
                                $_group[] = $ingroup;

                                // и перезапишем
                                $group = $_group;

                                // проставим флаг, что у нас мультигрупность
                                $multi = true;
                            }
                            // увеличим счётчик, по которому будем определять мультигрупность
                            $countof++;
                        }
                    }
                }
            }
            else
            {
                // пока оставим, для обратной своместимости со старой версией каталога
                if(count($defcatids) > 0) {
                    foreach ($defcatids as $cat_id) {
                        if($cat_id == $catid)
                            $group = $ingroup;
                    }
                }
            }
        }

        // Получим список свойства для построения формы фильтра
        if($multi && count($group) > 0) {

            // условие выборки
            $cond = '';

            // выборка свойств из нескольких групп
            foreach($group as $ingroup) {
                if(!empty($cond))
                    $cond .= " OR ";
                else
                    $cond .= " AND (`group_id`=0 OR ";

                $cond .= "`group_id`=".$ingroup['id'];

            }
            // закрываем скобки у запроса
            $cond .= ")";

            $fields = $kernel->db_get_list_simple("_catalog_item_props", "`module_id`='".$moduleid."' AND `showinfilters`='1'".$cond." ORDER BY `order` ASC");

        } else {
            // Если такая товарная группа не найдена, значит выборка только по общим свойствам
            if(!is_array($group))
                $fields = $kernel->db_get_list_simple("_catalog_item_props", "`module_id`='".$moduleid."' AND `showinfilters`='1' AND `group_id`='0' ORDER BY `order` ASC");
            else
                $fields = $kernel->db_get_list_simple("_catalog_item_props", "`module_id`='".$moduleid."' AND `showinfilters`='1' AND (`group_id`='0' OR `group_id`='".$group['id']."') ORDER BY `order` ASC");
        }

        // Если нет опций для отображения
        if(!is_array($fields) || count($fields) == 0)
            return $this->get_template_block("no_data");

        $begin = $this->get_template_block("begin");
        $begin = str_replace("%header%", $header, $begin);
        $begin = str_replace("%action%", $action, $begin);

        $templates["options"] = $this->get_template_block("options");
        $templates["option_enum"] = $this->get_template_block("option_enum");
        $templates["option_set"] = $this->get_template_block("option_set");
        $templates["option_number"] = $this->get_template_block("option_number");
        $templates["option_counter"] = $this->get_template_block("option_counter");
        $templates["option_counter_null"] = $this->get_template_block("option_counter_null");

        $not_selected = $kernel->priv_page_textlabels_replace("[#catalog_prop_type_enum_notselect#]");

        $option_id = 1;
        $items_count = 0;
        foreach($fields as $field)
        {

            $options = $templates["options"];

            // Особый формат вывода для ENUM и SET значений
            if($field['type'] == 'enum' || $field['type'] == 'set') {

                if ((isset($field['add_param'])) && (!empty($field['add_param'])))
                    $add_param = unserialize($field['add_param']);
                else
                    $add_param = false;

                $option = '';
                if($add_param) {
                    foreach($add_param as $param) {

                        // Пропускаем дефолтное значение aka "Не выбран"
                        if ($param == $not_selected)
                            continue;

                        // Проверяем, не было ли отмечено свойство при отправке формы
                        $option_checked = '';
                        if(isset($filters[$field['name_db']])) {

                            if(array_key_exists($param, $filters[$field['name_db']]) || $param == $filters[$field['name_db']]) {
                                $option_checked = ' checked="checked"';
                                $_SESSION['filters'][$field['name_db']][$param] = true;
                                $_SESSION['filters'][$field['name_db'].'_prev'][$param] = true;
                            } else {
                                if(isset($_SESSION['filters'][$field['name_db']][$param]))
                                    unset($_SESSION['filters'][$field['name_db']][$param]);
                            }
                        }

                        if(isset($_SESSION['filters'][$field['name_db']]) && is_array($_SESSION['filters'][$field['name_db']])) {
                            if(array_key_exists($param, $_SESSION['filters'][$field['name_db']]) || $param == $_SESSION['filters'][$field['name_db']])
                                $option_checked = ' checked="checked"';
                        }

                        // Убираем, если опция была отменена
                        if(!isset($filters[$field['name_db']])) {
                            unset($_SESSION['filters'][$field['name_db']]);
                            $option_checked = '';
                        }

                        // Выбираем особый шаблон опции в зависимости от типа значения
                        $option_item = '';
                        if($field['type'] == 'enum')
                            $option_item = $templates["option_enum"];
                        elseif($field['type'] == 'set')
                            $option_item = $templates["option_set"];

                        // Производим замену стандартных переменных
                        $option_item = str_replace("%option_id%", $option_id, $option_item);
                        $option_item = str_replace("%option_name%", $field['name_db'], $option_item);
                        $option_item = str_replace("%option_value%", $param, $option_item);
                        $option_item = str_replace("%option_checked%", $option_checked, $option_item);

                        // Делаем подсчёт товаров с данной опцией
                        if($counters)
                        {
                            $prfx = $kernel->pub_prefix_get();

                            if($multi && count($group) > 0)
                            {
                                if(!$field['group_id'] == 0)
                                {
                                    $cond = '';
                                    $common_id = '';
                                    $query = "SELECT COUNT(i2c.item_id) AS count FROM `".$prfx."_catalog_".$moduleid."_items` AS items
									LEFT JOIN `".$prfx."_catalog_".$moduleid."_item2cat` AS i2c ON items.id = i2c.item_id ";

                                    foreach($group as $ingroup) {

                                        if(!($field['group_id'] == $ingroup['id']))
                                            continue;

                                        $query .= " LEFT JOIN `".$prfx."_catalog_items_".$moduleid."_".$ingroup['name_db']."` AS ".$ingroup['name_db']." ON ".$ingroup['name_db'].".id = items.ext_id ";

                                        if(empty($cond))
                                            $cond .= " items.group_id='".$ingroup['id']."' ";
                                        else
                                            $cond .= " AND items.group_id='".$ingroup['id']."' ";

                                        if($field['type'] == 'set')
                                            $cond .= " AND FIND_IN_SET('".$param."', ".$ingroup['name_db'].".".$field['name_db'].") > 0";
                                        else
                                            $cond .= " AND ".$ingroup['name_db'].".".$field['name_db']."='".$param."'";

                                        if(empty($common_id))
                                            $common_id .= $ingroup['name_db'].".id";
                                        else
                                            $common_id .= ",".$ingroup['name_db'].".id";

                                    }

                                    if(!empty($cond))
                                        $cond .= " AND ";

                                    $query .= " WHERE ".$cond." i2c.cat_id='".$catid."' AND items.available='1'";

                                }
                                else
                                {
                                    if($field['type'] == 'set')
                                        $query = "SELECT COUNT(items.id) AS count FROM `".$prfx."_catalog_".$moduleid."_item2cat` AS i2c
										LEFT JOIN `".$prfx."_catalog_".$moduleid."_items` AS items ON items.id = i2c.item_id
										WHERE i2c.cat_id = ".$catid."
										AND items.available = '1'
										AND FIND_IN_SET('".$param."', items.".$field['name_db'].") > 0";
                                    else
                                        $query = "SELECT COUNT(items.id) AS count FROM `".$prfx."_catalog_".$moduleid."_item2cat` AS i2c
										LEFT JOIN `".$prfx."_catalog_".$moduleid."_items` AS items ON items.id = i2c.item_id
										WHERE i2c.cat_id = ".$catid."
										AND items.available = '1'
										AND items.".$field['name_db']." = '".$param."'";
                                }

                            } else {
                                if(!$field['group_id'] == 0 && isset($group['name_db']))
                                {
                                    if($field['type'] == 'set')
                                        $query = "SELECT COUNT(common.id) AS count FROM `".$prfx."_catalog_".$moduleid."_items` AS items
										LEFT JOIN `".$prfx."_catalog_".$moduleid."_item2cat` AS i2c ON items.id = i2c.item_id
										LEFT JOIN `".$prfx."_catalog_items_".$moduleid."_".$group['name_db']."` AS common ON common.id = items.ext_id
										WHERE items.group_id='".$group['id']."'
										AND i2c.cat_id = ".$catid."
										AND items.available = '1'
										AND FIND_IN_SET('".$param."', common.`".$field['name_db']."`) > 0";
                                    else
                                        $query = "SELECT COUNT(common.id) AS count FROM `".$prfx."_catalog_".$moduleid."_items` AS items
										LEFT JOIN `".$prfx."_catalog_".$moduleid."_item2cat` AS i2c ON items.id = i2c.item_id
										LEFT JOIN `".$prfx."_catalog_items_".$moduleid."_".$group['name_db']."` AS common ON common.id = items.ext_id
										WHERE items.group_id='".$group['id']."'
										AND i2c.cat_id = ".$catid."
										AND items.available = '1'
										AND common.`".$field['name_db']."`='".$param."'";
                                }
                                else
                                {
                                    if($field['type'] == 'set')
                                        $query = "SELECT COUNT(items.id) AS count FROM `".$prfx."_catalog_".$moduleid."_item2cat` AS i2c
										LEFT JOIN `".$prfx."_catalog_".$moduleid."_items` AS items ON items.id = i2c.item_id
										WHERE i2c.cat_id = ".$catid."
										AND items.available = '1'
										AND FIND_IN_SET('".$param."', items.".$field['name_db'].") > 0";
                                    else
                                        $query = "SELECT COUNT(items.id) AS count FROM `".$prfx."_catalog_".$moduleid."_item2cat` AS i2c
										LEFT JOIN `".$prfx."_catalog_".$moduleid."_items` AS items ON items.id = i2c.item_id
										WHERE i2c.cat_id = ".$catid."
										AND items.available = '1'
										AND items.".$field['name_db']." = '".$param."'";
                                }
                            }

                            $query = trim(preg_replace('/\s\s+/', ' ', $query));
                            $count = $kernel->runSQL($query);
                            $count = mysqli_fetch_assoc($count);

                            if(intval($count['count'])>=1)
                                $counter = $templates["option_counter"];
                            else
                                $counter = $templates["option_counter_null"];

                            $counter = str_replace("%count%", intval($count['count']), $counter);
                            $option_item = str_replace("%option_counter%", $counter, $option_item);
                            $items_count = $items_count + intval($count['count']);
                        }
                        else
                        {
                            $option_item = str_replace("%option_counter%", '', $option_item);
                        }

                        $option .= $option_item;
                        $option_id++;
                    }
                }
            } else { // Формат для вывода обычного числового инпута

                if(isset($filters[$field['name_db']]))
                    $_SESSION['filters'][$field['name_db']] = intval($filters[$field['name_db']]);

                $option = $templates["option_number"];

                // Запишем min-значение для числового инпута
                if(isset($filters[$field['name_db'].'_min'])) {
                    $option = str_replace("%option_min_value%", intval($filters[$field['name_db'].'_min']), $option);
                    $_SESSION['filters'][$field['name_db'].'_min'] = $filters[$field['name_db'].'_min'];
                } else {
                    if(isset($_SESSION['filters'][$field['name_db'].'_min'])) {
                        $option = str_replace("%option_min_value%", intval($_SESSION['filters'][$field['name_db'].'_min']), $option);
                    } else {
                        $option = str_replace("%option_min_value%", '0', $option);
                    }
                }

                // Запишем max-значение для числового инпута
                if(isset($filters[$field['name_db'].'_max'])) {
                    $option = str_replace("%option_max_value%", intval($filters[$field['name_db'].'_max']), $option);
                    $_SESSION['filters'][$field['name_db'].'_max'] = $filters[$field['name_db'].'_max'];
                } else {
                    if(isset($_SESSION['filters'][$field['name_db'].'_max'])) {
                        $option = str_replace("%option_max_value%", intval($_SESSION['filters'][$field['name_db'].'_max']), $option);
                    } else {
                        $option = str_replace("%option_max_value%", '0', $option);
                    }
                }

            }

            $option = str_replace("%option_id%", $option_id, $option);
            $option = str_replace("%option_name%", $field['name_db'], $option);

            // Запишем текущее значение для числового инпута
            if(isset($filters[$field['name_db']])) {
                $option = str_replace("%option_value%", intval($filters[$field['name_db']]), $option);
                $_SESSION['filters'][$field['name_db']] = $filters[$field['name_db']];
            } else {
                if(isset($_SESSION['filters'][$field['name_db']])) {
                    $option = str_replace("%option_value%", intval($_SESSION['filters'][$field['name_db']]), $option);
                } else {
                    $option = str_replace("%option_value%", '0', $option);
                }
            }


            // Собираем блоки опции
            $options = str_replace("%option_title%", $field['name_full'], $options);
            $options = str_replace("%option%", $option, $options);
            $list .= $options;
            $option_id++;
        }

        // Теперь собираем все блоки вместе
        $begin = str_replace("%options%", $list, $begin);
        $end = $this->get_template_block("end");
        $content = $begin.$end;

        // Подсчитываем общее количество товаров, если необходимо
        if($counters) {

            if(intval($items_count)>=1)
                $counter = $this->get_template_block("all_counter");
            else
                $counter = $this->get_template_block("all_counter_null");

            $counter = str_replace("%count%", intval($items_count), $counter);
            $content = str_replace("%all_counter%", $counter, $content);
        } else {
            $content = str_replace("%all_counter%", '', $content);
        }

        // Замена переменных каталога
        $content = $this->process_variables_out($content);
        return $content;
    }


    /**
     * Публичное действие, отображает полную информацию о товаре
     *
     * @param integer $itemid id-шник товара
     * @return string
     */
    public function pub_catalog_show_item_details($itemid)
    {

        global $kernel;
        //Прежде всего инфу по товару
        $itemid = intval($itemid);
        $idata = CatalogCommons::get_item_full_data($itemid);

        if (!$idata)
            frontoffice_manager::throw_404_error();

        //Узнаем его группу, что бы взять шаблон
        $group = CatalogCommons::get_group($idata['group_id']);

        $moduleid = $kernel->pub_module_id_get();
        if (!empty($_COOKIE[$moduleid.'_last_catid']))
            $lastcat = CatalogCommons::get_category(intval($_COOKIE[$moduleid.'_last_catid']));

        //приоритетнее категория
        if (!empty($lastcat['tpl_card']))
        {
            $tpl = CatalogCommons::get_templates_user_prefix().$lastcat['tpl_card'];
        }
        else
        {
            if (empty($group['template_items_one']))
            {
                return "У товарной группы не определён шаблон вывода карточки товаров";
            }
            else
            {
                $tpl = CatalogCommons::get_templates_user_prefix().$group['template_items_one'];
            }
        }

        Log::debug("Каталог. Шаблон карточки: ".$tpl,"catalog_card_tpl");
        $this->set_templates($kernel->pub_template_parse($tpl));

        //Шаблон карточки
        $block = $this->get_template_block('item');
        $props = CatalogCommons::get_props($idata['group_id'], true);

        //группируем свойства по ключу
        foreach($props as $item)
        {
            $new_props[$item['name_db']] = $item;
        }
        $template =  $this->templates;

        //смотрим группы вариаций товара
        $option_groups = $kernel->db_get_list_simple("_catalog_".$kernel->pub_module_id_get()."_item_props_groups", 'group_id='.$idata['group_id']);

        if (!empty($option_groups))
        {

            //забираем опции
            $options = $kernel->db_get_list_simple("_catalog_".$kernel->pub_module_id_get()."_item_props_options", 'item_id='.$itemid);

            //группируем
            foreach($options as $item)
            {
                //групируем по имени
                $options_names[$item['group_option_id']][$item['prop_name']] = $item['prop_name'];
                //групируем по товару
                $options_values[$item['group_option_id']][$item['option_choice']][$item['prop_name']] = $item['option_value'];

            }



            foreach($option_groups as $gg)
            {
                $groups_titles[$gg['group_id']] = $gg['title'];
            }




            if (!empty($options_values))
            {

                //смотрим сначала группы
                foreach($options_values as $group_option_id => $group_option)
                {

                    $source		= array();
                    $replace	= array();


                    if (!empty($template['item_prop_group['.$group_option_id.']']))
                    {
                        $group_block = $template['item_prop_group['.$group_option_id.']'];
                    }
                    elseif(!empty($template['item_prop_group']))
                    {
                        $group_block = $template['item_prop_group'];
                    }
                    else
                    {
                        $group_block = 'извините, шаблон для группы опций не найден!';
                    }



                    if (!empty($template['item_prop_option_item['.$group_option_id.']']))
                    {
                        $item_prop_option_block = $template['item_prop_option_item['.$group_option_id.']'];
                    }
                    elseif(!empty($template['item_prop_option_item']))
                    {
                        $item_prop_option_block = $template['item_prop_option_item'];
                    }
                    else
                    {
                        $item_prop_option_block = 'извините, шаблон для группы опций не найден!';
                    }


                    // далее данные у нас структурированы по вариациям
                    $items_content = '';

                    foreach($group_option as $choice => $variant)
                    {
                        $source_in	= NULL;
                        $replace_in = NULL;


                        $prop_data 			= array();

                        foreach($variant as $name_db => $item)
                        {
                            $prop_data[$name_db] = $item;
                        }

                        //также сделаем наследование от основного товара
                        $new_data = array_merge($idata, $prop_data);

                        $source_in[]	= '%option_group%';
                        $replace_in[]	= $group_option_id;

                        $source_in[]	= '%choice%';
                        $replace_in[]	= $choice;


                        $content  = str_replace($source_in,  $replace_in, $item_prop_option_block);

                        $items_content .= $this->process_item_props_out($new_data, $props, $content, $group_option);


                    }


                    $source 	= array();
                    $replace	= array();

                    //заголовок группы - например выберите цвет
                    $source[]	= '%group_title%';
                    $replace[]	= (!empty($groups_titles[$group_option_id])) ? $groups_titles[$group_option_id] : '';



                    $source[] 	= '%option_items%';
                    $replace[]	= $items_content;
                    $source[]	= '%option_group%';
                    $replace[]	= $group_option_id;


                    foreach($new_props as $item)
                    {
                        //делаем переменными также названия свойств
                        $source[]	= '%'.$item['name_db'].'_name%';
                        $replace[]	= $item['name_full'];
                    }

                    $group_block = str_replace($source, $replace, $group_block);

                    //Теперь ищем переменные, свойств и заменяем их
                    $block = $this->process_item_props_out($idata, $props, $block, $group_option);

                    $block = str_replace('%item_group%', $group_block, $block);
                }

            }
            else
            {

                $block = str_replace('%item_group%', '', $block);
            }
        }

        //Теперь ищем переменные, свойств и заменяем их
        $block = $this->process_item_props_out($idata, $props, $block);
        $catid = $this->get_current_catid(true);
        if ($catid == 0)
        { // если не знаем точно текущую категорию,
            //находим кратчайшую дорогу в категориях к этому товару
            $max_way = $this->get_max_catway2item($itemid);
            if (count($max_way) > 0)
            {
                $catid = $max_way[count($max_way) - 1]['id'];
                $this->current_cat_IDs[$moduleid] = $catid;
            }
        }
        else
        {
            $max_way = $this->get_way2cat($catid, true);
            $this->current_cat_IDs[$moduleid] = $catid;
        }

        $catalog_page = $kernel->pub_modul_properties_get('catalog_property_catalog_page', $moduleid);
        if(empty($catalog_page['value']))
            $catalog_page = 'catalog';
        else
            $catalog_page = $catalog_page['value'];

        $cway = array();
        $this->add_categories2waysite($max_way);
        foreach ($max_way as $cat)
        {
            if ($cat['id'] == 0)
                continue;
            if ($cat['id'] == $catid)
                $cwblock = $this->get_template_block('cat_way_active');
            else
                $cwblock = $this->get_template_block('cat_way_passive');
            $cwblock = str_replace('%cat_name%', $cat['name'], $cwblock);

            if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                $cwblock = str_replace('%cat_link%', '/'.$catalog_page.'/'.$kernel->pub_pretty_url($cat['name']).'-c'.$cat['id'].'/', $cwblock);
            else
                $cwblock = str_replace('%cat_link%', $catalog_page.'.html?'.$this->frontend_param_cat_id_name.'='.$cat['id'], $cwblock);

            $cway[] = $cwblock;
        }

        //+сам товар в дорогу, если у нас есть шаблон
        $way_item_tpl = $kernel->pub_modul_properties_get("catalog_property_way_item_tpl", $moduleid);
        if (empty($way_item_tpl) || !isset($way_item_tpl['value']) || empty($way_item_tpl['value']))
            $way_item_tpl = false;
        else
            $way_item_tpl = $way_item_tpl['value'];

        $cat = CatalogCommons::get_category($catid);
        if ($way_item_tpl && $cat)
        {
            $item_label = $way_item_tpl;
            foreach ($props as $prop)
            {
                if(!array_key_exists($prop['name_db'],$idata))
                    continue;
                $val = $this->format_value($idata[$prop['name_db']],$prop);
                $item_label = str_replace("%".$prop['name_db']."%", $val, $item_label);

            }

            if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                $kernel->pub_waysite_set(array('caption' => $item_label, 'url' => '/'.$catalog_page.'/'.$kernel->pub_pretty_url($cat['name']).'-c'.$cat['id'].'/'.$kernel->pub_pretty_url($item_label).'-i'.$itemid.'.html'));
            else
                $kernel->pub_waysite_set(array('caption' => $item_label, 'url' => '/'.$catalog_page.'.html?'.$this->frontend_param_item_id_name.'='.$itemid));

        }

        $cway_block = $this->get_template_block('cat_way_block');
        $cway_block = str_replace('%cat_way%', implode($this->get_template_block('cat_way_separator'), $cway), $cway_block);

        $block = str_replace('%cat_way_block%', $cway_block, $block);

        $last_cat_block = "";
        $category = false;
        if($catid) {
            $category = CatalogCommons::get_category($catid);
        } elseif (isset($_COOKIE[$moduleid.'_last_catid'])) {
            $catid = intval($_COOKIE[$moduleid.'_last_catid']);
            $category = CatalogCommons::get_category($catid);
            if ($category) {
                $last_cat_block = $this->get_template_block('last_cat_block');
                foreach ($category as $lk => $lv)
                {
                    $last_cat_block = str_replace("%".$lk."%", $lv, $last_cat_block);
                }
            }
        }

        if (!$category || $category['_hide_from_site'] == 1)
            frontoffice_manager::throw_404_error();

        // Ссылка на текущий товар
        $targetPage = $kernel->pub_page_current_get();
        if (defined("USE_PRETTY_URL") && USE_PRETTY_URL) {
            if(isset($category['name']) && isset($category['id']))
                $block = str_replace("%link%", '/'.$targetPage.'/'.$kernel->pub_pretty_url($category['name']).'-c'.$category['id'].'/'.$kernel->pub_pretty_url($idata['name']).'-i'.$idata['id'].'.html', $block);
            else
                $block = str_replace("%link%", '/'.$targetPage.'/'.$kernel->pub_pretty_url($idata['name']).'-i'.$idata['id'].'.html', $block);
        } else {
            $block = str_replace("%link%", $targetPage.'.html?'.$this->frontend_param_item_id_name.'='.$itemid, $block);
        }

        // Ссылка для добавления товара в корзину
        $basket_page = $kernel->pub_modul_properties_get('catalog_property_basket_page', $kernel->pub_module_id_get());
        if(empty($basket_page['value']))
            $basket_page = 'basket';
        else
            $basket_page = $basket_page['value'];

        if (defined("USE_PRETTY_URL") && USE_PRETTY_URL) {
            if(isset($category['name']) && isset($category['id']))
                $block = str_replace("%basket_link%", '/'.$basket_page.'/?catalog_basket_additemid='.$itemid.'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($category['name']).'-c'.$category['id'].'/'.$kernel->pub_pretty_url($item_label).'-i'.$itemid.'.html', $block);
            else
                $block = str_replace("%basket_link%", '/'.$basket_page.'/?catalog_basket_additemid='.$itemid.'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($item_label).'-i'.$itemid.'.html', $block);
        } else {
            $block = str_replace("%basket_link%", $basket_page.'.html?catalog_basket_additemid='.$itemid.'&redir2=/'.$kernel->pub_page_current_get().'.html?'.$this->frontend_param_item_id_name.'='.$itemid, $block);
        }

        // Ссылка для добавления в список сравнения товаров
        $compare_page = $kernel->pub_modul_properties_get('catalog_property_compare_page', $kernel->pub_module_id_get());
        if(empty($compare_page['value']))
            $compare_page = 'compare';
        else
            $compare_page = $compare_page['value'];

        if (defined("USE_PRETTY_URL") && USE_PRETTY_URL) {
            if(isset($category['name']) && isset($category['id']))
                $block = str_replace("%compare_link%", '/'.$compare_page.'/?add2compare='.$itemid.'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($category['name']).'-c'.$category['id'].'/'.$kernel->pub_pretty_url($item_label).'-i'.$itemid.'.html', $block);
            else
                $block = str_replace("%compare_link%", '/'.$compare_page.'/?add2compare='.$itemid.'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($item_label).'-i'.$itemid.'.html', $block);
        } else {
            $block = str_replace("%compare_link%", $compare_page.'.html?add2compare='.$itemid.'&redir2=/'.$kernel->pub_page_current_get().'.html?'.$this->frontend_param_item_id_name.'='.$itemid, $block);
        }

        // Ссылка для добавления в список желаний
        $wishlist_page = $kernel->pub_modul_properties_get('catalog_property_wishlist_page', $kernel->pub_module_id_get());
        if(empty($wishlist_page['value']))
            $wishlist_page = 'wishlist';
        else
            $wishlist_page = $wishlist_page['value'];

        $inwishlist = false;
        $userid = intval($kernel->pub_user_is_registred());
        $wishlist = $this->get_wishlist_items_by_userid($userid);
        if (is_array($wishlist)) {
            foreach ($wishlist as $list) {
                if($list['itemid'] == intval($itemid))
                    $inwishlist = true;
            }
        }

        if($userid > 0) { // если пользователь зарегистрирован и авторизован
            if($inwishlist) { // значит есть в списке желаний
                $wishlist_link = $this->get_template_block('wishlist_link_active');
                $wishlist_param = 'remove4wishlist';
            } else {
                $wishlist_link = $this->get_template_block('wishlist_link');
                $wishlist_param = 'add2wishlist';
            }
        } else {
            $wishlist_link = $this->get_template_block('wishlist_link_disabled');
            $wishlist_param = '';
        }

        // запишем в сессию просмотренный товар
        $session_name = 'viewed_items';
        $session_items = $kernel->pub_session_get($session_name, $kernel->pub_module_id_get());
        if (!$session_items)
            $session_items = [];

        $kernel->pub_session_set($session_name, array_merge(array_unique($session_items), ["0".$itemid => time()]));

        if($wishlist_link) { // значит етсь особый блок для вывода ссылки добавления в список желаний
            if($userid > 0) {
                if (defined("USE_PRETTY_URL") && USE_PRETTY_URL) {
                    if(isset($category['name']) && isset($category['id']))
                        $wishlist_link = str_replace("%link%", '/'.$wishlist_page.'/?'.$wishlist_param.'='.$itemid.'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($category['name']).'-c'.$category['id'].'/'.$kernel->pub_pretty_url($item_label).'-i'.$itemid.'.html', $wishlist_link);
                    else
                        $wishlist_link = str_replace("%link%", '/'.$wishlist_page.'/?'.$wishlist_param.'='.$itemid.'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($item_label).'-i'.$itemid.'.html', $wishlist_link);
                } else {
                    $wishlist_link = str_replace("%link%", $wishlist_page.'.html?'.$wishlist_param.'='.$itemid.'&redir2=/'.$kernel->pub_page_current_get().'.html?'.$this->frontend_param_item_id_name.'='.$itemid, $wishlist_link);
                }
            } else {
                $wishlist_link = str_replace("%link%", '', $wishlist_link);
            }

            $block = str_replace("%wishlist_link%", $wishlist_link, $block);
        }
        else
        { // оставим старый формат для обратной совместимости
            if($userid > 0) {
                if (defined("USE_PRETTY_URL") && USE_PRETTY_URL) {
                    if(isset($category['name']) && isset($category['id']))
                        $block = str_replace("%wishlist_link%", '/'.$wishlist_page.'/?'.$wishlist_param.'='.$itemid.'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($category['name']).'-c'.$category['id'].'/'.$kernel->pub_pretty_url($item_label).'-i'.$itemid.'.html', $block);
                    else
                        $block = str_replace("%wishlist_link%", '/'.$wishlist_page.'/?'.$wishlist_param.'='.$itemid.'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($item_label).'-i'.$itemid.'.html', $block);
                } else {
                    $block = str_replace("%wishlist_link%", $wishlist_page.'.html?'.$wishlist_param.'='.$itemid.'&redir2=/'.$kernel->pub_page_current_get().'.html?'.$this->frontend_param_item_id_name.'='.$itemid, $block);
                }
            } else {
                $block = str_replace("%wishlist_link%", '', $block);
            }
        }

        $block = str_replace('%last_cat_block%', $last_cat_block, $block);

        // заменим свойства категории в карточке товара
        if ($catid && is_numeric($catid))
            $block = $this->cats_props_out($catid, $block);

        $block = $this->process_filters_in_template($block);
        $block = $this->process_variables_out($block);
        $block = $this->replace_current_page_url($block);
        //очистим оставшиеся метки
        $block = $this->clear_left_labels($block);
        return $block;
    }

    /**
     * Обрабатывает вызовы внутренних фильтров в шаблоне
     *
     * @param string $content
     * @param string $ignored stringID фильтра, который игнорируем (базовая защита от рекурсии)
     * @return string
     */
    private function process_filters_in_template($content, $ignored = null)
    {
        //обработаем фильтры, если они есть в шаблоне
        if (preg_match_all("/%show_selection_([a-z0-9_-]+)%/iU", $content, $matches))
        { //тип 1: %show_selection_NAME%
            foreach ($matches[1] as $filterStringID)
            {
                if ($filterStringID == $ignored)
                    $replacement = '';
                else
                    $replacement = $this->pub_catalog_show_inner_selection_results($filterStringID, false, array(), 0, true, false);
                $content = str_ireplace("%show_selection_".$filterStringID."%", $replacement, $content);
            }
        }

        if (preg_match_all("/%show_selection_([a-z0-9_-]+)\\((.+)\\)%/iU", $content, $matches, PREG_SET_ORDER))
        { //тип 2 (с параметрами) : %show_selection_NAME(param1=value1;param2=value2)%
            foreach ($matches as $match)
            {
                $filterStringID = $match[1];
                if ($filterStringID == $ignored)
                    $replacement = '';
                else
                {
                    $paramsStr = trim($match[2]);
                    //remove any %NNNN_value%
                    $paramsStr = preg_replace("/%([a-z0-9_-]+)_value%/i", "", $paramsStr);
                    $params = explode(";", $paramsStr);
                    $paramsKV = array();

                    foreach ($params as $pstr)
                    {
                        list($pname, $pvalue) = explode("=", $pstr, 2);
                        $paramsKV[trim($pname)] = trim($pvalue);
                    }
                    $replacement = $this->pub_catalog_show_inner_selection_results($filterStringID, false, $paramsKV, 0, true, false);
                }
                $content = str_ireplace("%show_selection_".$filterStringID."(".$match[2].")%", $replacement, $content);
            }
        }
        return $content;
    }

    /**
     * Возвращает товары из группы по айдишникам
     *
     * @param string $group_name имя товарной группы
     * @param array  $itemids    id-шники товаров, которые нас интересуют
     * @return array
     */
    private function get_group_items($group_name, $itemids)
    {
        global $kernel;
        $query = "SELECT * FROM `".$kernel->pub_prefix_get()."_catalog_items_".$kernel->pub_module_id_get()."_".strtolower($group_name)."` ".
            "WHERE `id` IN (".implode(',', $itemids).")";
        $result = $kernel->runSQL($query);
        $items = array();
        while ($row = mysqli_fetch_assoc($result))
            $items[$row['id']] = $row;
        mysqli_free_result($result);
        return $items;
    }

    /**
     * Публичный метод для отображения и обработки формы оформления заказа
     *
     * @param $template string HTML-шаблон формы
     * @param $manager_mail_tpl string шаблон письма менеджеру
     * @param $manager_mail_subj string тема письма менеджеру
     * @param $manager_email string Email менеджера
     * @param $user_mail_tpl string шаблон письма пользователю
     * @param $user_mail_subj string тема письма пользователю
     *
     * @return string
     */
    public function pub_catalog_show_basket_order_form($template, $manager_mail_tpl, $manager_mail_subj, $manager_email, $user_mail_tpl, $user_mail_subj)
    {
        global $kernel;

        if (empty($template) || !file_exists($template))
            return "template not found.";

        $this->basket_init();
        $moduleid = $kernel->pub_module_id_get();
        $parsed_tpl = $kernel->pub_template_parse($template);
        $this->set_templates($parsed_tpl);

        $basket_page = $kernel->pub_modul_properties_get('catalog_property_basket_page', $moduleid);
        if(empty($basket_page['value']))
            $basket_page = 'catalog';
        else
            $basket_page = $basket_page['value'];

        $catalog_page = $kernel->pub_modul_properties_get('catalog_property_catalog_page', $moduleid);
        if(empty($catalog_page['value']))
            $catalog_page = 'catalog';
        else
            $catalog_page = $catalog_page['value'];

        $order_page = $kernel->pub_modul_properties_get('catalog_property_order_page', $moduleid);
        if(empty($order_page['value']))
            $order_page = 'order';
        else
            $order_page = $order_page['value'];

        $order_received = $kernel->pub_httpget_get("order_received");
        if ($order_received == "true")
        {
            $block = $this->get_template_block("order_received");
            if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                $block = str_replace("%catalog_link%", '/'.$catalog_page.'/', $block);
            else
                $block = str_replace("%catalog_link%", $catalog_page.'.html', $block);

            return $block;
        }

        $bitems = $this->get_basket_items();
        if (count($bitems) == 0) {
            $block = $this->get_template_block("no_basket_items");
            if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                $block = str_replace("%catalog_link%", '/'.$catalog_page.'/', $block);
            else
                $block = str_replace("%catalog_link%", $catalog_page.'.html', $block);

            return $block;
        }

        $ofields = CatalogCommons::get_order_fields2();
        $content = "";
        $userid = intval($kernel->pub_user_is_registred());
        $tinfo = $kernel->db_get_table_info('_catalog_'.$moduleid.'_basket_orders');

        // обрабатываем форму
        if ($kernel->pub_httppost_get("process_order"))
        {
            $form_ok = true;
            $user_email = false;
            $ajax = $kernel->pub_is_ajax_request();

            $basket_orderreceived_success = $kernel->pub_modul_properties_get('catalog_property_basket_orderreceived_success', $moduleid);
            if(empty($basket_orderreceived_success['value']))
                $order_received_success = '[#catalog_property_basket_orderreceived_success_message#]!';
            else
                $order_received_success = $basket_orderreceived_success['value'];

            $basket_orderreceived_error = $kernel->pub_modul_properties_get('catalog_property_basket_orderreceived_error', $moduleid);
            if(empty($basket_orderreceived_error['value']))
                $order_received_error = '[#catalog_property_basket_orderreceived_error_message#]';
            else
                $order_received_error = $basket_orderreceived_error['value'];

            $fvalues = array();
            $fvalues_orig = array();
            foreach ($ofields as $db_field => $ofield)
            {
                $postvar = nl2br(htmlspecialchars($kernel->pub_httppost_get($db_field, false)));
                $fvalues_orig[$db_field] = $postvar;
                $fvalues[$db_field] = nl2br(htmlspecialchars($kernel->pub_httppost_get($db_field))); //сохраним сразу и заэскейпленное значение

                if ($kernel->pub_is_valid_email($postvar))
                    $user_email = $postvar;

                if (mb_strlen($postvar)==0)
                    $field_empty=true;
                else
                    $field_empty=false;

                if ($field_empty && $ofield['isrequired'] == 1)
                { // если поле не заполнено, но помечено как обязательное
                    $msg = $this->get_template_block("required_field_not_filled");
                    $msg = str_replace("%field_name%", $ofield['name_full'], $msg);
                    $content .= $msg;
                    $form_ok = false;
                    break;
                }
                if (!$field_empty)
                {
                    if($ofield['type']=='enum')
                    {
                        $evalues = $this->get_enum_set_prop_values($tinfo[$db_field]['Type'],false);
                        if (!in_array($postvar,$evalues))
                        {
                            $msg = $this->get_template_block("incorrect_field_value");
                            $msg = str_replace("%field_name%", $ofield['name_full'], $msg);
                            $content .= $msg;
                            $form_ok = false;
                            break;
                        }
                    }
                    if ($ofield['regexp'] && !preg_match($ofield['regexp'], $postvar))
                    {
                        $msg = $this->get_template_block("incorrect_field_value");
                        $msg = str_replace("%field_name%", $ofield['name_full'], $msg);
                        $content .= $msg;
                        $form_ok = false;
                        break;
                    }
                }
            }

            // ошибок нет, можно отправить сообщения пользователям
            if ($form_ok) {

                //получаем инфо о заказе
                $currOrder = $this->get_current_basket_order();

                if($kernel->pub_session_get('promo_code')) {
                    $cupon = $kernel->pub_session_get('promo_code');
                    if (intval($cupon['promo_code']['id']) > 0) {
                        $query = 'UPDATE `'.$kernel->pub_prefix_get().'_catalog_'.$moduleid.'_cupons` AS cupons SET cupons.use_count = cupons.use_count - 1 WHERE `id` = '.(int) $cupon['promo_code']['id'].' AND `use_count` >= \'1\' AND `textcode` = \''.$cupon['promo_code']['textcode'].'\'';
                        $kernel->runSQL($query);
                    }
                }

                //обновляем запись в БД
                $updateSQL = "UPDATE `".$kernel->pub_prefix_get()."_catalog_".$moduleid."_basket_orders` SET ";
                foreach ($fvalues as $fk => $fv)
                {
                    $updateSQL .= "`".$fk."`='".$fv."', ";
                }
                $updateSQL .= " `userid`= ".$userid.",";
                $totalprice = floatval($this->convert_basket_sum_strings("%field_total[price]%", true));
                $updateSQL .= "`totalprice`=".$totalprice.", ";
                $updateSQL .= " `text`='".$kernel->pub_str_prepare_set($this->process_basket_items_tpl(CatalogCommons::get_templates_admin_prefix()."_order_info_db.html", $bitems, $fvalues))."',";
                $updateSQL .= " `lastaccess`= '".date("Y-m-d H:i:s")."', `isprocessed`=1 WHERE `id`='".$currOrder['id']."'";
                $kernel->runSQL($updateSQL);

                //чистим корзину
                $query = 'DELETE FROM `'.$kernel->pub_prefix_get().'_catalog_'.$moduleid.'_basket_items` WHERE `orderid`='.$currOrder['id'];
                $kernel->runSQL($query);

                //удаляем промо-код из сессии, если был
                $kernel->pub_session_unset('promo_code');

                //сбрасываем куки корзины для нового заказа
                setcookie($this->basketid_cookie_name, "", time() - $this->basketid_cookie_days * 24 * 60 * 60, '/');
                $this->get_current_basket_order();

                // отправляем письма о новом заказе на почты
                $manager_mail_subj = str_replace("%orderid%", $currOrder['id'], $manager_mail_subj);
                $user_mail_subj = str_replace("%orderid%", $currOrder['id'], $user_mail_subj);

                $fvalues_orig['id'] = $fvalues['id'] = $currOrder['id'];
                $from_name = $kernel->pub_http_host_get();
                $from_email = "noreply@".$kernel->pub_http_host_get();

                //письмо менеджеру
                $msg_body = $this->process_basket_items_tpl($manager_mail_tpl, $bitems, $fvalues_orig);
                $msg_body = str_replace("%orderid%", $currOrder['id'], $msg_body);
                $kernel->pub_mail(array($manager_email), array($manager_email), $from_email, $from_name, $manager_mail_subj, $msg_body, false, "", "", $user_email);

                //письмо юзеру
                if ($user_email)
                {
                    $msg_body = $this->process_basket_items_tpl($user_mail_tpl, $bitems, $fvalues_orig);
                    $msg_body = str_replace("%orderid%", $currOrder['id'], $msg_body);
                    $kernel->pub_mail(array($user_email), array($user_email), $from_email, $from_name, $user_mail_subj, $msg_body, false, "", "", $manager_email);
                }

                if ($ajax) { // если нужно отдать ответ на ajax-запрос об успешной отправке
                    echo json_encode(array("moduleid" => $moduleid, "message" => $order_received_success, "error" => "false"));
                    exit;
                } else {  // тогда отправка сообщения менеджеру и редирект на страницу успешной отправки заказа
                    if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                        $kernel->pub_redirect_refresh_global("/".$kernel->pub_page_current_get()."/?order_received=true");
                    else
                        $kernel->pub_redirect_refresh_global("/".$kernel->pub_page_current_get().".html?order_received=true");
                }

            } else {
                if ($ajax) { // если нужно отдать ответ на ajax-запрос об ошибке
                    echo json_encode(array("moduleid" => $moduleid, "message" => $order_received_error, "error" => "true"));
                    exit;
                } else { // тогда обычный редирект на страницу ошибки отправки
                    if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                        $kernel->pub_redirect_refresh_global("/".$kernel->pub_page_current_get()."/?order_received=false");
                    else
                        $kernel->pub_redirect_refresh_global("/".$kernel->pub_page_current_get().".html?order_received=false");
                }
            }

        } else { // значит вывод формы отправки заказа
            $form = $this->get_template_block("form");
            $form = $this->process_variables_out($form);

            if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                $form = str_replace("%catalog_link%", '/'.$catalog_page.'/', $form);
            else
                $form = str_replace("%catalog_link%", $catalog_page.'.html', $form);

            if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                $form = str_replace("%basket_link%", '/'.$basket_page.'/', $form);
            else
                $form = str_replace("%basket_link%", $basket_page.'.html', $form);

            if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                $form = str_replace("%order_link%", '/'.$order_page.'/', $form);
            else
                $form = str_replace("%order_link%", $order_page.'.html', $form);

            //заполним ранее введённые поля
            foreach ($ofields as $db_field => $ofield)
            {
                $postvar = $kernel->pub_httppost_get($db_field, false);
                if ($ofield['type'] == 'enum')
                {
                    $enum_elems=$this->get_enum_set_prop_values($tinfo[$db_field]['Type'],false);
                    $form = str_replace("%".$db_field."_".$postvar."_selected%", "selected", $form);
                    $form = str_replace("%".$db_field."_".$postvar."_checked%", "checked", $form);
                    foreach ($enum_elems as $enum_elem)
                    {
                        $form = str_replace("%".$db_field."_".$enum_elem."_selected%", " ", $form);
                        $form = str_replace("%".$db_field."_".$enum_elem."_checked%", " ", $form);
                    }
                }
                else
                    $form = str_replace("%".$db_field."_value%", htmlspecialchars($postvar), $form);
            }
            //очистим оставшиеся метки в шаблоне
            $form = $this->clear_left_labels($form);
            $content .= $form;
        }
        return $content;
    }


    /**
     * Строит HTML со списком товаров корзины на основании шаблона
     * используется в pub_catalog_show_basket_items и в письмах менеджеру и пользователю
     * при обработке заказа
     *
     * @param string $template шаблон
     * @param array $basket_items товары корзины
     * @param array $order_fields набор заполненных полей key=>value для заказа
     *
     * @return string
     */
    private function process_basket_items_tpl($template, $basket_items, $order_fields = array())
    {
        global $kernel;
        if (empty($template) || !file_exists($template))
            return "template not found.";
        $this->set_templates($kernel->pub_template_parse($template));

        $catalog_page = $kernel->pub_modul_properties_get('catalog_property_catalog_page', $kernel->pub_module_id_get());
        if(empty($catalog_page['value']))
            $catalog_page = 'catalog';
        else
            $catalog_page = $catalog_page['value'];

        if (count($basket_items) == 0) {
            $content = $this->get_template_block('list_null');
            if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                $content = str_replace("%catalog_link%", '/'.$catalog_page.'/', $content);
            else
                $content = str_replace("%catalog_link%", $catalog_page.'.html', $content);
            return $content;
        }

        $basket_page = $kernel->pub_modul_properties_get('catalog_property_basket_page', $kernel->pub_module_id_get());
        if(empty($basket_page['value']))
            $basket_page = 'basket';
        else
            $basket_page = $basket_page['value'];

        $order_page = $kernel->pub_modul_properties_get('catalog_property_order_page', $kernel->pub_module_id_get());
        if(empty($order_page['value']))
            $order_page = 'order';
        else
            $order_page = $order_page['value'];

        //Получаем общие свойства.
        //при этом, надо пройтись по свойствам и если там есть
        //картинки, то нужно продублировать их свойствами большого
        //и маленького изображения
        $props = CatalogCommons::get_props(0, false);
        if (isset($props['add_param']))
            $props['add_param'] = unserialize($props['add_param']);

        //Сформируем сначала строки с товарами
        $rows = '';
        $curr = 1;
        $matches = false;
        $groups = CatalogCommons::get_groups();
        $selected = $kernel->pub_session_get('selected'); // взяли отмеченные (выбранные) опции свойств товара из сессии

        foreach ($basket_items as $basketitem)
        {
            $item = $basketitem["item"];
            if ($curr % 2 == 0) //строка - чётная
                $odd_even = "even";
            else
                $odd_even = "odd";

            //Взяли блок строчки
            $block = $this->get_template_block('row_'.$odd_even);
            if (empty($block))
                $block = $this->get_template_block('row');

            $block = str_replace("%odd_even%", $odd_even, $block);
            $block = str_replace("%items_qty%", $basketitem["qty"], $block);

            if(isset($selected["itemid-".$basketitem['itemid']])) { // значит есть отмеченные опции у добавленного товара
                $fields = $selected["itemid-".$basketitem['itemid']]['fields'];
                if (!empty($fields)) {
                    foreach ($fields as $field => $value)
                    {
                        if(!empty($value)) {
                            $selected_block = $this->get_template_block("selected_".$field);
                            $selected_block = str_replace("%selected_value%", $value, $selected_block);
                            $block = str_replace("%selected_".$field."%", $selected_block, $block);
                        }
                        else
                        {
                            $block = str_replace("%selected_".$field."%", '', $block);
                        }
                    }
                }
            }

            if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                $block = str_replace("%remove_link%", '/'.$basket_page.'/?catalog_basket_removeitemid='.$basketitem['itemid'], $block);
            else
                $block = str_replace("%remove_link%", $basket_page.'.html?catalog_basket_removeitemid='.$basketitem['itemid'], $block);


            if (preg_match_all("/\\%field_sum\\[([a-z0-9_-]+)\\]\\%/iU", $block, $matches))
            {
                foreach ($matches[1] as $sum_field)
                {
                    $is_zero_price = false;
                    $sum = "";
                    if (isset($item[$sum_field]) && is_numeric($item[$sum_field]))
                    {
                        if (intval($item[$sum_field]) == 0)
                            $is_zero_price = true;
                        else
                            $sum = $basketitem['qty'] * $item[$sum_field];
                    }
                    else
                        $is_zero_price = true;

                    if ($is_zero_price)
                        $block = str_ireplace("%field_sum[".$sum_field."]%", $this->get_template_block('zero_price_label'), $block);
                    else
                        $block = str_ireplace("%field_sum[".$sum_field."]%", $sum, $block);
                }
            }

            //Теперь ищем переменные свойств и заменяем их
            $block = $this->process_item_props_out($item, $props, $block, $groups[$basketitem['item']['group_id']]);
            $cat_id = CatalogCommons::get_item2cat_id($basketitem['itemid']);
            $curr_cat = CatalogCommons::get_category($cat_id);

            if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                $block = str_replace("%link%", '/'.$catalog_page.'/'.$kernel->pub_pretty_url($curr_cat['name']).'-c'.$curr_cat['id'].'/'.$kernel->pub_pretty_url($item['name']).'-i'.$basketitem['itemid'].'.html', $block);
            else
                $block = str_replace("%link%", $catalog_page.'.html?'.$this->frontend_param_item_id_name.'='.$basketitem['itemid'], $block);

            $block .= $this->get_template_block('row_delimeter');
            $rows .= $block;
            $curr++;
        }

        $orderid = $this->get_current_basket_order();
        $content = $this->get_template_block('list');
        $content = str_replace("%row%", $rows, $content);
        $content = str_replace("%orderid%", $orderid['id'], $content);
        $content = str_replace("%datetime%", date('d.m.Y h:i:s'), $content);

        $promo_block = '';
        $promo_code = $kernel->pub_session_get('promo_code');
        if($promo_code) {
            $promo_id = $promo_code['promo_code']['id'];
            $promo_textcode = $promo_code['promo_code']['textcode'];
            $promo_value = intval($promo_code['promo_code']['value']);
            if ($promo_value > 0) {
                if($promo_code['promo_code']['type'] == 'dropprice') { // на величину в процентах
                    $promo_block = $this->get_template_block('promo_code_dropprice');
                } else { // на фиксированную величину
                    $promo_block = $this->get_template_block('promo_code_lowprice');
                }
                $promo_block = str_replace("%promo_code_id%", $promo_id, $promo_block);
                $promo_block = str_replace("%promo_code_textcode%", $promo_textcode, $promo_block);
                $promo_block = str_replace("%promo_code_value%", $promo_value, $promo_block);

                if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                    $promo_block = str_replace("%promo_code_remove_link%", '/'.$basket_page.'/?catalog_basket_remove_promocode='.$promo_id, $promo_block);
                else
                    $promo_block = str_replace("%promo_code_remove_link%", $basket_page.'.html?catalog_basket_remove_promocode='.$promo_id, $promo_block);
            }
        } else {
            $promo_block = $this->get_template_block('promo_code_form');
        }
        $content = str_replace("%promo_code%", $promo_block, $content);
        $content = $this->convert_basket_sum_strings($content);
        $content = $this->process_basket_ending($content);

        if ($order_fields)
        {
            foreach ($order_fields as $ofield => $ovalue)
            {
                $content = str_replace("%".$ofield."_value%", $ovalue, $content);
            }
        }


        $last_cat_block = "";
        if (isset($_COOKIE[$kernel->pub_module_id_get().'_last_catid']))
        {
            $lastcat = CatalogCommons::get_category(intval($_COOKIE[$kernel->pub_module_id_get().'_last_catid']));
            if ($lastcat)
            {
                $last_cat_block = $this->get_template_block('last_cat_block');
                foreach ($lastcat as $lk => $lv)
                {
                    $last_cat_block = str_replace("%".$lk."%", $lv, $last_cat_block);
                }
            }
        }
        $content = str_replace('%last_cat_block%', $last_cat_block, $content);


        if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
            $content = str_replace("%catalog_link%", '/'.$catalog_page.'/', $content);
        else
            $content = str_replace("%catalog_link%", $catalog_page.'.html', $content);

        if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
            $content = str_replace("%basket_link%", '/'.$basket_page.'/', $content);
        else
            $content = str_replace("%basket_link%", $basket_page.'.html', $content);

        if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
            $content = str_replace("%order_link%", '/'.$order_page.'/', $content);
        else
            $content = str_replace("%order_link%", $order_page.'.html', $content);

        $content = $this->replace_current_page_url($content);
        $content = $this->process_variables_out($content);
        //очистим оставшиеся метки
        $content = $this->clear_left_labels($content);

        return $content;
    }

    /**
     * Публичный метод для отображения списка заказов пользователя
     *
     * @param $template string шаблон
     * @param $perpage string кол-во на страницу
     * @param $maxpages string страниц в блоке
     *
     * @return string
     */
    public function pub_catalog_show_orders($header, $template, $perpage, $maxpages)
    {
        global $kernel;
        if (empty($template) || !file_exists($template))
            return "template not found.";

        $this->set_templates($kernel->pub_template_parse($template));

        $content = '';
        $offset = $this->get_offset_user();
        $page = $kernel->pub_httpget_get('page');

        $total = 0;

        if(empty($offset) && !empty($page) && (defined("USE_PRETTY_URL") && USE_PRETTY_URL))
            $offset = ($page-1) *  intval($perpage);

        $limit = $perpage;

        $userid = $kernel->pub_user_is_registred();
        if ($userid) {

            $query = "SELECT COUNT(*) AS count FROM `".$kernel->pub_prefix_get()."_catalog_".$kernel->pub_module_id_get()."_basket_orders` WHERE `userid`='".$userid."' AND `isprocessed` != '0'";
            $result = $kernel->runSQL($query);

            if ($row = mysqli_fetch_assoc($result))
                $total = $row['count'];

            mysqli_free_result($result);

            if ($offset >= $total)
                $offset = 0;



            if($total > 0) {
                $content = $this->get_template_block('list');
                $templates['order_row'] = $this->get_template_block('row');
                $templates['order_status_confirm'] = $this->get_template_block('order_status_confirm');
                $templates['order_status_confirmed'] = $this->get_template_block('order_status_confirmed');
                $templates['order_status_payment'] = $this->get_template_block('order_status_payment');
                $templates['order_status_shipping'] = $this->get_template_block('order_status_shipping');
                $templates['order_status_delivered'] = $this->get_template_block('order_status_delivered');
                $templates['order_status_canceled'] = $this->get_template_block('order_status_canceled');
                $orders_list = $this->get_basket_order_by_userid(intval($userid), $offset, $limit);

                if(count($orders_list) > 0) {
                    $list = '';
                    foreach ($orders_list as $order) {
                        $row = $templates['order_row'];

                        // основные параметры заказа
                        $row = str_replace("%id%", $order['id'], $row);
                        $row = str_replace("%date%", $order['lastaccess'], $row);
                        $row = str_replace("%content%", $order['text'], $row);
                        $row = str_replace("%totalprice%", $order['totalprice'], $row);

                        // статус заказа
                        switch (intval($order['isprocessed']))
                        {
                            case 1: // Оформлен, не подтвержден
                                $status = str_replace("%status_value%", $kernel->pub_page_textlabel_replace('[#catalog_order_status_confirm#]'), $templates['order_status_confirm']);
                                $row = str_replace("%status%", $status, $row);
                                break;
                            case 2: // Подтвержден, не оплачен
                                $status = str_replace("%status_value%", $kernel->pub_page_textlabel_replace('[#catalog_order_status_confirmed#]'), $templates['order_status_confirmed']);
                                $row = str_replace("%status%", $status, $row);
                                break;
                            case 3: // Оплачен, в обработке
                                $status = str_replace("%status_value%", $kernel->pub_page_textlabel_replace('[#catalog_order_status_payment#]'), $templates['order_status_payment']);
                                $row = str_replace("%status%", $status, $row);
                                break;
                            case 4: // В доставке
                                $status = str_replace("%status_value%", $kernel->pub_page_textlabel_replace('[#catalog_order_status_shipping#]'), $templates['order_status_shipping']);
                                $row = str_replace("%status%", $status, $row);
                                break;
                            case 5: // Доставлен
                                $status = str_replace("%status_value%", $kernel->pub_page_textlabel_replace('[#catalog_order_status_delivered#]'), $templates['order_status_delivered']);
                                $row = str_replace("%status%", $status, $row);
                                break;
                            case 6: // Отменен
                                $status = str_replace("%status_value%", $kernel->pub_page_textlabel_replace('[#catalog_order_status_canceled#]'), $templates['order_status_canceled']);
                                $row = str_replace("%status%", $status, $row);
                                break;
                            default:
                                $row = str_replace("%status%", '', $row);
                                break;
                        }

                        // оставшиеся данные заказа (имя, почта, телефон и т.д.)
                        foreach ($order as $key => $value) {
                            $row = str_replace("%".$key."%", $value, $row);
                        }

                        $list .= $row;
                    }
                    $content = str_replace("%rows%", $list, $content);
                }
            } else {
                $content = $this->get_template_block('list_null');
            }
        } else {
            frontoffice_manager::throw_404_error();
        }

        if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
            $purl = '/'.$kernel->pub_page_current_get().'/page-';
        else
            $purl = $kernel->pub_page_current_get().'.html?'.$this->frontend_param_offset_name.'=';

        $content = str_replace("%list_header%", $header, $content);
        $content = str_replace('%pages%', $this->build_pages_nav($total, $offset, $limit, $purl, $maxpages), $content);
        $content = $this->replace_current_page_url($content);
        $content = $this->process_variables_out($content);
        $content = $this->clear_left_labels($content);
        return $content;
    }

    /**
     * Публичный метод для отображения списка товаров в корзине
     *



     * @param $template string шаблон
     *
     * @return string
     */
    public function pub_catalog_show_basket_items($template)
    {
        $this->basket_init();
        return $this->process_basket_items_tpl($template, $this->get_basket_items());
    }

    /**
     * Публичный метод для отображения стикера корзины
     *
     * @param string $empty_tpl шаблон для пустой корзины
     * @param string $not_empty_tpl шаблон для корзины с товарами
     *
     * @return string
     */
    public function pub_catalog_show_basket_label($empty_tpl, $not_empty_tpl)
    {
        global $kernel;
        $this->basket_init();
        //$order = $this->get_basket_order_by_sid($this->get_current_basket_sessionid());
        //$basketsid = $this->get_current_basket_sessionid();

        $bitems = $this->get_basket_items();

        if (count($bitems) == 0)
            $template = $empty_tpl;
        else
            $template = $not_empty_tpl;
        $template = file_get_contents($template);

        $basket_page = $kernel->pub_modul_properties_get('catalog_property_basket_page', $kernel->pub_module_id_get());
        if(empty($basket_page['value']))
            $basket_page = 'basket';
        else
            $basket_page = $basket_page['value'];

        if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
            $template = str_replace("%basket_link%", '/'.$basket_page.'/', $template);
        else
            $template = str_replace("%basket_link%", $basket_page.'.html', $template);

        $order_page = $kernel->pub_modul_properties_get('catalog_property_order_page', $kernel->pub_module_id_get());
        if(empty($order_page['value']))
            $order_page = 'order';
        else
            $order_page = $order_page['value'];

        if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
            $template = str_replace("%order_link%", '/'.$order_page.'/', $template);
        else
            $template = str_replace("%order_link%", $order_page.'.html', $template);

        $template = $this->convert_basket_sum_strings($template);
        $template = $this->process_basket_ending($template);
        $template = $this->process_variables_out($template);
        //очистим оставшиеся метки
        $template = $this->clear_left_labels($template);

        return $template;
    }


    /**
     * Публичный метод для отображения стикера сравнения
     *
     * @param string $tpl шаблон для стикера для сравнения товаров
     *
     * @return string
     */
    public function pub_catalog_show_compare_label($tpl)
    {
        global $kernel;
        $tpl = $kernel->pub_template_parse($tpl);
        $compare_page = $kernel->pub_modul_properties_get('catalog_property_compare_page', $kernel->pub_module_id_get());
        if(empty($compare_page['value']))
            $compare_page = 'compare';
        else
            $compare_page = $compare_page['value'];

        $session_name = $kernel->pub_module_id_get().'_compared_items_'.$compare_page;

        if(isset($_SESSION[$session_name]))
            $count = $_SESSION[$session_name];
        else
            $count = 0;


        if (count($count) > 1)
        {
            $content = $tpl['compare'];
        }
        else
        {
            $content = $tpl['compare_null'];
        }

        $count = count($count);

        $end_array  = $kernel->pub_modul_properties_get('catalog_property_ending_basket', $kernel->pub_module_id_get());
        $end_array 	=  $end_array['value'];
        $source[]	= '%ending%';
        $replace[] 	= $kernel->ending($count, $end_array);
        $source[]	= '%count%';
        $replace[] 	= $count;

        $content = str_replace($source, $replace, $content);

        if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
            $content = str_replace("%compare_link%", '/'.$compare_page.'/', $content);
        else
            $content = str_replace("%compare_link%", $compare_page.'.html', $content);

        return $content;
    }


    /*
	*	Публичное действие для отображения сравнения товаров
	*
	*/
    public function pub_catalog_show_compare($tpl, $max_items)
    {

        global $kernel;
        $post_cb_name = 'additems2compare'; //параметр для чекбоксов
        $single_param_name = 'add2compare'; //параметр при единичном добавлении
        $remove_param_name = 'remove_from_compare'; //параметр для удаления
        $groupID = 0;
        $moduleid = $kernel->pub_module_id_get();
        $session_name = $moduleid.'_compared_items_'.$kernel->pub_page_current_get();
        if (isset($_SESSION[$session_name]) && $_SESSION[$session_name])
        {
            $items2compare = $_SESSION[$session_name];
            $groupID = $items2compare[key($items2compare)]['group_id'];
        }
        else
            $items2compare = array();

        if(isset($_GET['clear_all']) || isset($_POST['clear_all']))
            $items2compare = array();

        $is_modifed_list = false;
        if (count($items2compare) < $max_items && isset($_POST[$post_cb_name]) && is_array($_POST[$post_cb_name]))
        { //чекбоксы
            foreach ($_POST[$post_cb_name] as $id)
            {
                if (!CatalogCommons::is_valid_itemid($id))
                    continue;
                $idata = CatalogCommons::get_item_full_data($id);
                if (!$idata || ($groupID && $idata['group_id'] != $groupID))
                    continue;

                $items2compare[$idata['commonid']] = $idata;
                $groupID = $idata['group_id'];
                if (count($items2compare) == $max_items)
                    break;
            }
            $is_modifed_list = true;
        }
        //добавление единичного товара
        if (count($items2compare) < $max_items && isset($_REQUEST[$single_param_name]) && CatalogCommons::is_valid_itemid($_REQUEST[$single_param_name]))
        {
            $idata = CatalogCommons::get_item_full_data($_REQUEST[$single_param_name]);
            if ($idata && (!$groupID || $groupID == $idata['group_id']))
            {
                $items2compare[$idata['commonid']] = $idata;
                $groupID = $idata['group_id'];
            }
            $is_modifed_list = true;
        }

        //удаление из сравнения
        if (isset($_POST[$remove_param_name]) && is_array($_POST[$remove_param_name]))
        { //чекбоксами
            foreach ($_POST[$remove_param_name] as $riid)
            {
                if (isset($items2compare[$riid]))
                    unset($items2compare[$riid]);
            }
            $is_modifed_list = true;
        }
        elseif (isset($_REQUEST[$remove_param_name]) && isset($items2compare[$_REQUEST[$remove_param_name]])) //единичный товар
        {
            unset($items2compare[$_REQUEST[$remove_param_name]]);
            $is_modifed_list = true;
        }

        //добавим в сессию
        $_SESSION[$session_name] = $items2compare;

        //редирект назад если надо
        if ($is_modifed_list)
        {
            return $this->redirURL();
        }

        //отображение
        $this->set_templates($kernel->pub_template_parse($tpl));
        if (!$items2compare)
            return $this->get_template_block('list_null');

        if (count($items2compare) == 1)
            return $this->get_template_block('less_than_two');

        $content = $this->get_template_block('content');
        $props = CatalogCommons::get_props($groupID, true);
        foreach ($props as $prop)
        {
            $is_same_value = true;
            $val = null;
            $inum = 0;
            $pvalues = array();
            $only_empty_vals = true;
            foreach ($items2compare as $item)
            {
                $inum++;
                if ($inum == 1)
                    $val = $item[$prop['name_db']];
                elseif ($val != $item[$prop['name_db']])
                    $is_same_value = false;
                if(isset($this->templates[$prop['name_db'].'_value']))
                    $pvalue = $this->get_template_block($prop['name_db'].'_value');
                else
                    $pvalue = $this->get_template_block('prop_value');
                if(strlen($item[$prop['name_db']]))
                    $only_empty_vals = false;
                if($prop['type']=='pict' && isset($this->templates['image_val_tpl']))
                {
                    // <img src='%%prop_name_db%_small%' /> ||  %%prop_name_db%% + <!-- @image --> + <!-- @image_null -->
                    $val_block = $this->get_template_block('image_val_tpl');
                    $val_block = str_replace('%prop_name_db%', $prop['name_db'], $val_block);
                }
                else
                    $val_block = '%'.$prop['name_db'].'_value%';
                $value_block = $this->process_item_props_out($item, array($prop), $val_block);
                $pvalue = str_replace('%value%', $value_block, $pvalue);
                $pvalues[] = $pvalue;
            }
            if($only_empty_vals)
                continue;
            //различные блоки для одинаковых и разных значений свойств
            if ($is_same_value)
                $pline = $this->get_template_block('same_value_line');
            else
                $pline = $this->get_template_block('diff_value_line');
            $pline = str_replace('%name_full%', $prop['name_full'], $pline);
            $pline = str_replace('%prop_values%', implode($this->get_template_block('prop_values_separator'), $pvalues), $pline);
            $content = str_replace('%'.$prop['name_db'].'_line%', $pline, $content);
        }

        $catalog_page = $kernel->pub_modul_properties_get('catalog_property_catalog_page', $kernel->pub_module_id_get());
        if(empty($catalog_page['value']))
            $catalog_page = 'catalog';
        else
            $catalog_page = $catalog_page['value'];

        $basket_page = $kernel->pub_modul_properties_get('catalog_property_basket_page', $kernel->pub_module_id_get());
        if(empty($basket_page['value']))
            $basket_page = 'basket';
        else
            $basket_page = $basket_page['value'];

        $wishlist_page = $kernel->pub_modul_properties_get('catalog_property_wishlist_page', $kernel->pub_module_id_get());
        if(empty($wishlist_page['value']))
            $wishlist_page = 'wishlist';
        else
            $wishlist_page = $wishlist_page['value'];

        // получим список желаний
        $userid = intval($kernel->pub_user_is_registred());
        $wishlist = $this->get_wishlist_items_by_userid($userid);

        //в заголовке - вывод информации о сравниваемых товарах (название, фото)
        $iheaders = array();
        foreach ($items2compare as $item)
        {
            $iheader = $this->get_template_block('item_header');
            $iheader = $this->process_item_props_out($item, $props, $iheader);

            $cat_id = CatalogCommons::get_item2cat_id($item['commonid']);
            $cat = CatalogCommons::get_category($cat_id);

            if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                $iheader = str_replace("%link%", '/'.$catalog_page.'/'.$kernel->pub_pretty_url($cat['name']).'-c'.$cat_id.'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['commonid'].'.html', $iheader);
            else
                $iheader = str_replace("%link%", '/'.$catalog_page.'.html?'.$this->frontend_param_item_id_name.'='.$item['commonid'], $iheader);

            if (defined("USE_PRETTY_URL") && USE_PRETTY_URL) {
                if(isset($cat['name']) && isset($cat['id']))
                    $iheader = str_replace("%basket_link%", '/'.$basket_page.'/?catalog_basket_additemid='.$item['commonid'].'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($cat['name']).'-c'.$cat_id.'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['commonid'].'.html', $iheader);
                else
                    $iheader = str_replace("%basket_link%", '/'.$basket_page.'/?catalog_basket_additemid='.$item['commonid'].'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['commonid'].'.html', $iheader);
            } else {
                $iheader = str_replace("%basket_link%", $basket_page.'.html?catalog_basket_additemid='.$item['commonid'].'&redir2=/'.$kernel->pub_page_current_get().'.html?'.$this->frontend_param_item_id_name.'='.$item['commonid'], $iheader);
            }

            // Ссылка для добавления в список желаний
            $inwishlist = false;
            if (is_array($wishlist)) {
                foreach ($wishlist as $list) {
                    if($list['itemid'] == intval($item['commonid']))
                        $inwishlist = true;
                }
            }

            if($userid > 0) { // если пользователь зарегистрирован и авторизован
                if($inwishlist) { // значит есть в списке желаний
                    $wishlist_link = $this->get_template_block('wishlist_link_active');
                    $wishlist_param = 'remove4wishlist';
                } else {
                    $wishlist_link = $this->get_template_block('wishlist_link');
                    $wishlist_param = 'add2wishlist';
                }
            } else {
                $wishlist_link = $this->get_template_block('wishlist_link_disabled');
                $wishlist_param = '';
            }

            if($wishlist_link) { // значит етсь особый блок для вывода ссылки добавления в список желаний
                if($userid > 0) {
                    if (defined("USE_PRETTY_URL") && USE_PRETTY_URL) {
                        if(isset($cat['name']) && isset($cat_id))
                            $wishlist_link = str_replace("%link%", '/'.$wishlist_page.'/?'.$wishlist_param.'='.$item['commonid'].'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($cat['name']).'-c'.$cat_id.'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['commonid'].'.html', $wishlist_link);
                        else
                            $wishlist_link = str_replace("%link%", '/'.$wishlist_page.'/?'.$wishlist_param.'='.$item['commonid'].'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['commonid'].'.html', $wishlist_link);
                    } else {
                        $wishlist_link = str_replace("%link%", $wishlist_page.'.html?'.$wishlist_param.'='.$item['commonid'].'&redir2=/'.$kernel->pub_page_current_get().'.html?'.$this->frontend_param_item_id_name.'='.$item['commonid'], $wishlist_link);
                    }
                } else {
                    $wishlist_link = str_replace("%link%", '', $wishlist_link);
                }

                $iheader = str_replace("%wishlist_link%", $wishlist_link, $iheader);
            }
            else
            { // оставим старый формат для обратной совместимости
                if($userid > 0) {
                    if (defined("USE_PRETTY_URL") && USE_PRETTY_URL) {
                        if(isset($cat['name']) && isset($cat_id))
                            $iheader = str_replace("%wishlist_link%", '/'.$wishlist_page.'/?'.$wishlist_param.'='.$item['commonid'].'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($cat['name']).'-c'.$cat_id.'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['commonid'].'.html', $iheader);
                        else
                            $iheader = str_replace("%wishlist_link%", '/'.$wishlist_page.'/?'.$wishlist_param.'='.$item['commonid'].'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['commonid'].'.html', $iheader);
                    } else {
                        $iheader = str_replace("%wishlist_link%", $wishlist_page.'.html?'.$wishlist_param.'='.$item['commonid'].'&redir2=/'.$kernel->pub_page_current_get().'.html?'.$this->frontend_param_item_id_name.'='.$item['commonid'], $iheader);
                    }
                } else {
                    $iheader = str_replace("%wishlist_link%", '', $iheader);
                }
            }

            $iheaders[] = $iheader;
        }
        $content = str_replace('%items_headers%', implode($this->get_template_block('iheaders_separator'), $iheaders), $content);
        $content = str_replace('%total_compared%', count($items2compare), $content);

        $compare_page = $kernel->pub_modul_properties_get('catalog_property_compare_page', $kernel->pub_module_id_get());
        if(empty($compare_page['value']))
            $compare_page = 'compare';
        else
            $compare_page = $compare_page['value'];

        if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
            $content = str_replace("%compare_link%", '/'.$compare_page.'/', $content);
        else
            $content = str_replace("%compare_link%", $compare_page.'.html', $content);

        $content = $this->process_variables_out($content);
        $content = $this->clear_left_labels($content);
        return $content;

    }


    /*
	*	Публичное действие для отображения просмотренных товаров
	*
	*/
    public function pub_catalog_show_viewed($template, $expire, $max_items, $header = '')
    {
        global $kernel;
        $moduleid = $kernel->pub_module_id_get();
        $template = $kernel->pub_template_parse($template);
        $this->set_templates($template);

        $offset = 0;
        $limit = $max_items;

        // выборка товаров из БД
        $items = array();
        $user_items = array_unique($this->get_viewed_items_by_user($expire, $offset = 0, $limit = 100));
        foreach($user_items as $item) {
            $items[] = CatalogCommons::get_item(intval($item));
        }

        if(count($items) == 0)
            return $template['list_null'];
        else
            $content = $template['list'];

        $catalog_page = $kernel->pub_modul_properties_get('catalog_property_catalog_page', $kernel->pub_module_id_get());
        if(empty($catalog_page['value']))
            $catalog_page = 'catalog';
        else
            $catalog_page = $catalog_page['value'];

        $basket_page = $kernel->pub_modul_properties_get('catalog_property_basket_page', $kernel->pub_module_id_get());
        if(empty($basket_page['value']))
            $basket_page = 'basket';
        else
            $basket_page = $basket_page['value'];

        $compare_page = $kernel->pub_modul_properties_get('catalog_property_compare_page', $kernel->pub_module_id_get());
        if(empty($compare_page['value']))
            $compare_page = 'compare';
        else
            $compare_page = $compare_page['value'];

        $wishlist_page = $kernel->pub_modul_properties_get('catalog_property_wishlist_page', $kernel->pub_module_id_get());
        if(empty($wishlist_page['value']))
            $wishlist_page = 'wishlist';
        else
            $wishlist_page = $wishlist_page['value'];

        $rows = '';
        foreach ($items as $item)
        {
            $row = $template['row'];
            $props = CatalogCommons::get_props($item['group_id'], true);
            $row = $this->process_item_props_out($item, $props, $row);
            $cat_id = CatalogCommons::get_item2cat_id($item['id']);
            $cat = CatalogCommons::get_category($cat_id);

            if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                $row = str_replace("%link%", '/'.$catalog_page.'/'.$kernel->pub_pretty_url($cat['name']).'-c'.$cat_id.'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $row);
            else
                $row = str_replace("%link%", '/'.$catalog_page.'.html?'.$this->frontend_param_item_id_name.'='.$item['id'], $row);

            if (defined("USE_PRETTY_URL") && USE_PRETTY_URL) {
                if(isset($cat['name']) && isset($cat['id']))
                    $row = str_replace("%basket_link%", '/'.$basket_page.'/?catalog_basket_additemid='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($cat['name']).'-c'.$cat_id.'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $row);
                else
                    $row = str_replace("%basket_link%", '/'.$basket_page.'/?catalog_basket_additemid='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $row);
            } else {
                $row = str_replace("%basket_link%", $basket_page.'.html?catalog_basket_additemid='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'.html?'.$this->frontend_param_item_id_name.'='.$item['id'], $row);
            }

            if (defined("USE_PRETTY_URL") && USE_PRETTY_URL) {
                if(isset($cat['name']) && isset($cat['id']))
                    $row = str_replace("%compare_link%", '/'.$compare_page.'/?add2compare='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($cat['name']).'-c'.$cat['id'].'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $row);
                else
                    $row = str_replace("%compare_link%", '/'.$compare_page.'/?add2compare='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $row);
            } else {
                $row = str_replace("%compare_link%", $compare_page.'.html?add2compare='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'.html?'.$this->frontend_param_item_id_name.'='.$item['id'], $row);
            }

            if (defined("USE_PRETTY_URL") && USE_PRETTY_URL) {
                if(isset($cat['name']) && isset($cat['id']))
                    $row = str_replace("%wishlist_link%", '/'.$wishlist_page.'/?remove4wishlist='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($cat['name']).'-c'.$cat['id'].'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $row);
                else
                    $row = str_replace("%wishlist_link%", '/'.$wishlist_page.'/?remove4wishlist='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $row);
            } else {
                $row = str_replace("%wishlist_link%", $wishlist_page.'.html?remove4wishlist='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'.html?'.$this->frontend_param_item_id_name.'='.$item['id'], $row);
            }

            //также заменяем свойства категорий
            $row = $this->cats_props_out($cat['id'], $row);
            $rows .= $row;
        }

        $content = str_replace('%rows%', $rows, $content);
        $content = str_replace("%list_header%", $header, $content);
        $content = str_replace('%total%', count($items), $content);

        //выведем произвольные переменные каталога и очистим метки
        $content = $this->process_variables_out($content);
        $content = $this->clear_left_labels($content);
        $content = $this->replace_current_page_url($content);

        return $content;
    }


    /**
     * Публичный метод для вывода карточки категории
     *
     * @param string $tpl шаблон карточки категории
     *
     * @return string
     */
    public function pub_catalog_show_category($template1, $template2)
    {
        global $kernel;
        $moduleid = $kernel->pub_module_id_get();

        $itemid = $kernel->pub_httpget_get($this->frontend_param_item_id_name);
        if(!empty($itemid)) // если передан `itemid` с фронтенда значит выводится в карточке товара
            $template = $kernel->pub_template_parse($template2);
        else // значит используем oбщий шаблон
            $template = $kernel->pub_template_parse($template1);

        $this->set_templates($template);
        $content = $template['content'];

        //получим текущую категорию
        $cat_id = $this->get_current_catIDs();

        //получим свойства текущей категории
        $cat_props = CatalogCommons::get_category($cat_id);

        //если категоия скрыта или корневая - возврат
        if (($cat_props && $cat_props['_hide_from_site'] == 1) || $cat_id == 0)
            return;

        $catalog_page = $kernel->pub_modul_properties_get('catalog_property_catalog_page', $moduleid);
        if(empty($catalog_page['value']))
            $catalog_page = 'catalog';
        else
            $catalog_page = $catalog_page['value'];

        // заменим свойства категории в карточке товара
        if ($cat_props && is_array($cat_props))
        {
            foreach($cat_props as $name => $value)
            {
                if($value)
                {
                    if(isset($template[$name]))
                    {
                        $item = $template[$name];
                        $item = str_replace("%".$name."_value%", $value, $item);
                        $content = str_replace("%".$name."%", $item, $content);
                        $content = str_replace("%".$name."_value%", $value, $content);
                    }
                    else
                    {
                        $content = str_replace("%".$name."%", $value, $content);
                        $content = str_replace("%".$name."_value%", $value, $content);
                    }
                }
                else
                {
                    if(isset($template[$name."_null"]))
                    {
                        $item = $template[$name."_null"];
                        $content = str_replace("%".$name."%", $item, $content);
                    }
                    else
                    {
                        $content = str_replace("%".$name."%", '', $content);
                        $content = str_replace("%".$name."_value%", '', $content);
                    }
                }
            }
        }

        //теперь постоим путь категорий до родителя без добавления в дорогу сайта
        if(isset($template['cat_way_block'])) {
            $cway = array();
            $max_way = $this->get_way2cat($cat_id, true);
            foreach ($max_way as $cat)
            {

                if ($cat['id'] == $cat_id)
                    $cwblock = $template['cat_way_active'];
                else
                    $cwblock = $template['cat_way_passive'];

                $cwblock = str_replace('%cat_name%', $cat['name'], $cwblock);

                if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                    $cwblock = str_replace('%cat_link%', '/'.$catalog_page.'/'.$kernel->pub_pretty_url($cat['name']).'-c'.$cat['id'].'/', $cwblock);
                else
                    $cwblock = str_replace('%cat_link%', $catalog_page.'.html?'.$this->frontend_param_cat_id_name.'='.$cat['id'], $cwblock);

                $cway[] = $cwblock;
            }
            $cway_block = $template['cat_way_block'];
            $cway_block = str_replace('%cat_way%', implode($template['cat_way_separator'], $cway), $cway_block);
            $content = str_replace('%cat_way_block%', $cway_block, $content);
        }


        //и заменим ссылку на текущую категорию
        if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
            $content = str_replace('%link%', '/'.$catalog_page.'/'.$kernel->pub_pretty_url($cat_props['name']).'-c'.$cat_props['id'].'/', $content);
        else
            $content = str_replace('%link%', $catalog_page.'.html?'.$this->frontend_param_cat_id_name.'='.$cat_props['id'], $content);

        //выведем произвольные переменные каталога и очистим метки
        $content = $this->process_variables_out($content);
        $content = $this->clear_left_labels($content);
        $content = $this->replace_current_page_url($content);

        return $content;
    }


    /**
     * Публичный метод для вывода списка желаний
     *
     * @param string $tpl шаблон списка желаний пользователя
     * @param int $expire кол-во дней хранить историю
     * @param int $max_items мак. кол-во товаров в списке
     *
     * @return string
     */
    public function pub_catalog_show_wishlist($template, $expire, $max_items)
    {
        global $kernel;
        $moduleid = $kernel->pub_module_id_get();
        $template = $kernel->pub_template_parse($template);
        $this->set_templates($template);

        $ajax = $kernel->pub_is_ajax_request();

        $offset = 0;
        $limit = $max_items;

        $userid = intval($kernel->pub_user_is_registred());
        if($userid == 0)
            frontoffice_manager::throw_404_error();

        $add_to_wishlist_success = $kernel->pub_modul_properties_get('catalog_property_wishlist_additem_success', $moduleid);
        if(empty($add_to_wishlist_success['value']))
            $add_to_wishlist_success = '[#catalog_property_wishlist_additem_success_message#]';
        else
            $add_to_wishlist_success = $add_to_wishlist_success['value'];

        $add_to_wishlist_error = $kernel->pub_modul_properties_get('catalog_property_wishlist_additem_error', $moduleid);
        if(empty($add_to_wishlist_error['value']))
            $add_to_wishlist_error = '[#catalog_property_wishlist_additem_error_message#]';
        else
            $add_to_wishlist_error = $add_to_wishlist_error['value'];

        $remove_wishlist_success = $kernel->pub_modul_properties_get('catalog_property_wishlist_removeitem_success', $moduleid);
        if(empty($remove_wishlist_success['value']))
            $remove_wishlist_success = '[#catalog_property_wishlist_removeitem_success_message#]';
        else
            $remove_wishlist_success = $remove_wishlist_success['value'];

        $remove_wishlist_error = $kernel->pub_modul_properties_get('catalog_property_wishlist_removeitem_error', $moduleid);
        if(empty($remove_wishlist_error['value']))
            $remove_wishlist_error = '[#catalog_property_wishlist_removeitem_error_message#]';
        else
            $remove_wishlist_error = $remove_wishlist_error['value'];


        if (($kernel->pub_httppost_get("add2wishlist", false)) || ($kernel->pub_httpget_get("add2wishlist", false)))
        {

            if($kernel->pub_httppost_get("add2wishlist", false))
                $itemid = $kernel->pub_httppost_get("add2wishlist", false);
            else
                $itemid = $kernel->pub_httpget_get("add2wishlist", false);

            if ($itemid) {
                $query = 'INSERT IGNORE INTO `'.$kernel->pub_prefix_get().'_catalog_'.$moduleid.'_wishlist` '.
                    "(`lastaccess`, `userid`, `itemid`) VALUES (NOW(), '".$userid."', '".$itemid."') ON DUPLICATE KEY UPDATE `lastaccess`=NOW(), `userid`='".$userid."', `itemid`='".$itemid."'";
                if ($kernel->runSQL($query)) {
                    if ($ajax) { // если нужно отдать ответ на ajax-запрос об успешной отправке
                        echo json_encode(array("moduleid" => $moduleid, "message" => $add_to_wishlist_success, "error" => "false"));
                        exit;
                    } else {
                        $this->redirURL();
                    }
                } else {
                    if ($ajax) { // если нужно отдать ответ на ajax-запрос об ошибке отправки
                        echo json_encode(array("moduleid" => $moduleid, "message" => $add_to_wishlist_error, "error" => "true"));
                        exit;
                    } else {
                        $this->redirURL();
                    }
                }
            }

        }
        else if (($kernel->pub_httppost_get("remove4wishlist", false)) || ($kernel->pub_httpget_get("remove4wishlist", false)))
        {

            if($kernel->pub_httppost_get("remove4wishlist", false))
                $itemid = $kernel->pub_httppost_get("remove4wishlist", false);
            else
                $itemid = $kernel->pub_httpget_get("remove4wishlist", false);

            if ($itemid) {
                $query = 'DELETE FROM `'.$kernel->pub_prefix_get().'_catalog_'.$moduleid.'_wishlist` WHERE `itemid`='.$itemid.' AND `userid`='.$userid.'';
                if ($kernel->runSQL($query)) {
                    if ($ajax) { // если нужно отдать ответ на ajax-запрос об успешной отправке
                        echo json_encode(array("moduleid" => $moduleid, "message" => $remove_wishlist_success, "error" => "false"));
                        exit;
                    } else {
                        $this->redirURL();
                    }
                } else {
                    if ($ajax) { // если нужно отдать ответ на ajax-запрос об ошибке отправки
                        echo json_encode(array("moduleid" => $moduleid, "message" => $remove_wishlist_error, "error" => "true"));
                        exit;
                    } else {
                        $this->redirURL();
                    }
                }
            }

        }

        // выборка товаров из БД
        $items = array();
        $user_items = $this->get_wishlist_items_by_userid($userid, $offset, $limit);
        foreach($user_items as $item) {
            $items[] = CatalogCommons::get_item($item['itemid']);
        }

        if(count($items) == 0)
            return $template['list_null'];
        else
            $content = $template['list'];

        $catalog_page = $kernel->pub_modul_properties_get('catalog_property_catalog_page', $kernel->pub_module_id_get());
        if(empty($catalog_page['value']))
            $catalog_page = 'catalog';
        else
            $catalog_page = $catalog_page['value'];

        $basket_page = $kernel->pub_modul_properties_get('catalog_property_basket_page', $kernel->pub_module_id_get());
        if(empty($basket_page['value']))
            $basket_page = 'basket';
        else
            $basket_page = $basket_page['value'];

        $compare_page = $kernel->pub_modul_properties_get('catalog_property_compare_page', $kernel->pub_module_id_get());
        if(empty($compare_page['value']))
            $compare_page = 'compare';
        else
            $compare_page = $compare_page['value'];

        $wishlist_page = $kernel->pub_modul_properties_get('catalog_property_wishlist_page', $kernel->pub_module_id_get());
        if(empty($wishlist_page['value']))
            $wishlist_page = 'wishlist';
        else
            $wishlist_page = $wishlist_page['value'];

        $rows = '';

        foreach ($items as $item)
        {

            $row = $template['row'];
            $props = CatalogCommons::get_props($item['group_id'], true);
            $row = $this->process_item_props_out($item, $props, $row);
            $cat_id = CatalogCommons::get_item2cat_id($item['id']);
            $cat = CatalogCommons::get_category($cat_id);

            if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                $row = str_replace("%link%", '/'.$catalog_page.'/'.$kernel->pub_pretty_url($cat['name']).'-c'.$cat_id.'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $row);
            else
                $row = str_replace("%link%", '/'.$catalog_page.'.html?'.$this->frontend_param_item_id_name.'='.$item['id'], $row);

            if (defined("USE_PRETTY_URL") && USE_PRETTY_URL) {
                if(isset($cat['name']) && isset($cat['id']))
                    $row = str_replace("%basket_link%", '/'.$basket_page.'/?catalog_basket_additemid='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($cat['name']).'-c'.$cat_id.'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $row);
                else
                    $row = str_replace("%basket_link%", '/'.$basket_page.'/?catalog_basket_additemid='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $row);
            } else {
                $row = str_replace("%basket_link%", $basket_page.'.html?catalog_basket_additemid='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'.html?'.$this->frontend_param_item_id_name.'='.$item['id'], $row);
            }

            if (defined("USE_PRETTY_URL") && USE_PRETTY_URL) {
                if(isset($cat['name']) && isset($cat['id']))
                    $row = str_replace("%compare_link%", '/'.$compare_page.'/?add2compare='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($cat['name']).'-c'.$cat['id'].'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $row);
                else
                    $row = str_replace("%compare_link%", '/'.$compare_page.'/?add2compare='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $row);
            } else {
                $row = str_replace("%compare_link%", $compare_page.'.html?add2compare='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'.html?'.$this->frontend_param_item_id_name.'='.$item['id'], $row);
            }

            if (defined("USE_PRETTY_URL") && USE_PRETTY_URL) {
                if(isset($cat['name']) && isset($cat['id']))
                    $row = str_replace("%wishlist_link%", '/'.$wishlist_page.'/?remove4wishlist='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($cat['name']).'-c'.$cat['id'].'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $row);
                else
                    $row = str_replace("%wishlist_link%", '/'.$wishlist_page.'/?remove4wishlist='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $row);
            } else {
                $row = str_replace("%wishlist_link%", $wishlist_page.'.html?remove4wishlist='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'.html?'.$this->frontend_param_item_id_name.'='.$item['id'], $row);
            }

            //также заменяем свойства категорий
            $row = $this->cats_props_out($cat['id'], $row);

            $rows .= $row;
        }

        $content = str_replace('%rows%', $rows, $content);
        $content = str_replace('%total%', count($items), $content);

        //выведем произвольные переменные каталога и очистим метки
        $content = $this->process_variables_out($content);
        $content = $this->clear_left_labels($content);
        $content = $this->replace_current_page_url($content);

        return $content;
    }


    /**
     * Подсчитывает кол-во для корзины с учётом кол-ва товаров в корзине
     * @param string $column столбец, по которому считать
     * @param boolean $ignore_zero_price игнорировать приставку для товаров с нулевой ценой?
     * @return integer
     */
    private function get_basket_column_sum($column, $ignore_zero_price = false)
    {
        global $kernel;
        $total = 0;
        $bitems = $this->get_basket_items();
        $is_zero_price_found = false;
        foreach ($bitems as $bitem)
        {
            $item = $bitem['item'];
            if (isset($item[$column]) && is_numeric($item[$column]) && intval($item[$column]) > 0)
                $total += $bitem['qty'] * $item[$column];
            else
                $is_zero_price_found = true;
        }
        $ret = $this->cleanup_number($total);

        if ($is_zero_price_found && !$ignore_zero_price)
            $ret = $ret.$this->get_template_block('zero_total_label');

        return $ret;
    }

    /**
     * Конвертирует суммы столбцов в корзине товаров
     * меняет строки вида field_total[поле] на сумму по всем товарам с учётом кол-ва
     * +меняет строку %total_basket_items% на кол-во товаров в корзине
     *
     * @param string $text
     * @param boolean $ignore_zero_price игнорировать приставку для товаров с нулевой ценой?
     * @return string
     */
    private function convert_basket_sum_strings($text, $ignore_zero_price = false)
    {
        global $kernel;
        $text = str_replace("%total_basket_items%", count($this->get_basket_items()), $text);
        $matches = false;
        if (!preg_match_all("/\\%field_total\\[([a-z0-9_-]+)\\]\\%/iU", $text, $matches))
            return $text;
        foreach ($matches[1] as $match)
        {
            $val = $this->get_basket_column_sum($match, $ignore_zero_price);
            $promo_code = $kernel->pub_session_get('promo_code');
            if($promo_code) {
                $discount = intval($promo_code['promo_code']['value']);
                if ($discount > 0) {
                    if($promo_code['promo_code']['type'] == 'dropprice') // на величину в процентах
                        $val = $val - ($val * ($discount / 100));
                    elseif($promo_code['promo_code']['type'] == 'lowprice') // на фиксированную величину
                        $val = $val - $discount;
                }
            }
            $text = str_ireplace("%field_total[".$match."]%", $val, $text);
        }
        return $text;
    }


    /**
     * Подставляет правильное окончание в корзину
     *
     * @param string $content
     * @return string
     */
    private function process_basket_ending($content)
    {
        global $kernel;

        $basket_items =  count($this->get_basket_items());

        $end_array  = $kernel->pub_modul_properties_get('catalog_property_ending_basket', $kernel->pub_module_id_get());
        $end_array 	=  $end_array['value'];

        $ending = $kernel->ending($basket_items, $end_array);

        //заменяем переменные
        $content = str_replace('%ending%', $ending, $content);

        return $content;
    }

    /**
     * Убирает товар из корзины
     *
     * @param integer $itemid ID-шник товара
     * @return void
     */
    private function remove_item_from_basket($itemid,  $option_group =0, $choice = 0)
    {
        global $kernel;

        $itemid = intval($itemid);
        if ($itemid<1)
            return false;

        $currOrder = $this->get_current_basket_order();
        $where[] = 'orderid = '.  $currOrder['id'];
        $where[] = 'itemid = '. (int) $itemid;
        $where[] = '`option_group` = '. (int) $option_group; //для обычного (не комбинируемого товара) - 0, для уникальности
        $where[] = '`choice` = ' .(int) $choice;
        $where = ' WHERE '.join(' AND ', $where);

        $query = 'DELETE FROM `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_basket_items` '. $where;

        $this->update_basket_lastaccess();

        if($kernel->runSQL($query))
            return true;
        else
            return false;
    }

    /**
     * Обновляет кол-во товара в корзине
     *
     * @param integer $itemid ID-шник товара
     * @param integer $newqty новое кол-во
     * @return void
     */
    private function update_basket_item_qty($itemid, $newqty, $option_group=0, $choice=0)
    {
        global $kernel;

        $currOrder = $this->get_current_basket_order();
        $newqty = abs(intval($newqty));
        if ($newqty == 0) {
            $this->remove_item_from_basket($itemid, $option_group, $choice);
            return;
        }

        $set['qty'] = $newqty;
        $where[] = 'orderid = '.  $currOrder['id'];
        $where[] = 'itemid = '. (int) $itemid;
        $where[] = '`option_group` = '. (int) $option_group;
        $where[] = '`choice` = ' .(int) $choice;
        $where = join(' AND ', $where);

        if($kernel->db_update_record('_catalog_'.$kernel->pub_module_id_get().'_basket_items', $set, $where)) {
            $this->update_basket_lastaccess();
            return true;
        }
        return false;

    }

    /**
     * Обновляет время последнего изменения в корзине
     * @return void
     */
    private function update_basket_lastaccess()
    {
        global $kernel;
        $currOrder = $this->get_current_basket_order();
        $query = 'UPDATE `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_basket_orders` '.
            'SET `lastaccess`="'.date("Y-m-d H:i:s").'" '.
            'WHERE `id`='.$currOrder['id'];
        $kernel->runSQL($query);
    }

    /**
     * Возвращает запись о заказе товара из БД по ID-шнику сессии
     * @param string $sessionid ID-шник сессии
     * @return array
     */
    private function get_basket_order_by_sid($sessionid)
    {
        global $kernel;
        return $kernel->db_get_record_simple('_catalog_'.$kernel->pub_module_id_get().'_basket_orders', '`sessionid` = "'.$kernel->pub_str_prepare_set($sessionid).'"', "*");
    }

    /**
     * Возвращает список заказов из БД по ID-шнику пользователя
     * @param string $userid ID-шник пользователя
     * @param string $offset сдвиг выборки
     * @param string $limit лимит выборки
     * @return array
     */
    private function get_basket_order_by_userid($userid, $offset, $limit)
    {
        global $kernel;
        if($userid!==0)
            return $kernel->db_get_list_simple('_catalog_'.$kernel->pub_module_id_get().'_basket_orders', "`userid` = '".$kernel->pub_str_prepare_set($userid)."' AND `isprocessed` != '0'", "*", $offset, $limit);
        else
            return false;
    }


    /**
     * Возвращает товары из списка желаний из БД по ID-шнику пользователя
     * @param string $userid ID-шник пользователя
     * @param string $offset сдвиг выборки
     * @param string $limit лимит выборки
     * @return array
     */
    private function get_wishlist_items_by_userid($userid, $offset = 0, $limit = 100)
    {
        global $kernel;
        if($userid!==0)
            return $kernel->db_get_list_simple('_catalog_'.$kernel->pub_module_id_get().'_wishlist', "`userid` = '".$kernel->pub_str_prepare_set($userid)."'", "*", $offset, $limit);
        else
            return false;
    }

    /**
     * Возвращает ID ранее просмотренных товаров данного пользователя
     * @param string $offset сдвиг выборки
     * @param string $limit лимит выборки
     * @return array
     */
    private function get_viewed_items_by_user($expire, $offset = 0, $limit = 100)
    {
        global $kernel;
        $items = [];
        $session_name = 'viewed_items';
        $session_items = $kernel->pub_session_get($session_name, $kernel->pub_module_id_get());

        if (count($session_items) > 0) {
            $stored = array_slice(array_reverse($session_items), $offset, $limit);
            foreach ($stored as $item_id => $time) {
                if((time() - (intval($expire) * 24 * 60 * 60)) <= intval($time)) {
                    $items[] = intval($item_id);
                }
            }
        }

        if (count($items) > 0)
            return $items;
        else
            return [];

    }

    /**
     * Добавляет товар в корзину
     *
     * @param integer $itemid ID-шник товара
     * @param integer $qty кол-во, которое надо добавить
     * @return void
     */
    private function add_basket_item($itemid, $qty = 1, $option_group =0, $choice = 0)
    {

        global $kernel;

        $itemid = intval($itemid);
        if ($itemid<1)
            return false;

        $qty = intval($qty);
        if ($qty<1)
            return false;

        $basket_item = $this->get_basket_item_by_itemid($itemid, $option_group, $choice);

        //если такой товар уже есть, то обновление
        if ($basket_item) {
            $this->update_basket_item_qty($itemid, $basket_item['qty'] + $qty, $option_group, $choice);
            return true;
        } else { //в противном случае - добавление
            if(!empty($option_group) && !empty($choice)) {
                // смотрим, есть такое в бд или нет
                $options = $kernel->db_get_record_simple('_catalog_'.$kernel->pub_module_id_get().'_item_props_options', "`item_id` =" . (int)$itemid . " AND group_option_id = " . (int)$option_group . " AND option_choice = " . (int)$choice);

                if(!$options) {
                    $item = CatalogCommons::get_item($itemid);

                    if(!$item) {
                        return false;
                    }
                }
            } else {
                $item = CatalogCommons::get_item($itemid);
                if(!$item) {
                    return false;
                }
            }

            $currOrder = $this->get_current_basket_order();
            $set['orderid'] = $currOrder['id'];
            $set['itemid'] = (int)$itemid;
            $set['qty'] = $qty;

            //если у нас есть доп опции
            if(!empty($option_group) && !empty($choice))
            {
                $set['option_group'] = (int)$option_group;
                $set['choice'] = (int) $choice;
            }

            $kernel->db_add_record("_catalog_" . $kernel->pub_module_id_get() . "_basket_items", $set);
            $this->update_basket_lastaccess();
            return true;
        }
    }


    /**
     * Возвращает запись товара в корзине по itemid
     * @param integer $itemid ID-шник товара
     * @return array
     */
    private function get_basket_item_by_itemid($itemid, $option_group=0, $choice=0)
    {
        global $kernel;
        $currOrder = $this->get_current_basket_order();

        $where[]			= 'orderid = '.  $currOrder['id'];
        $where[]			= 'itemid = '. (int) $itemid;



        //по умолчанию 0, делаем это дело уникальным, чтобы не грохнулись все все подтовары товара
        $where[]	= '`option_group` = '. (int) $option_group;
        $where[] 	= '`choice` = ' .(int) $choice;


        $where = join(' AND ', $where);



        return $kernel->db_get_record_simple('_catalog_'.$kernel->pub_module_id_get().'_basket_items', $where);
    }

    /**
     * Добавляет заказ в БД, все поля пустые, кроме sessionid и lastaccess
     *
     * @param string $secret_session_id ID-шник сессии
     * @return string
     */
    private function add_basket_order($secret_session_id)
    {
        global $kernel;
        $userid = intval($kernel->pub_user_is_registred());
        $query = 'INSERT IGNORE INTO `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_basket_orders` '.
            "(`sessionid`, `lastaccess`, `userid`) VALUES ('".$secret_session_id."', NOW(), '".$userid."')";
        $kernel->runSQL($query);
        return $kernel->db_insert_id();
    }

    /**
     * Возвращает текущие товары из корзины
     * если надо, загружает из БД (lazy loading)
     *
     * @return array
     */
    private function get_basket_items()
    {
        if ($this->current_basket_items)
            return $this->current_basket_items;

        $currOrder = $this->get_current_basket_order();
        $bitems = $this->get_basket_items_fromdb($currOrder['id']);
        $arr = array();

        foreach ($bitems as $bitem)
        {
            $bitem['item'] = CatalogCommons::get_item_full_data($bitem['itemid'], $bitem['option_group'], $bitem['choice']);
            $arr[] = $bitem;
            $array_ids = $bitem['id'];
        }

        $this->current_basket_items = $arr;
        return $this->current_basket_items;
    }

    /**
     * Возвращает массив с товарами корзины из БД
     * @param integer $orderid IDшник заказа
     * @return array
     */
    private function get_basket_items_fromdb($orderid)
    {
        global $kernel;
        return $kernel->db_get_list_simple('_catalog_'.$kernel->pub_module_id_get().'_basket_items', ' `orderid` = '.$orderid.' ORDER BY id DESC');
    }

    /**
     * Заменяет в шаблоне %current_page_url%
     * на урл текущей страницы c учётом ТОЛЬКО параметра cid (catid)
     *
     * @param string $content
     *
     * @return string
     */
    private function replace_current_page_url($content)
    {
        global $kernel;
        if (defined("USE_PRETTY_URL") && USE_PRETTY_URL) {
            $url = '/'.$kernel->pub_page_current_get()."/";

            if (isset($_REQUEST[$this->frontend_param_cat_id_name])) {
                $cat_prop = CatalogCommons::get_category(intval($_REQUEST[$this->frontend_param_cat_id_name]));
                $url .= $kernel->pub_pretty_url($cat_prop['name']).'-c'.$_REQUEST[$this->frontend_param_cat_id_name]."/";
            }

            if (isset($_REQUEST[$this->frontend_param_item_id_name])) {
                $item_prop = CatalogCommons::get_item(intval($_REQUEST[$this->frontend_param_item_id_name]));
                $url .= $kernel->pub_pretty_url($item_prop['name']).'-i'.$_REQUEST[$this->frontend_param_item_id_name].".html";
            } else {
                $url .= "?";
                if (isset($_REQUEST[$this->frontend_param_offset_name]))
                    $url .= $this->frontend_param_offset_name."=".$_REQUEST[$this->frontend_param_offset_name]."&";

                if (isset($_REQUEST[$this->frontend_param_limit_name]))
                    $url .= $this->frontend_param_limit_name."=".$_REQUEST[$this->frontend_param_limit_name]."&";
            }

        } else {
            $url = $kernel->pub_page_current_get().".html?";
            if (isset($_REQUEST[$this->frontend_param_cat_id_name]))
                $url .= $this->frontend_param_cat_id_name."=".$_REQUEST[$this->frontend_param_cat_id_name]."&";
            if (isset($_REQUEST[$this->frontend_param_offset_name]))
                $url .= $this->frontend_param_offset_name."=".$_REQUEST[$this->frontend_param_offset_name]."&";
            if (isset($_REQUEST[$this->frontend_param_limit_name]))
                $url .= $this->frontend_param_limit_name."=".$_REQUEST[$this->frontend_param_limit_name]."&";
            if (isset($_REQUEST[$this->frontend_param_item_id_name]))
                $url .= $this->frontend_param_item_id_name."=".$_REQUEST[$this->frontend_param_item_id_name]."&";
        }

        $content = str_replace("%current_page_url%", $url, $content);
        $content = str_replace("%current_page_url_urlencoded%", urlencode($_SERVER['REQUEST_URI']), $content);
        return $content;
    }

    private function prepare_inner_filter_sql($sql, $params = array(), &$linkParams)
    {
        global $kernel;
        $matches = false;
        //param[param_name] меняем на строки из POST-запроса
        if (preg_match_all("/param\\[(.+)\\]/iU", $sql, $matches))
        {
            $allow_empty_params = false;
            if (mb_strpos($sql, 'REMOVE_NOT_SET') !== false)
                $allow_empty_params = true;

            foreach ($matches[1] as $param)
            {
                $is_in_request = false;
                if (isset($_REQUEST[$param]) && !empty($_REQUEST[$param]))
                    $is_in_request = true;

                $is_in_params = false;
                if (isset($params[$param]) && !empty($params[$param]))
                    $is_in_params = true;

                if ($is_in_request)
                {
                    if (is_array($_REQUEST[$param]))
                    { //например группа чекбоксов с именем name[]
                        if (
                            count($_REQUEST[$param]) == 1 &&
                            !preg_match("/\`(\w+)\`\.\`(\w+)\`\s*IN\s*\([\'|\`|\"]?+(.+)[\'|\`|\"]?\)/isU", $sql, $matches) &&
                            !preg_match("/FIND_IN_SET\(*+(param".preg_quote("[".$param."]").")\,\s+\`(\w+)\`\)*+/isU", $sql) &&
                            !preg_match("/CONCAT\s*\([\"|\'][\,][\"|\'][\,|\/]\s*[\`|\'](\w+)[\`|\'][\,|\/]\s*[\"|\'][\,][\"|\']\)\s+REGEXP\s*[\"|\'][\,|\/]\((.+)\)[\,|\/][\"|\']/isU", $sql) &&
                            !preg_match("/LIKE\s+([^(]*)(param".preg_quote("[".$param."]").")([^)]*)/isU", $sql) &&
                            !preg_match("/IN\\s+\\(([^(]*)(param".preg_quote("[".$param."]").")([^)]*)\\)/isU", $sql)
                        )
                        {
                            $value = $_REQUEST[$param][0];
                        }
                        else
                        {
                            $avalues = array();
                            foreach ($_REQUEST[$param] as $aparam)
                            {
                                $linkParams .= $param."[]=".urlencode($aparam)."&";
                                if (preg_match("/LIKE\s+([^(]*)(param".preg_quote("[".$param."]").")([^)]*)/isU", $sql)) {
                                    $avalues[] = $kernel->pub_str_prepare_set(str_replace(']', '\]', $aparam));
                                    $value = implode(",", $avalues);
                                } else {
                                    $avalues[] = "'" . $kernel->pub_str_prepare_set(str_replace(']', '\]', $aparam)) . "'";
                                    $value = implode(",", $avalues);
                                }
                            }
                        }
                    }
                    else
                    {
                        $linkParams .= $param."=".urlencode($_REQUEST[$param])."&";
                        $value = $kernel->pub_str_prepare_set(str_replace(']','\]', $_REQUEST[$param]));
                    }
                }
                elseif ($is_in_params)
                {
                    $linkParams .= $param."=".urlencode($params[$param])."&";
                    $pval = $params[$param];
                    $firstChar = mb_substr($pval, 0, 1);
                    $lastChar = mb_substr($pval, -1);

                    if (($firstChar == '"' && $lastChar == '"') || ($firstChar == "'" && $lastChar == "'"))
                        $pval = mb_substr($pval, 1, mb_strlen($pval) - 2);

                    $value = $kernel->pub_str_prepare_set($pval);
                }
                else
                {
                    if ($allow_empty_params)
                        $value = "%PARAM_NOT_SET%";
                    else
                        return false;
                    //$this->get_template_block('list_null');

                }
                $sql = str_ireplace("param[".$param."]", $value, $sql);
            }
            //убираем REMOVE_NOT_SET[..%PARAM_NOT_SET%..] полностью
            //..а для оставшихся просто убираем наш спецпрефикс REMOVE_NOT_SET и оставляет то, что было внутри скобок
            $pattern = '/REMOVE_NOT_SET\[(.*)([^\\\\]{1})\s*\]/sU';
            if (preg_match_all($pattern, $sql, $pmatches, PREG_SET_ORDER))
            {
                foreach ($pmatches as $m)
                {
                    $mc = $m[1].$m[2];
                    if (mb_strpos($mc, '%PARAM_NOT_SET%') === false)
                        $repl = $mc;
                    else
                        $repl = "";

                    $sql = str_replace($m[0], $repl, $sql);
                }

            }
        }
        $sql = trim(preg_replace('/\s\s+/', ' ', $sql));

        //var_dump($sql);
        // Чистовая обработка SQL-запроса
        $pmatches = false;
        if(preg_match_all("/FIND_IN_SET\(*+\'(\w+)\'\,\s+\`(\w+)\`\)\s+([\>|\<|\=|\!])\s+(\d+)/isU", $sql, $pmatches, PREG_SET_ORDER)) {
            /*$replace = '';
            foreach($gprops as $gprop) {
                $prop = explode('.', $gprop);
                if(isset($prop[1])) {
                    if ($prop[1] == $matches[2]) {
                        $fullname = str_replace('.', '`.`', trim($gprop));
                        $replace .= "FIND_IN_SET('" . $matches[1] . "', `" . $fullname . "`) " . $matches[3] . " " . $matches[4] . " OR ";

                    }
                }
            }
            $query = preg_replace("/FIND_IN_SET\(*+\'(\w+)\'\,\s+\`(\w+)\`\)\s+([\>|\<|\=|\!])\s+(\d+)/isU", $replace, $query);*/

        } else if(preg_match_all("/CONCAT\s*\([\"|\'][\,][\"|\'][\,|\/]\s*[\`|\'](\w+)[\`|\'][\,|\/]\s*[\"|\'][\,][\"|\']\)\s+REGEXP\s*[\"|\'][\,|\/]\((.+)\)[\,|\/][\"|\']/isU", $sql, $pmatches, PREG_SET_ORDER)) {
            foreach ($pmatches as $matches) {
                $sql = str_replace($matches[2], preg_quote(str_replace("'", "", str_replace(",", "|", preg_quote($matches[2])))), $sql);
            }
        }
        return $sql;
    }


    /**
     * Публичный метод для отображения выборки по внутреннему фильтру
     *
     * @param string  $filter_stringid   строковый ID-шник внутреннего фильтра
     * @param mixin (boolean | string)  $use_custom_template   использовать шаблон товарной группы (true), шаблон фильтра (false) или кастомный шаблон (string)?
     * @param array  $params массив параметров
     * @param boolean  $need_postprocessing очищать оставшиеся метки  и выводить переменные?
     * @param boolean  $is_direct выводить карточку товара при запросе прямой (прямая выборка из фильтра)?
     * @return string
     */
    public function pub_catalog_show_inner_selection_results($filter_stringid, $use_custom_template = false, $params = array(), $curr_cat_id = 0, $need_postprocessing = true, $is_direct = true)
    {
        global $kernel;
        $multi = false;
        $group = false;
        $moduleid = $kernel->pub_module_id_get();
        $curr_cat_id = intval($curr_cat_id);

        // Если передан `itemid` с фронтенд значит следует показать карточку товара
        $itemid = $kernel->pub_httpget_get($this->frontend_param_item_id_name);
        if (defined("USE_PRETTY_URL") && USE_PRETTY_URL && !empty($itemid) && $is_direct)
            return $this->pub_catalog_show_item_details($itemid);

        $filter = CatalogCommons::get_inner_filter_by_stringid($filter_stringid);
        if (!$filter)
            return "Inner filter '".htmlspecialchars($filter_stringid)."' not found";

        // Проверим, была ли указана тов. группа в настройках внутреннего фильтра
        if (!empty($filter['groupid']))
        {
            if ($filter['groupid'] == "-1") // пробуем автоматически определить товарную группу
            {
                // сделаем выборку по всем существующим товарным группам
                $groups = CatalogCommons::get_groups($moduleid, false);

                if(!intval($curr_cat_id) == 0)
                    $curr_cat_id = intval($curr_cat_id);
                else
                    $curr_cat_id = $this->get_current_catIDs();

                // счётчик мультигрупности
                $countof = 0;

                // ищем только ту товарную группу, в которой категория включена для отображения фильтров
                foreach ($groups as $ingroup)
                {
                    $defcatids = explode(",", $ingroup['defcatids']);
                    if(!empty($ingroup['filtercats'])) {
                        $filtercats = explode(",", $ingroup['filtercats']);
                        if(count($filtercats) > 0) {
                            foreach ($filtercats as $cat_id) {
                                if($cat_id == $curr_cat_id) {
                                    if($countof == 0) {
                                        $group = $ingroup;
                                    } else {

                                        // сначала добавим предыущую
                                        if($countof == 1)
                                            $_group[] = $group;

                                        // теперь текущую группу
                                        $_group[] = $ingroup;

                                        // и перезапишем
                                        $group = $_group;

                                        // проставим флаг, что у нас мультигрупность
                                        $multi = true;
                                    }
                                    // увеличим счётчик, по которому будем определять мультигрупность
                                    $countof++;
                                }
                            }
                        }
                    }
                    else
                    {
                        // пока оставим, для обратной своместимости со старой версией каталога
                        if(count($defcatids) > 0) {
                            foreach ($defcatids as $cat_id) {
                                if($cat_id == $curr_cat_id)
                                    $group = $ingroup;
                            }
                        }
                    }
                }
            }
            else
            {
                // была указана конкретная тов. группа, значит выборка по общим и/или дополнительным свойствам
                $group = CatalogCommons::get_group(intval($filter['groupid']));
                if (!$group) // Если мы не нашли товарную группу - запрос не получится
                    return "ERROR";
            }
        }

        if ($use_custom_template && is_string($use_custom_template)) { // значит была передана строка с названием файла шаблона
            if (empty($use_custom_template))
                return "У товарной группы не определён шаблон вывода списка товаров";

            $tpl = CatalogCommons::get_templates_user_prefix().$use_custom_template;
            $linkParams = "filterid=".$filter_stringid."&";
        } else if ($use_custom_template && !is_string($use_custom_template) && $group) { // значит используем шаблон товарной группы
            $category = CatalogCommons::get_category($curr_cat_id);
            if (!empty($category['tpl_items'])) { // приоритенне категория
                $tpl = CatalogCommons::get_templates_user_prefix().$category['tpl_items'];
            } else { // тогда шаблон, определенный в настройках группы
                if (empty($group['template_items_list']))
                    return "У товарной группы не определён шаблон вывода списка товаров";

                $tpl = CatalogCommons::get_templates_user_prefix().$group['template_items_list'];
            }
            $linkParams = "filterid=".$filter_stringid."&";
        } else { // значит используем шаблон указанный в настройках фильтра
            $category = CatalogCommons::get_category($curr_cat_id);
            if (!empty($category['tpl_items'])) { // приоритенне категория
                $tpl = CatalogCommons::get_templates_user_prefix().$category['tpl_items'];
            } else { // тогда шаблон, определенный в настройках фильтра
                $tpl = CatalogCommons::get_templates_user_prefix().$filter['template'];
            }
            $linkParams = "";
            if (isset($_REQUEST['filterid']))
            {
                $linkParams .= "filterid=".$filter_stringid."&";
                if(isset($_REQUEST['use_filter_template']))
                    $linkParams.='use_filter_template=1&';
            }
        }

        Log::debug("Каталог. Шаблон фильтра: ".$tpl,"catalog_filter_tpl");
        $this->set_templates($kernel->pub_template_parse($tpl));

        if(!$curr_cat_id == 0) {
            $filter['catids'] = $curr_cat_id;
            $linkParams .= "cid=".$curr_cat_id."&";
        } else if($curr_cat_id == 0) {
            return $this->get_template_block('no_data');
        }

        if (strlen($filter['catids']) == 0) {
            $curr_cat_id = $this->get_current_catIDs();
            if ($curr_cat_id)
            {
                if (is_array($curr_cat_id))
                {
                    foreach ($curr_cat_id as $ccid)
                    {
                        $linkParams .= "cid[".$ccid."]=on&";
                    }
                }
                else
                    $linkParams .= "cid=".$curr_cat_id."&";
            }
        }

        $cat_props = CatalogCommons::get_category($curr_cat_id);
        if ($cat_props && $cat_props['_hide_from_site'] == 1)
            frontoffice_manager::throw_404_error();

        if ($cat_props)
            setcookie($kernel->pub_module_id_get().'_last_catid', $cat_props['id'], time() + 31 * 24 * 60 * 60);

        $sql = $this->process_variables_out($filter['query']);
        $sql = $this->prepare_inner_filter_sql($sql, $params, $linkParams);

        if (!$sql)
        {
            $content = $this->process_filters_in_template($this->get_template_block('list_null'), $filter['stringid']);
            if ($curr_cat_id && is_numeric($curr_cat_id))
                $content = $this->cats_props_out($curr_cat_id, $content);

            return $content;
        }
        $filter['query'] = $sql;

        // сформируем запрос выборки
        $query = $this->convert_inner_filter_query2sql($filter, $group, $multi);

        Log::debug("Каталог. Запрос фильтра: <br>\n".$query,"catalog_filter_sql");
        if (!$query)
            return $this->process_filters_in_template($this->get_template_block('list_null'), $filter['stringid']);

        //обрежем и модифицируем запрос для получения общего кол-ва товаров
        $pos = mb_strpos(mb_strtolower($query), "order by");
        if ($pos === false)
            $countQuery = $query;
        else
            $countQuery = mb_substr($query, 0, $pos);

        $pos = mb_strpos(mb_strtolower($countQuery), " from");
        $countQuery = "SELECT SQL_CALC_FOUND_ROWS items.id ".mb_substr($countQuery, $pos)." LIMIT 1";
        $total = 0;
        $res=$kernel->runSQL($countQuery);
        mysqli_free_result( $res);

        $res=$kernel->runSQL("SELECT FOUND_ROWS() AS found");
        if ($fr=mysqli_fetch_assoc($res))
            $total=$fr['found'];
        mysqli_free_result( $res);

        /*
        Ограничения по количеству
        Так же нужно иметь возможность ограничить получаемый результат по количеству (LIMIT в mysql).
        К примеру, если нам нужно получить ТОП-5 товаров с низкой ценой, то мы установим значение 5.
        Если значение не установлено – тогда в результат отдается все найденные товары
        */
        if ((!empty($filter['limit']) && intval($filter['limit']) > 0) && $total > intval($filter['limit']))
            $total = intval($filter['limit']);

        $offset = $this->get_offset_user();
        $limit = $this->get_perpage_user();
        $page = $kernel->pub_httpget_get('page');

        if(empty($offset) && !empty($page) && (defined("USE_PRETTY_URL") && USE_PRETTY_URL))
            $offset = ($page-1) *  intval($filter['perpage']);

        if ($offset >= $total)
            $offset = 0;

        // получим кол-во на страницу из фронтенд, в противном случае используем то, что в настройках фильтра
        if ($limit == 0 || $limit >= intval($filter['limit']))
            $limit = intval($filter['perpage']);

        // если в запросе не была указана сортировка, добавляем вывод от последних товаров к старым
        if (stripos($query, "ORDER") === false)
            $query .= " ORDER by `id` DESC";

        // добавим LIMIT к запросу и выполним его
        if ($limit > 0)
            $query .= " LIMIT ".$offset.", ".$limit;

        //var_dump($query);
        $items = array();
        $result = $kernel->runSQL($query);
        if ($result)
        {
            while ($row = mysqli_fetch_assoc($result))
                $items[] = $row;
            mysqli_free_result($result);
        }
        $count = count($items);

        if ($count == 0)
        {
            $content = $this->process_filters_in_template($this->get_template_block('list_null'), $filter['stringid']);
            if ($curr_cat_id && is_numeric($curr_cat_id))
                $content = $this->cats_props_out($curr_cat_id, $content);
            return $content;
        }

        if ($multi) {
            foreach ($group as $ingroup) {
                $props[] = CatalogCommons::get_props($ingroup['id'], true);
            }
        } else {
            if ($group)
                $props = CatalogCommons::get_props($group['id'], true);
            else
                $props = CatalogCommons::get_props(0, false);
        }


        $catalog_page = $kernel->pub_modul_properties_get('catalog_property_catalog_page', $kernel->pub_module_id_get());
        if(empty($basket_page['value']))
            $catalog_page = 'catalog';
        else
            $catalog_page = $catalog_page['value'];

        if (empty($filter['targetpage']))
            $targetPage = $catalog_page;
        else
            $targetPage = $filter['targetpage'];

        $basket_page = $kernel->pub_modul_properties_get('catalog_property_basket_page', $kernel->pub_module_id_get());
        if(empty($basket_page['value']))
            $basket_page = 'basket';
        else
            $basket_page = $basket_page['value'];

        $compare_page = $kernel->pub_modul_properties_get('catalog_property_compare_page', $kernel->pub_module_id_get());
        if(empty($compare_page['value']))
            $compare_page = 'compare';
        else
            $compare_page = $compare_page['value'];

        $wishlist_page = $kernel->pub_modul_properties_get('catalog_property_wishlist_page', $kernel->pub_module_id_get());
        if(empty($wishlist_page['value']))
            $wishlist_page = 'wishlist';
        else
            $wishlist_page = $wishlist_page['value'];

        // получим список желаний
        $userid = intval($kernel->pub_user_is_registred());
        $wishlist = $this->get_wishlist_items_by_userid($userid);

        //Сформируем сначала строки с товарами
        $rows = '';
        $curr = 1;
        foreach ($items as $item)
        {
            if ($curr % 2 == 0) //строка - чётная
                $odd_even = "even";
            else
                $odd_even = "odd";
            //Взяли блок строчки
            $block = $this->get_template_block('row_'.$odd_even);
            if (empty($block))
                $block = $this->get_template_block('row');

            $block = str_replace("%odd_even%", $odd_even, $block);

            //Теперь ищем переменные, свойств и заменяем их
            if ($multi) {
                foreach ($props as $inprops) {
                    $block = $this->process_item_props_out($item, $inprops, $block, $group);
                }
            } else {
                $block = $this->process_item_props_out($item, $props, $block, $group);
            }

            // Определяем категорию если не была поределена ранее
            if(!isset($cat_props['name']) && !isset($cat_props['id'])) {
                $cat_id = CatalogCommons::get_item2cat_id($item['id']);
                $cat_props = CatalogCommons::get_category($cat_id);
            }

            // Ссылка на текущий товар в каталоге
            if (defined("USE_PRETTY_URL") && USE_PRETTY_URL) {
                if(isset($cat_props['name']) && isset($cat_props['id']))
                    $block = str_replace("%link%", '/'.$targetPage.'/'.$kernel->pub_pretty_url($cat_props['name']).'-c'.$cat_props['id'].'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $block);
                else
                    $block = str_replace("%link%", '/'.$targetPage.'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $block);
            } else {
                $block = str_replace("%link%", $targetPage.'.html?'.$this->frontend_param_item_id_name.'='.$item['id'], $block);
            }

            // Ссылка для добавления в корзину
            if (defined("USE_PRETTY_URL") && USE_PRETTY_URL) {
                if(isset($cat_props['name']) && isset($cat_props['id']))
                    $block = str_replace("%basket_link%", '/'.$basket_page.'/?catalog_basket_additemid='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($cat_props['name']).'-c'.$cat_props['id'].'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $block);
                else
                    $block = str_replace("%basket_link%", '/'.$basket_page.'/?catalog_basket_additemid='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $block);
            } else {
                $block = str_replace("%basket_link%", $basket_page.'.html?catalog_basket_additemid='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'.html?'.$this->frontend_param_item_id_name.'='.$item['id'], $block);
            }

            // Ссылка для добавления с писок сравнения товаров
            if (defined("USE_PRETTY_URL") && USE_PRETTY_URL) {
                if(isset($cat_props['name']) && isset($cat_props['id']))
                    $block = str_replace("%compare_link%", '/'.$compare_page.'/?add2compare='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($cat_props['name']).'-c'.$cat_props['id'].'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $block);
                else
                    $block = str_replace("%compare_link%", '/'.$compare_page.'/?add2compare='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $block);
            } else {
                $block = str_replace("%compare_link%", $compare_page.'.html?add2compare='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'.html?'.$this->frontend_param_item_id_name.'='.$item['id'], $block);
            }

            // Ссылка для добавления в список желаний
            $inwishlist = false;
            if (is_array($wishlist)) {
                foreach ($wishlist as $list) {
                    if($list['itemid'] == intval($item['id']))
                        $inwishlist = true;
                }
            }

            if($userid > 0) { // если пользователь зарегистрирован и авторизован
                if($inwishlist) { // значит есть в списке желаний
                    $wishlist_link = $this->get_template_block('wishlist_link_active');
                    $wishlist_param = 'remove4wishlist';
                } else {
                    $wishlist_link = $this->get_template_block('wishlist_link');
                    $wishlist_param = 'add2wishlist';
                }
            } else {
                $wishlist_link = $this->get_template_block('wishlist_link_disabled');
                $wishlist_param = '';
            }

            if($wishlist_link) { // значит етсь особый блок для вывода ссылки добавления в список желаний
                if($userid > 0) {
                    if (defined("USE_PRETTY_URL") && USE_PRETTY_URL) {
                        if(isset($cat_props['name']) && isset($cat_props['id']))
                            $wishlist_link = str_replace("%link%", '/'.$wishlist_page.'/?'.$wishlist_param.'='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($cat_props['name']).'-c'.$cat_props['id'].'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $wishlist_link);
                        else
                            $wishlist_link = str_replace("%link%", '/'.$wishlist_page.'/?'.$wishlist_param.'='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $wishlist_link);
                    } else {
                        $wishlist_link = str_replace("%link%", $wishlist_page.'.html?'.$wishlist_param.'='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'.html?'.$this->frontend_param_item_id_name.'='.$item['id'], $wishlist_link);
                    }
                } else {
                    $wishlist_link = str_replace("%link%", '', $wishlist_link);
                }

                $block = str_replace("%wishlist_link%", $wishlist_link, $block);
            }
            else
            { // оставим старый формат для обратной совместимости
                if($userid > 0) {
                    if (defined("USE_PRETTY_URL") && USE_PRETTY_URL) {
                        if(isset($cat_props['name']) && isset($cat_props['id']))
                            $block = str_replace("%wishlist_link%", '/'.$wishlist_page.'/?'.$wishlist_param.'='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($cat_props['name']).'-c'.$cat_props['id'].'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $block);
                        else
                            $block = str_replace("%wishlist_link%", '/'.$wishlist_page.'/?'.$wishlist_param.'='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $block);
                    } else {
                        $block = str_replace("%wishlist_link%", $wishlist_page.'.html?'.$wishlist_param.'='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'.html?'.$this->frontend_param_item_id_name.'='.$item['id'], $block);
                    }
                } else {
                    $block = str_replace("%wishlist_link%", '', $block);
                }
            }

            $rows .= $block;
            $curr++;
        }

        $content = $this->get_template_block('list');
        $content = str_replace("%row%", $rows, $content);
        $content = str_replace("%total_in_cat%", $total, $content);

        if ($cat_props)
            $content = $this->cats_props_out($cat_props['id'], $content);

        if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
            $purl = '/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($cat_props['name']).'-c'.$cat_props['id'].'/page-';
        else
            $purl = $kernel->pub_page_current_get().'.html?'.$linkParams.$this->frontend_param_offset_name.'=';

        $content = str_replace('%pages%', $this->build_pages_nav($total, $offset, $limit, $purl, intval($filter['maxpages'])), $content);

        if ($curr_cat_id && is_numeric($curr_cat_id))
            $content = $this->cats_props_out($curr_cat_id, $content);

        $content = $this->process_filters_in_template($content, $filter['stringid']);
        $content = $this->replace_current_page_url($content);

        if ($need_postprocessing)
        {
            $content = $this->process_variables_out($content);
            //очистим оставшиеся метки
            $content = $this->clear_left_labels($content);
        }
        return $content;
    }


    /**
     * Заменяет в блоке строки вида %variable[myvar]% на значения переменных
     * +вычисляет формулы вида %formula[0.1*6]%
     * +обрабатывает текст %nl2br[....]%, заменяя переводы строки на <br> (функция nl2br)
     * +добавляет текущую дату %date[Y-m-d H:s]% в указанном формате (функция date() )
     * +обрабатывет текст %xml_cleanup[...]% заменяя символы для валидности xmk
     * @param string $block
     * @return string
     */
    private function process_variables_out($block)
    {
        if (preg_match_all("|\\%variable\\[([a-z0-9_]+)\\]\\%|iU", $block, $matches))
        {
            $vars = CatalogCommons::get_variables();
            foreach ($matches[1] as $var)
            {
                if (array_key_exists($var, $vars))
                {
                    $repl = $vars[$var]['value'];
                    if (preg_match("|^([\\d,\\.]+)$|", $repl)) //для дробных числовых значений
                        $repl = str_replace(",", ".", $repl);
                }
                else
                    $repl = "";
                $block = str_ireplace("%variable[".$var."]%", $repl, $block);
            }
        }
        if (preg_match_all("|\\%date\\[(.+)\\]\\%|isU", $block, $matches, PREG_SET_ORDER))
        {
            foreach ($matches as $match)
            {
                $block = str_replace($match[0], date($match[1]), $block);
            }
        }
        if (preg_match_all("|\\%nl2br\\[(.+)\\]\\%|isU", $block, $matches, PREG_SET_ORDER))
        {
            foreach ($matches as $match)
            {
                $block = str_replace($match[0], nl2br($match[1]), $block);
            }
        }
        if (preg_match_all("|\\%strip_tags\\[(.+)\\]\\%|isU", $block, $matches, PREG_SET_ORDER))
        {
            foreach ($matches as $match)
            {
                $block = str_replace($match[0], trim(preg_replace("/[\n\r]/", "", str_replace('&nbsp;', '', strip_tags($match[1])))), $block);
            }
        }
        if (preg_match_all("|\\%trim\\[(.+)\\]\\%|isU", $block, $matches, PREG_SET_ORDER))
        {
            foreach ($matches as $match)
            {
                $block = str_replace($match[0], trim($match[1]), $block);
            }
        }
        if (preg_match_all("|\\%xml_cleanup\\[(.+)\\]\\%|isU", $block, $matches, PREG_SET_ORDER))
        {
            foreach ($matches as $match)
            {
                $block = str_replace($match[0], str_replace(array( '&', '"','>', '<', '\''), array('&amp;', '&quot;', '&gt;', '&lt;', '&apos;'), $match[1]), $block);
            }
        }

        if (preg_match_all("|\\%formula\\[(.+)\\]\\%|iU", $block, $matches))
        {
            foreach ($matches[1] as $match)
            {
                //проверим, не осталось ли незаполненных переменных
                if (preg_match("/%([a-z0-9_-]+)_value%/i", $match))
                    $repl = "";
                else
                    $repl = @eval("return ".$match.";");
                $block = str_ireplace("%formula[".$match."]%", $repl, $block);
            }
        }


        return $block;
    }

    /**
     * Подготавливает свойства товара для вывода во frontend, для методов
     * pub_catalog_show_inner_selection_results, pub_catalog_show_item_details,
     * pub_catalog_show_basket_items и pub_catalog_show_items
     *
     * @param array $item товар
     * @param array $props свойства товара
     * @param string $block часть шаблона для товара
     * @param array $group массив тов. группы, чтобы выводить название группы
     * @return string
     */
    private function process_item_props_out($item, $props, $block, $group = array())
    {

        global $kernel;

        if ($group && isset($group['name_full']))
            $block = str_replace("%group.name%", $group['name_full'], $block);

        $propskv = array();
        foreach ($props as $cp)
        {

            $propskv[$cp['name_db']] = $cp;
            $value = '';
            if (isset($item[$cp['name_db']]))
                $value = $item[$cp['name_db']];

            //Взяли блок для переменной, если его нет - то строка будет пустой
            if (mb_strlen($value) == 0)
                $block = str_replace("%".$cp['name_db']."%", $this->get_template_block($cp['name_db']."_null"), $block);
            else
            {
                $block = str_replace("%".$cp['name_db']."%", $this->get_template_block($cp['name_db']), $block);
                $value = $this->format_value($value,$cp);
                $block = str_replace('%'.$cp['name_db'].'_value%', $value, $block);
            }
            $block = str_replace('%'.$cp['name_db'].'_name%', $cp['name_full'], $block);

            // Если это картинка, то нужно ещё обработать доп переменные на большое/маленькое изображение и на размеры изображения
            if ($cp['type'] == 'pict' && !empty($value))
            {
                //Сначала размеры большого изображения
                if (file_exists($kernel->pub_site_root_get().'/'.$value))
                {
                    $size = @getimagesize($value);
                    if ($size)
                    {
                        $block = str_replace('%'.$cp['name_db'].'_width%', $size[0], $block);
                        $block = str_replace('%'.$cp['name_db'].'_height%', $size[1], $block);
                    }
                    $block = str_replace('%'.$cp['name_db'].'_value%', '/'.$value, $block);
                }
                //кроме этого надо добавить переменные для малого и исходного изображения
                $path_parts = pathinfo($value);
                $path_small = $path_parts['dirname'].'/tn/'.$path_parts['basename'];
                $path_source = $path_parts['dirname'].'/source/'.$path_parts['basename'];

                if (file_exists($path_small))
                { //размеры маленького изображения, если есть
                    $size = @getimagesize($path_small);
                    if ($size)
                    {
                        $block = str_replace('%'.$cp['name_db'].'_small_width%', $size[0], $block);
                        $block = str_replace('%'.$cp['name_db'].'_small_height%', $size[1], $block);
                    }
                    $block = str_replace('%'.$cp['name_db'].'_small%', '/'.$path_small, $block);
                }

                if (file_exists($path_source))
                { //размеры исходного изображения, если есть
                    $size = @getimagesize($path_source);
                    if ($size)
                    {
                        $block = str_replace('%'.$cp['name_db'].'_source_width%', '/'.$size[0], $block);
                        $block = str_replace('%'.$cp['name_db'].'_source_height%', '/'.$size[1], $block);
                    }
                    $block = str_replace('%'.$cp['name_db'].'_source%', '/'.$path_source, $block);
                }
            }

        }
        if (isset($item['commonid']))
            $item_common_id = $item['commonid'];
        else
            $item_common_id = $item['id'];




        $source[] 	= '%item_id%';
        $replace[]	= $item_common_id;

        if (!empty($item['choice']))
        {



            $source[] 	=	'%choice%';
            $replace[]	=	$item['choice'];
            $source[] 	= 	'%option_group%';
            $replace[]	= 	$item['option_group'];
            $source[] 	= 	'%basket_url_ending%';
            $replace[]	= 	'&option_group='.$item['option_group'].'&choice='.$item['choice'];
        }
        else
        {

            $source[] 	= '%basket_url_ending%';
            $replace[]	= '';

            $source[] 	=	'%choice%';
            $replace[]	=	0;
            $source[] 	= 	'%option_group%';
            $replace[]	= 	0;
        }

        $block = str_replace($source, $replace, $block);

        //проверим, осталось ли в шаблоне чтото вида %aaaaa_value%
        //пример использование - alt в img, равное названию товара
        $matches = false;
        if (preg_match_all("/\\%([a-z0-9_-]+)_value\\%/iU", $block, $matches))
        {
            foreach ($matches[1] as $prop)
            {
                if (isset($item[$prop]) && isset($propskv[$prop]))
                    $block = str_replace("%".$prop."_value%", $this->format_value($item[$prop],$propskv[$prop]), $block);
            }
        }
        return $block;
    }


    /**
     * Форматирует $value в шаблоне  в соответствии с его типом  и
     * возвращает новое значение
     * @param 	string $value		- переменная, хранящая значение
     * @param 	array  $prop		- свойство (массив)
     * @return 	string
     */
    private function format_value($value,array $prop)
    {
        global $kernel;
        switch ($prop['type'])
        {
            case 'number':
                $value = $this->cleanup_number($value);
                break;
            case 'date':
                $dformat = $kernel->pub_modul_properties_get('catalog_property_date_format', $kernel->pub_module_id_get());
                $dformat = trim($dformat['value']);
                if (empty($dformat))
                    $dformat = 'd.m.Y';
                $time_val = strtotime($value);
                $value = date($dformat, $time_val);
                break;
            case 'set':
                $vblocks = array();

                if (isset($this->templates[$prop['name_db'].'_separator']))
                    $sep = $this->templates[$prop['name_db'].'_separator'];
                elseif (isset($this->templates['item_sets_separator']))
                    $sep = $this->templates['item_sets_separator'];
                else
                    $sep = "\n";

                $set_value_tpl = $this->get_template_block($prop['name_db'].'_val');
                $set_value_tpl_empty = $this->get_template_block($prop['name_db'].'_empty');

                if (empty($set_value_tpl))
                    $set_value_tpl = '%setvalue%';

                if (empty($set_value_tpl_empty))
                    $set_value_tpl_empty = '%setvalue%';

                if(is_array($prop['add_param']))
                    $prop_params = $prop['add_param'];
                else
                    $prop_params = unserialize($prop['add_param']);

                if (is_array($prop_params)) {
                    foreach ($prop_params as $param) {
                        if(in_array($param, explode(",", $value)))
                            $vblock = str_replace("%setvalue%", $param, $set_value_tpl);
                        else
                            $vblock = str_replace("%setvalue%", $param, $set_value_tpl_empty);

                        $trimblock = trim($vblock);
                        if(!empty($trimblock))
                            $vblocks[] = $vblock;
                    }
                }

                $value = implode($sep, $vblocks);
                break;
        }
        return $value;
    }


    /**
     * Публичный метод для отображения списка товаров
     *
     * @param integer        $limit                    товаров на страницу
     * @param integer        $show_cats_if_empty_items выводить ли список категорий, если нет товаров?
     * @param string  		$cats_header   заголовок списка категорий
     * @param string        $cats_tpl                 файл шаблона для списка категорий
     * @param string        $multi_group_tpl          файл шаблона для разных групп
     * @param integer       $pages_in_block          мак.кол-во страниц в блоке
     * @param integer|boolean $catid                    idшник категории (для прямого вызова)
     * @param string|boolean  $custom_template          файл шаблона (для прямого вызова)
     * @param string|boolean  $is_direct          флаг, если рямой вызов
     * @return string
     */
    public function pub_catalog_show_items($limit, $show_cats_if_empty_items, $cats_header = '', $cats_tpl, $multi_group_tpl = '', $pages_in_block=5, $catid = false, $custom_template = false, $is_direct = false)
    {
        global $kernel;
        if (!$catid)
        {
            $itemid = $kernel->pub_httpget_get($this->frontend_param_item_id_name);
            if (!empty($itemid))
                return $this->pub_catalog_show_item_details($itemid);
        }
        else
            $this->add_categories2waysite($this->get_way2cat($catid, true));

        if (isset($_REQUEST['filterid'])) //значит работаем по внутреннему фильтру
            return $this->pub_catalog_show_inner_selection_results($_REQUEST['filterid'], !isset($_REQUEST['use_filter_template']), array(), 0, true, false);

        if (!$catid)
        {
            $catid = $this->get_current_catid(true);
            if ($catid == 0)
                $catid = $this->get_default_catid();
            else
                $this->add_categories2waysite($this->get_way2cat($catid, true));
        }

        $category = CatalogCommons::get_category($catid);
        if ($category) {
            // если категория существует, но скрыта в настройках
            if($category['_hide_from_site'] == 1)
                frontoffice_manager::throw_404_error();
        } else {
            // необходимо выводить список категорий, если нет товаров
            if (!$show_cats_if_empty_items)
                frontoffice_manager::throw_404_error();
        }

        if (!$custom_template && !$is_direct) //remember last catid
            setcookie($kernel->pub_module_id_get().'_last_catid', $category['id'], time() + 31 * 24 * 60 * 60);

        $total = $this->get_cat_items_count($catid, true);
        $offset = $this->get_offset_user();
        $page = $kernel->pub_httpget_get('page');
        if(empty($offset) && !empty($page) && (defined("USE_PRETTY_URL") && USE_PRETTY_URL))
            $offset = ($page-1) * $limit;

        if ($total == 0)
            $items = array();
        else
        {
            $poupularity_sort_days = 0;
            $popprop = $kernel->pub_modul_properties_get("catalog_property_popular_days");
            if ($popprop['isset'])
                $poupularity_sort_days = intval($popprop['value']);
            if ($poupularity_sort_days > 0)
            {
                $statConds = array();

                if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                    $statUrlPrefix = "`uri`='/".$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($category['name']).'-c'.$category['id'].'/'.$kernel->pub_pretty_url($item['name']).'-i';
                else
                    $statUrlPrefix = "`uri`='/".$kernel->pub_page_current_get().".html?".$this->frontend_param_item_id_name."=";

                $itemids = $this->get_cat_itemids($catid);

                foreach ($itemids as $itemid)
                {
                    if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                        $statConds[] = $statUrlPrefix.$itemid.".html'";
                    else
                        $statConds[] = $statUrlPrefix.$itemid."'";
                }
                $time = strtotime("-".$poupularity_sort_days." days");
                $fromTs = mktime(0, 0, 0, date("m", $time), date("d", $time), date("Y", $time));
                $allitems0 = $this->get_cat_items($catid, 0, 0, true);

                if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                    $skipLen = 1 + strlen($kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($category['name']).'-c'.$category['id'].'/'.$kernel->pub_pretty_url($item['name']).'-i');
                else
                    $skipLen = 1 + strlen("/".$kernel->pub_page_current_get().".html?".$this->frontend_param_item_id_name."=");

                $cond = "`tstc`>=".$fromTs." AND (".implode(" OR ", $statConds).") GROUP BY `itemid` ORDER BY count DESC";
                $fields = "COUNT(uri) AS count,SUBSTR(`uri`,".$skipLen.") AS itemid";

                $fitems = $kernel->db_get_list_simple("_stat_uri", $cond, $fields);
                $farray = array();

                $pos = 0;
                foreach ($fitems as $fitem)
                {
                    $farray[$fitem['itemid']] = $pos;
                    $pos++;
                }
                $allitems = array();
                $noStatItems = array();
                foreach ($allitems0 as $aitem)
                {
                    if (isset($farray[$aitem['id']]))
                        $allitems[$farray[$aitem['id']]] = $aitem;
                    else
                        $noStatItems[] = $aitem;
                }
                ksort($allitems, SORT_NUMERIC);

                $allitems = array_merge($allitems, $noStatItems);
                if ($limit > 0)
                    $items = array_slice($allitems, $offset, $limit);
                else
                    $items = $allitems;

            }
            else
                $items = $this->get_cat_items($catid, $offset, $limit, true);
        }

        $count = count($items);
        if ($count == 0)
        {
            if ($show_cats_if_empty_items) {
                return $this->pub_catalog_show_cats($cats_header, $cats_tpl, $catid);
            }
            else
            {
                if ($custom_template)
                {
                    $tpl = CatalogCommons::get_templates_user_prefix().$custom_template;
                    $this->set_templates($kernel->pub_template_parse($tpl));
                    $content = $this->get_template_block('list_null');
                }
                else
                {
                    if (!empty($multi_group_tpl))
                    {
                        //приоритенне категория
                        if (!empty($category['tpl_items']))
                        {
                            $tpl = CatalogCommons::get_templates_user_prefix() . $category['tpl_items'];
                            $this->set_templates($kernel->pub_template_parse($tpl));
                            $content = $this->get_template_block('list_null');
                        }
                        else
                        {
                            $this->set_templates($kernel->pub_template_parse($multi_group_tpl));
                            $content = $this->get_template_block('list_null');
                        }
                    }
                    else
                        $content = $kernel->priv_page_textlabels_replace("[#catalog_show_items_list_no_items#]");
                }
                $content = $this->cats_props_out($category['id'], $content);
                return $content;
            }
        }

        $itemids = array();
        $groupid = 0;
        //проверим, принадлежат ли все товары к одной товарной группе
        //и сохраним id-шники
        $is_single_group = true;
        if ($count > 0)
        {
            $groupid = $items[0]['group_id'];
            foreach ($items as $item)
            {
                $itemids[] = $item['ext_id'];
                if ($groupid != $item['group_id'])
                {
                    $is_single_group = false;
                    break;
                }
            }
        }
        $group = false;
        if ($is_single_group)
        {
            $group = CatalogCommons::get_group($groupid);
            if ($custom_template)
                $tpl = CatalogCommons::get_templates_user_prefix().$custom_template;
            else
            {
                if(empty($group['template_items_list']))
                {
                    return $kernel->priv_page_textlabels_replace("[#catalog_no_group_template_list#]");
                }
                else
                {
                    //приоритенне категория
                    if (!empty($category['tpl_items']))
                    {
                        $tpl = CatalogCommons::get_templates_user_prefix() . $category['tpl_items'];
                    }
                    else
                    {
                        $tpl = CatalogCommons::get_templates_user_prefix() . $group['template_items_list'];
                    }
                }

            }
            Log::debug("Каталог. Шаблон списка: ".$tpl,"catalog_itemList_tpl");
            $this->set_templates($kernel->pub_template_parse($tpl));

            if ($count == 0)
                return $this->get_template_block('list_null');

            $items2 = $this->get_group_items($group['name_db'], $itemids);
            $newitems = array();
            foreach ($items as $item)
            {
                $tmp = $item + $items2[$item['ext_id']];
                $tmp['id'] = $item['id'];
                $newitems[] = $tmp;
            }
            $items = $newitems;
            $props = CatalogCommons::get_props($groupid, true);
        }
        else
        {
            if ($custom_template)
            { //experimental
                $this->set_templates($kernel->pub_template_parse(CatalogCommons::get_templates_user_prefix().$custom_template));
                $props = CatalogCommons::get_common_props($kernel->pub_module_id_get(), false);
            }
            else
            {
                if (!empty($multi_group_tpl))
                {
                    //приоритенне категория
                    if (!empty($category['tpl_items']))
                    {
                        $tpl = CatalogCommons::get_templates_user_prefix() . $category['tpl_items'];
                    }
                    else
                    {
                        $tpl = $multi_group_tpl;
                    }

                    $props = CatalogCommons::get_common_props($kernel->pub_module_id_get(), false);
                }
                else
                {

                    $tpl = CatalogCommons::get_templates_user_prefix() . $category['tpl_items'];
                    $props = CatalogCommons::get_common_props($kernel->pub_module_id_get(), false);

                    //	return "К сожалению, в этой версии каталога не предусмотрен вывод одним списком товаров, принадлежащих к разным товарным группам.";
                }
                $this->set_templates($kernel->pub_template_parse($tpl));
            }
        }

        //Получаем свойства, к этой группе.
        //при этом, надо пройтись по свойствам и если там есть
        //картинки, то нужно продублировать их свойствами большого
        //и маленького изображения
        for ($i = 0; $i < count($props); $i++) {
            if (isset($props[$i]['add_param']))
                $props[$i]['add_param'] = unserialize($props[$i]['add_param']);
        }

        $targetPage = $kernel->pub_page_current_get();

        if ($is_single_group)
            $groups = array($group['id'] => $group);
        else
            $groups = CatalogCommons::get_groups();

        $basket_page = $kernel->pub_modul_properties_get('catalog_property_basket_page', $kernel->pub_module_id_get());
        if(empty($basket_page['value']))
            $basket_page = 'basket';
        else
            $basket_page = $basket_page['value'];

        $compare_page = $kernel->pub_modul_properties_get('catalog_property_compare_page', $kernel->pub_module_id_get());
        if(empty($compare_page['value']))
            $compare_page = 'compare';
        else
            $compare_page = $compare_page['value'];

        $wishlist_page = $kernel->pub_modul_properties_get('catalog_property_wishlist_page', $kernel->pub_module_id_get());
        if(empty($wishlist_page['value']))
            $wishlist_page = 'wishlist';
        else
            $wishlist_page = $wishlist_page['value'];

        // получим список желаний
        $userid = intval($kernel->pub_user_is_registred());
        $wishlist = $this->get_wishlist_items_by_userid($userid);

        //Сформируем сначала строки с товарами
        $rows = '';
        $curr = 1;
        foreach ($items as $item)
        {
            if ($curr % 2 == 0) //строка - чётная
                $odd_even = "even";
            else
                $odd_even = "odd";

            //Взяли блок строчки
            $block = $this->get_template_block('row_'.$odd_even);
            if (empty($block))
                $block = $this->get_template_block('row');

            $block = str_replace("%odd_even%", $odd_even, $block);
            //Теперь ищем переменные, свойств и заменяем их
            $block = $this->process_item_props_out($item, $props, $block, $groups[$item['group_id']]);

            // Ссылка на текущий товар
            if (defined("USE_PRETTY_URL") && USE_PRETTY_URL) {
                if(isset($category['name']) && isset($category['id']))
                    $block = str_replace("%link%", '/'.$targetPage.'/'.$kernel->pub_pretty_url($category['name']).'-c'.$category['id'].'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $block);
                else
                    $block = str_replace("%link%", '/'.$targetPage.'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $block);
            } else {
                $block = str_replace("%link%", $targetPage.'.html?'.$this->frontend_param_item_id_name.'='.$item['id'], $block);
            }

            // Ссылка для добавления в товара в корзину
            if (defined("USE_PRETTY_URL") && USE_PRETTY_URL) {
                if(isset($category['name']) && isset($category['id']))
                    $block = str_replace("%basket_link%", '/'.$basket_page.'/?catalog_basket_additemid='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($category['name']).'-c'.$category['id'].'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $block);
                else
                    $block = str_replace("%basket_link%", '/'.$basket_page.'/?catalog_basket_additemid='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $block);
            } else {
                $block = str_replace("%basket_link%", $basket_page.'.html?catalog_basket_additemid='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'.html?'.$this->frontend_param_item_id_name.'='.$item['id'], $block);
            }

            // Ссылка для добавления в список сравнения товаров
            if (defined("USE_PRETTY_URL") && USE_PRETTY_URL) {
                if(isset($category['name']) && isset($category['id']))
                    $block = str_replace("%compare_link%", '/'.$compare_page.'/?add2compare='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($category['name']).'-c'.$category['id'].'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $block);
                else
                    $block = str_replace("%compare_link%", '/'.$compare_page.'/?add2compare='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $block);
            } else {
                $block = str_replace("%compare_link%", $compare_page.'.html?add2compare='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'.html?'.$this->frontend_param_item_id_name.'='.$item['id'], $block);
            }

            // Ссылка для добавления в список желаний
            $inwishlist = false;
            if (is_array($wishlist)) {
                foreach ($wishlist as $list) {
                    if($list['itemid'] == intval($item['id']))
                        $inwishlist = true;
                }
            }

            if($userid > 0) { // если пользователь зарегистрирован и авторизован
                if($inwishlist) { // значит есть в списке желаний
                    $wishlist_link = $this->get_template_block('wishlist_link_active');
                    $wishlist_param = 'remove4wishlist';
                } else {
                    $wishlist_link = $this->get_template_block('wishlist_link');
                    $wishlist_param = 'add2wishlist';
                }
            } else {
                $wishlist_link = $this->get_template_block('wishlist_link_disabled');
                $wishlist_param = '';
            }

            if($wishlist_link) { // значит етсь особый блок для вывода ссылки добавления в список желаний
                if($userid > 0) {
                    if (defined("USE_PRETTY_URL") && USE_PRETTY_URL) {
                        if(isset($category['name']) && isset($category['id']))
                            $wishlist_link = str_replace("%link%", '/'.$wishlist_page.'/?'.$wishlist_param.'='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($category['name']).'-c'.$category['id'].'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $wishlist_link);
                        else
                            $wishlist_link = str_replace("%link%", '/'.$wishlist_page.'/?'.$wishlist_param.'='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $wishlist_link);
                    } else {
                        $wishlist_link = str_replace("%link%", $wishlist_page.'.html?'.$wishlist_param.'='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'.html?'.$this->frontend_param_item_id_name.'='.$item['id'], $wishlist_link);
                    }
                } else {
                    $wishlist_link = str_replace("%link%", '', $wishlist_link);
                }

                $block = str_replace("%wishlist_link%", $wishlist_link, $block);
            }
            else
            { // оставим старый формат для обратной совместимости
                if($userid > 0) {
                    if (defined("USE_PRETTY_URL") && USE_PRETTY_URL) {
                        if(isset($category['name']) && isset($category['id']))
                            $block = str_replace("%wishlist_link%", '/'.$wishlist_page.'/?'.$wishlist_param.'='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($category['name']).'-c'.$category['id'].'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $block);
                        else
                            $block = str_replace("%wishlist_link%", '/'.$wishlist_page.'/?'.$wishlist_param.'='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $block);
                    } else {
                        $block = str_replace("%wishlist_link%", $wishlist_page.'.html?'.$wishlist_param.'='.$item['id'].'&redir2=/'.$kernel->pub_page_current_get().'.html?'.$this->frontend_param_item_id_name.'='.$item['id'], $block);
                    }
                } else {
                    $block = str_replace("%wishlist_link%", '', $block);
                }
            }

            $rows .= $block;
            $curr++;
        }
        $moduleid = $kernel->pub_module_id_get();
        $session_name = $moduleid.'_compared_items';

        if (isset($_SESSION[$session_name]) && $_SESSION[$session_name])
        {
            $items2compare = $_SESSION[$session_name];
        }
        else
        {
            $items2compare = array();
        }

        $content = $this->get_template_block('list');
        $content = str_replace("%row%", $rows, $content);
        $content = str_replace("%total_in_cat%", $total, $content);
        $content = str_replace('%total_compared%', count($items2compare), $content);

        if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
            $purl = '/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($category['name']).'-c'.$category['id'].'/page-';
        else
            $purl = $kernel->pub_page_current_get().'.html?'.$this->frontend_param_cat_id_name.'='.$catid.'&'.$this->frontend_param_offset_name.'=';

        $content = str_replace('%pages%', $this->build_pages_nav($total, $offset, $limit, $purl, $pages_in_block), $content);

        $content = $this->process_variables_out($content);
        $content = $this->replace_current_page_url($content);

        if(!$is_direct) {
            $content = $this->cats_props_out($category['id'], $content);
            $content = $this->process_filters_in_template($content);
            $content = $this->clear_left_labels($content);
        }

        return $content;
    }


    /**
     * Возвращает дорогу к категории от корня
     * @param integer $id id-шник категории
     * @param boolean $skip_root пропускать рут-категорию
     * @param array $cached_cats массив всех категорий
     * @return array
     */
    private function get_way2cat($id, $skip_root = false, $cached_cats = null)
    {

        global $kernel;
        if ($id == 0)
            return array();
        if (is_null($cached_cats))
            $cached_cats = CatalogCommons::get_all_categories($kernel->pub_module_id_get());
        $cats = array();
        $cid = $id;
        do
        {
            //$cat = CatalogCommons::get_category($cid);
            if (!isset($cached_cats[$cid]))
                break;
            $cat = $cached_cats[$cid];
            $cats[] = $cat;
            $cid = $cat['parent_id'];
        }
        while ($cid != 0);

        $depth = count($cats);
        $res = array();
        for ($i = 0; $i < count($cats); $i++)
        {
            $elem = $cats[$i];
            $elem['depth'] = $depth--;
            $res[] = $elem;
        }
        if (!$skip_root)
        {
            $elem = array('depth' => 0, 'id' => 0, 'name' => 'root');
            $res[] = $elem;
        }
        return array_reverse($res);
    }

    /**
     * Публичный метод для отображения дерева категорий
     *
     * @param string  $list_header   заголовок списка категорий
     * @param string  $template   имя файла шаблона
     * @param integer  $fromcat    id-шник категории, с которой строим
     * @param integer $fromlevel  Уровень начала построения
     * @param integer $openlevels Кол-во раскрываемых уровней меню
     * @param integer $showlevels макс. кол-во выводимых уровней меню
     * @param string $items_pagename страница товара
     * @param mixed $cur_fromcat текущая категория - начало построения
     * @param bolean $need_add_way использовать для построения дороги
     * @return string
     * @access public
     */
    public function pub_catalog_show_cats($list_header = '', $template, $fromcat = 0, $fromlevel = 1, $openlevels = 1, $showlevels = 1, $items_pagename = '', $cur_fromcat = false, $need_add_way=true)
    {
        global $kernel;

        if (empty($template) || !file_exists($template))
            return "template not found.";

        $items_pagename = trim($items_pagename);
        if (mb_strlen($items_pagename) == 0)
            $items_pagename = $kernel->pub_page_current_get();

        if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
            $items_pagename = '/'.$items_pagename;
        else
            $items_pagename .= '.html';

        $parsed_template = $kernel->pub_template_parse($template);
        $this->set_templates($parsed_template);

        $fromcat = intval($fromcat);
        if ($cur_fromcat)
            $fromcat = $this->get_current_catid(true);

        $curr_cid = $this->get_current_catid(true);
        $cway = array();

        if ($curr_cid == 0)
        {
            $itemid = $kernel->pub_httpget_get($this->frontend_param_item_id_name);
            if (!empty($itemid))
            {
                $cway = $this->get_max_catway2item(intval($itemid));
                if (count($cway) > 0)
                {
                    $curr_cid = $cway[count($cway) - 1]['id'];
                    array_unshift($cway, array('depth' => 0, 'id' => 0, 'name' => 'root'));
                }
            }
            if ($curr_cid == 0)
            {
                $need_add_way = false;
                $curr_cid = $this->get_default_catid();
            }
        }

        if (empty($cway))
            $cway = $this->get_way2cat($curr_cid);
        if (count($cway) > 0)
            $curr_cat = $cway[count($cway) - 1];
        else
            $curr_cat = false;

        if ($need_add_way)
            $this->add_categories2waysite($cway);

        // позиция категории начала построения в пути
        $fromcat_depth_in_way = intval($this->is_cat_in_array($fromcat, $cway));

        if ($fromcat_depth_in_way >= 0) { // Категория начала построения присутствует в пути

            if (count($cway) < $fromcat_depth_in_way + $fromlevel)
                return $this->get_template_block('depth_in_way');

            // пользователь не дошёл до нужной глубины - меню не нужно
            $catid = $cway[$fromcat_depth_in_way + $fromlevel - 1]['id'];
            $cats = $this->get_child_categories2($catid, 0, array(), $showlevels, $cway, $openlevels);
        } else { // Категория начала построения НЕ присутствует в пути

            if ($fromlevel != 1)
                return $this->get_template_block('depth_out_way');

            $catid = $fromcat;
            $cats = $this->get_child_categories($catid, 0, array(), $showlevels);
        }

        $content = '';
        $prev_depth = -1;
        $opened_depths = array();
        $cats_props = CatalogCommons::get_cats_props();

        foreach ($cats as $cat)
        {
            if($cat["_hide_from_site"] == '1')
                continue;

            if ($prev_depth != $cat['depth']) {
                if ($prev_depth > $cat['depth']) {
                    for ($pd = $prev_depth; $pd > $cat['depth']; $pd--) {
                        $content .= $this->get_template_block_with_depth('end', $pd);
                    }

                    if (array_key_exists($prev_depth, $opened_depths))
                        unset($opened_depths[$prev_depth]);

                    //если ещё нет открытой категории с этим уровнем, откроем
                    if (!array_key_exists($cat['depth'], $opened_depths))
                        $content .= $this->get_template_block_with_depth('begin', $cat['depth']);

                }  else {// $prev_depth < $cat['depth']
                    $content .= $this->get_template_block_with_depth('begin', $cat['depth']);
                }

                $opened_depths[$cat['depth']] = true;
                $first = true;
            } else {
                $first = false;
            }

            if ($first)
                $cblock = '';
            else
                $cblock = $this->get_template_block_with_depth('delimiter', $cat['depth']);

            if ($curr_cid == $cat['id']) { //это текущая категория
                $cblock .= $this->get_template_block_with_depth('activelink', $cat['depth']);
            } else if ($this->is_cat_in_array($cat['id'], $cway) >= 0) { //эта категория присутствует в пути от корня
                $cblock .= $this->get_template_block_with_depth('passiveactive', $cat['depth']);
            } else {
                $cblock .= $this->get_template_block_with_depth('link', $cat['depth']);
            }

            foreach ($cats_props as $cat_prop)
            {
                $prop_value = '';
                if (isset($cat[$cat_prop['name_db']]))
                    $prop_value = $cat[$cat_prop['name_db']];

                if (empty($prop_value)) {
                    $cblock = str_replace("%".$cat_prop['name_db']."%", $this->get_template_block($cat_prop['name_db'].'_null'), $cblock);
                } else {

                    $pblock = $this->get_template_block($cat_prop['name_db']);

                    if ($cat_prop['type'] == 'pict')
                    {
                        $size = @getimagesize($prop_value);
                        if ($size === false)
                            $size = array(0 => "", 1 => "");

                        $cblock = str_replace('%'.$cat_prop['name_db'].'_width%', $size[0], $cblock);
                        $pblock = str_replace('%'.$cat_prop['name_db'].'_width%', $size[0], $pblock);

                        $cblock = str_replace('%'.$cat_prop['name_db'].'_height%', $size[1], $cblock);
                        $pblock = str_replace('%'.$cat_prop['name_db'].'_height%', $size[1], $pblock);

                        $cblock = str_replace('%'.$cat_prop['name_db'].'_value%', '/'.$prop_value, $cblock);
                        $pblock = str_replace('%'.$cat_prop['name_db'].'_value%', '/'.$prop_value, $pblock);

                        //кроме этого надо добавить переменные для малого и исходного изображения
                        $path_parts = pathinfo($prop_value);
                        $path_small = $path_parts['dirname'].'/tn/'.$path_parts['basename'];
                        $path_source = $path_parts['dirname'].'/source/'.$path_parts['basename'];

                        $cblock = str_replace('%'.$cat_prop['name_db'].'_small%', '/'.$path_small, $cblock);
                        $pblock = str_replace('%'.$cat_prop['name_db'].'_small%', '/'.$path_small, $pblock);

                        $cblock = str_replace('%'.$cat_prop['name_db'].'_source%', '/'.$path_source, $cblock);
                        $pblock = str_replace('%'.$cat_prop['name_db'].'_source%', '/'.$path_source, $pblock);
                    }

                    $cblock = str_replace("%".$cat_prop['name_db']."%", $pblock, $cblock);
                    $cblock = str_replace("%".$cat_prop['name_db']."_value%", $prop_value, $cblock);

                }
                $cblock = str_replace('%'.$cat_prop['name_db'].'_name%', $cat_prop['name_full'], $cblock);

            }

            //проверим, осталось ли в шаблоне чтото вида %aaaaa_value%
            //пример использование - alt в img
            $matches = false;
            if (preg_match_all("/\\%([a-z0-9_-]+)_value\\%/iU", $cblock, $matches))
            {
                foreach ($matches[1] as $prop)
                {
                    if (isset($cat[$prop]))
                        $cblock = str_ireplace("%".$prop."_value%", $cat[$prop], $cblock);
                }
            }

            if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                $cblock = str_replace('%link%', $items_pagename.'/'.$kernel->pub_pretty_url($cat['name']).'-c'.$cat['id'].'/', $cblock);
            else
                $cblock = str_replace('%link%', $items_pagename.'?'.$this->frontend_param_cat_id_name.'='.$cat['id'], $cblock);

            $cblock = str_replace('%id%', $cat['id'], $cblock);

            // Список товаров на основе списка категорий (experimental)
            $match = false;
            if (preg_match("/\%show_items_list\[(.+\.html)\,\s(.+)\]\%/U", $cblock, $match))
            {
                $items_tpl = $match[1];
                $inner_filter = $match[2];
                $items_block = $this->pub_catalog_show_inner_selection_results($inner_filter, $items_tpl, array(), $cat['id'], true, false);
                $this->set_templates($parsed_template);
                $cblock = str_replace($match[0], $items_block, $cblock);
                Log::debug("Каталог: список категорий. Выборка по внутреннему фильтру  `".$inner_filter."` с использованием шаблона `".$items_tpl."``","pub_catalog_show_cats");
            }
            else if (preg_match("/\%show_items_list\[(.+\.html)\]\%/U", $cblock, $match))
            {
                $items_tpl = $match[1];
                $this->set_templates($items_tpl);
                $items_block = $this->pub_catalog_show_items(0, true, "", "", "", 15, $cat['id'], $items_tpl, false);
                $this->set_templates($parsed_template);
                $cblock = str_replace($match[0], $items_block, $cblock);
                Log::debug("Каталог: список категорий. Вывод товаров по шаблону `".$items_tpl."`". $items_tpl,"pub_catalog_show_cats");
            } else if (preg_match("/\%show_items_list\[(.+)\]\%/U", $cblock, $match))
            {
                $inner_filter = $match[1];
                $items_block = $this->pub_catalog_show_inner_selection_results($inner_filter, false, array(), $cat['id'], true, false);
                $this->set_templates($parsed_template);
                $cblock = str_replace($match[0], $items_block, $cblock);
                Log::debug("Каталог: список категорий. Выборка по внутреннему фильтру  `".$inner_filter."` без использования шаблона","pub_catalog_show_cats");
            }

            $total = $this->get_cat_items_count($cat['id'], true);
            $cblock = str_replace('%cat_items_count%', $total, $cblock);

            if($cat["_hide_from_site"] !== 1) {
                $content .= $cblock;
            }

            $prev_depth = $cat['depth'];
        }
        arsort($opened_depths);
        //закроем все открытые "глубины"
        foreach ($opened_depths as $ok => $ov)
        {
            $content .= $this->get_template_block_with_depth('end', $ok);
        }

        if($curr_cat && $first = true && $prev_depth == -1)
            $content = $this->get_template_block('no_data');

        if ($curr_cat)
        {
            if (isset($curr_cat['name']))
                $content = str_replace("%curr_category_name%", $curr_cat['name'], $content);

            $content = $this->cats_props_out($curr_cat['id'], $content);
        }

        $content = str_replace("%list_header%", $list_header, $content);
        $content = $this->process_variables_out($content);
        $content = $this->clear_left_labels($content);
        return $content;
    }


    /**
     * Убирает нули справа после запятой в дробных числах
     *
     * @param float $num
     * @return string
     */
    function cleanup_number($num)
    {
        return preg_replace("/\\.([0]+)$/", "", $num);
    }

    /**
     * Возвращает id-шник текущей категории во front-end
     * Сохраняет выбранную категорию в сессии
     *
     * @param boolean  $http_only  "искать" только в хттт-параметре?
     * @return integer id-шник текущей категории
     */
    private function get_current_catid($http_only = false)
    {
        global $kernel;
        $catid = intval($kernel->pub_httpget_get($this->frontend_param_cat_id_name));
        if ($http_only)
            return ($catid < 1) ? 0 : $catid;

        if ($catid > 0)
        {
            $kernel->pub_session_set("curr_cat_id", $catid);
            return $catid;
        }
        elseif (!is_null($catid = $kernel->pub_session_get("curr_cat_id")))
            return $catid;
        else
            return 0;
        /*
        elseif (($catid = $this->get_default_catid()) > 0)
            return $catid;
        else
            return $this->get_random_catid();*/
    }

    /**
     * Возвращает id-шник категории "по-умолчанию", если такая есть
     *
     * @return integer id-шник категории
     */
    private function get_default_catid()
    {

        global $kernel;
        $ret = 0;
        if ($row = $kernel->db_get_record_simple('_catalog_'.$kernel->pub_module_id_get().'_cats','`is_default`=1','id'))
            $ret = $row['id'];

        return $ret;
    }

    /**
     * Добавляет товарную группу в БД.
     * создаёт запись в _catalog_item_groups и новую таблицу
     *
     * @param string $name имя товарной группы
     * @param string $namedb БД-имя товарной группы
     * @return integer ID добавленной группы
     */
    private function add_group($name, $namedb)
    {
        global $kernel;

        if (mb_strlen($name) == 0)
            return 0;

        if (mb_strlen($namedb) == 0)
            $namedb = $name;

        $namedb = strtolower($this->translate_string2db($namedb));
        $list_items = $kernel->pub_httppost_get('list_items');
        $one_items = $kernel->pub_httppost_get('one_items');

        if ($namedb == 'items') //это название зарезервировано, т.к. используется как алиас в выборках
            $namedb = 'gitems';

        $n = 2;
        $namedb0 = $namedb;
        while ($this->is_group_exists($namedb))
            $namedb = $namedb0.$n++;

        // Категории по-умолчанию
        $ccbs = isset($_POST['ccb']) ? $_POST['ccb'] : array();
        $catids = array();
        foreach ($ccbs as $catid => $value)
        {
            if ($value == 1)
                $catids[] = $catid;
        }
        $defcatids = implode(",", $catids);

        // Категории отображения фильтров
        $fcbs = isset($_POST['fcb']) ? $_POST['fcb'] : array();
        $catids = array();
        foreach ($fcbs as $catid => $value)
        {
            if ($value == 1)
                $catids[] = $catid;
        }
        $filtercats = implode(",", $catids);

        $query = 'INSERT INTO `'.$kernel->pub_prefix_get().'_catalog_item_groups`'.
            ' (`module_id`,`name_db`,`name_full`, `template_items_list`, `template_items_one`, `defcatids`, `filtercats`) '.
            'VALUES ("'.$kernel->pub_module_id_get().'","'.$namedb.'","'.$kernel->pub_str_prepare_set($name).'","'.$list_items.'","'.$one_items.'","'.$defcatids.'","'.$filtercats.'")';
        $kernel->runSQL($query);

        $id = $kernel->db_insert_id();

        //по-умолчанию добавляем все common-свойства как видимые для новой тов. группы
        $cprops = CatalogCommons::get_common_props($kernel->pub_module_id_get(), false);
        foreach ($cprops as $cprop)
        {
            $this->add_group_visible_prop($kernel->pub_module_id_get(), $id, $cprop['name_db']);
        }

        $query = 'CREATE TABLE `'.$kernel->pub_prefix_get().'_catalog_items_'.$kernel->pub_module_id_get().'_'.$namedb.'` ( '
            .' `id` int(10) unsigned NOT NULL auto_increment, '
            .' PRIMARY KEY  (`id`) '
            .' ) ENGINE=MyISAM DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci AUTO_INCREMENT=1';
        $kernel->runSQL($query);
        return $id;
    }

    /**
     * Сохраняет товарную группу в БД.
     *
     * @param integer $id id товарной группы
     * @param string $name имя товарной группы
     * @param string $namedb БД-имя группы
     * @return integer id
     */
    private function save_group($id, $name, $namedb)
    {
        if (mb_strlen($name) == 0)
            return 0;
        if (mb_strlen($namedb) == 0)
            $namedb = $name;
        global $kernel;
        if ($id < 1)
            return 0;
        $group = CatalogCommons::get_group($id);
        if (!$group)
            return 0;

        //Кроме этого, получим значение выбранных для групп шаблонов
        $list_items = $kernel->pub_httppost_get('list_items');
        $one_items = $kernel->pub_httppost_get('one_items');

        $namedb = $this->translate_string2db($namedb);
        $namedb = strtolower($namedb);

        // Категории по-умолчанию
        $ccbs = isset($_POST['ccb']) ? $_POST['ccb'] : array();
        $catids = array();
        foreach ($ccbs as $catid => $value)
        {
            if ($value == 1)
                $catids[] = $catid;
        }
        $defcatids = implode(",", $catids);

        // Категории отображения фильтров
        $fcbs = isset($_POST['fcb']) ? $_POST['fcb'] : array();
        $catids = array();
        foreach ($fcbs as $catid => $value)
        {
            if ($value == 1)
                $catids[] = $catid;
        }
        $filtercats = implode(",", $catids);

        if ($namedb != $group['name_db'])
        { //изменилось БД-имя товарной группы
            $n = 1;
            while ($this->is_group_exists($namedb))
                $namedb .= $n++;
            if ($namedb == 'items') //это название зарезервировано, т.к. используется как алиас в выборках
                $namedb = 'gitems';

            $query = 'ALTER TABLE `'.$kernel->pub_prefix_get().'_catalog_items_'.$kernel->pub_module_id_get().'_'.$group['name_db'].'` '.
                'RENAME `'.$kernel->pub_prefix_get().'_catalog_items_'.$kernel->pub_module_id_get().'_'.$namedb.'` ';

            $kernel->runSQL($query);
        }
        $query = 'UPDATE `'.$kernel->pub_prefix_get().'_catalog_item_groups` '.
            'SET `name_db`="'.$kernel->pub_str_prepare_set($namedb).'", '.
            '`name_full`="'.$kernel->pub_str_prepare_set($name).'", '.
            '`defcatids`="'.$defcatids.'", '.
            '`filtercats`="'.$filtercats.'", '.
            '`template_items_list`="'.$list_items.'", '.
            '`template_items_one`="'.$kernel->pub_str_prepare_set($one_items).'" '.
            'WHERE `id`='.$id;

        $kernel->runSQL($query);
        return $id;
    }


    /**
     * Конвертирует строку создания ENUM или SET-поля в массив
     *
     * @param string $str строка типа ENUM - "enum('знач1','знач2','знач3')" или SET - "set('1','2','3')"
     * @param boolean $needDefault добавлять первое дефолтовое значение?
     * @return array массив со значениями enum
     */
    private function get_enum_set_prop_values($str, $needDefault = true)
    {

        $str = preg_replace('~^(enum|set)~', '', $str);
        $elems = explode("','", mb_substr($str, 2, -2));
        $res = array();
        //Добавим сюда сразу 0-вое значение
        //при выводе оно будет пропускаться
        //и при сохранении снова же добавляться.
        if ($needDefault)
            $res[0] = 'Не выбран';

        foreach ($elems as $el)
            $res[] = str_replace("''", "'", stripslashes($el));
        $res = array_unique($res);
        return $res;
    }

    /**
     * Пересоздаёт шаблоны для редактирования ВСЕХ товарных групп
     * @param $force boolean
     * @return void
     */
    private function regenerate_all_groups_tpls($force = false)
    {
        //$groups = CatalogCommons::get_groups();
        //foreach ($groups as $group)
        //    $this->regenerate_group_tpls($group['id'], $force);
    }

    /**
     * Генерируем шаблон для редактирования товара
     *
     * Шаблон создаётся для заданной товарной группы. Потом он может быть
     * отредактированны админом так, как ему надо
     * @param integer $groupid
     */
    /*
    function regenerate_admin_template($groupid)
    {
        global $kernel;

        if ($groupid == 0)
            return true;

        $group = $this->get_group($groupid);
        $props = CatalogCommons::get_props($groupid, true);

        //Произведём первичную сортировку массива со свойствами, что бы шаблон был
        //оптимизирован изначально. В дальнейшем пользователь его самостоятельно поменяет
        //Получим свойства, которые
        $sort_def =  array();
        $sort_def['string'] = '01';
        $sort_def['enum']   = '02';
        $sort_def['number'] = '03';
        $sort_def['pict']   = '04';
        $sort_def['text']   = '05';
        $sort_def['html']   = '06';
        $sort_def['file']   = '07';


        $sort = array();
        //Сформируем массив с индексами $props по которому потом и будем
        //строить результ
        foreach ($props as $key => $val)
            $sort[$sort_def[$val['type']].'_'.$key] = $key;

        ksort($sort);

        //Начинаем проходить по массиву возможных свойств, проверять
        //подходит ли нам свойство и начинаем формировать шаблон
        $content = '';
        $this->set_templates($kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'blank_adminedit_item_tpl.html'));
        $lines = array();
        $num = 0;
        foreach ($sort as $id_in_prop)
        {
            $val = $props[$id_in_prop];
            $line = $this->get_template_block('add_prop_'.$val['type']);
            $line = str_replace('%prop_name_db%'  , $props['name_db']  , $line);
            $line = str_replace('%prop_name_full%', $props['name_full'], $line);
            //$line = str_replace('%prop_value%', ,$line);

            $line = str_replace('%class%', $kernel->pub_table_tr_class($num),$line);

            $num++;
        }

    }
    */

    /**
     * Формирует часть шаблона со свойствами товара
     *
     * Используется при генерации шаблонво списка товаров и карточки товаров
     * @param array $template распаршенный шаблон
     * @param array $props массив со свойствами
     * @param boolean $include_html формировать для свойства типа HTML
     * @return array
     */
    private function regenerate_group_tpls_only_prop($template, $props, $include_html = false)
    {
        $list_prop_addon = "\n\n";

        if (isset($template['cat_way_block']) && isset($template['cat_way_separator']) &&
            isset($template['cat_way_active']) && isset($template['cat_way_passive'])
        )
        {
            $list_prop_addon .= "<!-- @cat_way_block -->\n".$template['cat_way_block']."\n\n";
            $list_prop_addon .= "<!-- @cat_way_separator -->\n".$template['cat_way_separator']."\n\n";
            $list_prop_addon .= "<!-- @cat_way_active -->\n".$template['cat_way_active']."\n\n";
            $list_prop_addon .= "<!-- @cat_way_passive -->\n".$template['cat_way_passive']."\n\n";
        }
        $prop_names_block = '';
        foreach ($props as $prop)
        {
            if ($prop['type'] == 'html' && !$include_html)
                continue;
            if (isset($template[$prop['name_db']]))
                continue;
            $prop_names_block .= "%".$prop['name_db']."%\n";
        }

        $only_names = $prop_names_block;

        $only_values = '';
        foreach ($props as $prop)
        {
            if ($prop['type'] == 'html' && !$include_html)
                continue;

            if (isset($this->templates[$prop['name_db']])) //чтобы иметь возможность делать особые метки для некоторых полей
                $field = $this->templates[$prop['name_db']];
            else
                $field = $this->get_template_block('prop_'.$prop['type']);

            $field = trim($field);
            $list_prop_addon .= "<!-- @".$prop['name_db']."_null -->";
            $list_prop_addon .= $template[$prop['type'].'_null'];

            //Заменим в самом свойстве в строке эти переменные
            $field = str_replace('%prop_name_full%', $prop['name_full'], $field);
            $field = str_replace('%prop_value%', '%'.$prop['name_db'].'_value%', $field);
            $field = str_replace('%prop%', '%'.$prop['name_db'].'%', $field);
            $field = str_replace('%prop_name_db%', $prop['name_db'], $field);
            $only_values .= "<!-- @".$prop['name_db']." -->\n".$field."\n";

            //И ещё заменим в доп свойствах, если они там есть
            $list_prop_addon = str_replace('%prop_name_full%', $prop['name_full'], $list_prop_addon);
            $list_prop_addon = str_replace('%prop_value%', '%'.$prop['name_db'].'_value%', $list_prop_addon);
        }
        return array('only_values' => $only_values, 'addon' => $list_prop_addon, 'only_names' => $only_names);
    }


    /**
     * Пересоздаёт шаблоны для редактирования товарной группы
     * и вывода всех полей товара во фронтэнд
     *
     * @param integer $groupid id-шник товарной группы
     * @param boolean $for_item_card Если тру, то шаблон будет создаваться для карточки тоавара а не для списка
     * @param boolean $force требуется ли принудительное пересоздание шаблонов, даже если они были изменены
     * @return boolean
     */
    private function regenerate_group_tpls($groupid, $for_item_card = false, $force = false)
    {
        global $kernel;
        if ($groupid == 0)
            return true;
        $group = CatalogCommons::get_group($groupid);
        //пересоздаём шаблон для фронтэнда
        if ($for_item_card)
            $viewfilename = CatalogCommons::get_templates_user_prefix().$kernel->pub_module_id_get().'_'.$group['name_db'].'_card.html';
        else
            $viewfilename = CatalogCommons::get_templates_user_prefix().$kernel->pub_module_id_get().'_'.$group['name_db'].'_list.html';

        $props = CatalogCommons::get_props($groupid, true);
        $visible_props = array_keys($this->get_group_visible_props($groupid));
        foreach ($props as $k => $prop)
        {
            if ($prop['group_id'] == 0 && !in_array($prop['name_db'], $visible_props))
                unset($props[$k]);
        }
        //Пока уберём это проверку, так как она должна будет делаться в форме, и подтвержаться там
        //if ($force || !CatalogCommons::isTemplateChanged($viewfilename, $group['front_tpl_md5']))
        //{//только если шаблон не был изменён или пользователь подтвердил

        if ($for_item_card)
            $arr_template = $kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'frontend_templates/blank_item_one.html');
        else
            $arr_template = $kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'frontend_templates/blank_items_list.html');

        $this->set_templates($arr_template);

        $viewfh = '';
        //Начнём конструировать таблицу шаблон для отображения списка товаров

        //блок информации по товару в списке
        //Вместе с ним сразу создаём шаблон для карточки товара
        $arr_prop = $this->regenerate_group_tpls_only_prop($arr_template, $props, $for_item_card);

        if ($for_item_card)
        {
            $viewfh .= "<!-- @item -->\n";
            $viewfh .= str_replace('%list_prop%', $arr_prop['only_names'], $this->get_template_block('item'));
        }
        else
        { //шаблон списка
            $viewfh .= "<!-- @list -->\n";
            $viewfh .= $this->get_template_block('list');
            $cats_props = CatalogCommons::get_cats_props();
            foreach ($cats_props as $cprop)
            {
                $tblock = $this->get_template_block('category_'.$cprop['name_db']);
                if (!is_null($tblock))
                    $viewfh .= "\n\n<!-- @category_".$cprop['name_db']." -->\n".$tblock."\n\n";
            }
        }

        //блок, если в списке нет элементов
        if (!$for_item_card)
        {
            $viewfh .= "\n\n\n<!-- @list_null -->\n";
            $viewfh .= $this->get_template_block('list_null');
        }


        //Теперь собственно добавим это в результирующий шаблон
        if (!$for_item_card)
        {
            $row_odd = $this->get_template_block('row_odd');
            $row_even = $this->get_template_block('row_even');
            if (!empty($row_odd) && !empty($row_even))
            {
                $viewfh .= "\n\n\n<!-- @row_odd -->\n";
                $viewfh .= str_replace('%list_prop%', $arr_prop['only_names'], $this->get_template_block('row_odd'));
                $viewfh .= "\n\n\n<!-- @row_even -->\n";
                $viewfh .= str_replace('%list_prop%', $arr_prop['only_names'], $this->get_template_block('row_even'));
            }
            else
            {
                $viewfh .= "\n\n\n<!-- @row -->\n";
                $viewfh .= str_replace('%list_prop%', $arr_prop['only_names'], $this->get_template_block('row'));
            }

        }

        $viewfh .= "\n".$arr_prop['only_values'];

        //Добавим доп блоки, которые были сформированы при обработке свойств
        $viewfh .= $arr_prop['addon'];

        //Теперь добавим блок, разделитель между товарами в списке
        //и всё это только для списка товаров
        if (!$for_item_card)
        {
            $viewfh .= "\n\n\n<!-- @row_delimeter -->\n";
            $viewfh .= $this->get_template_block('row_delimeter');


            //блок для вывода навигации по страницам
            $viewfh .= "\n\n\n<!-- @pages -->\n";
            $viewfh .= $this->get_template_block('pages');
            //...и всё для неё
            $viewfh .= "\n<!-- @page_first -->\n";
            $viewfh .= $this->get_template_block('page_first');
            $viewfh .= "\n<!-- @page_backward -->\n";
            $viewfh .= $this->get_template_block('page_backward');
            $viewfh .= "\n<!-- @page_backward_disabled -->\n";
            $viewfh .= $this->get_template_block('page_backward_disabled');
            $viewfh .= "\n<!-- @page_previous -->\n";
            $viewfh .= $this->get_template_block('page_previous');
            $viewfh .= "\n<!-- @page_previous_disabled -->\n";
            $viewfh .= $this->get_template_block('page_previous_disabled');
            $viewfh .= "\n<!-- @page_forward -->\n";
            $viewfh .= $this->get_template_block('page_forward');
            $viewfh .= "\n<!-- @page_forward_disabled -->\n";
            $viewfh .= $this->get_template_block('page_forward_disabled');
            $viewfh .= "\n<!-- @page_next -->\n";
            $viewfh .= $this->get_template_block('page_next');
            $viewfh .= "\n<!-- @page_next_disabled -->\n";
            $viewfh .= $this->get_template_block('page_next_disabled');
            $viewfh .= "\n<!-- @page_last -->\n";
            $viewfh .= $this->get_template_block('page_last');
            $viewfh .= "\n<!-- @page_active -->\n";
            $viewfh .= $this->get_template_block('page_active');
            $viewfh .= "\n<!-- @page_passive -->\n";
            $viewfh .= $this->get_template_block('page_passive');
            $viewfh .= "\n<!-- @page_delimeter -->\n";
            $viewfh .= $this->get_template_block('page_delimeter');
            $viewfh .= "\n<!-- @page_null -->\n";
            $viewfh .= $this->get_template_block('page_null');
        }
        //fwrite($viewfh, $this->get_template_block('footer'));

        //Теперь запишем эту информацию
        $kernel->pub_file_save($viewfilename, $viewfh);
        //fclose($viewfh);

        //$query = 'UPDATE `'.$kernel->pub_prefix_get().'_catalog_item_groups` SET `front_tpl_md5`="'.md5_file($viewfilename).'" WHERE `id`='.$groupid;
        //$kernel->runSQL($query);

        //Сразу же сделаем шаблон для карточки товара
        //}
        return true;
        //пересоздаём шаблон редактирования для админки
        //Это пока отключили
        /*
        $editfilename = CatalogCommons::get_templates_admin_prefix().$kernel->pub_module_id_get().'_'.$group['name_db'].'_edit_tpl.html';

        if ($force || !CatalogCommons::isTemplateChanged($editfilename, $group['back_tpl_md5']))
        {//только если файл не был изменён или пользователь подтвердил
            $this->set_templates($kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'blank_edit_tpl.html'));
            $editfh = fopen($editfilename, "w");
            if (!$editfh)
                return false;
            $props = CatalogCommons::get_props($groupid, true);
            $tinfo = $this->get_dbtable_info('_catalog_items_'.$kernel->pub_module_id_get().'_'.$group['name_db']);
            $tinfo = $tinfo + $this->get_dbtable_info('_catalog_'.$kernel->pub_module_id_get().'_items');
            $block = $this->get_template_block("form_header");
            $block = str_replace('%rows%', count($props)+1, $block);
            $cats = $this->get_child_categories(0);
            $cats_content = '';
            foreach ($cats as $cat)
            {
                $catline = $this->get_template_block("category_item");
                $catline = str_replace("%id%", $cat['id'], $catline);
                $catline = str_replace("%catname%", $cat['name'], $catline);
                $catline = str_replace("%shift%", str_repeat("&nbsp;",$cat['depth']), $catline);
                $cats_content .= $catline;
            }
            $block = str_replace("%categories%", $cats_content, $block);
            fwrite($editfh,"<!-- @form_header -->\n".$block);
            foreach ($props as $prop)
            {
                $field = '';
                switch ($prop['type'])
                {
                    case 'enum':
                        fwrite($editfh,"\n<!-- @prop_".$prop['name_db']." -->\n");
                        $field = $this->get_template_block('add_prop_enum');
                        $vals  = $this->get_enum_prop_values($tinfo[$prop['name_db']]['Type']);
                        $options = $this->get_template_block('prop_enum_value');
                        $options = str_replace('%enum_value%', '',$options);
                        $options = str_replace('%enum_name%', $kernel->pub_page_textlabel_replace('[#catalog_prop_need_select_label#]'),$options);
                        foreach ($vals as $val)
                        {
                            $option = $this->get_template_block('prop_enum_value');
                            $option = str_replace('%enum_value%', $val,$option);
                            $option = str_replace('%enum_name%', $val,$option);
                            $options .= $option;
                        }
                        $field = str_replace('%prop_enum_values%', $options, $field);
                        break;
                    case 'file':
                    case 'pict':
                         fwrite($editfh,"\n<!-- @prop_".$prop['name_db']."_edit -->\n");
                         $field = $this->get_template_block('edit_prop_'.$prop['type']);
                         $field = str_replace('%prop_name_full%',$prop['name_full'],$field);
                         $field = str_replace('%prop_name_db%',$prop['name_db'],$field);
                         fwrite($editfh, $field."\n");
                         fwrite($editfh,"\n<!-- @prop_".$prop['name_db']."_add -->\n");
                         $field = $this->get_template_block('add_prop_'.$prop['type']);
                        break;
                    case 'number':
                    case 'text':
                    case 'string':
                    case 'html':
                        fwrite($editfh,"\n<!-- @prop_".$prop['name_db']." -->\n");
                        $field = $this->get_template_block('add_prop_'.$prop['type']);
                        break;
                }
                $field = str_replace('%prop_name_full%',$prop['name_full'],$field);
                $field = str_replace('%prop_name_db%',$prop['name_db'],$field);
                fwrite($editfh, $field."\n");
            }
            fwrite($editfh,"\n<!-- @form_footer -->\n".$this->get_template_block("form_footer"));
            fclose($editfh);
            $query = 'UPDATE `'.$kernel->pub_prefix_get().'_catalog_item_groups` SET `back_tpl_md5`="'.md5_file($editfilename).'" WHERE `id`='.$groupid;
            $kernel->runSQL($query);
        }
        */
    }


    /**
     * Подготавливает значение свойства для обновления БД
     *
     * @param string $val  значение свойства
     * @param string $type тип свойства
     * @return string
     */
    private function prepare_property_value($val, $type)
    {
        global $kernel;
        if (!is_array($val))
        {
            $val = trim($val);
            if (mb_strlen($val) == 0)
                return 'NULL';
        }
        switch ($type)
        {
            case 'number':
                $val = str_replace(',', '.', $val);
                $val = str_replace(' ', '', $val);
                if (!is_numeric($val))
                    $val = 0;
                break;
            case 'date':
                $dvals = explode(".", $val);
                $val = '"'.$dvals[2].'-'.$dvals[1].'-'.$dvals[0].'"';
                break;
            case 'set':
                $elems = array();
                foreach (array_keys($val) as $el)
                {
                    $elems[] = $kernel->pub_str_prepare_set($el);
                }
                $val = "'".implode(",", $elems)."'";
                break;
            default:
                $val = '"'.$kernel->pub_str_prepare_set($val).'"';
                break;
        }
        return $val;
    }

    /**
     * Подготавливает значение свойства для обновления БД (используется при импорте)
     *
     * @param string $val  значение свойства
     * @param string $type тип свойства
     * @return string
     */
    private function prepare_property_value2($val, $type)
    {
        global $kernel;

        $ret = trim($val);
        if (mb_strlen($ret) == 0)
            return 'NULL';
        if ($type == 'number')
        {
            //$ret = str_replace(',','.',$ret);
            $ret = preg_replace('/[^\d\\.,]/', '', $ret); //уберём всё кроме цифр, точек и запятых
            $pos = mb_strpos($ret, '.');
            if ($pos)
            { //если нашлась точка, разобъём на целую и дробную части по ней
                $part1 = mb_substr($ret, 0, $pos);
                $part2 = mb_substr($ret, $pos + 1);
            }
            else
            { //если точка не нашлась, попробуем разбить запятой
                $pos = mb_strpos($ret, ',');
                if ($pos)
                {
                    $part1 = mb_substr($ret, 0, $pos);
                    $part2 = mb_substr($ret, $pos + 1);
                }
                else
                { //не нашлась ни точка, ни запятая
                    $part1 = $ret;
                    $part2 = '0';
                }
            }

            $part1 = preg_replace('/[^\d]/', '', $part1); //ещё раз уберём всё кроме цифр (,.) в обоих частях
            $part2 = preg_replace('/[^\d]/', '', $part2);
            $ret = $part1.'.'.$part2;

            //if (!is_numeric($ret)) $ret = 0;
        }
        else
            $ret = '"'.$kernel->pub_str_prepare_set($ret).'"';
        return $ret;
    }

    /**
     * Сохраняет категорию в БД
     *
     * @param $id integer id-шник категории
     * @return string
     */
    private function save_category($id)
    {
        global $kernel;
        if ($kernel->pub_httppost_get('isdefault'))
        { //значит эта категория будет по-умолчанию, сбрасываем другую
            $query = "UPDATE `".$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_cats` SET `is_default`=0 WHERE `is_default`=1';
            $kernel->runSQL($query);
            $isdef = 1;
        }
        else
            $isdef = 0;

        if ($kernel->pub_httppost_get('_hide_from_waysite'))
            $_hide_from_waysite = 1;
        else
            $_hide_from_waysite = 0;

        if ($kernel->pub_httppost_get('_hide_from_site'))
            $_hide_from_site = 1;
        else
            $_hide_from_site = 0;

        $props = CatalogCommons::get_cats_props();
        $cat = CatalogCommons::get_category($id);
        $query = 'UPDATE `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_cats` SET `_hide_from_waysite`= '.$_hide_from_waysite.', `_hide_from_site`= '.$_hide_from_site.', ';

        for ($i = 0; $i < count($props); $i++)
        {
            $prop = $props[$i];
            if ($prop['type'] == 'file' || $prop['type'] == 'pict')
            {
                if (isset($_FILES[$prop['name_db']]))
                {
                    if ($prop['type'] == 'pict')
                        $val = $this->process_pict_upload($_FILES[$prop['name_db']], $prop);
                    else
                        $val = $this->process_file_upload($_FILES[$prop['name_db']]);
                }
                elseif (!empty($cat[$prop['name_db']]))
                    $val = $cat[$prop['name_db']];
                else
                    $val = '';
            }
            else
                $val = $kernel->pub_httppost_get($prop['name_db'], false);
            $query .= '`'.$prop['name_db'].'`='.$this->prepare_property_value($val, $prop['type']).',';
        }



        if (!empty($_POST['tpl_items']))
        {
            $query .= " `tpl_items`='".$kernel->pub_httppost_get('tpl_items')."',";
        }

        if (!empty($_POST['tpl_card']))
        {
            $query .= " `tpl_card`='".$kernel->pub_httppost_get('tpl_card')."',";
        }

        $query .= ' `is_default`='.$isdef.' WHERE `id`='.$id;
        $kernel->runSQL($query);
        $this->regenerate_all_groups_tpls(false);
        return $kernel->pub_httppost_response('[#common_saved_label#]', 'category_items&id='.$id);
    }

    /**
     * Обновляет порядок товаров в категории с шагом $this->order_inc
     *
     * @param integer $catid id-шник категории
     * @return void
     */
    private function refresh_items_order_in_cat($catid)
    {
        global $kernel;
        $itemids = $this->get_cat_itemids($catid);
        $order = $this->order_inc;
        foreach ($itemids as $itemid)
        {
            $query = 'UPDATE `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_item2cat` '.
                'SET `order`='.$order.' '.
                'WHERE `item_id`='.$itemid.' AND `cat_id`='.$catid;
            $kernel->runSQL($query);
            $order += $this->order_inc;
        }
    }

    /**
     * Сохраняет товары категории в БД
     * @param integer $catid IDшник категории
     * @return void
     */
    private function save_category_items($catid)
    {
        global $kernel;

        $val = $kernel->pub_httppost_get("saveorder");
        if (!empty($val)) //сохраняем порядок товаров?
        {
            $iorders = $kernel->pub_httppost_get("iorder");
            foreach ($iorders as $itemid => $order)
            {
                if (is_numeric($order))
                {
                    $query = 'UPDATE `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_item2cat` SET `order`='.$order.' WHERE `cat_id`='.$catid.' AND `item_id`='.$itemid;
                    $kernel->runSQL($query);
                }
            }
            $this->refresh_items_order_in_cat($catid);
        }

        $val = $kernel->pub_httppost_get("saveselected");
        if (!empty($val)) // производим действие с отмеченными?
        {
            $vals = $kernel->pub_httppost_get("icb");
            $itemids = array();
            foreach ($vals as $itemid => $checked)
            {
                $itemid=intval($itemid);
                if(!$itemid)
                    continue;
                if ($checked)
                    $itemids[] = $itemid;
            }
            switch ($kernel->pub_httppost_get("withselected"))
            {
                case 'set_available':
                    CatalogCommons::set_items_available($itemids,1);
                    break;
                case 'set_unavailable':
                    CatalogCommons::set_items_available($itemids,0);
                    break;
                case "remove_from_current":
                    if (count($itemids))
                    {
                        $query = 'DELETE FROM `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_item2cat` WHERE `cat_id`='.$catid.' AND `item_id` IN ('.implode(',', $itemids).')';
                        $kernel->runSQL($query);
                    }
                    break;
                case "move2":
                    $moveid = intval($kernel->pub_httppost_get("cats"));
                    $parents = $this->get_way2cat($moveid, true);

                    if ($moveid > 0)
                    {
                        if (count($itemids))
                        {
                            $query = 'DELETE FROM `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_item2cat` WHERE `cat_id`='.$catid.' AND `item_id` IN ('.implode(',', $itemids).')';
                            $kernel->runSQL($query);
                        }
                        foreach ($itemids as $itemid)
                        {
                            $order = $this->get_next_order_in_cat($moveid);
                            $query = 'INSERT IGNORE INTO `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_item2cat`
               	                    (`cat_id`,`order`,`item_id`)
               	                    VALUES
               	                    ("'.$moveid.'","'.$order.'","'.$itemid.'")';

                            if($add_parents_cats && count($parents) > 0) {
                                foreach ($parents as $parent) {
                                    if(intval($parent['id']) !== $moveid)
                                        $query .= ', ("'.intval($parent['id']).'","'.$order.'","'.$itemid.'")';
                                }
                            }

                            $kernel->runSQL($query);
                        }
                    }
                    break;
                case "delete_selected":
                    $this->delete_items($itemids);
                    break;
            }

        }
    }


    /**
     * Удаляет товары по id-шникам
     *
     * @param array $itemids массив id-шников товаров
     * @return void
     */
    private function delete_items($itemids)
    {
        foreach ($itemids as $itemid)
            CatalogCommons::delete_item($itemid);
    }


    /**
     * Обработка file-upload, для свойств типа "файл"
     *
     * @param  string $file имя поля (input type='file') в html-форме
     * @return string имя сохранённого файла
     */
    private function process_file_upload($file)
    {
        global $kernel;

        if (!is_uploaded_file($file['tmp_name']))
            return '';

        //Имя файла пропустим через транслит, что бы исключить руские буквы
        //отделив сначала расширение
        $file_ext = pathinfo($file['name'], PATHINFO_EXTENSION);
        $filename = basename($file['name'], ".".$file_ext);
        $only_name = $this->translate_string2db($filename);
        $filename = $only_name.".".$file_ext;

        //Путь, куда будут сохранятся файл или изображение
        $dir = 'content/files/'.$kernel->pub_module_id_get().'/';

        //Проверим наличе дубликата, и добавим цифру если что
        $i = 0;
        while (file_exists($dir.$filename))
        {
            $filename = $only_name.'_'.$i.'.'.$file_ext; //$i."_".$filename;
            $i++;
        }

        $kernel->pub_file_move($file['tmp_name'], $dir.$filename, true, true);
        return $dir.$filename;
    }

    /**
     * Обработка file-upload, для свойств типа "картинка"
     *
     * @param string $file  имя поля (input type='file') в html-форме
     * @param array $prop  Массив с параметрами свойства, в нём для картинки передаются всяки дополнения
     * @return string имя сохранённого файла
     */
    private function process_pict_upload($file, $prop)
    {
        global $kernel;
        if (!is_uploaded_file($file['tmp_name']))
            return '';
        //Прежде определим, заданы ли параметры у этого
        //свойства с картинкой
        if ((isset($prop['add_param'])) && (!empty($prop['add_param'])))
            $prop['add_param'] = unserialize($prop['add_param']);
        else
            return $this->process_file_upload($file, $prop);

        //Имя файла пропустим через транслит, что бы исключить руские буквы
        //отделив сначала расширение
        $file_ext = pathinfo($file['name'], PATHINFO_EXTENSION);
        $filename = basename($file['name'], ".".$file_ext);
        $only_name = $this->translate_string2db($filename);
        $filename = $only_name;

        //Теперь определим, нужно ли нам что-то делать с исходным изображением

        //У картинки всё сложнее, надо определить, будем ли мы
        //создавать большое изображение и добавлять к нему водяной знак
        $big = null;
        if ($prop['add_param']['big']['isset'])
        {
            $big = array(
                'width' => $prop['add_param']['big']['width'],
                'height' => $prop['add_param']['big']['height']
            );
        }
        //Параметры водяной марки для большого изображения...
        $watermark_image_big = 0;
        $_tmp = $prop['add_param']['big'];
        $_tmp['water_path'] = $kernel->priv_file_full_patch($_tmp['water_path']);
        $_is_add = $kernel->pub_httppost_get($prop['name_db'].'_need_add_big_water');

        if (isset($_tmp['water_add']) &&
            //(file_exists($_tmp['water_path'])) &&
            is_file($_tmp['water_path']) &&
            ($_tmp['water_add'] == 1 || ($_tmp['water_add'] == 2 && !empty($_is_add)))
        )
        {
            $watermark_image_big = array(
                'path' => $_tmp['water_path'],
                'place' => $_tmp['water_position'],
                'transparency' => 25 //@todo use field settings
            );
        }

        //А теперь смотрим, нужно ли нам модифицировать исходное изображение
        $source_res = 0;
        if ($prop['add_param']['source']['isset'])
        {
            $source_res = array(
                'width' => $prop['add_param']['source']['width'],
                'height' => $prop['add_param']['source']['height']
            );
        }

        //... может и знак надо к нему добавить
        $watermark_image_source = 0;
        $_tmp = $prop['add_param']['source'];
        $_tmp['water_path'] = $kernel->priv_file_full_patch($_tmp['water_path']);
        $_is_add = $kernel->pub_httppost_get($prop['name_db'].'_need_add_source_water');

        if (isset($_tmp['water_add']) &&
            file_exists($_tmp['water_path']) &&
            ($_tmp['water_add'] == 1 || ($_tmp['water_add'] == 2 && !empty($_is_add)))
        )
        {
            $watermark_image_source = array(
                'path' => $_tmp['water_path'],
                'place' => $_tmp['water_position'],
                'transparency' => 25 //@todo use field settings
            );
        }

        //теперь параметры малого изображения
        $thumb = 0;
        if ($prop['add_param']['small']['isset'])
        {
            $thumb = array(
                'width' => $prop['add_param']['small']['width'],
                'height' => $prop['add_param']['small']['height']
            );
        }

        //Задаём путь для сохранения обработанных изображений.
        //такой путь должен существовать
        $path_to_save = 'content/files/'.$kernel->pub_module_id_get();
        $path_to_create = $kernel->pub_module_id_get();

        if (!empty($prop['add_param']['pict_path']))
        {
            $path_to_create .= "/".$prop['add_param']['pict_path'];
            $path_to_save .= "/".$prop['add_param']['pict_path'];
        }

        //Теперь вызовим созданий директорий, что бы они точно были
        //$kernel->pub_dir_create_in_files($path_to_create);
        $kernel->pub_dir_create_in_files($path_to_create."/tn");
        $kernel->pub_dir_create_in_files($path_to_create."/source");
        $filename = $kernel->pub_image_save($file['tmp_name'], $filename, $path_to_save, $big, $thumb, $watermark_image_big, $source_res, $watermark_image_source);
        //Обязательно добавим к файлу путь, так как иначе может возникнуть большая путаница
        return $path_to_save.'/'.$filename;
    }


    /**
     * Сохраняет изменённые товары в админке при быстром редактировании (пункт "Товары" в меню)
     * @return void
     */
    private function change_selected_items()
    {
        global $kernel;
        $kv = $_POST['iv'];
        if (!is_array($kv))
            return;
        foreach ($kv as $itemid => $idata)
        {
            $udata = array();
            foreach ($idata as $k => $v)
            {
                if (empty($v))
                    $udata[] = "`".$k."`=NULL";
                else
                    $udata[] = "`".$k."`='".$kernel->pub_str_prepare_set($v)."'";
            }
            $query = 'UPDATE `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_items` SET '.implode(",", $udata).' WHERE id='.$itemid;
            $kernel->runSQL($query);
        }
    }

    /**
     * Выполняет действия с выбранными товарами в админке (пункт "Товары" в меню)
     *
     */
    private function save_selected_items()
    {
        global $kernel;
        $vals = $kernel->pub_httppost_get("icb");
        if (!is_array($vals))
            return;
        $itemids = array();
        foreach ($vals as $itemid => $checked)
        {
            $itemid =  intval($itemid);
            if (!$itemid)
                continue;
            $itemids[] = $itemid;
        }
        $action = $kernel->pub_httppost_get('withselected');
        switch ($action)
        {
            case 'delete_selected':
                $this->delete_items($itemids);
                break;
            case 'set_available':
                CatalogCommons::set_items_available($itemids,1);
                break;
            case 'set_unavailable':
                CatalogCommons::set_items_available($itemids,0);
                break;
            default: //добавление в категорию, параметр- айдишник категории
                if (count($itemids) > 0)
                {
                    $cat_itemids = $this->get_cat_itemids($action);
                    $query = 'INSERT INTO `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_item2cat` (`item_id`, `cat_id`, `order`) VALUES ';
                    $vals = array();
                    $order = $this->get_next_order_in_cat($action);
                    foreach ($itemids as $itemid)
                    {
                        if (in_array($itemid, $cat_itemids))
                            continue; //если товар уже есть вэтой категории - пропускаем
                        $vals[] = ' ('.$itemid.','.$action.', '.$order.')';
                        $order += $this->order_inc;
                    }
                    if (count($vals) > 0)
                    {
                        $query .= implode(',', $vals);
                        $kernel->runSQL($query);
                    }
                }
                break;
        }

    }

    /**
     * Сохраняет товар в БД после его редактирования или добавления
     * @return string
     */
    private function save_item()
    {
        global $kernel;
        $itemid = $kernel->pub_httppost_get("id");
        //Если $itemid равен 0, то это новый товар и сначала его просто добавим
        //а потом вызовем стандартную функцию сохранения
        if (intval($itemid) == 0)
        {
            $itemid = $this->add_item();
            if (!$itemid)
                return $kernel->pub_httppost_errore('[#interface_global_label_error#]', true);
        }
        $item = CatalogCommons::get_item_full_data($itemid);
        $moduleid = $kernel->pub_module_id_get();
        $main_prop = $this->get_common_main_prop();
        //сначала сохраним common-свойства
        $props = CatalogCommons::get_common_props($kernel->pub_module_id_get());
        $visible_props = array_keys($this->get_group_visible_props($item['group_id']));
        $query = 'UPDATE `'.$kernel->pub_prefix_get().'_catalog_'.$moduleid.'_items` SET ';
        for ($i = 0; $i < count($props); $i++)
        {
            $prop = $props[$i];
            $nameDB = $prop['name_db'];
            if(!in_array($nameDB, $visible_props)) {
                continue;
            }

            if ($prop['type'] == 'file' || $prop['type'] == 'pict')
            {
                if (isset($_FILES[$nameDB]))
                {
                    if ($prop['type'] == 'pict')
                        $val = $this->process_pict_upload($_FILES[$nameDB], $prop);
                    else
                        $val = $this->process_file_upload($_FILES[$nameDB]);
                }
                elseif (!empty($item[$nameDB]))
                    $val = $item[$nameDB];
                else
                    $val = '';
            }
            else
            {
                $val = $kernel->pub_httppost_get($nameDB, false);
            }
            if ($val && $main_prop == $nameDB)
            {
                $exrec = $kernel->db_get_record_simple("_catalog_".$moduleid."_items", "`".$nameDB."`='".$kernel->pub_str_prepare_set($val)."'", "id");
                if ($exrec && $exrec['id'] != $itemid)
                {
                    $msg = $kernel->pub_page_textlabel_replace('[#catalog_not_uniq_main_prop_save#]');
                    $msg = str_replace('%fieldname%', $prop['name_full'], $msg);
                    return $kernel->pub_httppost_errore($msg, true);
                }
            }
            $query .= '`'.$prop['name_db'].'`='.$this->prepare_property_value($val, $prop['type']).',';
        }

        $available = $kernel->pub_httppost_get("available");
        if (empty($available))
            $available = 0;
        else
            $available = 1;

        $insitemap = $kernel->pub_httppost_get("insitemap");
        if (empty($insitemap))
            $insitemap = 0;
        else
            $insitemap = 1;

        $query .= ' `available`='.$available.', `insitemap`='.$insitemap.' WHERE `id`='.$itemid;
        $kernel->runSQL($query);

        //теперь custom-свойства этой товарной группы
        //если они есть у этой товарной группы
        $props = CatalogCommons::get_props($item['group_id'], false);
        $group = CatalogCommons::get_group($item['group_id']);

        if (count($props) > 0)
        {
            $query = 'UPDATE `'.$kernel->pub_prefix_get().'_catalog_items_'.$moduleid.'_'.strtolower($group['name_db']).'` SET ';
            for ($i = 0; $i < count($props); $i++)
            {
                $prop = $props[$i];
                if ($prop['type'] == 'file' || $prop['type'] == 'pict')
                {
                    if (isset($_FILES[$prop['name_db']]))
                    {
                        if ($prop['type'] == 'pict')
                            $val = $this->process_pict_upload($_FILES[$prop['name_db']], $prop);
                        else
                            $val = $this->process_file_upload($_FILES[$prop['name_db']]);
                    }
                    elseif (!empty($item[$prop['name_db']]))
                        $val = $item[$prop['name_db']];
                    else
                        $val = '';
                }
                else
                    $val = $kernel->pub_httppost_get($prop['name_db'], false);
                $query .= '`'.$prop['name_db'].'`='.$this->prepare_property_value($val, $prop['type']);

                if ($i != count($props) - 1)
                    $query .= ',';
            }
            $query .= ' WHERE `id`='.$item['ext_id'];
            $kernel->runSQL($query);
        }

        //...и категории
        $item_catids = $this->get_item_catids_with_order($itemid);
        $cats = CatalogCommons::get_all_categories($moduleid);
        $query = 'INSERT INTO `'.$kernel->pub_prefix_get().'_catalog_'.$moduleid.'_item2cat` (`item_id`, `cat_id`, `order`) VALUES ';
        $vals = array();
        if (isset($_POST['ccb']) && is_array($_POST['ccb']))
            $ccbPost=$_POST['ccb'];
        else
            $ccbPost=array();
        foreach ($cats as $cat)
        {
            if (isset($ccbPost[$cat['id']]))
            { //добавляем запись, только если отмечен чекбокс...
                if (!array_key_exists($cat['id'], $item_catids))
                { //...и товар ещё не принадлежит к категории
                    $order = $this->get_next_order_in_cat($cat['id']);
                    $vals[] = ' ('.$itemid.','.$cat['id'].', '.$order.')';
                }
            }
            else
            { //чекбокс не отмечен
                if (array_key_exists($cat['id'], $item_catids))
                { //товар был в категории, но чекбокс снят - удалим
                    $del_q = 'DELETE FROM `'.$kernel->pub_prefix_get().'_catalog_'.$moduleid.'_item2cat`
                              WHERE  `item_id`='.$itemid.' AND `cat_id`='.$cat['id'];
                    $kernel->runSQL($del_q);
                }
            }
        }
        if ($vals)
        {
            $query .= implode(',', $vals);
            $kernel->runSQL($query);
        }

        $addlinkedid = intval($kernel->pub_httppost_get('addlinkedid'));
        if ($addlinkedid)
        {
            $id2 = intval($kernel->pub_httppost_get('addlinkedid'));
            $this->add_items_link($itemid, $id2);
            return $kernel->pub_httppost_response("[#catalog_linked_item_added_msg#]", "item_edit&id=".$itemid.'&redir2='.$kernel->pub_httppost_get('redir2'));
        }
        return $kernel->pub_httppost_response('[#common_saved_label#]', $kernel->pub_httppost_get('redir2'));
    }


    /**
     * Добавляет товар в БД
     *
     * @return integer id-шник добавленого товара
     */
    private function add_item()
    {
        global $kernel;
        $groupid = intval($kernel->pub_httppost_get("group_id"));
        if (!$groupid)
            return 0;
        $group = CatalogCommons::get_group($groupid);
        //сохраним в кукисах ID-шник товарной группы, чтобы сделать её активной при след. добавлении
        setcookie("last_add_item_groupid", $groupid);
        $query = 'INSERT INTO '.$kernel->pub_prefix_get().'_catalog_items_'.$kernel->pub_module_id_get().'_'.strtolower($group['name_db']).
            ' (`id`) VALUES (NULL)';
        $result = $kernel->runSQL($query);
        $ext_id = $kernel->db_insert_id($result);
        $query = 'INSERT INTO `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_items` '.
            '(`ext_id`,`group_id`) VALUES '.
            '('.$ext_id.','.$groupid.')';
        $kernel->runSQL($query);
        $id = $kernel->db_insert_id();
        return $id;
    }

    /**
     * Сохраняет свойство(поле) для товарной группы. Создаёт запись в _catalog_item_groups и новую таблицу
     *
     * @param integer $pid id-шник свойства
     * @param string $name_full полное имя свойства
     * @param string $name_db БД-имя свойства
     * @param string $cb_inlist
     * @param string $sort
     * @param string $cb_ismain
     * @return void
     */
    private function save_prop($pid, $name_full, $name_db, $cb_inlist, $cb_infilters, $sort, $cb_ismain)
    {
        global $kernel;
        $prop = CatalogCommons::get_prop($pid);

        if (empty($cb_inlist))
            $inlist = 0;
        else
            $inlist = 1;

        if (empty($cb_infilters))
            $infilters = 0;
        else
            $infilters = 1;

        if (empty($cb_ismain))
            $ismain = 0;
        else
            $ismain = 1;

        $name_db = $this->translate_string2db($name_db);
        $moduleid = $kernel->pub_module_id_get();
        //изменилось ли БД-имя?
        if ($name_db != $prop['name_db'])
        {
            $n = 1;
            while ($this->is_prop_exists($prop['group_id'], $name_db) || $this->is_prop_exists(0, $name_db))
                $name_db .= $n++;
            $query = 'UPDATE `'.$kernel->pub_prefix_get().'_catalog_item_props` SET `name_db`="'.$name_db.'" WHERE `id`='.$pid;
            $kernel->runSQL($query);
            if ($prop['group_id'] > 0)
            {
                $group = CatalogCommons::get_group($prop['group_id']);
                $table = '_catalog_items_'.$kernel->pub_module_id_get().'_'.strtolower($group['name_db']);

                if(file_exists($kernel->pub_site_root_get()."/modules/catalog/templates_admin/items_search_form_".$group['name_db'].".html"))
                    @unlink($kernel->pub_site_root_get()."/modules/catalog/templates_admin/items_search_form_".$group['name_db'].".html");

            }
            else
            {
                $table = '_catalog_'.$moduleid.'_items';
                //изменилось БД-имя и это общее свойство - обновим таблицу видимых свойств для групп
                $query = 'UPDATE `'.$kernel->pub_prefix_get().'_catalog_visible_gprops`
                    SET `prop`="'.$kernel->pub_str_prepare_set($name_db).'"
                    WHERE `prop`="'.$prop['name_db'].'" AND `module_id`="'.$moduleid.'"';
                $kernel->runSQL($query);
            }
            $values = null;
            if ($prop['type'] == 'enum' || $prop['type'] == 'set')
            {
                $tinfo = $kernel->db_get_table_info($table);
                $values = $this->get_enum_set_prop_values($tinfo[$prop['name_db']]['Type']);
            }
            $db_type = $this->convert_field_type_2_db($prop['type'], $values);

            $query = 'ALTER TABLE `'.$kernel->pub_prefix_get().$table.'` CHANGE COLUMN `'.$prop['name_db'].'` `'.$name_db.'` '.$db_type;
            $kernel->runSQL($query);
        }

        //изменились поля?
        if ($name_full != $prop['name_full'] || $inlist != $prop['showinlist'] || $infilters != $prop['showinfilters'] || $sort != $prop['sorted'] || $ismain != $prop['ismain'])
        {
            if ($sort > 0)
            { //сбросим `sorted` для остальных полей
                $query = 'UPDATE `'.$kernel->pub_prefix_get().'_catalog_item_props` SET `sorted`=0 WHERE `group_id`=0 AND `module_id`="'.$moduleid.'"';
                $kernel->runSQL($query);
            }
            if ($ismain == 1)
            { //сбросим `ismain` для остальных полей
                $query = 'UPDATE `'.$kernel->pub_prefix_get().'_catalog_item_props` SET `ismain`=0 WHERE `group_id`=0 AND `module_id`="'.$moduleid.'"';
                $kernel->runSQL($query);
            }

            $query = 'UPDATE `'.$kernel->pub_prefix_get().'_catalog_item_props` SET
                `name_full`="'.$name_full.'",
                `showinlist`='.$inlist.',
                `showinfilters`='.$infilters.',
                `ismain`='.$ismain.',
                `sorted`='.$sort.'
                WHERE `id`='.$pid;
            $kernel->runSQL($query);
        }

        //Если это изображение, то обновим информацию по изображениям
        if ($prop['type'] == 'pict')
        {

            $prop['add_param']['pict_path'] = $kernel->pub_httppost_get('pict_path');

            //Исходное изображение
            if ($kernel->pub_httppost_get('pict_source_isset'))
                $prop['add_param']['source']['isset'] = true;
            else
                $prop['add_param']['source']['isset'] = false;

            $prop['add_param']['source']['width'] = intval($kernel->pub_httppost_get('pict_source_width'));
            $prop['add_param']['source']['height'] = intval($kernel->pub_httppost_get('pict_source_height'));
            $prop['add_param']['source']['water_add'] = intval($kernel->pub_httppost_get('pict_source_water_add'));
            $prop['add_param']['source']['water_path'] = $kernel->pub_httppost_get('path_source_water_path');
            $prop['add_param']['source']['water_position'] = intval($kernel->pub_httppost_get('pict_source_water_position'));

            // большое изображение
            if ($kernel->pub_httppost_get('pict_big_isset'))
                $prop['add_param']['big']['isset'] = true;
            else
                $prop['add_param']['big']['isset'] = false;

            $prop['add_param']['big']['width'] = intval($kernel->pub_httppost_get('pict_big_width'));
            $prop['add_param']['big']['height'] = intval($kernel->pub_httppost_get('pict_big_height'));
            $prop['add_param']['big']['water_add'] = intval($kernel->pub_httppost_get('pict_big_water_add'));
            $prop['add_param']['big']['water_path'] = $kernel->pub_httppost_get('path_big_water_path');
            $prop['add_param']['big']['water_position'] = intval($kernel->pub_httppost_get('pict_big_water_position'));

            //Малое изображение
            if ($kernel->pub_httppost_get('pict_small_isset'))
                $prop['add_param']['small']['isset'] = true;
            else
                $prop['add_param']['small']['isset'] = false;

            $prop['add_param']['small']['width'] = intval($kernel->pub_httppost_get('pict_small_width'));
            $prop['add_param']['small']['height'] = intval($kernel->pub_httppost_get('pict_small_height'));

            //Теперь обновим и запишим этот массив в mysql
            $query = "UPDATE `".$kernel->pub_prefix_get()."_catalog_item_props` SET
                `add_param`='".serialize($prop['add_param'])."'
                WHERE `id`=".$pid;

            $kernel->runSQL($query);
        }
        else if ($prop['type'] == 'set' || $prop['type'] == 'enum')
        {
            $prop['add_param'] = null;
            if ($prop['group_id'] > 0) {
                $group = CatalogCommons::get_group($prop['group_id']);
                $table = '_catalog_items_'.$kernel->pub_module_id_get().'_'.strtolower($group['name_db']);
            } else {
                $table = '_catalog_'.$moduleid.'_items';
            }
            $tinfo = $kernel->db_get_table_info($table);
            $prop['add_param'] = $this->get_enum_set_prop_values($tinfo[$prop['name_db']]['Type'], false);

            $query = "UPDATE `".$kernel->pub_prefix_get()."_catalog_item_props` SET
                `add_param`='".serialize($prop['add_param'])."'
                WHERE `id`=".$pid;

            $kernel->runSQL($query);
        }

        if (($name_full != $prop['name_full'] || $name_db != $prop['name_db']) && in_array($prop['type'], array('string', 'text', 'html', 'number', 'enum')))
        { //изменилось что-то, что требует регенерации шаблона поиска
            if ($prop['group_id'] == 0)
            { //изменилось общее свойство
                $groups = CatalogCommons::get_groups($kernel->pub_module_id_get());
                foreach ($groups as $group)
                {
                    $this->generate_search_form($group['id'], array());
                }
            }
            else //изменилось свойство группы
                $this->generate_search_form($prop['group_id'], array());
        }

        //Перегенерацию шаблонов убираем пока, она будет ручной
        /*
        if ($prop['group_id']>0)
            $this->regenerate_group_tpls($prop['group_id']);
        else
        {
            $this->regenerate_all_groups_tpls(false);
            CatalogCommons::regenerate_frontend_item_common_block($kernel->pub_module_id_get(), false);
        }
        */
    }


    /**
     * Сохраняет свойство(поле) для КАТЕГОРИИ.
     *
     * @param integer $pid id-шник свойства
     * @param string $name_full полное имя свойства
     * @param string $name_db БД-имя свойства
     * @return void
     */
    private function save_cat_prop($pid, $name_full, $name_db)
    {
        global $kernel;
        $prop = CatalogCommons::get_cat_prop($pid);
        if (isset($prop['add_param']))
            $prop['add_param'] = @unserialize($prop['add_param']);
        $table = "_catalog_".$kernel->pub_module_id_get()."_cats_props";

        if($name_db == 'content')
            return false;

        //изменилось ли БД-имя?
        if ($name_db != $prop['name_db'])
        {
            $n = 1;
            while ($this->is_cat_prop_exists($name_db))
                $name_db .= $n++;
            $query = 'UPDATE `'.$kernel->pub_prefix_get().$table.'` SET '.
                '`name_db`="'.$kernel->pub_str_prepare_set($name_db).'" WHERE `id`='.$pid;
            $kernel->runSQL($query);

            $values = null;
            if ($prop['type'] == 'enum')
            {
                $tinfo = $kernel->db_get_table_info($table);
                $values = $this->get_enum_set_prop_values($tinfo[$prop['name_db']]['Type']);
            }
            $db_type = $this->convert_field_type_2_db($prop['type'], $values);
            $query = 'ALTER TABLE `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_cats` CHANGE COLUMN `'.$prop['name_db'].'` `'.$name_db.'` '.$db_type;
            $kernel->runSQL($query);
        }

        //изменилось название?
        if ($name_full != $prop['name_full'])
        {
            $query = 'UPDATE `'.$kernel->pub_prefix_get().$table.'` SET `name_full`="'.$kernel->pub_str_prepare_set($name_full).'" WHERE `id`='.$pid;
            $kernel->runSQL($query);
        }

        //Если это изображение, то обновим информацию по изображениям
        if ($prop['type'] == 'pict')
        {
            $prop['add_param']['pict_path'] = $kernel->pub_httppost_get('pict_path');

            //Исходное изображение
            if ($kernel->pub_httppost_get('pict_source_isset'))
                $prop['add_param']['source']['isset'] = true;
            else
                $prop['add_param']['source']['isset'] = false;

            $prop['add_param']['source']['width'] = intval($kernel->pub_httppost_get('pict_source_width'));
            $prop['add_param']['source']['height'] = intval($kernel->pub_httppost_get('pict_source_height'));
            $prop['add_param']['source']['water_add'] = intval($kernel->pub_httppost_get('pict_source_water_add'));
            $prop['add_param']['source']['water_path'] = $kernel->pub_httppost_get('path_source_water_path');
            $prop['add_param']['source']['water_position'] = intval($kernel->pub_httppost_get('pict_source_water_position'));

            // большое изображение
            if ($kernel->pub_httppost_get('pict_big_isset'))
                $prop['add_param']['big']['isset'] = true;
            else
                $prop['add_param']['big']['isset'] = false;

            $prop['add_param']['big']['width'] = intval($kernel->pub_httppost_get('pict_big_width'));
            $prop['add_param']['big']['height'] = intval($kernel->pub_httppost_get('pict_big_height'));
            $prop['add_param']['big']['water_add'] = intval($kernel->pub_httppost_get('pict_big_water_add'));
            $prop['add_param']['big']['water_path'] = $kernel->pub_httppost_get('path_big_water_path');
            $prop['add_param']['big']['water_position'] = intval($kernel->pub_httppost_get('pict_big_water_position'));

            //Малое изображение
            if ($kernel->pub_httppost_get('pict_small_isset'))
                $prop['add_param']['small']['isset'] = true;
            else
                $prop['add_param']['small']['isset'] = false;

            $prop['add_param']['small']['width'] = intval($kernel->pub_httppost_get('pict_small_width'));
            $prop['add_param']['small']['height'] = intval($kernel->pub_httppost_get('pict_small_height'));


            //Теперь обновим и запишим этот массив в mysql
            $query = "UPDATE `".$kernel->pub_prefix_get().$table."` SET `add_param`='".serialize($prop['add_param'])."' WHERE `id`=".$pid;
            $kernel->runSQL($query);
        }
    }

    /**
     * Добавляет поле(свойство) в товарную группу в БД.
     * Создаёт запись в _catalog_item_props и изменяет таблицу _catalog_items_[moduleid]_[groupdbname]
     *
     * @return string БД-имя добавленного свойства
     */
    private function add_prop_in_group()
    {
        global $kernel;
        //Взяли параметры из формы
        $pvalues = $kernel->pub_httppost_get('enum_values', false);
        $pname = $kernel->pub_httppost_get('name_full');
        $pnamedb = $kernel->pub_httppost_get('name_db');
        $group_id = $kernel->pub_httppost_get('group_id');
        $ptype = $kernel->pub_httppost_get('ptype');
        $inlist = $kernel->pub_httppost_get('inlist');
        $sorted = $kernel->pub_httppost_get('sorted');
        $ismain = $kernel->pub_httppost_get('ismain');
        $visibility = $kernel->pub_httppost_get('visibility');
        $group_id = intval($group_id);

        if (empty($inlist))
            $inlist = 0;
        else
            $inlist = 1;

        if (empty($ismain))
            $ismain = 0;
        else
            $ismain = 1;

        if (empty($visibility))
            $visibility = 0;
        else
            $visibility = 1;

        if (empty($sorted))
            $sorted = 0;
        else
            $sorted = 1;

        if($pnamedb == 'content')
            return false;

        //Проверим и проставим значения
        $group = CatalogCommons::get_group($group_id);
        if (mb_strlen($pnamedb) == 0)
            $pnamedb = $pname;
        $namedb = $this->translate_string2db($pnamedb);
        $n = 2;
        $namedb0 = $namedb;
        while ($this->is_prop_exists($group_id, $namedb) || $this->is_prop_exists(0, $namedb))
            $namedb = $namedb0.$n++;

        if (empty($pvalues))
            $values = "NULL";
        else
        {
            $pva = explode("\n", $pvalues);
            $values = array();
            foreach ($pva as $v)
            {
                $v = trim($v);
                if (mb_strlen($v) != 0)
                    $values[] = $v;
            }
            if (count($values) == 0)
                $values = "NULL";
        }

        //узнаем order у последнего св-ва в этой группе и добавим 10
        $gprops = CatalogCommons::get_props($group_id, true);
        $props_count = count($gprops);
        if ($props_count == 0)
            $order = 10;
        else
            $order = $gprops[$props_count - 1]['order'] + 10;
        $moduleid = $kernel->pub_module_id_get();
        if ($ismain == 1)
        { //сбросим `ismain` для остальных полей
            $query = 'UPDATE `'.$kernel->pub_prefix_get().'_catalog_item_props` SET `ismain`=0 WHERE `group_id`=0 AND module_id="'.$moduleid.'"';
            $kernel->runSQL($query);
        }
        if ($sorted > 0)
        { //сбросим `sorted` для остальных полей
            $query = 'UPDATE `'.$kernel->pub_prefix_get().'_catalog_item_props` SET `sorted`=0 WHERE `group_id`=0 AND module_id="'.$moduleid.'"';
            $kernel->runSQL($query);
        }

        if ($ptype == 'pict') {
            $add_param = '"'.$kernel->pub_str_prepare_set(serialize(self::make_default_pict_prop_addparam())).'"';
        }
        else if ($ptype == 'set' || $ptype == 'enum')
        {
            $add_param = '"'.$kernel->pub_str_prepare_set(serialize($values)).'"';
        }
        else
        {
            $add_param = "NULL";
        }

        //Собственно запросы по добавлению
        $query = 'INSERT INTO `'.$kernel->pub_prefix_get().'_catalog_item_props`
                 (`module_id`,`group_id`,`name_db`,`name_full`,`type`, `showinlist`, `sorted`,`order`, `ismain`,`add_param`)
                 VALUES
                 ("'.$kernel->pub_module_id_get().'",'.$group_id.',"'.$namedb.'","'.$pname.'","'.$ptype.'",'.$inlist.','.$sorted.', '.$order.', '.$ismain.','.$add_param.')';
        $kernel->runSQL($query);

        if ($group_id > 0)
        {
            $query = 'ALTER TABLE `'.$kernel->pub_prefix_get().'_catalog_items_'.$moduleid.'_'.strtolower($group['name_db']).'` ADD COLUMN `'.$namedb."` ".$this->convert_field_type_2_db($ptype, $values);

            if(file_exists($kernel->pub_site_root_get()."/modules/catalog/templates_admin/items_search_form_".$group['name_db'].".html"))
                @unlink($kernel->pub_site_root_get()."/modules/catalog/templates_admin/items_search_form_".$group['name_db'].".html");

        }
        else
        { //common-свойство
            $query = 'ALTER TABLE `'.$kernel->pub_prefix_get().'_catalog_'.$moduleid.'_items`
                      ADD COLUMN `'.$namedb."` ".$this->convert_field_type_2_db($ptype, $values);

            if ($visibility == 1) { // добавляем как видимое для всех тов. групп
                $groups = CatalogCommons::get_groups();
                foreach ($groups as $agroup)
                {
                    $this->add_group_visible_prop($kernel->pub_module_id_get(), $agroup['id'], $namedb);
                }
            }
        }
        $kernel->runSQL($query);
        /*
        if ($group_id > 0)
            $this->regenerate_group_tpls($group_id, false);
        else
        {
            $this->regenerate_all_groups_tpls(false);
            CatalogCommons::regenerate_frontend_item_common_block($kernel->pub_module_id_get(), false);
        }*/

        if (in_array($ptype, array('string', 'text', 'html', 'number', 'enum')))
        { //тип свойства такой, который используется в шаблоне поиска
            if ($group_id == 0)
            { //добавили общее свойство
                $groups = CatalogCommons::get_groups($moduleid);
                foreach ($groups as $group)
                {
                    $this->generate_search_form($group['id'], array());
                }
            }
            else //добавили свойство группы
                $this->generate_search_form($group_id, array());
        }
        return $namedb;
    }


    /**
     * Добавляет поле(свойство) для категории.
     * Создаёт запись в _catalog_item_props и изменяет таблицу _catalog_items_[moduleid]_[groupdbname]
     *
     * @param string $pname имя свойства
     * @param string $ptype тип свойства
     * @param string $pvalues набор значений для свойства типа "набор значений"
     * @param string $pnamedb БД-имя
     * @return string БД-имя добавленного свойства
     */
    private function add_prop_in_cat($pname, $ptype, $pvalues, $pnamedb)
    {
        global $kernel;
        if (mb_strlen($pnamedb) == 0)
            $pnamedb = $pname;

        if($pnamedb == 'content')
            return false;

        $namedb = $this->translate_string2db($pnamedb);
        $n = 1;
        while ($this->is_cat_prop_exists($namedb))
            $namedb .= $n++;
        if (empty($pvalues))
            $values = "NULL";
        else
        {
            $pva = explode("\n", $pvalues);
            $values = array();
            foreach ($pva as $v)
            {
                $v = trim($v);
                if (mb_strlen($v) != 0)
                    $values[] = $v;
            }
            if (count($values) == 0)
                $values = "NULL";
        }
        if ($ptype == 'pict')
            $add_param = '"'.$kernel->pub_str_prepare_set(serialize(self::make_default_pict_prop_addparam())).'"';
        else
            $add_param = "NULL";
        $query = 'INSERT INTO `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_cats_props`
                 (`name_db`,`name_full`,`type`,`add_param`)
                 VALUES
                 ("'.$namedb.'","'.$kernel->pub_str_prepare_set($pname).'","'.$ptype.'",'.$add_param.')';
        $kernel->runSQL($query);

        $query = 'ALTER TABLE `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_cats`
                  ADD COLUMN `'.$namedb."` ".$this->convert_field_type_2_db($ptype, $values);
        $kernel->runSQL($query);
        return $namedb;
    }


    /**
     * Возвращает товары из БД
     *
     * @param integer $offset        смещение
     * @param integer $limit         лимит
     * @param integer $group_id      id-шник товарной группы
     * @param boolean $only_visible  только видимые?
     * @return array
     */
    private function get_items($offset = 0, $limit = 100, $group_id = 0, $only_visible = true)
    {
        global $kernel;
        $where = array();
        if ($group_id > 0)
            $where[] = ' `group_id`='.$group_id;
        if ($only_visible)
            $where[] = ' `available`=1';
        if (count($where) > 0)
            $query = implode(" AND ", $where);
        else
            $query = "true";

        $sort_field = CatalogCommons::get_common_sort_prop();
        if ($sort_field)
        {
            $query .= ' ORDER BY ISNULL(`'.$sort_field['name_db'].'`)  , `'.$sort_field['name_db'].'` ';
            if ($sort_field['sorted'] == 2)
                $query .= " DESC ";
        }

        if ($limit == 0)
            $limit = null;

        $items = $kernel->db_get_list_simple('_catalog_'.$kernel->pub_module_id_get().'_items', $query, "*", $offset, $limit);
        return $items;
    }

    /**
     * Проверяет, существует ли товарная группа с указанным именем для текущего модуля
     *
     * @param string $name имя товарной группы
     * @return boolean
     */
    private function is_group_exists($name)
    {
        global $kernel;
        return $kernel->db_get_record_simple('_catalog_item_groups', '`name_db` = "'.$name.'"'.' AND `module_id` = "'.$kernel->pub_module_id_get().'"');
    }


    /**
     * Проверяет, существует ли свойство с БД-именем $pname для категорий
     * для текущего модуля
     *
     * @param string $pname имя свойства
     * @return boolean
     */
    private function is_cat_prop_exists($pname)
    {
        $reserved = array('id', 'parent_id', 'is_default', 'order', '_hide_from_waysite', '_hide_from_site', '_items_count', '_subcats_count', 'depth');
        if (in_array($pname, $reserved))
            return true;
        global $kernel;
        return $kernel->db_get_record_simple('_catalog_'.$kernel->pub_module_id_get().'_cats_props', '`name_db` ="'.$pname.'"');
    }

    /**
     * Проверяет, существует ли свойство ($pname) в тов. группе
     * с указанным id ($group_id) для текущего модуля
     *
     * @param string $group_id id-шник товарной группы
     * @param string $pname имя свойства
     * @return boolean
     */
    private function is_prop_exists($group_id, $pname)
    {
        $reservedCommon = array('id', 'module_id', 'group_id', 'available', 'ext_id');
        $reservedCustom = array('id');
        if ($group_id == 0 && in_array($pname, $reservedCommon))
            return true;
        elseif (in_array($pname, $reservedCustom))
            return true;
        global $kernel;
        return $kernel->db_get_record_simple('_catalog_item_props', '`name_db` ="'.$pname.'" AND `group_id`='.$group_id);
    }


    /**
     * Возвращает id-шники товаров, принадлежих к категории
     *
     * @param integer $catid id-шник категории
     * @return array
     */
    private function get_cat_itemids($catid)
    {
        global $kernel;
        $rows = $kernel->db_get_list_simple('_catalog_'.$kernel->pub_module_id_get().'_item2cat', '`cat_id`='.$catid.' ORDER BY `order`', 'item_id');
        $ret = array();
        foreach ($rows as $row)
        {
            $ret[] = $row['item_id'];
        }
        return $ret;
    }


    /**
     * Возвращает id-шники категорий, к которым принадлежит товар
     *
     * @param integer $itemid id-шник товара
     * @return array
     */
    private function get_item_catids($itemid)
    {
        global $kernel;
        $query = 'SELECT `cat_id` FROM `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_item2cat` WHERE `item_id`='.$itemid;
        $ret = array();
        $result = $kernel->runSQL($query);
        while ($row = mysqli_fetch_assoc($result))
            $ret[] = $row['cat_id'];
        mysqli_free_result($result);
        return $ret;
    }

    /**
     * Возвращает id-шники категорий и порядок в них, к которым принадлежит товар
     * в виде массива с элементами [id-шник категории]=>[порядок в категории]
     * используется при сохранении товара (принадлежность к категориям)
     *
     * @param integer $itemid id-шник товара
     * @return array
     */
    private function get_item_catids_with_order($itemid)
    {
        global $kernel;
        $query = 'SELECT `cat_id`, `order` FROM `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_item2cat` WHERE `item_id`='.$itemid;
        $ret = array();
        $result = $kernel->runSQL($query);
        while ($row = mysqli_fetch_assoc($result))
            $ret[$row['cat_id']] = $row['order'];
        mysqli_free_result($result);
        return $ret;
    }

    /**
     * Возвращает товары, принадлежащие к категории
     *
     * @param integer  $catid  id-шник категории
     * @param integer  $offset смещение
     * @param integer  $limit  лимит
     * @param boolean $only_visible  только видимые?
     * @return array
     */
    private function get_cat_items($catid, $offset = 0, $limit = 20, $only_visible = false)
    {
        global $kernel;

        $query = 'SELECT items.*, i2c.`order` FROM `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_item2cat` AS i2c '.
            ' LEFT JOIN `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_items` AS items ON items.id=i2c.item_id ';
        $query .= 'WHERE i2c.cat_id='.$catid;

        if ($only_visible)
            $query .= ' AND `available`=1 ';

        $sort_field = CatalogCommons::get_common_sort_prop();
        if ($sort_field)
        {
            $query .= ' ORDER BY ISNULL(items.`'.$sort_field['name_db'].'`), items.`'.$sort_field['name_db']."` ";
            if ($sort_field['sorted'] == 2)
                $query .= " DESC ";
        }
        else
            $query .= ' ORDER BY i2c.`order` ASC ';
        if ($limit != 0)
            $query .= ' LIMIT '.$offset.','.$limit;
        $ret = array();
        $result = $kernel->runSQL($query);
        while ($row = mysqli_fetch_assoc($result))
            $ret[] = $row;
        mysqli_free_result($result);
        return $ret;
    }

    /**
     * Возвращает кол-во товаров, принадлежащих к категории
     *
     * @param integer  $catid  id-шник категории
     * @param boolean $only_visible  только видимые?
     * @return integer
     */
    private function get_cat_items_count($catid, $only_visible = true)
    {
        global $kernel;
        $query = 'SELECT COUNT(*) AS count FROM `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_item2cat` AS i2c '.
            'LEFT JOIN `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_items` AS `items` ON i2c.item_id=items.id '.
            'WHERE i2c.cat_id='.$catid;
        if ($only_visible)
            $query .= ' AND items.`available`=1 ';
        $count = 0;
        $result = $kernel->runSQL($query);
        if ($row = mysqli_fetch_assoc($result))
            $count = $row['count'];
        mysqli_free_result($result);
        return $count;
    }


    private function get_categories_tree($node_id, $module_id = false)
    {
        global $kernel;
        if (!$module_id)
            $module_id = $kernel->pub_module_id_get();
        $data = array();
        $rows = CatalogCommons::get_child_cats_with_count($module_id, $node_id, 'cats.id,cats.name', true);
        foreach ($rows as $row)
        {
            $array = array(
                'data' => htmlspecialchars($row['name']).'&nbsp;('.$row['_items_count'].')',
                'attr' => array("id" => $row['id'], 'rel' => 'default'),
            );
            if ($row['_subcats_count'] == 0)
                $array['leaf'] = true;
            else
            {
                $array['children'] = $this->get_categories_tree($row['id'], $module_id);
                ;
                $array['leaf'] = false;
                $array['attr']['rel'] = "folder";
            }
            $data[] = $array;
        }
        return $data;
    }

    /**
     * Проверяет, есть ли категория с указанным id в массиве
     *
     * @param integer $catid
     * @param array   $cats
     * @return integer позицию в массиве если найдена, иначе -1
     */
    private function is_cat_in_array($catid, $cats)
    {
        for ($i = 0; $i < count($cats); $i++)
        {
            if ($cats[$i]['id'] == $catid)
                return $i;
        }
        return -1;
    }


    /**
     * Возвращает вложенные категории
     *
     * @param integer $node_id id-шник родительской категории
     * @param integer $depth текущая глубина
     * @param array   $data массив с данными
     * @param integer $maxdepth максимальная глубина
     * @param string|boolean $module_id
     * @return array
     */
    private function get_child_categories($node_id, $depth = 0, $data = array(), $maxdepth = 100, $module_id = false)
    {
        global $kernel;
        if ($depth >= $maxdepth)
            return $data;
        $currdepth = $depth++;
        if (!$module_id)
            $module_id = $kernel->pub_module_id_get();
        $rows = CatalogCommons::get_child_cats_with_count($module_id, $node_id, 'cats.*', true);
        foreach ($rows as $row)
        {
            $row['depth'] = $currdepth;
            $data[] = $row;
            if ($row['_subcats_count'] > 0)
                $data = $this->get_child_categories($row['id'], $depth, $data, $maxdepth, $module_id);
        }
        return $data;
    }

    /**
     * Возвращает вложенные категории с учётом кол-ва раскрываемых уровней
     *
     * @param integer $node_id id-шник родительской категории
     * @param integer $depth текущая глубина
     * @param array   $data массив с данными
     * @param integer $maxdepth максимальная глубина
     * @param array   $way дорога пользователя в категориях
     * @param integer $openlevels кол-во раскрываемых уровней
     * @return array
     */
    private function get_child_categories2($node_id, $depth = 0, $data = array(), $maxdepth = 100, $way, $openlevels)
    {
        global $kernel;
        if ($depth >= $maxdepth)
            return $data;
        $currdepth = $depth++;
        $rows = CatalogCommons::get_child_cats_with_count($kernel->pub_module_id_get(), $node_id, 'cats.*', true);
        foreach ($rows as $row)
        {
            $row['depth'] = $currdepth;
            $data[] = $row;
            $wpos = $this->is_cat_in_array($row['id'], $way);
            if (($wpos >= 0 || $depth < $openlevels) && $row['_subcats_count'] > 0)
                $data = $this->get_child_categories2($row['id'], $depth, $data, $maxdepth, $way, $openlevels);
        }
        return $data;
    }

    /**
     * Конвертирует строку в приемлимую для использования в БД
     *
     * @param string $s строка для конвертирования
     * @return string
     */
    private function translate_string2db($s)
    {
        global $kernel;
        $res = $kernel->pub_translit_string($s);
        $res = preg_replace("/[^0-9a-z_]/i", '', $res);
        return $res;
    }

    /**
     * Конвертирует тип поля для использования в БД
     * @param string $type тип поля
     * @param array $values значения (для enum)
     * @return string
     */
    private function convert_field_type_2_db($type, $values = null)
    {
        global $kernel;
        switch ($type)
        {
            case 'text':
                return 'text';
            case 'set':
                $arr = array();
                foreach ($values as $val)
                    $arr[] = "'".$kernel->pub_str_prepare_set(str_replace(',', '', $val))."'";
                return 'set ('.implode(',', array_unique($arr)).')';
            case 'html':
                return 'text';
            case 'file':
            case 'pict':
            case 'string':
                return 'varchar (255)';
            case 'date':
                return 'date';
            case 'enum':
                $arr = array();
                foreach ($values as $val)
                    $arr[] = "'".$kernel->pub_str_prepare_set($val)."'";
                return 'enum ('.implode(',', array_unique($arr)).')';
            case 'number':
                return 'decimal(12,2)';
            default:
                return 'text';
        }
    }


    /**
     * Выводит список товаров в админке
     * Сюда попадают все товары магазина. Есть возможность в форме устаноить фильтр по группе, к которой принадлежит товар
     * @param integer $group_id IDшник группы
     * @return string
     */
    private function show_items($group_id)
    {
        global $kernel;
        //Получим все необхоимые данные
        $offset = $this->get_offset_admin();
        $limit = $this->get_limit_admin();
        $groups = CatalogCommons::get_groups();
        //$purl='show_items&group_id='.$group_id.'&'.$this->admin_param_offset_name.'=';
        $purl = 'show_items';
        if (isset($_GET['search_results']) && $kernel->pub_session_get("search_items_query"))
        {
            //пока без ограничения
            $offset = 0;
            $limit = 10000;
            $squery = $kernel->pub_session_get("search_items_query");
            $result = $kernel->runSQL($squery);
            $items = array();
            while ($row = mysqli_fetch_assoc($result))
                $items[] = $row;
            mysqli_free_result($result);
            $total = count($items);
            $header_label = $kernel->pub_page_textlabel_replace("[#catalog_items_all_list_search_results_mainlabel#]");
            $purl .= '&search_results=1&group_id='.$group_id;
        }
        else
        {
            $kernel->pub_session_unset("search_items_query");
            if ($group_id == -1) //показываем товары без категории
            {
                $header_label = $kernel->pub_page_textlabel_replace("[#catalog_items_all_list_filter_mainlabel#]");
                $items = CatalogCommons::get_items_without_cat($offset, $limit);
                $total = CatalogCommons::get_items_without_cat_count();
            }
            else
            { //groupid >= 0
                $purl .= '&group_id='.$group_id;
                if (count($groups) == 1)
                { //если у нас только одна тов. группа, её и выберем
                    $groupsTmp = $groups;
                    $firstGroup = array_shift($groupsTmp);
                    $group_id = $firstGroup['id'];
                }

                $header_label = $kernel->pub_page_textlabel_replace("[#catalog_items_all_list_filter_mainlabel#]");
                $total = CatalogCommons::get_items_count($group_id, false);
                $items = $this->get_items($offset, $limit, $group_id, false);
            }
        }
        $count = count($items);
        $purl .= '&'.$this->admin_param_offset_name.'=';
        $this->set_templates($kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'items_list.html'));
        $content = '';
        // Сформируем список доступных товарных групп
        if (count($groups) > 0)
        {
            $content .= $this->get_template_block('header');
            $groups_vals = $this->get_template_block('group_value');
            $groups_vals = str_replace('%group_id%', 0, $groups_vals);
            $groups_vals = str_replace('%group_name%', '[#catalog_items_all_list_filter_all_group#]', $groups_vals);
            if ($group_id == 0)
                $groups_vals = str_replace('%gselected%', "selected", $groups_vals);
            else
                $groups_vals = str_replace('%gselected%', "", $groups_vals);

            $groups_vals .= $this->get_template_block('group_value');
            $groups_vals = str_replace('%group_id%', -1, $groups_vals);
            $groups_vals = str_replace('%group_name%', '[#catalog_items_all_list_filter_no_cat#]', $groups_vals);
            if ($group_id == -1)
                $groups_vals = str_replace('%gselected%', "selected", $groups_vals);
            else
                $groups_vals = str_replace('%gselected%', "", $groups_vals);
            foreach ($groups as $group)
            {
                $option = $this->get_template_block('group_value');
                $option = str_replace('%group_id%', $group['id'], $option);
                $option = str_replace('%group_name%', $group['name_full'], $option);
                if ($group_id == $group['id'])
                    $option = str_replace('%gselected%', 'selected', $option);
                else
                    $option = str_replace('%gselected%', '', $option);
                $groups_vals .= $option;
            }
            $content = str_replace('%group_values%', $groups_vals, $content);
        }

        $search_form = "";
        $search_link_display = "none";
        if ($group_id > 0)
        {
            $curr_group = $groups[$group_id];
            $group_form_filename = "modules/catalog/templates_admin/items_search_form_".$curr_group['name_db'].".html";
            if (file_exists($group_form_filename))
                $search_form = file_get_contents($group_form_filename);
            else
            {
                $search_form = $this->generate_search_form($curr_group['id'], array());
                //заново установим шаблон
                $this->set_templates($kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'items_list.html'));
                $search_form = str_replace("%search_form_action%", $kernel->pub_redirect_for_form('show_items'), $search_form);
                $search_form = str_replace("%groupid%", $curr_group['id'], $search_form);
                $kernel->pub_file_save($group_form_filename, $search_form);
            }
            $search_link_display = "block";
        }

        $content = str_replace('%search_form%', $search_form, $content);
        $content = str_replace('%search_link_display%', $search_link_display, $content);
        $content = str_replace('%header_label%', $header_label, $content);

        //Теперь будем выводить товары, если надо
        if ($count == 0)
            $content .= $this->get_template_block('no_data');
        else
        {
            $content .= $this->get_template_block('table_header');
            $content = str_replace('%form_action%', $kernel->pub_redirect_for_form('save_selected_items'), $content);
            $cprops = CatalogCommons::get_common_props($kernel->pub_module_id_get(), true);
            $cfields_block = '';
            foreach ($cprops as $cprop)
            {
                $block = $this->get_template_block('list_prop_name');
                $block = str_replace('%list_prop_name%', $cprop['name_full'], $block);
                $cfields_block .= $block;
            }
            $content = str_replace('%list_prop_names%', $cfields_block, $content);
            $num = $offset + 1;
            $common_table_info = $kernel->db_get_table_info('_catalog_'.$kernel->pub_module_id_get().'_items');
            $enum_props_cache = array();
            foreach ($items as $item)
            {
                $line = $this->get_template_block('table_body');
                if($item['available']==1)
                    $ablock = $this->get_template_block('available_block');
                else
                    $ablock = $this->get_template_block('not_available_block');
                $line = str_replace('%ablock%',$ablock,$line);

                $cvals = '';
                foreach ($cprops as $cprop)
                {
                    $prop_value = '';
                    if ($cprop['type'] == 'number' || $cprop['type'] == "string")
                    {
                        $prop_line = $this->get_template_block('list_prop_value_edit');
                        $prop_value = htmlspecialchars($item[$cprop['name_db']]);
                    }
                    elseif ($cprop['type'] == 'enum')
                    {
                        $prop_line = $this->get_template_block('list_prop_value_select_edit');
                        $cache_key = $cprop['name_db'];
                        if (isset($enum_props_cache[$cache_key]))
                            $enum_props = $enum_props_cache[$cache_key];
                        else
                        {
                            $enum_props = $this->get_enum_set_prop_values($common_table_info[$cprop['name_db']]['Type']);
                            $enum_props_cache[$cache_key] = $enum_props;
                        }
                        $optlines = '';
                        foreach ($enum_props as $enum_option)
                        {
                            if ($item[$cprop['name_db']] == $enum_option)
                                $optline = $this->get_template_block('list_prop_value_select_option_selected_edit');
                            else
                                $optline = $this->get_template_block('list_prop_value_select_option_edit');

                            $optline = str_replace('%option_value%', $enum_option, $optline);
                            $optline = str_replace('%option_value_escaped%', htmlspecialchars($enum_option), $optline);
                            $optlines .= $optline;
                        }
                        $prop_line = str_replace('%options%', $optlines, $prop_line);
                    }
                    else
                    {
                        $prop_line = $this->get_template_block('list_prop_value');
                        $prop_value = $item[$cprop['name_db']];
                        if ($cprop['type'] == 'pict' && !empty($prop_value))
                        {
                            $path_parts = pathinfo($prop_value);
                            $path_small = $path_parts['dirname'].'/tn/'.$path_parts['basename'];
                            if (file_exists($path_small))
                                $prop_value = "<img src='/".$path_small."' width=50 />";
                        }

                    }
                    $prop_line = str_replace("%name_db%", $cprop['name_db'], $prop_line);
                    $prop_line = str_replace('%list_prop_value%', $prop_value, $prop_line);
                    $cvals .= $prop_line;
                }
                $line = str_replace('%list_prop_values%', $cvals, $line);
                $line = str_replace('%id%', $item['id'], $line);
                $line = str_replace('%group%', $groups[$item['group_id']]['name_full'], $line);

                $line = str_replace('%number%', $num++, $line);
                $content .= $line;
            }
            $content .= $this->get_template_block('table_footer');

            $cats = $this->get_child_categories(0);
            $cat_lines = "";
            foreach ($cats as $cat)
            {
                $cat_line = $this->get_template_block('category_value');
                $cat_line = str_replace("%shift%", str_repeat("&nbsp;&nbsp;", $cat['depth']), $cat_line);
                $cat_line = str_replace("%category_id%", $cat['id'], $cat_line);
                $cat_line = str_replace("%category_name%", $cat['name'], $cat_line);
                $cat_lines .= $cat_line;
            }
            $content = str_replace("%category_values%", $cat_lines, $content);
            $content = str_replace('%pages%', $this->build_pages_nav($total, $offset, $limit, $purl, 0, 'url'), $content);
        }

        if (count($groups) > 0)
        {
            $content .= $this->get_template_block('addform');
            $content = str_replace('%form_action%', $kernel->pub_redirect_for_form('item_add'), $content);
            $groups_vals = '';
            $last_add_item_groupid = 0;
            if (isset($_COOKIE['last_add_item_groupid']))
                $last_add_item_groupid = $_COOKIE['last_add_item_groupid'];
            foreach ($groups as $group)
            {
                $option = $this->get_template_block('group_value');
                $option = str_replace('%group_id%', $group['id'], $option);
                $option = str_replace('%group_name%', $group['name_full'], $option);
                if ($last_add_item_groupid == $group['id'])
                    $option = str_replace('%gselected%', 'selected', $option);
                else
                    $option = str_replace('%gselected%', '', $option);
                $groups_vals .= $option;
            }
            $content = str_replace('%group_values%', $groups_vals, $content);
        }
        else
            $content .= $this->get_template_block('no_groups');

        $content = str_replace('%group_id%', $group_id, $content);
        $content = str_replace('%redir2%', urlencode($purl.$offset), $content);
        return $content;
    }


    /**
     * Выводит список внутренних фильтров в админке
     *
     * @return string
     * /
     */
    private function show_inner_filters()
    {
        global $kernel;
        $filters = CatalogCommons::get_inner_filters();
        $this->set_templates($kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'inner_filters.html'));
        $html = $this->get_template_block('header');
        $content = '';
        if (count($filters) > 0)
        {
            $content .= $this->get_template_block('table_header');
            //$content     = str_replace('%form_action%', $kernel->pub_redirect_for_form('regen_tpls4groups'), $content);
            foreach ($filters as $filter)
            {
                $line = $this->get_template_block('table_body');
                $line = str_replace('%name%', $filter['name'], $line);
                $line = str_replace('%stringid%', $filter['stringid'], $line);
                $line = str_replace('%id%', $filter['id'], $line);
                $content .= $line;
            }
            $content .= $this->get_template_block('table_footer');
            //$content = str_replace("%form_action%", $kernel->pub_redirect_for_form('regen_tpl4itemlist'), $content);
        }
        else
            $content = $this->get_template_block('no_data');

        // Теперь заменим в итоговой форме переменную табличкой
        $html = str_replace("%table%", $content, $html);

        $groups = CatalogCommons::get_groups();
        $group_lines = '';
        foreach ($groups as $group)
        {
            $line = $this->get_template_block('group_item');
            $line = str_replace('%name%', $group['name_full'], $line);
            $line = str_replace('%id%', $group['id'], $line);
            $group_lines .= $line;
        }
        $html = str_replace("%group_items%", $group_lines, $html);
        $html = str_replace("%gen_form_action%", $kernel->pub_redirect_for_form('show_gen_search_form'), $html);
        return $html;
    }


    /**
     * Выводит список товарных групп в админке
     *
     * @return string
     * /
     */
    private function show_groups()
    {
        global $kernel;
        $groups = CatalogCommons::get_groups(null, true);
        $this->set_templates($kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'groups_list.html'));

        $html = $this->get_template_block('header');
        $content = '';
        if (count($groups) > 0)
        {
            $content .= $this->get_template_block('table_header');
            $content = str_replace('%form_action%', $kernel->pub_redirect_for_form('regen_tpls4groups'), $content);
            $num = 1;
            foreach ($groups as $group)
            {
                $line = $this->get_template_block('table_body');
                if ($group['_items_count'] == 0)
                    $delete_block = $this->get_template_block('delete_block');
                else
                    $delete_block = '';
                $line = str_replace('%delete_block%', $delete_block, $line);
                $line = $kernel->pub_array_key_2_value($line, $group);
                $line = str_replace('%number%', $num++, $line);
                $content .= $line;
            }
            $content .= $this->get_template_block('table_footer');
            $content = str_replace("%form_action%", $kernel->pub_redirect_for_form('regen_tpl4itemlist'), $content);
        }
        else
            $content = $this->get_template_block('no_data');

        // Теперь заменим в итоговой форме переменную табличкой с существующими группами (если они есть)
        $html = str_replace("%table%", $content, $html);

        return $html;
    }

    /**
     *
     *    Форма редактирования товара в админке
     *
     * @param integer $id       - idшник товара
     * @param integer $group_id - idшник шруппы, если идёт добавление нового товара
     * @param integer $id_cat   - При ID = 0, сюда может передаваться ID категории, в которой создаётся товар
     * @return string
     */
    private function show_item_form($id = 0, $group_id = 0, $id_cat = 0)
    {
        global $kernel;


        $a = (!empty($_GET['a'])) ? $kernel->pub_httpget_get('a') : 'default';

        $this->set_templates($kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'items_edit.html'));


        switch($a)
        {


            case 'option_delete':


                $where[]			= 'option_choice = '.  	$kernel->pub_httpget_get('choice');
                $where[]			= 'item_id = '.  		$kernel->pub_httpget_get('id');
                $where[]			= 'group_option_id = '. $kernel->pub_httpget_get('group_option_id');

                $where = join(' AND ', $where);


                $sql = "DELETE FROM ".$kernel->pub_prefix_get()."_catalog_".$kernel->pub_module_id_get()."_item_props_options WHERE ".$where;

                $kernel->runSQL($sql);

                $kernel->pub_redirect_refresh('item_edit&id='.$kernel->pub_httpget_get('id'));

                break;
            case 'option_group_update':


                //забираем все доп опции, чтобы знать, что тут обновлено, а что добавлено
                $options = $kernel->db_get_list_simple("_catalog_".$kernel->pub_module_id_get()."_item_props_options", 'item_id=' . $id);

                //группируем
                foreach ($options as $item)
                {
                    $options_values[$item['group_option_id']][$item['option_choice']][$item['prop_name']] = $item['option_value'];

                }

                $item = CatalogCommons::get_item_full_data($id);
                $group = CatalogCommons::get_group($item['group_id']);

                //сначала сохраним common-свойства
                $props = CatalogCommons::get_common_props($kernel->pub_module_id_get());

                //теперь свойства группы
                $props2 = CatalogCommons::get_props($item['group_id']);

                //объединим в одну
                $props = array_merge($props, $props2);

                foreach($props as $prop)
                {
                    $db_group_types[$prop['name_db']] 	= $prop['type'];
                    $db_group_params[$prop['name_db']]	= $prop['add_param'];
                }

                foreach($_POST['i'] as $group_option_id => $choice)
                {

                    foreach($choice as  $choice_id=> $item)
                    {
                        foreach($item as $name_db => $value)
                        {
                            if ($db_group_types[$name_db] == 'pict' or $db_group_types[$name_db] == 'file')
                            {


                                if(!empty($_FILES['if']['tmp_name'][$group_option_id][$choice_id][$name_db]))
                                {
                                    if ($db_group_types[$name_db] == 'pict')
                                    {

                                        $file['tmp_name'] 	= $_FILES['if']['tmp_name'][$group_option_id][$choice_id][$name_db];
                                        $file['name']		= $_FILES['if']['name'][$group_option_id][$choice_id][$name_db];
                                        $prop['add_param'] =  $db_group_params[$name_db];
                                        $value = $this->process_pict_upload($file, $prop);
                                    }
                                    else
                                    {
                                        $file['tmp_name'] 	= $_FILES['if']['tmp_name'][$group_option_id][$choice_id][$name_db];
                                        $file['name']		= $_FILES['if']['name'][$group_option_id][$choice_id][$name_db];
                                        $value = $this->process_file_upload($file);
                                    }
                                }
                            }
                            elseif($db_group_types[$name_db] == 'set')
                            {
                                $value = join(',', $value);
                            }


                            //обновление
                            if (isset($options_values[$group_option_id][$choice_id][$name_db]))
                            {
                                $where = array();
                                //если есть какие-то изменения
                                if (($options_values[$group_option_id][$choice_id][$name_db] !== $value) && ($choice_id != '%choice%'))
                                {

                                    $update['option_value'] = $value;

                                    $where[]			= 'group_option_id = '. $group_option_id;
                                    $where[]			= 'option_choice = '.  	$choice_id;
                                    $where[]			= 'item_id = '.  		$kernel->pub_httpget_get('id');
                                    $where[]			= "prop_name = '". 		$name_db."'";

                                    $where = join(' AND ', $where);

                                    $kernel->db_update_record('_catalog_'.$kernel->pub_module_id_get().'_item_props_options', $update, $where);
                                }
                            }
                            //добавление
                            else
                            {

                                if ((!empty($value) && ($choice_id != '%choice%')))
                                {
                                    $insert['option_value']	 	= $value;
                                    $insert['item_id']			= $kernel->pub_httpget_get('id');
                                    $insert['group_option_id']	= $group_option_id;
                                    $insert['option_choice']	= $choice_id;
                                    $insert['prop_name']		= $name_db;

                                    $kernel->db_add_record('_catalog_'.$kernel->pub_module_id_get().'_item_props_options', $insert);
                                }
                            }

                        }
                    }
                }


                return $kernel->pub_httppost_response('Данные успешно обновились', 'item_edit&id='.$kernel->pub_httpget_get('id'));


                break;
            case 'defualt':
            default:

                //Получим все необходимые данные
                if ($id > 0)
                {
                    $item = CatalogCommons::get_item_full_data($id);
                    $group = CatalogCommons::get_group($item['group_id']);
                    $item_catids = $this->get_item_catids($id);


                }
                else
                {
                    //Новый товар
                    $item = array();
                    $item['available'] = 1;
                    $item['insitemap'] = 1;
                    $item['id'] = 0;
                    $group = CatalogCommons::get_group($group_id);
                    $item_catids = explode(",", $group['defcatids']);
                }
                $moduleid = $kernel->pub_module_id_get();
                $tinfo = $kernel->db_get_table_info('_catalog_items_'.$moduleid.'_'.$group['name_db']);
                $tinfo = $tinfo + $kernel->db_get_table_info('_catalog_'.$moduleid.'_items');
                $props = CatalogCommons::get_props($group['id'], true);
                $this->set_templates($kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'items_edit.html'));
                //Произведём первичную сортировку массива со свойствами, чтобы шаблон был
                //оптимизирован изначально. В дальнейшем пользователь его самостоятельно поменяет
                //Получим свойства, которые
                $sort_def = array();
                $sort_def['string'] = '01';
                $sort_def['enum'] = '02';
                $sort_def['number'] = '03';
                $sort_def['pict'] = '04';
                $sort_def['text'] = '05';
                $sort_def['html'] = '06';
                $sort_def['file'] = '07';
                $sort_def['date'] = '08';
                $sort_def['set'] = '09';
                $sort_def['fileselect'] = '10';
                $sort_def['imageselect'] = '11';

                $visible_props = $this->get_group_visible_props($group['id']);
                //Прежде всего сформируем массив свойств, преобразованных для HTML вывода
                $lines = array();
                foreach ($props as $prop)
                {
                    if ($prop['group_id'] == 0 && !array_key_exists($prop['name_db'], $visible_props))
                        continue;
                    $template_line = $this->get_template_block('prop_'.$prop['type']);
                    //Запишем значения заменяемых переменных для большинства свойств
                    $pname_db = $prop['name_db'];
                    $pname_full = $prop['name_full'];
                    $origValue = isset($item[$pname_db]) ? $item[$pname_db] : "";
                    $pvalue = htmlspecialchars($origValue);

                    $need_add_water_marka = "";
                    //Теперь, для более сложных свойств нужно сделать чуть больше
                    //и возможно изменить переменную $pvalue а может и $template_line

                    switch ($prop['type'])
                    {
                        case 'enum':
                            //Получили значения для перечечления
                            $enum_vals = $this->get_enum_set_prop_values($tinfo[$prop['name_db']]['Type']);
                            $enum_options = array();
                            $not_selected_lang_var = $kernel->priv_page_textlabels_replace("[#catalog_prop_type_enum_notselect#]");
                            foreach ($enum_vals as $enum_val)
                            {
                                if (isset($item[$prop['name_db']]) && $item[$prop['name_db']] == $enum_val)
                                    $enum_templ = $this->get_template_block('prop_enum_value_selected');
                                else
                                    $enum_templ = $this->get_template_block('prop_enum_value');
                                if ($enum_val != $not_selected_lang_var)
                                    $enum_templ = str_replace("%enum_key%", htmlspecialchars($enum_val), $enum_templ);
                                else
                                    $enum_templ = str_replace("%enum_key%", "", $enum_templ);
                                $enum_templ = str_replace("%enum_val%", htmlspecialchars($enum_val), $enum_templ);
                                $enum_options[] = $enum_templ;
                            }
                            $pvalue = join("", $enum_options);
                            break;
                        case 'set':
                            //Получили значения для перечечления
                            $enum_vals = $this->get_enum_set_prop_values($tinfo[$prop['name_db']]['Type'], false);
                            $enum_options = array();
                            $currValues = explode(",", $origValue);
                            foreach ($enum_vals as $enum_val)
                            {
                                if (in_array($enum_val, $currValues))
                                    $enum_templ = $this->get_template_block('prop_set_value_checked');
                                else
                                    $enum_templ = $this->get_template_block('prop_set_value');
                                $enum_templ = str_replace("%value%", $enum_val, $enum_templ);
                                $enum_templ = str_replace("%value_escaped%", htmlspecialchars($enum_val), $enum_templ);
                                $enum_options[] = $enum_templ;
                            }
                            $pvalue = join("", $enum_options);
                            break;

                        case 'html':
                            //Создадим редактор контента
                            $editor = new edit_content();
                            $editor->set_edit_name($prop['name_db']);
                            $editor->set_simple_theme(true);
                            if (isset($item[$prop['name_db']]))
                                $editor->set_content($item[$prop['name_db']]);
                            $pvalue = $editor->create();
                            break;
                        case 'number':
                            if (isset($item[$prop['name_db']]))
                                $pvalue = $this->cleanup_number($item[$prop['name_db']]);
                            break;
                        case 'file':
                        case 'pict':
                            if ($pvalue)
                            { //Изменения нужно вносить только в том случае, если есть какой-то загруженный файл
                                $template_line = $this->get_template_block('prop_'.$prop['type'].'_edit');
                                $pvalue = "/".$item[$prop['name_db']];
                                //Нужно проставить id свойсвта, что бы можно было удалить файл
                                $template_line = str_replace('%id%', $id, $template_line);
                                //Для файлов картинок нужно вставить вопросы о необходимости
                                //добавлять водяной знак к исходной картинке, или большой картинке
                            }
                            //Если это картинка и стоит запрос на добавление вопроса о водяном знаки - добавим его
                            if ($prop['type'] == 'pict' && isset($prop['add_param']))
                            {
                                $prop['add_param'] = unserialize($prop['add_param']);
                                if (isset($prop['add_param']['source']['water_add']) && (intval($prop['add_param']['source']['water_add']) == 2))
                                    $need_add_water_marka .= $this->get_template_block('prop_'.$prop['type'].'_marka_source');

                                if (isset($prop['add_param']['big']['water_add']) && (intval($prop['add_param']['big']['water_add']) == 2))
                                    $need_add_water_marka .= $this->get_template_block('prop_'.$prop['type'].'_marka_big');
                            }
                            break;

                        case 'date':
                            if (!empty($pvalue))
                                $tvalue = strtotime($pvalue);
                            else
                                $tvalue = time();
                            $pvalue = date("d.m.Y", $tvalue);
                            break;
                    }

                    //сначало это, так как тут у нас есть важные параметры
                    $template_line = str_replace('%need_add_water_marka%', $need_add_water_marka, $template_line);

                    //Всё прошли, теперь просто заменим
                    $template_line = str_replace('%prop_value%', $pvalue, $template_line);
                    $template_line = str_replace('%prop_name_full%', $pname_full, $template_line);
                    $template_line = str_replace('%prop_name_db%', $pname_db, $template_line);
                    $lines[$sort_def[$prop['type']].'_'.$prop['id']] = $template_line;

                    $names_props[$pname_db] = $pname_full;
                    $db_props[$pname_db] 	= $pname_db;
                    $types_props[$pname_db] = $prop['type'];
                }


                //Отсортируем свойства пока в автоматическом режиме
                //ksort($lines); // временно отключено
                //Проставим чередование строк только после сортировки
                $num = 1;
                foreach ($lines as $key => $val)
                {
                    $lines[$key] = str_replace("%class%", $kernel->pub_table_tr_class($num), $val);
                    $num++;
                }
                //Начнём строить итоговоую форму
                $content = $this->get_template_block('form');
                $move2group_block=$linked_block = "";
                if ($id > 0)
                { //связанные товары только при редактировании
                    $main_prop = $this->get_common_main_prop();
                    $linked_vals = "";
                    if ($main_prop)
                    {
                        $linked_items = $this->get_linked_items($id);
                        foreach ($linked_items as $litem)
                        {
                            $linked_val = $this->get_template_block('linked_item');
                            $linked_val = str_replace("%lid%", $litem['id'], $linked_val);
                            $linked_val = str_replace("%namestring%", htmlspecialchars($litem[$main_prop]), $linked_val);
                            $linked_vals .= $linked_val;
                        }
                        $linked_data = $this->get_template_block('linked_search_block');
                    }
                    else
                        $linked_data = $this->get_template_block('linked_no_main_prop_block');
                    $linked_block = $this->get_template_block('linked');
                    $linked_block = str_replace("%linked_data%", $linked_data, $linked_block);
                    $linked_block = str_replace("%linked_items%", $linked_vals, $linked_block);


                    $groups = CatalogCommons::get_groups();
                    if(count($groups)==1)//некуда переносить - группа только одна
                        $move2group_block='';
                    else
                    {
                        $move2group_block = $this->get_template_block('move2block');
                        $m2g_lines = array();
                        foreach($groups as $mgroup)
                        {
                            if($mgroup['id']==$group['id'])
                                continue;
                            $gl = $this->get_template_block('m2g_line');
                            $gl = $kernel->pub_array_key_2_value($gl,$mgroup);
                            $m2g_lines[]=$gl;
                        }
                        $move2group_block = str_replace('%groups%',implode("\n",$m2g_lines), $move2group_block);
                        $move2group_block = str_replace('%m2g_action%',$kernel->pub_redirect_for_form('move_item2group'),$move2group_block);
                    }
                }
                $content = str_replace("%linked%", $linked_block, $content);
                $content = str_replace("%move2block%", $move2group_block, $content);

                // Отметка о том, включён товар или нет
                if ($item['available'] == 1) {
                    $content = str_replace('%isavalchecked%', 'checked', $content);
                    $content = str_replace('%insitemapdisabled%', '', $content);
                } else {
                    $content = str_replace('%isavalchecked%', '', $content);
                    $content = str_replace('%insitemapdisabled%', 'disabled="disabled"', $content);
                }

                // Отметка о том, включён ли товар в карте сайта
                if (isset($item['insitemap'])) {

                    if ($item['insitemap'] == 1)
                        $content = str_replace('%insitemapchecked%', 'checked', $content);
                    else
                        $content = str_replace('%insitemapchecked%', '', $content);

                } else {
                    $content = str_replace('%insitemapdisabled%', 'disabled="disabled"', $content);
                }

                $content = str_replace("%group.name%", $group['name_full'], $content);
                $checkedIDs = $item_catids;
                $checkedIDs[] = $id_cat;

                $property_add_parents_cats = $kernel->pub_modul_properties_get("catalog_property_add_parents_cats", $kernel->pub_module_id_get());
                if ($property_add_parents_cats['isset'] && $property_add_parents_cats['value'] == 'true')
                    $add_parents_cats = true;
                else
                    $add_parents_cats = false;

                $all_cats = CatalogCommons::get_all_categories($moduleid);
                $opened_cats = array();
                foreach ($checkedIDs as $chid)
                {
                    $way = $this->get_way2cat($chid, true, $all_cats);
                    foreach ($way as $wel)
                    {
                        $opened_cats[$wel['id']] = true;
                        if ($wel['id'] != 0 && $add_parents_cats)
                            $checkedIDs[] = $wel['id'];
                    }
                }
                $opened_cats = array_keys($opened_cats);
                $catsblock = $this->build_item_categories_block(0, $checkedIDs, $opened_cats);
                $form_action = $kernel->pub_redirect_for_form('item_save');
                $redir2 = $kernel->pub_httpget_get('redir2');
                if (!$redir2)
                {
                    if($id_cat)
                        $redir2 = 'category_items&id='.$id_cat;
                    else
                        $redir2 = 'show_items';
                }




                if (!empty($id))
                {
                    $groups = $kernel->db_get_list_simple('_catalog_'.$kernel->pub_module_id_get().'_item_props_groups', 'group_id=' . $item['group_id'] . ' OR group_id =0');


                    foreach ($groups as $item)
                    {
                        $groups_keys[$item['id']] = $item['title'];
                    }

                    $options_content = '';


                    if(!empty($groups))
                    {

                        //забираем опции
                        $options = $kernel->db_get_list_simple("_catalog_".$kernel->pub_module_id_get()."_item_props_options", 'item_id=' . $id);


                        //группируем
                        foreach ($options as $item)
                        {
                            //групируем по имени
                            $options_names[$item['group_option_id']][$item['prop_name']] = $item['prop_name'];
                            //групируем по товару
                            $options_values[$item['group_option_id']][$item['option_choice']][$item['prop_name']] = $item['option_value'];

                            $options_ids[$item['group_option_id']][$item['option_choice']][$item['prop_name']] = $item['id'];

                            $display[$item['group_option_id']][$item['prop_name']] = '1';
                        }


                        $groups_content = $this->get_template_block('option_groups');


                        foreach ($groups_keys as $group_option_id => $group)
                        {

                            $th_source = array();
                            $th_replace = array();


                            //опции для добавления
                            $option_data = '';

                            $i = 0;
                            //опции в селект меню
                            foreach ($db_props as $key => $option)
                            {
                                $option_source = array();
                                $option_replace = array();


                                $option_source[] = '%option_value%';
                                $option_replace[] = $key;

                                $option_source[] = '%option_name%';
                                $option_replace[] = $names_props[$key];


                                $option_data .= str_replace($option_source, $option_replace, $this->get_template_block('option_add_prop'));
                            }


                            $th_line = '';

                            //th
                            foreach ($db_props as $name_db => $item)
                            {


                                $prop_source = array();
                                $prop_replace = array();

                                $prop_source[] = '%list_prop_name%';
                                $prop_replace[] = $names_props[$item];
                                $prop_source[] = '%list_prop_db%';
                                $prop_replace[] = $item;
                                $prop_source[] = '%display%';

                                $prop_replace[] = (isset($options_names[$group_option_id][$name_db])) ? '' : 'none';

                                $th_line .= str_replace($prop_source, $prop_replace, $this->get_template_block('list_prop_name'));
                            }


                            $th_source[] = '%line%';
                            $th_replace[] = $th_line;

                            $th_source[] = '%options%';
                            $th_replace[] = $option_data;

                            $table_content = str_replace($th_source, $th_replace, $this->get_template_block('list_tr_th'));

                            //tr
                            if(!empty($options_values[$group_option_id]))
                            {

                                foreach ($options_values[$group_option_id] as $choice => $variant)
                                {

                                    $source_in = NULL;
                                    $replace_in = NULL;

                                    $tr = '';

                                    //td значения
                                    foreach ($db_props as $name_db => $item)
                                    {


                                        $prop_source = array();
                                        $prop_replace = array();

                                        switch ($types_props[$name_db])
                                        {

                                            case 'enum':

                                                //Получили значения для перечечления
                                                $enum_vals = $this->get_enum_set_prop_values($tinfo[$name_db]['Type']);


                                                $enum_options = array();
                                                $not_selected_lang_var = $kernel->priv_page_textlabels_replace("[#catalog_prop_type_enum_notselect#]");


                                                foreach ($enum_vals as $enum_val)
                                                {
                                                    if(isset($variant[$name_db]) && $variant[$name_db] == $enum_val)
                                                    {
                                                        $enum_templ = $this->get_template_block('prop_enum_value_selected');
                                                    }
                                                    else
                                                    {
                                                        $enum_templ = $this->get_template_block('prop_enum_value');
                                                    }
                                                    if($enum_val != $not_selected_lang_var)
                                                    {
                                                        $enum_templ = str_replace("%enum_key%", htmlspecialchars($enum_val), $enum_templ);
                                                    }
                                                    else
                                                    {
                                                        $enum_templ = str_replace("%enum_key%", "", $enum_templ);
                                                    }
                                                    $enum_templ = str_replace("%enum_val%", htmlspecialchars($enum_val), $enum_templ);
                                                    $enum_options[] = $enum_templ;
                                                }

                                                $value = join("", $enum_options);

                                                $tpl = 'list_enum_value';

                                                break;
                                            case 'set':

                                                //Получили значения для перечеcления
                                                $enum_vals = $this->get_enum_set_prop_values($tinfo[$name_db]['Type'], false);
                                                $enum_options = array();

                                                if(!empty($variant[$name_db]))
                                                {

                                                    $curr_values = explode(",", $variant[$name_db]);

                                                    foreach ($enum_vals as $enum_val)
                                                    {
                                                        if(in_array($enum_val, $curr_values))
                                                        {
                                                            $enum_templ = $this->get_template_block('prop_set_list_value_checked');
                                                        }
                                                        else
                                                        {
                                                            $enum_templ = $this->get_template_block('prop_set_list_value');
                                                        }

                                                        $enum_templ = str_replace("%option_value%", $enum_val, $enum_templ);
                                                        $enum_templ = str_replace('%prop_name_db%', 'i[' . $group_option_id . '][' . $choice . '][' . $name_db . ']', $enum_templ);
                                                        $enum_options[] = $enum_templ;
                                                    }
                                                }
                                                else
                                                {


                                                    foreach ($enum_vals as $enum_val)
                                                    {
                                                        $enum_templ = $this->get_template_block('prop_set_list_value');
                                                        $enum_templ = str_replace("%option_value%", $enum_val, $enum_templ);
                                                        $enum_templ = str_replace('%prop_name_db%', 'i[' . $group_option_id . '][' . $choice . '][' . $name_db . ']', $enum_templ);
                                                        $enum_options[] = $enum_templ;
                                                    }
                                                }

                                                $value = join("", $enum_options);


                                                $tpl = 'list_set_value';
                                                break;

                                            case 'pict':

                                                if(!empty($variant[$name_db]))
                                                {
                                                    $path_parts = pathinfo($variant[$name_db]);
                                                    $path_small = $path_parts['dirname'] . '/tn/' . $path_parts['basename'];


                                                    $prop_source[] = '%list_prop_hidden%';
                                                    //	$prop_replace[] = (isset($variant[$name_db])) ? $variant[$name_db] : '';
                                                    $prop_replace[] = $variant[$name_db];


                                                    if(file_exists($path_small))
                                                    {
                                                        $value = '<img src="/' . $path_small . '" width=50 />';
                                                    }
                                                    else
                                                    {
                                                        $value = '<img src="/' . $variant[$name_db] . '" width=50 />';
                                                    }


                                                    $tpl = 'list_pict_value';

                                                    //$value =  $variant[$name_db];
                                                }
                                                else
                                                {
                                                    $tpl = 'list_pict_value_null';

                                                    $value = '';
                                                }


                                                break;

                                            case 'date':
                                                $value = (isset($variant[$name_db])) ? $variant[$name_db] : '';
                                                if(!empty($value))
                                                {
                                                    $value = strtotime($value);
                                                }
                                                else
                                                {
                                                    $value = time();
                                                }
                                                $value = date("d.m.Y", $value);

                                                $tpl = 'list_prop_value';
                                                break;
                                            case 'text':
                                                $tpl = 'list_text_value';
                                                $value = (isset($variant[$name_db])) ? $variant[$name_db] : '';
                                                break;
                                            default:

                                                $value = (isset($variant[$name_db])) ? $variant[$name_db] : '';

                                                $tpl = 'list_prop_value';

                                                break;
                                        }


                                        $prop_source[] = '%list_prop_value%';
                                        //	$prop_replace[] = (isset($variant[$name_db])) ? $variant[$name_db] : '';
                                        $prop_replace[] = $value;

                                        $prop_source[] = '%display%';
                                        $prop_replace[] = (isset($display[$group_option_id][$name_db])) ? '' : 'none';

                                        $prop_source[] = '%list_prop_name%';
                                        $prop_replace[] = $name_db;

                                        //@todo подпаврить!
                                        $prop_source[] = '%group%';
                                        $prop_replace[] = $group_option_id;

                                        $prop_source[] = '%choice%';
                                        $prop_replace[] = $choice;


                                        $tr .= str_replace($prop_source, $prop_replace, $this->get_template_block($tpl));
                                    }


                                    $source_in[] = '%choice%';
                                    $replace_in[] = $choice;

                                    $source_in[] = '%good_id%';
                                    $replace_in[] = $kernel->pub_httpget_get('id');


                                    $source_in[] = '%delete_action%';
                                    $replace_in[] = $kernel->pub_redirect_for_form('item_edit&id=' . $kernel->pub_httpget_get('id') . '&group_option_id=' . $group_option_id . '&choice=' . $choice . '&a=option_delete');

                                    $source_in[] = '%line%';
                                    $replace_in[] = $tr;


                                    $table_content .= str_replace($source_in, $replace_in, $this->get_template_block('list_tr'));

                                }

                            }

                            //скрытая таблица для шаблона
                            $table_hidden = '';

                            $tr = '';

                            foreach ($db_props as $name_db => $item)
                            {


                                $prop_source = array();
                                $prop_replace = array();


                                switch ($types_props[$name_db])
                                {

                                    case 'enum':

                                        //Получили значения для перечечления
                                        $enum_vals = $this->get_enum_set_prop_values($tinfo[$name_db]['Type']);


                                        $enum_options = array();
                                        $not_selected_lang_var = $kernel->priv_page_textlabels_replace("[#catalog_prop_type_enum_notselect#]");


                                        foreach ($enum_vals as $enum_val)
                                        {
                                            if(isset($variant[$name_db]) && $variant[$name_db] == $enum_val)
                                            {
                                                $enum_templ = $this->get_template_block('prop_enum_value_selected');
                                            }
                                            else
                                            {
                                                $enum_templ = $this->get_template_block('prop_enum_value');
                                            }
                                            if($enum_val != $not_selected_lang_var)
                                            {
                                                $enum_templ = str_replace("%enum_key%", htmlspecialchars($enum_val), $enum_templ);
                                            }
                                            else
                                            {
                                                $enum_templ = str_replace("%enum_key%", "", $enum_templ);
                                            }
                                            $enum_templ = str_replace("%enum_val%", htmlspecialchars($enum_val), $enum_templ);
                                            $enum_options[] = $enum_templ;
                                        }

                                        $value = join("", $enum_options);

                                        $tpl = 'list_enum_value';

                                        break;
                                    case 'set':


                                        //Получили значения для перечеcления
                                        $enum_vals = $this->get_enum_set_prop_values($tinfo[$name_db]['Type'], false);
                                        $enum_options = array();

                                        foreach ($enum_vals as $enum_val)
                                        {
                                            $enum_templ = $this->get_template_block('prop_set_list_value');
                                            $enum_templ = str_replace("%option_value%", $enum_val, $enum_templ);
                                            $enum_templ = str_replace('%prop_name_db%', 'i[%group%][%choice%][%list_prop_name%]', $enum_templ);
                                            $enum_options[] = $enum_templ;
                                        }


                                        $value = join("", $enum_options);


                                        $tpl = 'list_set_hidden_value';

                                        break;

                                    case 'pict':

                                        $tpl = 'list_pict_value_null';

                                        $value = '';
                                        break;

                                    case 'date':

                                        $value = '';
                                        $tpl = 'list_prop_value';

                                        break;
                                    case 'text':
                                        $tpl = 'list_text_value';
                                        $value = '';
                                        break;
                                    default:

                                        $value = '';
                                        $tpl = 'list_prop_value';

                                        break;
                                }


                                $prop_source[] = '%list_prop_value%';
                                $prop_replace[] = $value;

                                $prop_source[] = '%display%';
                                $prop_replace[] = (isset($display[$group_option_id][$name_db])) ? '' : 'none';

                                $prop_source[] = '%list_prop_name%';
                                $prop_replace[] = $name_db;

                                //@todo подправить!
                                $prop_source[] = '%group%';
                                $prop_replace[] = $group_option_id;

                                $tr .= str_replace($prop_source, $prop_replace, $this->get_template_block($tpl));

                            }

                            $hidden_source = array();
                            $hidden_replace = array();

                            $hidden_source[] = '%line%';
                            $hidden_replace[] = $tr;


                            $table_hidden = str_replace($hidden_source, $hidden_replace, $this->get_template_block('list_tr_hidden'));

                            $group_source = array();
                            $group_replace = array();


                            $group_source[] = '%hidden_option_template%';
                            $group_replace[] = $table_hidden;

                            $group_source[] = '%option_groups_rows%';
                            $group_replace[] = $table_content;

                            $group_source[] = '%option_group_name%';
                            $group_replace[] = $groups_keys[$group_option_id];


                            //@todo подправить!
                            $group_source[] = '%group%';
                            $group_replace[] = $group_option_id;

                            $group_source[] = '%action%';
                            $group_replace[] = $kernel->pub_redirect_for_form('item_edit&id=' . $kernel->pub_httpget_get('id') . '&a=option_group_update&group=' . $group_option_id);


                            $options_content .= str_replace($group_source, $group_replace, $groups_content);

                        }
                    }


                    $content = str_replace('%props%', implode("\n", $lines), $content);
                    $content = str_replace('%categories%', $catsblock, $content);
                    $content = str_replace('%form_action%', $form_action, $content);
                    $content = str_replace('%id%', $id, $content);

                    //@todo подправить!
                    if(!empty($group_option_id))
                    {
                        $content = str_replace('%group_id%', $group_option_id, $content);
                    }
                    else
                    {
                        $content = str_replace('%group_id%', '', $content);
                    }
                    $content = str_replace('%options%', $options_content, $content);
                }
                else
                {

                    $content = str_replace('%group_id%', $group['id'], $content);
                    $content = str_replace('%options%', '', $content);
                }

                $content = str_replace('%props%', implode("\n", $lines), $content);
                $content = str_replace('%categories%', $catsblock, $content);
                $content = str_replace('%form_action%', $form_action, $content);
                $content = str_replace('%id%', $id, $content);

                $content = str_replace('%redir2%', urlencode($redir2), $content);
                return $content;

        }




    }




    private function build_item_categories_block($pid, array $checkedIDs = array(), $opened_cats = array())
    {
        global $kernel;
        $moduleid = $kernel->pub_module_id_get();
        $prfx = $kernel->pub_prefix_get();
        $sql = 'SELECT cats.id,cats.name, COUNT(subcats.id) AS _subcats_count FROM `'.$prfx.'_catalog_'.$moduleid.'_cats` AS cats
                LEFT JOIN '.$prfx.'_catalog_'.$moduleid.'_cats AS subcats ON subcats.parent_id=cats.id
                WHERE cats.`parent_id` = '.$pid.'
                GROUP BY cats.id
                ORDER BY cats.`order`';
        $cats = $kernel->db_get_list($sql);
        $categories = array();
        foreach ($cats as $cat)
        {
            $opened = in_array($cat['id'], $opened_cats);
            if ($cat['_subcats_count'] == 0 || $opened)
                $bname = 'category_line_no_childs';
            else
                $bname = 'category_line';
            if (in_array($cat['id'], $checkedIDs))
                $bname .= '_checked';

            $catline = $this->get_template_block($bname);
            if ($opened && $cat['_subcats_count'] > 0)
            {
                $blockstart = $this->get_template_block('subcats_block_start');
                $blockstart = str_replace("%id%", $cat['id'], $blockstart);
                $placeholder = $blockstart.$this->build_item_categories_block($cat['id'], $checkedIDs, $opened_cats).$this->get_template_block('subcats_block_end');
            }
            else
                $placeholder = '';
            $catline = str_replace("%placeholder%", $placeholder, $catline);
            $catline = str_replace("%id%", $cat["id"], $catline);
            $catline = str_replace("%name%", $cat["name"], $catline);
            $categories[] = $catline;
        }
        return implode("\n", $categories);
    }

    /**
     * Возвращает связанные товары
     *
     * @param integer $itemid
     * @param boolean $only_visible только видимые?
     * @param integer $offset
     * @param integer $limit
     * @return array
     */
    private function get_linked_items($itemid, $only_visible = false, $offset = 0, $limit = 0)
    {
        global $kernel;
        $return = array();

        $query = 'SELECT * FROM `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_items` WHERE ('.
            'id IN (SELECT itemid2 FROM `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_items_links` WHERE itemid1='.$itemid.') OR '.
            'id IN (SELECT itemid1 FROM `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_items_links` WHERE itemid2='.$itemid.') )';

        if ($only_visible)
            $query .= " AND `available`=1";
        $sort_field = CatalogCommons::get_common_sort_prop();
        if ($sort_field)
        {
            $query .= ' ORDER BY ISNULL(`'.$sort_field['name_db'].'`), `'.$sort_field['name_db']."` ";
            if ($sort_field['sorted'] == 2)
                $query .= " DESC ";
        }
        if ($limit > 0)
            $query .= " LIMIT ".$offset.",".$limit;
        $result = $kernel->runSQL($query);
        while ($row = mysqli_fetch_assoc($result))
            $return[$row['id']] = $row;
        mysqli_free_result($result);
        return $return;
    }

    /**
     * Возвращает количество связанных товаров
     *
     * @param integer $itemid
     * @param boolean $only_visible только видимые?
     * @return integer
     */
    private function get_linked_items_count($itemid, $only_visible = false)
    {
        global $kernel;
        $count = 0;
        $query = 'SELECT COUNT(*) AS count FROM `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_items` WHERE ('.
            'id IN (SELECT itemid2 FROM `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_items_links` WHERE itemid1='.$itemid.') OR '.
            'id IN (SELECT itemid1 FROM `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_items_links` WHERE itemid2='.$itemid.') )';

        if ($only_visible)
            $query .= " AND `available`=1";

        $result = $kernel->runSQL($query);
        if ($row = mysqli_fetch_assoc($result))
            $count = $row['count'];
        mysqli_free_result($result);
        return $count;
    }

    /**
     *    Выводит свойства категорий в админке
     *
     * @return string
     */
    private function show_cat_props()
    {
        global $kernel;
        $this->set_templates($kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'category_props.html'));
        $tinfo = $kernel->db_get_table_info('_catalog_'.$kernel->pub_module_id_get().'_cats');

        $props = CatalogCommons::get_cats_props();
        $content = $this->get_template_block('header');
        if (count($props))
        {
            $content .= $this->get_template_block('table_header');
            $num = 1;
            foreach ($props as $prop)
            {
                if ($prop['type'] == 'enum')
                {
                    $line = $this->get_template_block('property_enum');
                    $property_enum_values = array();
                    $enum_vals = $this->get_enum_set_prop_values($tinfo[$prop['name_db']]['Type']);
                    foreach ($enum_vals as $ev)
                    {
                        $property_enum_values[] = str_replace("%val%", $ev, $this->get_template_block('property_enum_value'));
                    }
                    $line = str_replace('%property_enum_values%', implode($this->get_template_block('property_enum_sep'), $property_enum_values), $line);
                }
                else
                    $line = $this->get_template_block('property');
                $line = str_replace('%property_name%', $prop['name_full'], $line);
                $line = str_replace('%property_dbname%', $prop['name_db'], $line);
                $line = str_replace('%num%', $num, $line);
                $line = str_replace('%property_type%', "[#catalog_prop_type_".$prop['type']."#]", $line);

                if ($prop['name_db'] == 'name')
                    $line = str_replace('%actions%', '', $line);
                else
                {
                    $actions = $this->get_template_block('property_del_link').$this->get_template_block('property_edit_link');
                    $actions = str_replace('%property_id%', $prop['id'], $actions);
                    $actions = str_replace('%property_name%', $prop['name_full'], $actions);
                    $line = str_replace('%actions%', $actions, $line);
                }
                $content .= $line;
                $num++;
            }
            $content .= $this->get_template_block('table_footer');
        }
        else
            $content .= $this->get_template_block('no_props');
        $content .= $this->get_template_block('add_prop_form');
        $content = str_replace('%form_action%', $kernel->pub_redirect_for_form('prop_add'), $content);
        return $content;
    }

    /**
     * Возвращает видимые общие свойства для группы
     *
     * @param integer $groupid
     * @return array
     */
    private function get_group_visible_props($groupid)
    {
        global $kernel;
        $items = array();
        $query = 'SELECT * FROM `'.$kernel->pub_prefix_get().'_catalog_visible_gprops` WHERE group_id='.$groupid.
            " AND `module_id`='".$kernel->pub_module_id_get()."'";
        $result = $kernel->runSQL($query);
        while ($row = mysqli_fetch_assoc($result))
            $items[$row['prop']] = $row;
        mysqli_free_result($result);
        return $items;
    }

    /**
     *    Выводит свойства товарной группы в админке
     *   в вывод попадают все свойства товарной группы, как общие так и не общие
     *
     * @param $id integer - idшник товарной группы (= 0 для общих свойств)
     * @return string
     */
    private function show_group_props($id)
    {
        global $kernel;

        $this->set_templates($kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'group_props_list.html'));

        $html = $this->get_template_block('header');
        $html = str_replace('%form_action%', $kernel->pub_redirect_for_form('save_gprops_order'), $html);
        $html = str_replace('%gid%', $id, $html);
        $content = '';

        //При вызове из общих свойстов $id=0, и значит мы покажем только общие свойтва
        $tinfo_global = $kernel->db_get_table_info('_catalog_'.$kernel->pub_module_id_get().'_items');
        if ($id == 0)
        {
            //Если формируется только общий список свойств
            $html = str_replace('%label_table%', $this->get_template_block('label_table_global'), $html);
            $gvisprops = array();
            $tinfo_group = array();
        }
        else
        {
            //Список свойств формируется из категории
            $group = CatalogCommons::get_group($id);
            $tinfo_group = $kernel->db_get_table_info('_catalog_items_'.$kernel->pub_module_id_get().'_'.$group['name_db']);
            $html = str_replace('%label_table%', $this->get_template_block('label_table_group'), $html);
            $html = str_replace('%name%', $group['name_full'], $html);
            $gvisprops = $this->get_group_visible_props($id);
        }
        //Возьмём все свойства, общие и не общие
        $props = CatalogCommons::get_props($id, true);
        if (count($props))
        {
            $content .= $this->get_template_block('table_header');
            $num = 1;
            foreach ($props as $prop)
            {
                //Проверим, общее это свойство или нет и поменяем заголовки и параметры таблицы
                if (intval($prop['group_id']) > 0)
                {
                    $is_global = $this->get_template_block('property_is_no_global');
                    $tinfo = $tinfo_group;
                }
                else
                {
                    $is_global = $this->get_template_block('property_is_global');
                    $tinfo = $tinfo_global;
                }


                if ($prop['type'] == 'enum' || $prop['type'] == 'set')
                { //для ENUM и SET - спецобработка
                    $line = $this->get_template_block('property_'.$prop['type']);
                    $property_enum_values = array();
                    $enum_vals = $this->get_enum_set_prop_values($tinfo[$prop['name_db']]['Type'], false);
                    foreach ($enum_vals as $ev)
                    {
                        $property_enum_values[] = str_replace("%val%", $ev, $this->get_template_block('property_'.$prop['type'].'_value'));
                    }
                    $line = str_replace('%property_'.$prop['type'].'_values%', implode($this->get_template_block('property_'.$prop['type'].'_sep'), $property_enum_values), $line);
                }
                else
                    $line = $this->get_template_block('property');

                //Добавим возможные действия
                $actions = $this->get_template_block('property_del_link').$this->get_template_block('property_edit_link');
                $actions = str_replace('%property_id%', $prop['id'], $actions);
                $actions = str_replace('%property_name%', $prop['name_full'], $actions);
                $actions = str_replace('%groupid%', $prop['group_id'], $actions);
                //Кроме того добавим id именно той группы, для готорой мы всё этос строим
                //что бы было понятно куда возвращаться после того как было вызвано редактирование
                //или удаление свойства.
                $actions = str_replace('%idg_control%', $id, $actions);


                if ($prop['group_id'] == 0 && $id != 0) { // выводим чекбокс только для общих свойств и при редактировании товарной группы
                    $viscb = $this->get_template_block('visible_in_group_checkbox');
                    if (array_key_exists($prop['name_db'], $gvisprops))
                        $viscb = str_replace("%grprop_checked%", ' checked="checked"', $viscb);
                    else
                        $viscb = str_replace("%grprop_checked%", "", $viscb);
                    $viscb = str_replace("%prop%", $prop['name_db'], $viscb);
                    $line = str_replace('%visible_in_group_checkbox%', $viscb, $line);
                } else {
                    $line = str_replace('%visible_in_group_checkbox%', '', $line);
                }

                if ($prop['type'] == 'number' || $prop['type'] == 'enum' || $prop['type'] == 'set') {
                    $visfl = $this->get_template_block('visible_in_filters_checkbox');

                    if ($prop['showinfilters'])
                        $visfl = str_replace("%grprop_checked%", ' checked="checked"', $visfl);
                    else
                        $visfl = str_replace("%grprop_checked%", "", $visfl);

                    if ($prop['group_id'] == 0 && $id != 0)
                        $visfl = str_replace("%grprop_disabled%", ' class="disabled" onclick="return false;" onkeydown="e = e || window.event; if(e.keyCode !== 9) return false;"', $visfl);
                    else
                        $visfl = str_replace("%grprop_disabled%", "", $visfl);

                    $visfl = str_replace("%prop%", $prop['name_db'], $visfl);
                    $line = str_replace('%visible_in_filters_checkbox%', $visfl, $line);
                } else {
                    $line = str_replace('%visible_in_filters_checkbox%', '', $line);
                }

                $line = str_replace('%property_name%', $prop['name_full'], $line);
                $line = str_replace('%property_dbname%', $prop['name_db'], $line);
                $line = str_replace('%property_global%', $is_global, $line);
                $line = str_replace('%num%', $num, $line);
                $line = str_replace('%property_type%', "[#catalog_prop_type_".$prop['type']."#]", $line);
                $line = str_replace('%actions%', $actions, $line);
                $line = str_replace('%order%', $prop['order'], $line);
                $line = str_replace('%property_id%', $prop['id'], $line);

                $content .= $line;
                $num++;
            }
            $content .= $this->get_template_block('table_footer');
        }
        else
            $content .= $this->get_template_block('no_props');


        $item_props_groups = $kernel->db_get_list_simple('_catalog_'.$kernel->pub_module_id_get().'_item_props_groups', 'group_id='.$id);



        $content2 = '';
        if (!empty($item_props_groups))
        {
            foreach($item_props_groups as $item)
            {


                $group_source = array();
                $group_replace = array();

                $group_source[] 	= '%id%';
                $group_replace[]	= $item['id'];

                $group_source[] 	= '%title%';
                $group_replace[]		= $item['title'];

                $group_source[] 	= '%groupid%';
                $group_replace[]		= $id;


                $content2 .= str_replace($group_source, $group_replace, $this->get_template_block('props_groups_tr'));
            }

            $source 	= array();
            $replace	= array();


            $source[] 	= '%groupid%';
            $replace[]		= $id;

            $source[] 	= '%props_groups_table%';
            $replace[]	= str_replace('%rows%', $content2, $this->get_template_block('props_groups_table'));


            $content2 = str_replace($source, $replace, $this->get_template_block('props_groups_list'));
        }
        else
        {
            $group_source = array();
            $group_replace = array();

            $group_source[] 	= '%groupid%';
            $group_replace[]	= $id;

            $content2 =  str_replace($group_source, $group_replace, $this->get_template_block('props_groups_null'));
        }



        $source 	= array();
        $replace	= array();

        $source[] 	= '%table%';
        $replace[]	= $content;

        $source[] 	= '%groupid%';
        $replace[]	= $id;

        $source[]	= '%props_groups%';
        $replace[]	= $content2;

        $html = str_replace($source, $replace, $html);
        return $html;
    }

    /**
     * Возвращает список темплейтов в папке
     *
     * @param string $path
     * @param boolean $hide_not_selected спрятать "не выбрано"?
     * @return array
     */
    private function get_template_file($path = 'modules/catalog/templates_user', $hide_not_selected = false)
    {
        global $kernel;
        $array_select = array();
        $exts = array('html', 'htm');
        $d = dir($path);
        if(!$d)
            return $array_select;
        while ($entry = $d->read())
        {
            $link = $path.'/'.$entry;
            if (is_file($link))
            {
                $ext = pathinfo($entry, PATHINFO_EXTENSION);
                if (!in_array($ext, $exts))
                    continue;
                if($kernel->is_windows)
                    $entry = iconv('windows-1251','utf-8', $entry);
                $array_select[$entry] = $entry;
            }
        }
        $d->close();
        if (!$hide_not_selected)
            $array_select[''] = '[#label_properties_no_select_option#]';
        ksort($array_select);
        return $array_select;
    }


    /**
     * Конвертирует строку запроса для внутреннего фильтра в нормальный SQL
     * добавляет LEFT JOIN'ы при необходимости, учитывает категории
     * @param array $filter - DB-запись о фильтре
     * @param array $groups - DB-запись о тов. группе
     * @param boolean $multigroup - флаг мультигрупности (в $group несколько тов. групп)
     * @return string
     */
    private function convert_inner_filter_query2sql($filter, $groups, $multigroup = false)
    {
        global $kernel;
        $query = $filter['query'];
        $moduleid = $kernel->pub_module_id_get();
        //чтобы спрятать в админке
        $query = preg_replace("/REMOVE_NOT_SET\\[(.+)\\]/sU", " $1 ", $query);
        if (stripos($query, "LIKE") !== false)
        { //LIKE [ресивер] меняем на LIKE '%ресивер%'
            $query = preg_replace("/LIKE(?:\\s*)\\[(.+)\\]/iU", "LIKE '%$1%'", $query);
        }

        //[string] меняем на 'string'
        $query = preg_replace("/\\[(.+)\\]/iU", "'$1'", $query);

        //ORDERASC (field) меняем на ORDER BY `field` ASC
        //ORDERDESC (field) меняем на ORDER BY `field` DESC
        $pattern = "/ORDER(ASC|DESC)(?:\\s*)\\(([a-z0-9_\\.]+)\\)/iU";
        $query = preg_replace($pattern, "ORDER BY $2 $1", $query);

        //GROUP (field) меняем на GROUP BY `field`
        $query = preg_replace("/GROUP(?:\\s*)\\((.+)\\)/iU", "GROUP BY `$1`", $query);


        //если были ORDER BY, GROUP BY, надо перед первым поставить закрывающую скобку и запомнить
        $pattern = "/(ORDER\\s+BY)|(GROUP\\s+BY)/iU";
        $closed = false;
        $matches = false;
        if (preg_match($pattern, $query, $matches))
        {
            $query = str_replace($matches[0], ") ".$matches[0], $query);
            $closed = true;
        }

        $cprops = CatalogCommons::get_props2(0);
        $cfields = "items.id";
        if (count($cprops) > 0)
            $cfields .= ", items.".implode(", items.", array_keys($cprops));

        if ($groups)
        {
            if($multigroup && count($groups) > 0)
            {
                // фильтр по нескольким товарным группам
                $cond = '';
                $gfields = '';
                $queryPrefix = "SELECT ".$cfields."%gfields% FROM ".$kernel->pub_prefix_get()."_catalog_".$moduleid."_items AS items";
                foreach($groups as $group)
                {
                    $gprops = CatalogCommons::get_props2($group['id']);
                    if (count($gprops) > 0)
                        $gfields .= ", ".$group['name_db'].".".implode(", ".$group['name_db'].".", array_keys($gprops));

                    $queryPrefix .= " LEFT JOIN ".$kernel->pub_prefix_get()."_catalog_items_".$moduleid."_".strtolower($group['name_db'])." AS ".$group['name_db']." ON items.ext_id = ".$group['name_db'].".id AND items.`group_id`='".$group['id']."'";

                    if(!empty($cond))
                        $cond .= " OR ";
                    else
                        $cond .= " AND (";

                    $cond .= "items.`group_id`=".$group['id'];
                }

                // закрываем скобки у запроса
                $cond .= ")";

                // показываем только товары, доступные для frontend + ограничиваемся нужными нам тов. группами
                $query = " items.`available`=1".$cond.") AND (".$query;

                // заменяем метку на поля выборки полученные ранее
                $queryPrefix = str_replace('%gfields%', $gfields, $queryPrefix);
            }
            else
            {
                // значит фильтр по одной товарной группе
                $gfields = '';
                $group = $groups;
                $gprops = CatalogCommons::get_props2($group['id']);
                if (count($gprops) > 0)
                    $gfields = ", ".$group['name_db'].".".implode(", ".$group['name_db'].".", array_keys($gprops));

                $queryPrefix = "SELECT ".$cfields.$gfields." FROM ".$kernel->pub_prefix_get()."_catalog_".$moduleid."_items AS items ".
                    "LEFT JOIN ".$kernel->pub_prefix_get()."_catalog_items_".$moduleid."_".strtolower($group['name_db'])." AS ".$group['name_db']." ON items.ext_id = ".$group['name_db'].".id ";

                //показываем только товары, доступные для frontend + ограничиваемся нужной нам тов. группой
                $query = " items.`available`=1 AND items.`group_id`=".$group['id'].") AND (".$query;
            }
        }
        else
        { //фильтр по всем товарам (общие свойства)
            $queryPrefix = "SELECT ".$cfields." FROM ".$kernel->pub_prefix_get()."_catalog_".$moduleid."_items AS items ";
            $query = " items.`available`=1 ) AND  (".$query;

        }
        if (!$closed)
            $query .= ")";

        // если все категории - никаких условий не добавляется
        // в текущей - определяем текущую и добавляем  LEFT JOIN sf_catalog_catalog1_cats AS cats ON cats.id =curcatid
        //   если текущая ==0, берём из всех
        // в выбранных LEFT JOIN sf_catalog_catalog1_cats AS cats ON cats.id IN (catid1,catid2,catid3)
        if ($filter['catids'] != "0") //все категории
        {
            if (strlen($filter['catids']) == 0) //показывать товары из текущей
            {
                $curr_cat_id = $this->get_current_catIDs();
                if ($curr_cat_id)
                {
                    if (is_array($curr_cat_id))
                        $query = " LEFT JOIN ".$kernel->pub_prefix_get()."_catalog_".$moduleid."_item2cat AS i2c ON i2c.item_id=items.id  WHERE ( i2c.cat_id IN (".implode(",", $curr_cat_id).") AND ".$query;
                    else
                        $query = " LEFT JOIN ".$kernel->pub_prefix_get()."_catalog_".$moduleid."_item2cat AS i2c ON i2c.item_id=items.id WHERE ( i2c.cat_id=".$curr_cat_id." AND ".$query;
                }
                else
                {
                    $catid = intval($kernel->pub_httpget_get("fcid")); //доп.возможность передать фильтру айдишник категории, незаметный для других методов

                    if ($catid == 0 && isset($this->current_cat_IDs[$moduleid])) //не нашли ранее, но есть catid, заполненный в карточке товара
                        $catid = $this->current_cat_IDs[$moduleid];

                    if ($catid == 0) //"выбранная категория", но категорий ни в каком виде не передано
                        return null;

                    $query = " LEFT JOIN ".$kernel->pub_prefix_get()."_catalog_".$moduleid."_item2cat AS i2c ON i2c.item_id=items.id WHERE ( i2c.cat_id=".$catid." AND ".$query;
                }

            } else { //выбранные категории
                $query = " LEFT JOIN " . $kernel->pub_prefix_get() . "_catalog_" . $moduleid . "_item2cat AS i2c ON i2c.item_id=items.id  WHERE ( i2c.cat_id IN (" . $filter['catids'] . ") AND " . $query;
            }
        } else {
            $query = " WHERE (" . $query;
        }

        if ($multigroup && count($groups) > 0 && isset($gfields)) {
            $gprops = explode(',', $gfields);
            if (count($gprops) > 0) {

                $matches = false;
                if (preg_match_all("/\`(\w+)\`\s*IN\s*\([\'|\`|\"]?+(.+)[\'|\`|\"]?\)/isU", $query, $matches, PREG_SET_ORDER)) {

                    foreach ($matches as $match) {
                        $replace = '';
                        foreach ($gprops as $gprop) {
                            $prop = explode('.', $gprop);
                            if (isset($prop[1])) {
                                if ($prop[1] == $match[1]) {
                                    $fullname = str_replace('.', '`.`', trim($gprop));
                                    $replace .= "`" . $fullname . "` IN ('" . $match[2] . "') OR ";
                                }
                            }
                        }
                        $query = preg_replace("/\`(" . $match[1] . ")\`\s*IN\s*\([\'|\`|\"]?+(.+)[\'|\`|\"]?\)/isU", $replace, $query);
                    }

                }
                if (preg_match_all("/CONCAT\s*\([\"|\'][\,][\"|\'][\,|\/]\s*[\`|\'](\w+)[\`|\'][\,|\/]\s*[\"|\'][\,][\"|\']\)\s+REGEXP\s*[\"|\'][\,|\/]\((.+)\)[\,|\/][\"|\']/isU", $query, $matches, PREG_SET_ORDER)) {

                    foreach ($matches as $match) {
                        $replace = '';
                        foreach ($gprops as $gprop) {
                            $prop = explode('.', $gprop);
                            if (isset($prop[1])) {
                                if ($prop[1] == $match[1]) {
                                    $fullname = str_replace('.', '`.`', trim($gprop));
                                    $replace .= "CONCAT(\",\", `" . $fullname . "`, \",\") REGEXP \",(" . $match[2] . "),\" OR ";
                                }
                            }
                        }
                        $query = preg_replace("/CONCAT\s*\([\"|\'][\,][\"|\'][\,|\/]\s*[\`|\'](" . $match[1] . ")[\`|\'][\,|\/]\s*[\"|\'][\,][\"|\']\)\s+REGEXP\s*[\"|\'][\,|\/]\((.+)\)[\,|\/][\"|\']/isU", $replace, $query);
                    }

                }
                if (preg_match_all("/FIND_IN_SET\(*+\'(\w+)\'\,\s+\`(\w+)\`\)\s+([\>|\<|\=|\!])\s+(\d+)/isU", $query, $matches, PREG_SET_ORDER)) {

                    foreach ($matches as $match) {
                        $replace = '';
                        foreach ($gprops as $gprop) {
                            $prop = explode('.', $gprop);
                            if (isset($prop[1])) {
                                if ($prop[1] == $match[2]) {
                                    $fullname = str_replace('.', '`.`', trim($gprop));
                                    $replace .= "FIND_IN_SET('" . $match[1] . "', `" . $fullname . "`) " . $match[3] . " " . $match[4] . " OR ";

                                }
                            }
                        }
                        $query = preg_replace("/FIND_IN_SET\(*+\'(\w+)\'\,\s+\`(" . $match[2] . ")\`\)\s+([\>|\<|\=|\!])\s+(\d+)/isU", $replace, $query);
                    }

                }

                $query = str_replace('OR )', ')', $query);
                $query = str_replace('OR  AND', 'OR ', $query);
                $query = str_replace('OR ()', '', $query);
                $query = str_replace('AND ()', '', $query);
                $query = str_replace(' )', ')', $query);
            }
        }

        $query = $queryPrefix." ".$query;
        $query = iconv("UTF-8", "UTF-8//IGNORE", $query);
        $query = trim(preg_replace('/\s\s+/', ' ', $query.""));
        return $query;
    }

    /**
     *    Сохранение внутреннего фильтра в админке
     *
     * @param $id integer - idшник внутреннего фильтра ( -1 == добавление)
     * @return mixed если всё ок - вернёт false, иначе - сообщение об ошибке
     */
    private function save_inner_filter($id)
    {
        global $kernel;

        $name = trim($kernel->pub_httppost_get('name'));
        $stringid = trim($kernel->pub_httppost_get('stringid'));
        $query = trim($kernel->pub_httppost_get('query'));
        $template = trim($kernel->pub_httppost_get('template'));
        $limit = intval($kernel->pub_httppost_get('limit'));
        $perpage = intval($kernel->pub_httppost_get('perpage'));
        $maxpages = intval($kernel->pub_httppost_get('maxpages'));
        $targetpage = trim($kernel->pub_httppost_get('targetpage'));
        $groupid = trim($kernel->pub_httppost_get('groupid'));

        if (empty($name) || empty($stringid) || empty($query) || empty($template))
            return "[#catalog_edit_inner_filter_save_msg_error#] [#catalog_edit_inner_filter_save_msg_emptyfields#]";


        $exFilter = CatalogCommons::get_inner_filter_by_stringid($stringid);
        if ($stringid == "null" || ($exFilter && $exFilter['id'] != $id))
            return "[#catalog_edit_inner_filter_save_msg_error#] [#catalog_edit_inner_filter_save_msg_stringid_exists#]";

        $cattype = $kernel->pub_httppost_get('cattype');

        switch ($cattype)
        {
            case "all":
                $catids = "0";
                break;
            case "current":
                $catids = "";
                break;
            case "selected":
            default:
                $ccbs = $_POST['ccb'];
                $cids_arr = array();
                foreach ($ccbs as $catID => $ischecked)
                {
                    if ($ischecked == 1)
                        $cids_arr[] = $catID;
                }
                $catids = implode(",", $cids_arr);
                break;

        }
        if ($id > 0)
        {
            $query = 'UPDATE `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_inner_filters` '.
                'SET `name`="'.$name.'", '.
                '`stringid`="'.$stringid.'", '.
                '`query`="'.$query.'", '.
                '`template`="'.$template.'", '.
                '`limit`="'.$limit.'", '.
                '`perpage`="'.$perpage.'", '.
                '`maxpages`="'.$maxpages.'", '.
                '`catids`="'.$catids.'", '.
                '`targetpage`="'.$targetpage.'", '.
                '`groupid`="'.$groupid.'" '.
                ' WHERE `id`='.$id;
        }
        else
        {
            $query = 'INSERT INTO `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_inner_filters` '.
                "(`name`, `stringid`, `query`, `template`, `limit`, `perpage`, `maxpages`, `catids`, `targetpage`,`groupid`) VALUES ".
                "('".$name."','".$stringid."', '".$query."', '".$template."', ".$limit.",".$perpage.",".$maxpages.",'".$catids."','".$targetpage."','".$groupid."')";
        }
        $kernel->runSQL($query);
        return false;
    }

    /**
     *    Форма редактирования внутреннего фильтра в админке
     *
     * @param $id integer - idшник внутреннего фильтра ( -1 == добавление)
     * @return string
     */
    private function show_inner_filter_form($id)
    {
        global $kernel;
        $this->set_templates($kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'inner_filter_form.html'));

        $content = $this->get_template_block('form_header');

        //По умолчанию создаём новую
        $item['name'] = '';
        $item['stringid'] = '';
        $item['template'] = '';
        $item['query'] = '';
        $item['maxpages'] = '10';
        $item['limit'] = '';
        $item['perpage'] = '10';
        $item['catids'] = '';
        $item['targetpage'] = '';
        $item['groupid'] = 0;

        $name_form = '[#catalog_edit_inner_filter_label_add#]';

        //А если редактирование - то переопределим.
        if ($id > 0)
        {
            $item = CatalogCommons::get_inner_filter($id);
            $name_form = '[#catalog_edit_inner_filter_label_edit#]';
        }

        $groups = CatalogCommons::get_groups();
        ///$groupline = $this->get_template_block('group_item');

        $selected_group = array();

        $chk = "";
        $groupselectlist = "";
        foreach ($groups as $group)
        {
            if ($item['groupid'] == $group['id'])
            {
                $chk = ' selected="selected"';
                $selected_group = $group;
            } else {
                $chk = "";
            }

            $line = $this->get_template_block('group_option_for_select');
            $line = str_replace('%name%', $group['name_full'], $line);
            $line = str_replace('%id%', $group['id'], $line);
            $line = str_replace('%sel%', $chk, $line);
            $groupselectlist .= $line;

        }

        if(empty($chk)) {
            if($item['groupid'] == "0")
                $content = str_replace('%group_all_selected%', ' selected="selected"', $content);
            else if ($item['groupid'] == "-1")
                $content = str_replace('%group_detect_selected%', ' selected="selected"', $content);
        } else {
            $content = str_replace('%group_all_selected%', '', $content);
            $content = str_replace('%group_detect_selected%', '', $content);
        }

        //Сформируем строчки селектов, для выбора шаблонов
        //пока это будет делать прям здесь, потом необходимо будет переделывать
        //через свойства.
        // Если есть дополнительные параметры обработаем их
        $arr_files = $this->get_template_file();

        //И теперь добавим собственно для шаблона списка товаров
        $html_template_list = '';
        foreach ($arr_files as $key => $val)
        {
            $chk = '';
            if ($key == $item['template'])
                $chk = ' selected="selected"';

            $line = $this->get_template_block('option_for_select');
            $line = str_replace('%name%', $val, $line);
            $line = str_replace('%key%', $key, $line);
            $line = str_replace('%sel%', $chk, $line);
            $html_template_list .= $line;
        }

        //Строим список категорий, к которым будет применяться фильтр
        $cats = $this->get_child_categories(0);
        $item_catids = explode(",", $item['catids']);
        $categories = array();
        foreach ($cats as $cat)
        {
            $repl = "";
            if (in_array($cat['id'], $item_catids))
                $repl = ' checked="checked"';
            $catline = $this->get_template_block('category_item');
            $catline = str_replace("%checked%", $repl, $catline);
            $catline = str_replace("%id%", $cat["id"], $catline);
            $catline = str_replace("%catname%", $cat["name"], $catline);
            $catline = str_replace("%shift%", str_repeat("&nbsp;&nbsp;&nbsp;", $cat['depth']), $catline);
            $categories[] = $catline;
        }

        //$item['catids'] - пустое - текущая категория; 0 - все; catid1,catid2,... - выбранные
        if ($item['catids'] == "0")
            $cattype = "all";
        elseif (empty($item['catids']))
            $cattype = "current";
        else
            $cattype = "selected";
        switch ($cattype)
        {
            case "current":
                $content = str_replace('%cattype_all%', "", $content);
                $content = str_replace('%cattype_selected%', "", $content);
                $content = str_replace('%cattype_current%', " selected", $content);
                $content = str_replace('%selectedcats_display%', "none", $content);
                break;
            case "all":
                $content = str_replace('%cattype_all%', " selected", $content);
                $content = str_replace('%cattype_selected%', "", $content);
                $content = str_replace('%cattype_current%', "", $content);
                $content = str_replace('%selectedcats_display%', "none", $content);
                break;
            case "selected":
            default:
                $content = str_replace('%cattype_all%', "", $content);
                $content = str_replace('%cattype_selected%', " selected", $content);
                $content = str_replace('%cattype_current%', "", $content);
                $content = str_replace('%selectedcats_display%', "block", $content);
                break;
        }

        $content = str_replace('%template%', $html_template_list, $content);
        $content = str_replace('%id%', $id, $content);
        $content = str_replace('%name%', $item['name'], $content);
        $content = str_replace('%groupselectlist%', $groupselectlist, $content);
        $content = str_replace('%stringid%', $item['stringid'], $content);
        $content = str_replace('%query%', $item['query'], $content);
        $content = str_replace('%limit%', $item['limit'], $content);
        $content = str_replace('%perpage%', $item['perpage'], $content);
        $content = str_replace('%maxpages%', $item['maxpages'], $content);
        $content = str_replace('%targetpage%', $item['targetpage'], $content);

        if ($selected_group)
        {
            $sql_val = $this->convert_inner_filter_query2sql($item, $selected_group);
            $sql_val = $this->process_variables_out($sql_val);
        }
        else
            $sql_val = 'n/a';
        $content = str_replace('%sql%', $sql_val, $content);

        $content = str_replace('%categories%', join("\n", $categories), $content);
        $content = str_replace('%groups_props%', CatalogCommons::get_all_group_props_html(), $content);
        $content = str_replace('%form_header_txt%', $name_form, $content);
        $content = str_replace('%form_action%', $kernel->pub_redirect_for_form('inner_filter_save'), $content);

        return $content;
    }


    /**
     *    Форма редактирования товарной группы в админке
     *
     * @param $id integer - idшник товарной группы (0 : общие свойства, -1 : добавление)
     * @return string
     */
    private function show_group_form($id)
    {
        global $kernel;
        $this->set_templates($kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'group_form.html'));

        $content = $this->get_template_block('form_header');
        //По умолчанию создаём новую
        $group['name_full'] = '';
        $group['name_db'] = '';
        $group['template_items_list'] = '';
        $group['template_items_one'] = '';
        $group['defcatids'] = '';

        $name_form = '[#catalog_edit_group_label_add#]';

        //А если редактирование - то переопределим.
        if ($id > 0)
        {
            $group = CatalogCommons::get_group($id);
            $name_form = '[#catalog_edit_group_label_edit#]';
        }

        //Сформируем строчки селектов, для выбора шаблонов
        //пока это будет делать прям здесь, потом необходимо будет переделывать
        //через свойства.
        // Если есть дополнительные параметры обработаем их
        $arr_files = $this->get_template_file();

        //И теперь добавим собственно для шаблона списка товаров
        $html_template_list = '';
        foreach ($arr_files as $key => $val)
        {
            $chk = '';
            if ($key == $group['template_items_list'])
                $chk = ' selected="selected"';

            $line = $this->get_template_block('option_for_select');
            $line = str_replace('%name%', $val, $line);
            $line = str_replace('%key%', $key, $line);
            $line = str_replace('%sel%', $chk, $line);
            $html_template_list .= $line;
        }

        //И теперь добавим собственно для шаблона карточки товаров
        $html_template_item = '';
        foreach ($arr_files as $key => $val)
        {
            $chk = '';
            if ($key == $group['template_items_one'])
                $chk = ' selected="selected"';

            $line = $this->get_template_block('option_for_select');
            $line = str_replace('%name%', $val, $line);
            $line = str_replace('%key%', $key, $line);
            $line = str_replace('%sel%', $chk, $line);
            $html_template_item .= $line;
        }

        $cats = $this->get_child_categories(0);
        $defcatids = false;
        if (isset($group['defcatids'])) {
            $defcatids = explode(",", $group['defcatids']);
        }
        $defaults_categories = array();

        foreach ($cats as $cat)
        {
            $repl = "";
            if ($defcatids) {
                if (in_array($cat['id'], $defcatids))
                    $repl = ' checked="checked"';
            }

            $catline = $this->get_template_block('default_category_item');
            $catline = str_replace("%checked%", $repl, $catline);
            $catline = str_replace("%id%", $cat["id"], $catline);
            $catline = str_replace("%catname%", $cat["name"], $catline);
            $catline = str_replace("%shift%", str_repeat("&nbsp;&nbsp;&nbsp;", $cat['depth']), $catline);
            $defaults_categories[] = $catline;
        }

        $filtercats = false;
        if (isset($group['filtercats'])) {
            $filtercats = explode(",", $group['filtercats']);
        }
        $filter_categories = array();

        foreach ($cats as $cat)
        {
            $repl = "";
            if ($filtercats) {
                if (in_array($cat['id'], $filtercats))
                    $repl = ' checked="checked"';
            }

            $catline = $this->get_template_block('filter_category_item');
            $catline = str_replace("%checked%", $repl, $catline);
            $catline = str_replace("%id%", $cat["id"], $catline);
            $catline = str_replace("%catname%", $cat["name"], $catline);
            $catline = str_replace("%shift%", str_repeat("&nbsp;&nbsp;&nbsp;", $cat['depth']), $catline);
            $filter_categories[] = $catline;
        }

        $content = str_replace('%template_list%', $html_template_list, $content);
        $content = str_replace('%template_item%', $html_template_item, $content);
        $content = str_replace('%groupid%', $id, $content);
        $content = str_replace('%name%', $group['name_full'], $content);
        $content = str_replace('%dbname%', $group['name_db'], $content);
        $content = str_replace('%defaults_categories%', join("\n", $defaults_categories), $content);
        $content = str_replace('%filter_categories%', join("\n", $filter_categories), $content);

        $content = str_replace('%form_header_txt%', $name_form, $content);
        $content = str_replace('%form_action%', $kernel->pub_redirect_for_form('group_save'), $content);
        $content = str_replace('%form_action2%', $kernel->pub_redirect_for_form('regen_tpls4groups&id_group='.$id), $content);
        return $content;
    }

    /**
     *    Вывод товаров категории в админке
     *
     * @return string
     */
    private function show_category_items()
    {
        global $kernel;

        //Взяли шаблон
        $this->set_templates($kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'category_items.html'));
        $cprops = CatalogCommons::get_common_props($kernel->pub_module_id_get(), true);


        $id_cat = $kernel->pub_httpget_get("id");
        $id_cat = intval($id_cat);
        if ($id_cat <= 0)
            return $this->get_template_block('not_select_cat');


        $cdata = CatalogCommons::get_category($id_cat);

        //Определим список достпуных товарных групп, для создания новых
        //товаров
        // Сформируем список достпных товарных групп, для добавления нового товара
        $groups = CatalogCommons::get_groups();
        $groups_vals = '';
        $selected_groupid = 0;
        if (isset($_COOKIE['last_add_item_groupid']))
            $selected_groupid = $_COOKIE['last_add_item_groupid'];
        if (count($groups) > 0)
        {
            foreach ($groups as $group)
            {
                $option = $this->get_template_block('group_value');
                $option = str_replace('%group_id%', $group['id'], $option);
                $option = str_replace('%group_name%', $group['name_full'], $option);
                if ($selected_groupid == $group['id'])
                    $option = str_replace('%gselected%', 'selected', $option);
                else
                    $option = str_replace('%gselected%', '', $option);
                $groups_vals .= $option;
            }
        }


        //Сначала построим колонки свойств, которые будут выводиться
        $colum_th = '';
        foreach ($cprops as $cprop)
            $colum_th .= '<th>'.$cprop['name_full'].'</th>';

        $offset = $this->get_offset_admin();
        $limit = $this->get_limit_admin();

        $total = $this->get_cat_items_count($id_cat, false);

        //Сформируем список товаров
        //товары категории
        $items = $this->get_cat_items($cdata['id'], $offset, $limit, false);
        $count = count($items);
        //По умолчанию вернём что нет товаров тут
        $items_html = $this->get_template_block('cat_items_empty');
        $pblock = '';
        $purl = 'category_items&id='.$id_cat.'&'.$this->admin_param_offset_name.'=';
        if ($count > 0)
        {
            //Сначала формируем массив с достпными строками товара
            $lines = array();
            $common_table_info = $kernel->db_get_table_info('_catalog_'.$kernel->pub_module_id_get().'_items');
            $enum_props_cache = array();
            foreach ($items as $item)
            {
                $line = $this->get_template_block('cat_items_line');
                if($item['available']==1)
                    $ablock = $this->get_template_block('available_block');
                else
                    $ablock = $this->get_template_block('not_available_block');
                $line = str_replace('%ablock%',$ablock,$line);

                $colum_td = '';
                foreach ($cprops as $cprop)
                {
                    /*
                    $prop_value = $item[$cprop['name_db']];
                    if ($cprop['type'] == 'number' || $cprop['type'] == "string")
                    {
                        $prop_value = htmlspecialchars($prop_value);
                        $prop_value ='<input style="width:88%; margin:0 auto; float:none;" type="text" name="iv['.$item['id'].']['.$cprop['name_db'].']" value="'.$prop_value.'">';
                    }
                    */

                    $prop_value = '';
                    if ($cprop['type'] == 'number' || $cprop['type'] == "string")
                    {
                        $prop_line = $this->get_template_block('list_prop_value_edit');
                        $prop_value = htmlspecialchars($item[$cprop['name_db']]);
                    }
                    elseif ($cprop['type'] == 'enum')
                    {
                        $prop_line = $this->get_template_block('list_prop_value_select_edit');
                        $cache_key = $cprop['name_db'];
                        if (isset($enum_props_cache[$cache_key]))
                            $enum_props = $enum_props_cache[$cache_key];
                        else
                        {
                            $enum_props = $this->get_enum_set_prop_values($common_table_info[$cprop['name_db']]['Type']);
                            $enum_props_cache[$cache_key] = $enum_props;
                        }
                        $optlines = '';
                        foreach ($enum_props as $enum_option)
                        {
                            if ($item[$cprop['name_db']] == $enum_option)
                                $optline = $this->get_template_block('list_prop_value_select_option_selected_edit');
                            else
                                $optline = $this->get_template_block('list_prop_value_select_option_edit');

                            $optline = str_replace('%option_value%', $enum_option, $optline);
                            $optline = str_replace('%option_value_escaped%', htmlspecialchars($enum_option), $optline);
                            $optlines .= $optline;
                        }
                        $prop_line = str_replace('%options%', $optlines, $prop_line);
                    }
                    else
                    {
                        $prop_line = $this->get_template_block('list_prop_value');
                        $prop_value = $item[$cprop['name_db']];
                        if ($cprop['type'] == 'pict' && !empty($prop_value))
                        {
                            $path_parts = pathinfo($prop_value);
                            $path_small = $path_parts['dirname'].'/tn/'.$path_parts['basename'];
                            if (file_exists($path_small))
                                $prop_value = "<img src='/".$path_small."' width=50 />";
                        }
                    }
                    $prop_line = str_replace("%name_db%", $cprop['name_db'], $prop_line);
                    $prop_line = str_replace('%list_prop_value%', $prop_value, $prop_line);
                    $colum_td .= $prop_line;
                }
                $line = str_replace('%colum_td%', $colum_td, $line);
                $line = str_replace('%item_order%', $item['order'], $line);
                $line = str_replace('%item_id%', $item['id'], $line);
                $line = str_replace('%group_name%', $groups[$item['group_id']]['name_full'], $line);
                $lines[] = $line;
            }
            //Теперь возьмём форму самой таблицы и вставим туда эти строки
            $items_html = $this->get_template_block('cat_items');
            $items_html = str_replace('%form_action%', $kernel->pub_redirect_for_form('category_items_save'), $items_html);
            $items_html = str_replace('%cat_items_line%', join("\n", $lines), $items_html);
            $pblock = $this->build_pages_nav($total, $offset, $limit, $purl, 0, 'url');
            $search_block = $this->get_template_block('search_block');
        }
        else
            $search_block = '';
        //Построим список категорий куда пользователь может перенести товар
        $cats = $this->get_child_categories(0, 0, array());
        $options = '';
        $cat_shift = $this->get_template_block('cat_shift');
        foreach ($cats as $cat)
        {
            $option = $this->get_template_block('cat_option');
            $option = str_replace('%cat_id%', $cat['id'], $option);
            $option = str_replace('%cat_name%', str_repeat($cat_shift, $cat['depth']).$cat['name'], $option);
            $options .= $option;
        }
        //Получили то, что всегда будет выводиться, несмотря ни на что
        $content = $this->get_template_block('header');

        //сначала итемсы, так как там ещё есть переменные
        $content = str_replace('%items%', $items_html, $content);
        //Теперь всё остальное
        $content = str_replace('%colum_th%', $colum_th, $content);
        $content = str_replace('%cname%', $cdata['name'], $content);
        $content = str_replace('%cid%', $cdata['id'], $content);
        $content = str_replace('%group_values%', $groups_vals, $content);
        $content = str_replace('%cats_options%', $options, $content);
        $content = str_replace('%numcolspan%', count($cprops) + 2, $content);
        $content = str_replace('%pages%', $pblock, $content);
        $content = str_replace('%catid%', $id_cat, $content);
        $content = str_replace('%search_block%', $search_block, $content);
        $content = str_replace('%redir2%', urlencode($purl.$offset), $content);

        return $content;
    }

    /**
     *    Форма редактирования категории в админке
     *
     * @param $id integer - id-шник категории
     * @return string
     */
    private function show_category_form($id)
    {
        global $kernel;
        $cat = CatalogCommons::get_category($id);
        $this->set_templates($kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'category_form.html'));
        $content = $this->get_template_block('header');

        $source[] 	= '%form_action%';
        $replace[]	=  $kernel->pub_redirect_for_form('category_save');
        $source[] 	= '%pid%';
        $replace[]	=  $cat['parent_id'];

        if ($cat['_hide_from_waysite'] == 1)
        {
            $source[] 	= '%_hide_from_waysitechecked%';
            $replace[]	= 'checked';
        }
        else
        {
            $source[] 	= '%_hide_from_waysitechecked%';
            $replace[]	= '';
        }

        if ($cat['_hide_from_site'] == 1)
        {
            $source[] 	= '%_hide_from_sitechecked%';
            $replace[]	= 'checked';
        }
        else
        {
            $source[] 	= '%_hide_from_sitechecked%';
            $replace[]	= '';
        }

        if ($cat['is_default'] == 1)
        {
            $source[] 	= '%isdefaultchecked%';
            $replace[]	= 'checked';
        }
        else
        {
            $source[] 	= '%isdefaultchecked%';
            $replace[]	= '';
        }

        $arr_files = $this->get_template_file();


        //список
        $html_template_list = '';
        foreach ($arr_files as $key => $val)
        {
            $chk = '';
            if ($key == $cat['tpl_items'])
                $chk = ' selected="selected"';

            $line = $this->get_template_block('option_for_select');
            $line = str_replace('%name%', $val, $line);
            $line = str_replace('%key%', $key, $line);
            $line = str_replace('%sel%', $chk, $line);
            $html_template_list .= $line;
        }

        //карточка
        $html_template_item = '';
        foreach ($arr_files as $key => $val)
        {
            $chk = '';
            if ($key == $cat['tpl_card'])
                $chk = ' selected="selected"';

            $line = $this->get_template_block('option_for_select');
            $line = str_replace('%name%', $val, $line);
            $line = str_replace('%key%', $key, $line);
            $line = str_replace('%sel%', $chk, $line);
            $html_template_item .= $line;
        }


        $source[] 	= '%template_items%';
        $replace[]	= $html_template_list;


        $source[] 	= '%template_card%';
        $replace[]	= $html_template_item;

        $content = str_replace($source, $replace, $content);


        $props = CatalogCommons::get_cats_props();
        $tinfo = $kernel->db_get_table_info('_catalog_'.$kernel->pub_module_id_get().'_cats');
        $lines = '';
        foreach ($props as $prop)
        {
            $line = $this->get_template_block('prop_'.$prop['type']);

            switch ($prop['type'])
            {
                //Для простых свойств всё просто. Просто заменяем значение в форме
                case 'text':
                case 'string':
                case 'number':
                    $line = str_replace('%prop_value%', htmlspecialchars($cat[$prop['name_db']]), $line);
                    break;

                // Выбор файла/изображения из папки `content`
                case 'fileselect':
                case 'imageselect':
                    $line = str_replace('%prop_value%', $cat[$prop['name_db']], $line);

                // Дата-время
                case 'date':
                    if ($cat[$prop['name_db']])
                    {
                        $line = str_replace('%prop_value%', date('d.m.Y', strtotime($cat[$prop['name_db']])), $line);
                    }
                    else
                        $line = str_replace('%prop_value%', date('d.m.Y'), $line);
                    break;

                //Для файла нужно вывести форму загрузки файла, и возможность удалить этот файл
                case 'file':
                    if ($cat[$prop['name_db']])
                    {
                        $prop_val = $this->get_template_block('prop_file_value');
                        $prop_val = str_replace('%path%', '/'.$cat[$prop['name_db']], $prop_val);
                    }
                    else
                        $prop_val = $this->get_template_block('prop_file_value_null');
                    $line = str_replace('%prop_value%', $prop_val, $line);

                    break;

                //Ихображение
                case 'pict':
                    if ($cat[$prop['name_db']])
                    {
                        $prop_val = $this->get_template_block('prop_pict_value');
                        $prop_val = str_replace('%path%', '/'.$cat[$prop['name_db']], $prop_val);
                    }
                    else
                        $prop_val = $this->get_template_block('prop_pict_value_null');
                    $line = str_replace('%prop_value%', $prop_val, $line);
                    break;

                //Перечесление
                case 'enum':
                    $vals = $this->get_enum_set_prop_values($tinfo[$prop['name_db']]['Type'], false);
                    $options = $this->get_template_block('prop_enum_value');
                    $options = str_replace('%enum_value%', '', $options);
                    $options = str_replace('%enum_name%', '[#catalog_edit_category_need_select_label#]', $options);
                    foreach ($vals as $val)
                    {
                        $option = $this->get_template_block('prop_enum_value');
                        $option = str_replace('%enum_value%', htmlspecialchars($val), $option);
                        $option = str_replace('%enum_name%', htmlspecialchars($val), $option);
                        if ($val == $cat[$prop['name_db']])
                            $option = str_replace('%selected%', 'selected', $option);
                        $options .= $option;
                    }
                    $options = str_replace('%selected%', '', $options);
                    $line = str_replace('%prop_enum_values%', $options, $line);
                    break;

                //Текст, редактируемый редактором контента
                case 'html':
                    $editor = new edit_content(true);
                    $editor->set_edit_name($prop['name_db']);
                    $editor->set_simple_theme();
                    $editor->set_content($cat[$prop['name_db']]);
                    $line = str_replace('%prop_value%', $editor->create(), $line);
                    break;
            }
            $line = str_replace('%prop_name_full%', $prop['name_full'], $line);
            $line = str_replace('%prop_name_db%', $prop['name_db'], $line);
            $lines .= $line;
        }
        $content = str_replace('%props%', $lines, $content);
        $content = str_replace('%id%', $cat['id'], $content);
        $content .= $this->get_template_block('footer');
        return $content;
    }


    /**
     *  Удаляет свойство из БД
     *
     * @param $propid  integer id-шник свойства
     * @param $groupid integer id-шник товарной группы
     * @return void
     */
    private function delete_prop($propid, $groupid = 0)
    {
        global $kernel;
        $prop = CatalogCommons::get_prop($propid);

        $query = 'DELETE FROM `'.$kernel->pub_prefix_get().'_catalog_item_props` WHERE `id`='.$propid;
        $kernel->runSQL($query);
        if ($groupid > 0)
        {
            $group = CatalogCommons::get_group($groupid);
            $query = 'ALTER TABLE `'.$kernel->pub_prefix_get().'_catalog_items_'.$kernel->pub_module_id_get().'_'.strtolower($group['name_db']).'` DROP COLUMN `'.$prop['name_db']."`";
            $kernel->runSQL($query);
            //$this->regenerate_group_tpls($groupid, false);

            if(file_exists($kernel->pub_site_root_get()."/modules/catalog/templates_admin/items_search_form_".$group['name_db'].".html"))
                @unlink($kernel->pub_site_root_get()."/modules/catalog/templates_admin/items_search_form_".$group['name_db'].".html");

        }
        else
        { //общее свойство
            $query = 'ALTER TABLE `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_items` DROP COLUMN `'.$prop['name_db']."`";
            $kernel->runSQL($query);


            $query = 'DELETE FROM `'.$kernel->pub_prefix_get()."_catalog_visible_gprops` WHERE `prop`='".$prop['name_db']."' ".
                "AND `module_id`='".$kernel->pub_module_id_get()."'";
            $kernel->runSQL($query);

            $this->regenerate_all_groups_tpls(false);
            //CatalogCommons::regenerate_frontend_item_common_block($kernel->pub_module_id_get(), false);
        }

        if (in_array($prop['type'], array('string', 'text', 'html', 'number', 'enum')))
        { //тип свойства был такой, который используется в шаблоне поиска
            if ($prop['group_id'] == 0)
            { //это было общее свойство
                $groups = CatalogCommons::get_groups($kernel->pub_module_id_get());
                foreach ($groups as $group)
                {
                    $this->generate_search_form($group['id'], array());
                }
            }
            else //это было свойство группы
                $this->generate_search_form($prop['group_id'], array());
        }
    }

    /**
     *  Удаляет свойство КАТЕГОРИИ
     *
     * @param $propid  integer id-шник свойства категории
     * @return void
     */
    private function delete_cat_prop($propid)
    {
        global $kernel;
        $prop = CatalogCommons::get_cat_prop($propid);
        $query = 'DELETE FROM `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_cats_props` WHERE `id`='.$propid;
        $kernel->runSQL($query);
        $query = 'ALTER TABLE `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_cats`'
            .' DROP COLUMN `'.$prop['name_db']."`";
        $kernel->runSQL($query);
    }

    /**
     *    Форма редактирования свойства КАТЕГОРИИ в админке
     *
     * @param $id integer - id-шник свойства
     * @return string
     */
    private function show_cat_prop_form($id)
    {
        global $kernel;
        $prop = CatalogCommons::get_cat_prop($id);

        if (isset($prop['add_param']))
            $prop['add_param'] = @unserialize($prop['add_param']);
        $this->set_templates($kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'category_property_form.html'));

        //Для определённых свойств нужно сделать дополнения
        $block_addparam = '';
        switch ($prop['type'])
        {
            case 'enum':
                $tinfo = $kernel->db_get_table_info('_catalog_'.$kernel->pub_module_id_get().'_cats');
                $vals = $this->get_enum_set_prop_values($tinfo[$prop['name_db']]['Type']);
                $lines = '';
                foreach ($vals as $val)
                {
                    $line = $this->get_template_block('enum_val');
                    $line = str_replace('%val_name%', $val, $line);
                    $line = str_replace('%val_name_urlencoded%', urlencode($val), $line);
                    $lines .= $line;
                }
                $block_addparam = $this->get_template_block('enum_vals');
                $block_addparam = str_replace('%lines%', $lines, $block_addparam);
                $block_addparam = str_replace('%add_enum%', $kernel->pub_redirect_for_form('cat_enum_prop_add'), $block_addparam);
                break;
            case 'pict':
                $addons_param = $this->get_template_block('addons_pict');
                //Сначала всё, что касается исходного изображения
                $source_check = "";
                if ($prop['add_param']['source']['isset'])
                    $source_check = ' checked="checked"';
                //Отметим тек значение по дабавлению водяного знака
                $_tmp_array = array("pswas0" => "", "pswas1" => "", "pswas2" => "");
                $_tmp_array['pswas'.intval($prop['add_param']['source']['water_add'])] = ' selected="selected"';
                $addons_param = $kernel->pub_array_key_2_value($addons_param, $_tmp_array);

                //Аналогично обрабатываем список возможного расположения водяного знака
                $_tmp_array = array("pswps0" => "", "pswps1" => "", "pswps2" => "", "pswps3" => "", "pswps4" => "");
                $_tmp_array['pswps'.intval($prop['add_param']['source']['water_position'])] = ' selected="selected"';
                $addons_param = $kernel->pub_array_key_2_value($addons_param, $_tmp_array);

                //Теперь оставшиеся простые значения
                $addons_param = str_replace('%source_check%', $source_check, $addons_param);
                $addons_param = str_replace('%path_source_water_path%', $prop['add_param']['source']['water_path'], $addons_param);
                $addons_param = str_replace('%pict_source_width%', $prop['add_param']['source']['width'], $addons_param);
                $addons_param = str_replace('%pict_source_height%', $prop['add_param']['source']['height'], $addons_param);

                //Теперь всё, что касается большого изображения
                $big_check = "";
                if ($prop['add_param']['big']['isset'])
                    $big_check = ' checked="checked"';

                //Отметим тек значение по дабавлению водяного знака
                $_tmp_array = array("pbwas0" => "", "pbwas1" => "", "pbwas2" => "");
                $_tmp_array['pbwas'.intval($prop['add_param']['big']['water_add'])] = ' selected="selected"';
                $addons_param = $kernel->pub_array_key_2_value($addons_param, $_tmp_array);

                //Аналогично обрабатываем список возможного расположения водяного знака
                $_tmp_array = array("pbwps0" => "", "pbwps1" => "", "pbwps2" => "", "pbwps3" => "", "pbwps4" => "");
                $_tmp_array['pbwps'.intval($prop['add_param']['big']['water_position'])] = ' selected="selected"';
                $addons_param = $kernel->pub_array_key_2_value($addons_param, $_tmp_array);

                //Теперь оставшиеся простые значения
                $addons_param = str_replace('%big_check%', $big_check, $addons_param);
                $addons_param = str_replace('%path_big_water_path%', $prop['add_param']['big']['water_path'], $addons_param);
                $addons_param = str_replace('%pict_big_width%', $prop['add_param']['big']['width'], $addons_param);
                $addons_param = str_replace('%pict_big_height%', $prop['add_param']['big']['height'], $addons_param);

                //Теперь всё, что касается малого изображения
                $small_check = "";
                if ($prop['add_param']['small']['isset'])
                    $small_check = ' checked="checked"';

                $addons_param = str_replace('%small_check%', $small_check, $addons_param);
                $addons_param = str_replace('%pict_small_width%', $prop['add_param']['small']['width'], $addons_param);
                $addons_param = str_replace('%pict_small_height%', $prop['add_param']['small']['height'], $addons_param);

                //Общее для всех параметров
                $addons_param = str_replace('%pict_path%', $prop['add_param']['pict_path'], $addons_param);
                $addons_param = str_replace('%pict_path_start%', 'content/files/'.$kernel->pub_module_id_get().'/', $addons_param);
                $block_addparam = $addons_param; //added
                break;
        }

        //Собственно вывод
        $content = $this->get_template_block('header');

        //первым делом, так как там есть метки
        $content = str_replace('%block_addparam%', $block_addparam, $content);

        $content = str_replace('%form_action%', $kernel->pub_redirect_for_form('cat_prop_save'), $content);
        $content = str_replace('%name_full%', $prop['name_full'], $content);
        $content = str_replace('%name_db%', $prop['name_db'], $content);
        $content = str_replace('%prop_type%', $kernel->pub_page_textlabel_replace('[#catalog_prop_type_'.$prop['type'].'#]'), $content);
        $content = str_replace('%id%', $prop['id'], $content);

        //$content = str_replace('%group_id%', $prop['group_id'], $content);
        return $content;
    }


    /**
     *    Форма редактирования и добавления свойства в админке
     *
     * @param integer $id_prop - id-шник свойства
     * @param integer $id_group - id-шник группы
     * @param integer $id_group_control  - id-шник
     * @return string
     */
    private function show_prop_form($id_prop = 0, $id_group = 0, $id_group_control = 0)
    {
        global $kernel;

        $id_prop = intval($id_prop);
        $id_group = intval($id_group);
        $id_group_control = intval($id_group_control);

        $this->set_templates($kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'property_form.html'));

        //Получим массив с параметрами свойства или сделаем пустой,
        //если это новое свойство
        $_tmp_label = "_add";
        $action = "prop_add";
        $off_type = '';
        $prop = array("id" => 0,
            "group_id" => $id_group,
            "name_db" => "",
            "name_full" => "",
            "type" => "string",
            "showinlist" => 0,
            "showinfilters" => 0,
            "sorted" => 0,
            "ismain" => 0,
            "visibility" => 1
        );

        if ($id_prop > 0)
        {
            $prop = CatalogCommons::get_prop($id_prop);
            $action = "prop_save";
            $_tmp_label = "";
            $off_type = 'disabled="disabled"';
        }

        //Сразу определимся с заголовком, так как у нас могут быть четыре разных варианта
        $form_label = 'label_form_group';
        if ($id_group == 0)
            $form_label = 'label_form_global';

        $form_label .= $_tmp_label;
        $form_label = $this->get_template_block(trim($form_label));

        if ($id_group > 0)
        {
            $arr = CatalogCommons::get_group($id_group);
            $form_label = str_replace('%name%', $arr['name_full'], $form_label);
        }

        // Определим какой тип поля должен быть выбран, и заменим в шаблоне
        $select_type = $this->get_template_block('prop_type');
        $select_type = str_replace('value="'.$prop['type'].'"', 'value="'.$prop['type'].'" selected="selected"', $select_type);
        $content = $this->get_template_block('header');
        if ($prop['group_id'] == 0)
        {
            $content = str_replace("%sort_enabled%", "", $content);
            $content = str_replace("%ismain_enabled%", "", $content);
            $inlist_checked = '';
            if ($prop['showinlist'] == 1)
                $inlist_checked = ' checked="checked"';
            if ($prop['ismain'] == 1)
                $content = str_replace("%ismain_checked%", " checked", $content);
        }
        else
        {
            $inlist_checked = ' disabled="true"';
            // Сортировка возможна только для общих свойств
            $content = str_replace("%sort_enabled%", " disabled='true'", $content);
            // Основным может быть только общее свойство
            $content = str_replace("%ismain_enabled%", " disabled='true'", $content);
        }
        $content = str_replace("%ismain_checked%", "", $content);


        // Установка групповой видимости только, когда новое общее свойство
        if ($id_group == 0 && $id_prop == 0) {
            $content = str_replace("%visibility_checked%", " checked", $content);
        } elseif ($id_group == 0 && $id_prop > 0) {
            $content = str_replace("%visibility_checked%", "", $content);
            $content = str_replace("%visibility_enabled%", " disabled='true'", $content);
        } else {
            $content = str_replace("%visibility_checked%", "", $content);
            $content = str_replace("%visibility_enabled%", " disabled='true'", $content);
        }

        $sort_params = "";
        $sort_param = $this->get_template_block('sort_param');
        $sort_param = str_replace("%sort_value%", 0, $sort_param);
        $sort_param = str_replace("%sort_checked%", $prop['sorted'] == 0 ? " selected" : "", $sort_param);
        $sort_param = str_replace("%sort_name%", $kernel->pub_page_textlabel_replace("[#catalog_prop_sort_no#]"), $sort_param);
        $sort_params .= $sort_param;
        $sort_param = $this->get_template_block('sort_param');
        $sort_param = str_replace("%sort_value%", 1, $sort_param);
        $sort_param = str_replace("%sort_checked%", $prop['sorted'] == 1 ? " selected" : "", $sort_param);
        $sort_param = str_replace("%sort_name%", $kernel->pub_page_textlabel_replace("[#catalog_prop_sort_asc#]"), $sort_param);
        $sort_params .= $sort_param;
        $sort_param = $this->get_template_block('sort_param');
        $sort_param = str_replace("%sort_value%", 2, $sort_param);
        $sort_param = str_replace("%sort_checked%", $prop['sorted'] == 2 ? " selected" : "", $sort_param);
        $sort_param = str_replace("%sort_name%", $kernel->pub_page_textlabel_replace("[#catalog_prop_sort_desc#]"), $sort_param);
        $sort_params .= $sort_param;
        $content = str_replace("%sort_params%", $sort_params, $content);
        $content .= $this->get_template_block('footer');

        //Теперь, в зависимости от того, какое это поле, возможно нам нужно показать
        //что-то дополнительное
        $addons_param = '';
        switch ($prop['type'])
        {
            case 'set':
            case 'enum':
                if ($prop['group_id'] == 0)
                    $tinfo = $kernel->db_get_table_info('_catalog_'.$kernel->pub_module_id_get().'_items');
                else
                {
                    $group = CatalogCommons::get_group($prop['group_id']);
                    $tinfo = $kernel->db_get_table_info('_catalog_items_'.$kernel->pub_module_id_get().'_'.$group['name_db']);
                }
                $addons_param = $this->get_template_block($prop['type'].'_vals');
                $vals = $this->get_enum_set_prop_values($tinfo[$prop['name_db']]['Type']);
                $lines = '';
                foreach ($vals as $num_val => $val)
                {
                    //пропускаем нулевое (не выбранное значение)
                    if ($num_val == 0)
                        continue;
                    $line = $this->get_template_block($prop['type'].'_val');
                    $line = str_replace('%action_del%', 'enum_prop_delete&enumval='.urlencode($val).'&propid=%id%&id_group_control='.$id_group_control, $line);
                    $line = str_replace('%val_name%', $val, $line);
                    $lines .= $line;
                }
                $addons_param = str_replace('%vals%', $lines, $addons_param);
                $addons_param = str_replace('%form_action%', $kernel->pub_redirect_for_form('enum_prop_add&id_group_control='.$id_group_control), $addons_param);
                break;
            case 'pict':
                $addons_param = $this->get_template_block('addons_pict');
                if (isset($prop['add_param']['big']['water_position']))
                    $prop['add_param']['big']['place'] = $prop['add_param']['big']['water_position'];
                if (isset($prop['add_param']['source']['water_position']))
                    $prop['add_param']['source']['place'] = $prop['add_param']['source']['water_position'];
                $addons_param = self::process_image_settings_block($addons_param, $prop['add_param']);

                //Общее для всех параметров
                $addons_param = str_replace('%pict_path%', $prop['add_param']['pict_path'], $addons_param);
                $addons_param = str_replace('%pict_path_start%', 'content/files/'.$kernel->pub_module_id_get().'/', $addons_param);
                break;
        }


        //Обязательно первым, так там есть ещё переменные
        //для картинки например новые парметры будут стоять в двух местах, так как способ их
        //ввода не отличается ни при заведении нового, ни при вводе старого
        $content = str_replace('%addons_param%', $addons_param, $content);

        //Теперь, если это поле "изображение" то тут ещё толпа параметров, которые надо
        //получить из свойств модуля
        $content = str_replace('%form_action%', $kernel->pub_redirect_for_form($action.'&id_group_control='.$id_group_control), $content);
        $content = str_replace('%form_label%', $form_label, $content);
        $content = str_replace('%name_full%', $prop['name_full'], $content);
        $content = str_replace('%name_db%', $prop['name_db'], $content);
        $content = str_replace('%prop_type%', $select_type, $content);
        $content = str_replace('%id%', $prop['id'], $content);
        $content = str_replace('%group_id%', $prop['group_id'], $content);
        $content = str_replace('%off_type%', $off_type, $content);
        $content = str_replace('%inlist_checked%', $inlist_checked, $content);

        // Теперь, параметр отвечающий за отображение в фильтрах
        $infilters_checked = '';
        if ($prop['showinfilters'] == 1)
            $infilters_checked = ' checked="checked"';

        $content = str_replace('%infilters_checked%', $infilters_checked, $content);

        return $content;
    }

    /**
     * Возвращает настройки для построения меню категорий
     *
     * @return data_tree
     */
    private function create_categories_tree()
    {
        global $kernel;
        $nodes = $this->get_categories_tree(0);

        $tree = new data_tree('[#catalog_menu_label_cats#]', '0', $nodes);
        $tree->set_action_click_node('category_items');
        $tree->set_action_move_node('category_move');
        $tree->set_drag_and_drop(true);
        $tree->set_tree_ID($kernel->pub_module_id_get());
        $tree->set_add_action('category_add');
        $tree->set_add_context_label('[#catalog_category_add_label#]');
        $tree->set_new_node_name('[#catalog_category_new_name#]');

        //$tree->not_click_main = true;
        //$tree->set_node_default($node_default);

        //Создаём контекстное меню
        $tree->contextmenu_action_remove('[#catalog_category_remove_label#]', 'category_delete', 0, '[#catalog_category_del_alert#]');
        $tree->set_name_cookie($this->structure_cookie_name);
        return $tree;
    }

    /**
     * Функция для построения меню для административного интерфейса
     *
     * @param pub_interface $menu Обьект класса для управления построением меню
     * @return boolean true
     */
    public function interface_get_menu($menu)
    {
        $menu->set_menu_block('[#catalog_menu_label_cats#]');
        $tree = $this->create_categories_tree();
        $menu->set_tree($tree);
        $menu->set_menu_block('[#catalog_menu_label#]');
        $menu->set_menu("[#catalog_menu_all_props#]", "show_group_props&id=0" /*, array('flush' => 1)*/);
        $menu->set_menu("[#catalog_menu_groups#]", "show_groups", array('flush' => 1));
        $menu->set_menu("[#catalog_menu_cat_props#]", "show_cat_props", array('flush' => 1));
        $menu->set_menu("[#catalog_menu_items#]", "show_items", array('flush' => 1));
        $menu->set_menu("[#catalog_inner_filters#]", "show_inner_filters", array('flush' => 1));
        $menu->set_menu("[#catalog_basket_order_settings_label#]", "show_order_fields", array('flush' => 1));
        $menu->set_menu("[#catalog_menu_variables#]", "show_variables", array('flush' => 1));
        $menu->set_menu("[#catalog_show_order_label#]", "show_orders", array('flush' => 1));
        $menu->set_menu("[#catalog_sshow_cupons_label#]", "show_cupons", array('flush' => 1));
        $menu->set_menu_block('[#catalog_menu_label_import_export#]');
        $menu->set_menu("[#catalog_import_csv_menuitem#]", "import_csv", array('flush' => 1));
        $menu->set_menu("[#catalog_export_csv_menuitem#]", "show_csv_export", array('flush' => 1));
        $menu->set_menu("[#catalog_menu_label_import_commerceml#]", "import_commerceml", array('flush' => 1));
        //$menu->set_menu_default('show_items');
        return true;
    }

    /**
     * Возвращает последний order вложенных категорий
     *
     * @param integer $pid id-шник родительской категории
     * @param integer $skip сколько категорий пропустить
     * @return integer
     */
    public function get_last_order_in_cat($pid, $skip = -1)
    {
        global $kernel;
        $sql = 'SELECT * FROM `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_cats` WHERE `parent_id` = '.$pid;
        if ($skip == -1) //просто последнюю, с упорядоченные по убыванию
            $sql .= ' ORDER BY `order` DESC LIMIT 1';
        else
            $sql .= ' ORDER BY `order` ASC LIMIT '.$skip.', 1';
        $query = $kernel->runSQL($sql);
        if (mysqli_num_rows($query) > 0)
        {
            $row = mysqli_fetch_assoc($query);
            return $row['order'];
        }
        else
            return 1;
    }

    /**
     * Тестирование фильтра из админки
     *
     * @param integer $id  ID-шник фильтра
     * @return string
     */
    private function test_filter($id)
    {
        $filter = CatalogCommons::get_inner_filter($id);
        if (!$filter)
            return "not found";
        if ($filter['catids'] == "") //показывать товары из текущей
            return "Ошибка - невозможно показать товары из ТЕКУЩЕЙ категории";
        if (preg_match("/param\\[(.+)\\]/iU", $filter['query']))
            return "Ошибка - невозможно показать товары без параметров формы";

        $response = $this->pub_catalog_show_inner_selection_results($filter['stringid'], false, array(), 0, true, false);
        return $response;
    }

    /**
     * Отображает поля корзины (заказа)
     *
     * @return string
     */
    private function show_order_fields()
    {
        global $kernel;
        $fields = CatalogCommons::get_order_fields();
        $this->set_templates($kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'order_fields_list.html'));
        $html = $this->get_template_block('header');
        $content = '';
        $tinfo = $kernel->db_get_table_info('_catalog_'.$kernel->pub_module_id_get().'_basket_orders');
        $html = str_replace('%form_action%', $kernel->pub_redirect_for_form('save_order_fields_order'), $html);
        $num = 1;
        if (count($fields) > 0)
        {
            $content .= $this->get_template_block('table_header');
            foreach ($fields as $field)
            {
                if ($field['type'] == 'enum')
                {
                    $line = $this->get_template_block('line_enum');
                    $property_enum_values = array();
                    $enum_vals = $this->get_enum_set_prop_values($tinfo[$field['name_db']]['Type']);
                    foreach ($enum_vals as $ev)
                    {
                        $property_enum_values[] = str_replace("%val%", $ev, $this->get_template_block('property_enum_value'));
                    }
                    $line = str_replace('%property_enum_values%', implode($this->get_template_block('property_enum_sep'), $property_enum_values), $line);
                }
                else
                    $line = $this->get_template_block('line');
                $line = str_replace('%name_db%', $field['name_db'], $line);
                $line = str_replace('%name_full%', $field['name_full'], $line);
                $line = str_replace('%order%', $field['order'], $line);
                $line = str_replace('%id%', $field['id'], $line);
                if ($field['isrequired'] == 1)
                    $line = str_replace('%property_required%', $this->get_template_block('property_is_required'), $line);
                else
                    $line = str_replace('%property_required%', $this->get_template_block('property_is_not_required'), $line);
                $line = str_replace('%property_type%', "[#catalog_prop_type_".$field['type']."#]", $line);
                //Добавим возможные действия
                $actions = $this->get_template_block('property_del_link').$this->get_template_block('property_edit_link');
                $actions = str_replace('%id%', $field['id'], $actions);
                $actions = str_replace('%name_full%', $field['name_full'], $actions);
                $line = str_replace('%actions%', $actions, $line);
                $line = str_replace('%num%', $num, $line);
                $content .= $line;
                $num++;
            }
            $content .= $this->get_template_block('table_footer');
        }
        else
            $content = $this->get_template_block('no_data');
        $html = str_replace("%table%", $content, $html);
        $html = str_replace("%form_action_tpls%", $kernel->pub_redirect_for_form('regenerate_order_tpls'), $html);
        return $html;
    }

    /**
     *    Форма редактирования и добавления поля
     *  корзины (заказа) в админке
     *
     * @param $id integer - id-шник поля
     * @return string
     */
    private function show_order_field_form($id)
    {
        global $kernel;
        $this->set_templates($kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'order_field_form.html'));
        //Если $id == 0 значит это новое поле
        if ($id > 0)
        { //редактирование
            $prop = CatalogCommons::get_order_field($id);
            $action = "order_field_save";
            $off_type = 'disabled="disabled"'; //если это редактирование, запрещаем менять тип
            $form_label = "label_form_edit";
        }
        else
        { //добавление
            $action = "order_field_add";
            $off_type = '';
            $form_label = "label_form_add";
            $prop = array("id" => 0,
                "name_db" => "",
                "name_full" => "",
                "type" => "string",
                "regexp" => "",
                "isrequired" => 0,
            );
        }
        $form_label = $this->get_template_block(trim($form_label));

        //Определим какой тип поля должен быть выбран, и заменим в шаблоне
        $select_type = $this->get_template_block('prop_type');
        $select_type = str_replace('value="'.$prop['type'].'"', 'value="'.$prop['type'].'" selected="selected"', $select_type);
        $content = $this->get_template_block('header');
        $content .= $this->get_template_block('footer');

        //Теперь, в зависимости от того, какое это поле, возможно нам нужно показать
        //что-то дополнительное
        if ($prop['type'] == 'enum')
        {
            //Если это поле "список значений", получим уже введённые значения
            $tinfo = $kernel->db_get_table_info('_catalog_'.$kernel->pub_module_id_get().'_basket_orders');

            $addons_param = $this->get_template_block('enum_vals');
            $vals = $this->get_enum_set_prop_values($tinfo[$prop['name_db']]['Type']);
            $lines = '';
            foreach ($vals as $num_val => $val)
            {
                //пропускаем нулевое (не выбранное значение)
                if ($num_val == 0)
                    continue;
                $line = $this->get_template_block('enum_val');
                $line = str_replace('%action_del%', 'order_enum_field_delete&enumval='.urlencode($val).'&id=%id%', $line);
                $line = str_replace('%val_name%', $val, $line);
                $lines .= $line;
            }
            $addons_param = str_replace('%vals%', $lines, $addons_param);
            $addons_param = str_replace('%form_action%', $kernel->pub_redirect_for_form('order_enum_field_add'), $addons_param);
        }
        else
            $addons_param = $this->get_template_block('enum_new');

        $content = str_replace('%addons_param%', $addons_param, $content);
        $content = str_replace('%form_action%', $kernel->pub_redirect_for_form($action), $content);
        $content = str_replace('%form_label%', $form_label, $content);
        $content = str_replace('%name_full%', $prop['name_full'], $content);
        $content = str_replace('%name_db%', $prop['name_db'], $content);
        $content = str_replace('%prop_type%', $select_type, $content);
        $content = str_replace('%id%', $prop['id'], $content);
        $content = str_replace('%off_type%', $off_type, $content);
        $content = str_replace('%regexp%', $prop['regexp'], $content);
        if ($prop['isrequired'] == 1)
            $content = str_replace('%req_checked%', "checked", $content);
        else
            $content = str_replace('%req_checked%', "", $content);
        return $content;
    }


    /**
     * Сохраняет поле для заказов (корзины)
     *
     * @param integer $id id-шник поля
     * @param string $name_full полное имя поля
     * @param string $name_db БД-имя поля
     * @param string $regexp регэксп для поля
     * @param string $cb_req чекбокс из POST - обязательное поле или нет
     * @return void
     */
    private function save_order_field($id, $name_full, $name_db, $regexp, $cb_req)
    {
        global $kernel;
        $prop = CatalogCommons::get_order_field($id);
        if (empty($cb_req))
            $req = 0;
        else
            $req = 1;

        $name_db = $this->translate_string2db($name_db);

        //изменилось ли БД-имя?
        if ($name_db != $prop['name_db'])
        {
            $n = 1;
            while (CatalogCommons::is_order_field_exists($name_db))
                $name_db .= $n++;
            $query = 'UPDATE `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_basket_order_fields` SET '.
                '`name_db`="'.$kernel->pub_str_prepare_set($name_db).'" WHERE `id`='.$id;
            $kernel->runSQL($query);
            $table = $kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_basket_orders';
            $values = null;
            if ($prop['type'] == 'enum')
            {
                $tinfo = $kernel->db_get_table_info($table);
                $values = $this->get_enum_set_prop_values($tinfo[$prop['name_db']]['Type']);
            }
            $db_type = $this->convert_field_type_2_db($prop['type'], $values);
            $query = 'ALTER TABLE `'.$table.'` CHANGE COLUMN `'.$prop['name_db'].'` `'.$name_db.'` '.$db_type;
            $kernel->runSQL($query);
        }

        //изменились поля?
        if ($name_full != $prop['name_full'] || $regexp != $prop['regexp'] || $req != $prop['isrequired'])
        {
            $query = 'UPDATE `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_basket_order_fields` SET '.
                '`name_full`="'.$kernel->pub_str_prepare_set($name_full).'", '.
                '`isrequired`='.$req.', '.
                '`regexp`="'.$kernel->pub_str_prepare_set($regexp).'" '.
                'WHERE `id`='.$id;
            $kernel->runSQL($query);
        }
    }


    /**
     * Добавляет поле в таблицу заказов (корзины)
     *
     * @param string $name_full полное имя поля
     * @param string $name_db БД-имя поля
     * @param string $regexp регэксп для поля
     * @param string $cb_req чекбокс из POST - обязательное поле или нет
     * @param string $ptype тип поля
     * @param string $value значения для типа enum
     * @return string БД-имя добавленного свойства
     */
    private function add_order_field($name_full, $name_db, $regexp, $cb_req, $ptype, $value)
    {
        global $kernel;
        if (empty($cb_req))
            $req = 0;
        else
            $req = 1;
        if (mb_strlen($name_db) == 0)
            $name_db = $name_full;
        $namedb = $this->translate_string2db($name_db);
        $n = 2;
        $namedb0 = $namedb;
        while (CatalogCommons::is_order_field_exists($namedb))
            $namedb = $namedb0.$n++;
        if (empty($value))
            $values = "NULL";
        else
        {
            $pva = explode("\n", $value);
            $values = array();
            foreach ($pva as $v)
            {
                $v = trim($v);
                if (mb_strlen($v) != 0)
                    $values[] = $v;
            }
            if (count($values) == 0)
                $values = "NULL";
        }

        //узнаем order у последнего св-ва и добавим 10
        $gprops = CatalogCommons::get_order_fields();
        $props_count = count($gprops);
        if ($props_count == 0)
            $order = 10;
        else
            $order = $gprops[$props_count - 1]['order'] + 10;

        //Собственно запросы по добавлению
        $query = 'INSERT INTO `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_basket_order_fields`
                  (`name_db`,`name_full`,`type`, `order`, `isrequired`, `regexp`)
                  VALUES
                  ("'.$namedb.'","'.$kernel->pub_str_prepare_set($name_full).'","'.$ptype.'", '.$order.', '.$req.', "'.$kernel->pub_str_prepare_set($regexp).'")';
        $kernel->runSQL($query);

        $query = 'ALTER TABLE `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_basket_orders`
                  ADD COLUMN `'.$namedb."` ".$this->convert_field_type_2_db($ptype, $values);
        $kernel->runSQL($query);

        return $namedb;
    }


    /**
     *  Удаляет поле из таблицы заказов (корзины)
     *
     * @param $id  integer id-шник поля
     * @return void
     */
    private function delete_order_field($id)
    {
        global $kernel;
        $prop = CatalogCommons::get_order_field($id);
        $query = 'DELETE FROM `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_basket_order_fields` WHERE `id`='.$id;
        $kernel->runSQL($query);
        $query = 'ALTER TABLE `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_basket_orders` '.
            'DROP COLUMN `'.$prop['name_db']."`";
        $kernel->runSQL($query);
    }


    /**
     * Удаляет одно из возможных значений поля типа enum
     * для таблицы заказов (корзины)
     * @param $id integer id-шник поля
     * @return void
     */
    private function delete_order_enum_field($id)
    {
        global $kernel;
        $enumval = $kernel->pub_httpget_get("enumval", false);
        $prop = CatalogCommons::get_order_field($id);
        $table = '_catalog_'.$kernel->pub_module_id_get().'_basket_orders';
        $tinfo = $kernel->db_get_table_info($table);
        $evals = $this->get_enum_set_prop_values($tinfo[$prop['name_db']]['Type'], false);
        $newevals = array();
        foreach ($evals as $eval)
        {
            if ($eval != $enumval)
                $newevals[] = $eval;
        }
        $query = 'UPDATE `'.$kernel->pub_prefix_get().$table.'` SET `'.$prop['name_db'].'`=NULL '.
            'WHERE `'.$prop['name_db'].'`="'.$kernel->pub_str_prepare_set($enumval).'"';
        $kernel->runSQL($query);
        $query = 'ALTER TABLE `'.$kernel->pub_prefix_get().$table.'` CHANGE `'.$prop['name_db'].'` `'.$prop['name_db'].'` '.$this->convert_field_type_2_db('enum', $newevals);
        $kernel->runSQL($query);
    }


    /**
     *  К уже существующему полю таблицы заказов (корзины)
     *  типа enum  добавляет новое значение
     * @param $id  integer id-шник поля
     * @return void
     */
    private function add_order_enum_field($id)
    {
        global $kernel;
        $enumval = $kernel->pub_httppost_get("enumval", false);
        $prop = CatalogCommons::get_order_field($id);
        $table = '_catalog_'.$kernel->pub_module_id_get().'_basket_orders';
        $tinfo = $kernel->db_get_table_info($table);
        $evals = $this->get_enum_set_prop_values($tinfo[$prop['name_db']]['Type'], false);
        $evals[] = $enumval;
        $query = 'ALTER TABLE `'.$kernel->pub_prefix_get().$table.'` CHANGE `'.$prop['name_db'].'` `'.$prop['name_db'].'` '.$this->convert_field_type_2_db('enum', $evals);
        $kernel->runSQL($query);
    }


    /**
     * Пересоздаёт шаблон для списка товаров в корзине
     *
     * @param $out_filename string имя генерируемого файла шаблона
     * @return void
     */
    private function regenerate_basket_items_tpl($out_filename)
    {
        global $kernel;

        if (empty($out_filename))
            return;
        $outfilename = CatalogCommons::get_templates_user_prefix().$out_filename;

        //только общие свойства
        $props = CatalogCommons::get_props(0, false);
        $blank_tpl = $kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'frontend_templates/blank_basket.html');
        $this->set_templates($blank_tpl);

        $outfh = '';
        $outfh .= "<!-- @list -->\n";
        $outfh .= $this->get_template_block('list');
        //блок, если в списке нет элементов
        $outfh .= "\n\n\n<!-- @list_null -->\n";
        $outfh .= $this->get_template_block('list_null')."\n\n\n";

        $row_odd = $this->get_template_block('row_odd');
        $row_even = $this->get_template_block('row_even');
        if (!empty($row_odd) && !empty($row_even))
        {
            $row_block = "\n\n\n<!-- @row_odd -->\n";
            $row_block .= $this->get_template_block('row_odd');
            $row_block .= "\n\n\n<!-- @row_even -->\n";
            $row_block .= $this->get_template_block('row_even');
        }
        else
        {
            $row_block = "\n\n\n<!-- @row -->\n";
            $row_block .= $this->get_template_block('row');
        }

        $props_db_names = array();
        //$props_txt = '';
        foreach ($props as $prop)
        {
            $props_db_names[] = "%".$prop['name_db']."%";
            $block = "<!-- @".$prop['name_db']." -->\n".$this->get_template_block('prop_'.$prop['type'])."\n\n";
            $block .= "<!-- @".$prop['name_db']."_null -->\n".$this->get_template_block($prop['type'].'_null')."\n\n";
            $block = str_replace("%prop_name_full%", $prop['name_full'], $block);
            $block = str_replace("%prop_value%", '%'.$prop['name_db'].'_value%', $block);
            $outfh .= $block;
        }

        $row_block = str_replace("%props%", implode("\n", $props_db_names), $row_block);
        $outfh .= $row_block;
        $outfh .= "\n\n\n<!-- @row_delimeter -->\n";
        $outfh .= $this->get_template_block('row_delimeter');
        $kernel->pub_file_save($outfilename, $outfh);
    }


    /**
     * Пересоздаёт шаблон для списка формы оформления заказа в корзине
     *
     * @param $out_filename string имя генерируемого файла шаблона
     * @return void
     */
    private function regenerate_basket_order_tpl($out_filename)
    {
        global $kernel;

        if (empty($out_filename))
            return;
        $outfilename = CatalogCommons::get_templates_user_prefix().$out_filename;

        $tinfo = $kernel->db_get_table_info('_catalog_'.$kernel->pub_module_id_get().'_basket_orders');

        $props = CatalogCommons::get_order_fields();
        $blank_tpl = $kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'frontend_templates/blank_order.html');
        $this->set_templates($blank_tpl);

        $outfh = "<!-- @form -->\n".$this->get_template_block('header');

        foreach ($props as $prop)
        {
            $block = $this->get_template_block('prop_'.$prop['type'])."\n\n";
            if ($prop['type'] == 'enum')
            {
                $enum_vals = $this->get_enum_set_prop_values($tinfo[$prop['name_db']]['Type'], false);
                $str = "\n";
                foreach ($enum_vals as $eval)
                {
                    $line = $this->get_template_block("enum_item")."\n";
                    $line = str_replace("%enum_val%", $eval, $line);
                    $str .= $line;
                }
                $block = str_replace("%enum_vals%", $str, $block);
            }
            $block = str_replace("%prop_name_full%", $prop['name_full'], $block);
            $block = str_replace("%prop_name_db%", $prop['name_db'], $block);
            $block = str_replace("%prop_value%", '%'.$prop['name_db'].'_value%', $block);
            if ($prop['isrequired'] == 1)
                $block = str_replace("%req%", $this->get_template_block('required'), $block);
            else
                $block = str_replace("%req%", $this->get_template_block('not_required'), $block);

            $outfh .= $block;
        }

        $outfh .= "\n".$this->get_template_block('footer');

        $outfh .= "\n\n<!-- @order_received -->\n".$this->get_template_block("order_received");
        $outfh .= "\n\n<!-- @required_field_not_filled -->\n".$this->get_template_block("required_field_not_filled");
        $outfh .= "\n\n<!-- @incorrect_field_value -->\n".$this->get_template_block("incorrect_field_value");
        $outfh .= "\n\n<!-- @no_basket_items -->\n".$this->get_template_block("no_basket_items");
        $outfh .= "\n\n<!-- @no_email_error -->\n".$this->get_template_block("no_email_error");

        $kernel->pub_file_save($outfilename, $outfh);
    }

    /**
     * Удаляет фильтр из БД
     *
     * @param integer $id
     * @return void
     */
    private function delete_inner_filter($id)
    {
        global $kernel;
        $kernel->runSQL('DELETE FROM `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_inner_filters` WHERE `id`='.intval($id));
    }

    /**
     * Отображает форму для экспорта в CSV-файл
     * @param string $template шаблон
     * @return string
     */
    private function show_csv_export_form($template = '')
    {
        global $kernel;
        $this->set_templates($kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'export_csv.html'));
        $content = $this->get_template_block('import_form');
        $content = str_replace('%form_action%', $kernel->pub_redirect_for_form('make_csv_export'), $content);
        $props_html = CatalogCommons::get_all_group_props_html(true);
        $content = str_replace('%all_props%', $props_html, $content);
        $content = str_replace('%template_value%', htmlspecialchars($template), $content);
        $filters = CatalogCommons::get_inner_filters();
        $filter_lines = '';
        foreach ($filters as $filter)
        {
            $filter_line = $this->get_template_block('filter_items');
            $filter_line = str_replace('%filterid%', $filter['id'], $filter_line);
            $filter_line = str_replace('%filtername%', htmlspecialchars($filter['name']), $filter_line);
            $filter_lines .= $filter_line;
        }
        $content = str_replace('%filter_items%', $filter_lines, $content);
        return $content;
    }

    /**
     * Создаёт CSV-текст из шаблона для экспорта
     *
     * @param string $template
     * @param integer $filterid
     * @return string
     */
    private function make_csv_export($template, $filterid)
    {
        global $kernel;
        //сначала создадим заголовок
        $headers = array();
        $matches = false;
        $match = false;

        $moduleId = $kernel->pub_module_id_get();

        $fields_separator = ";";
        $hitems = explode($fields_separator, $template);
        $gprops = CatalogCommons::get_all_group_props_array();
        $allprops = array();
        foreach ($gprops as $gprop)
        {
            $allprops = array_merge($allprops, $gprop['props']);
        }

        $st_cat_list_names = false;
        $st_cat_list_ids = false;

        foreach ($hitems as $hitem)
        {
            $hitem = trim($hitem);
            if (empty($hitem))
                continue;

            if ($hitem == "%__category_names__%") {
                $headers[] = "Category Names";
                $st_cat_list_names = true;
                continue;
            }
            if ($hitem == "%__category_ids__%") {
                $headers[] = "Category IDs";
                $st_cat_list_ids = true;
                continue;
            }

            if (preg_match("|\\%linked_items\\[(.+)\\]\\%|iU", $hitem, $match))
            { //для связанных товаров, %linked_items[/]%

                $hitem = str_replace($match[0], $kernel->pub_page_textlabel_replace('[#catalog_linked_items_list_label#]'), $hitem);
                $headers[] = $hitem;
                continue;
            }

            if (preg_match_all("|\\%([a-z0-9_-]+)\\%|iU", $hitem, $matches))
            {
                foreach ($matches[1] as $match)
                {
                    if (!isset($allprops[$match]))
                        $replacement = $match;
                    else
                        $replacement = $allprops[$match]['name_full'];
                    $hitem = str_replace("%".$match."%", $replacement, $hitem);
                }
                $headers[] = $hitem;
            }
        }
        if (count($headers) == 0)
            return "";
        $lines = implode($fields_separator, $headers)."\n";
        if ($filterid == 0)
            $items = $this->get_items(0, 0, 0, false);
        else
        {
            $items = array();
            $filter = CatalogCommons::get_inner_filter($filterid);
            if (empty($filter['groupid']))
                $group = false;
            else
                $group = CatalogCommons::get_group(intval($filter['groupid']));
            $sql = $this->process_variables_out($filter['query']);
            $tmpStr = "";
            $sql = $this->prepare_inner_filter_sql($sql, array(), $tmpStr);
            $filter['query'] = $sql;
            $query = $this->convert_inner_filter_query2sql($filter, $group);
            $result = $kernel->runSQL($query);
            if ($result)
            {
                while ($row = mysqli_fetch_assoc($result))
                    $items[] = $row;
                mysqli_free_result($result);
            }
        }
        if($st_cat_list_ids || $st_cat_list_names){
            $category_hierarchy = array();
            $cats = $kernel->db_get_list_simple("_catalog_".$moduleId."_cats", "true", "`id`, `parent_id`, `name`");
            foreach ($cats as $v){
                $category_hierarchy[$v["id"]] = array("id" => $v["id"], "name" => $v["name"], "parent_id" => $v["parent_id"],"res_ids" => array($v["id"]),"res_names" => "");
            }

            foreach ($category_hierarchy as $k=>$v){
                $tmp_cat = $v["parent_id"];
                $tmp_name_array = array($v["name"]);
                while($tmp_cat!= 0 && isset($category_hierarchy[$tmp_cat])){
                    $category_hierarchy[$k]["res_ids"][] =  $category_hierarchy[$tmp_cat]["id"];
                    $tmp_name_array[] =  $category_hierarchy[$tmp_cat]["name"];
                    $tmp_cat = $category_hierarchy[$tmp_cat]["parent_id"];
                }
                $tmp_name_array = array_reverse($tmp_name_array);
                $category_hierarchy[$k]["res_names"] = implode("//", $tmp_name_array);;
            }
        }

        $main_prop = $this->get_common_main_prop();
        foreach ($items as $item)
        {
            $itemFD = CatalogCommons::get_item_full_data($item['id']);
            $line = $template;
            foreach ($itemFD as $prop => $value)
            {
                if ($prop == "id") //чтобы айдишник был общий, а не из таблицы тов. группы
                    $value = $itemFD["commonid"];
                elseif (is_string($value))
                    $value = '"'.str_replace('"', '\\"', $value).'"';
                $line = str_replace("%".$prop."%", $value, $line);

            }
            if ($st_cat_list_names || $st_cat_list_ids){
                $item_cats = $kernel->db_get_list_simple("_catalog_".$moduleId."_item2cat", "item_id = ".$item['id']);
                if(count($item_cats) == 0){
                    $line = str_replace('%__category_names__%', '', $line);
                    $line = str_replace('%__category_ids_%', '', $line);
                } else {
                    $array_item_cat_names = array();
                    $array_item_cat_ids = array();
                    foreach ($item_cats as $v) {
                        if($st_cat_list_names) $array_item_cat_names[] = $category_hierarchy[$v["cat_id"]]["res_names"];
                        if($st_cat_list_ids) $array_item_cat_ids[] = $v["cat_id"];
                    }

                    if($st_cat_list_names){
                        $line = str_replace('%__category_names__%', implode("||", $array_item_cat_names), $line);
                    }

                    if($st_cat_list_ids){
                        $array_item_cat_ids = array_unique($array_item_cat_ids);
                        $line = str_replace('%__category_ids__%', implode("||", $array_item_cat_ids), $line);
                    }
                }
            }

            if ($main_prop && preg_match("|\\%linked_items\\[(.+)\\]\\%|iU", $line, $match))
            { //
                $separator = $match[1];
                $linked_items = $this->get_linked_items($itemFD["commonid"], false, 0, 0);
                $linked_names = array();
                foreach ($linked_items as $litem)
                {
                    $linked_names[] = '"'.str_replace('"', '\\"', $litem[$main_prop]).'"';
                }
                $line = str_replace($match[0], implode($separator, $linked_names), $line);
            }
            //заменим оставшиеся метки на пустую строку

            $line = preg_replace("|\\%([a-z0-9_-]+)\\%|iU", "", $line);
            $lines .= $line."\n";
        }
        if (!empty($_POST['cp1251']))
        {
            $lines = iconv('UTF-8', 'CP1251//IGNORE', $lines);
        }
        return $lines;
    }

    private function show_gen_search_form($groupid)
    {
        global $kernel;
        $this->set_templates($kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'gen_search_form.html'));
        $content = $this->get_template_block('form');
        $content = str_replace('%form_action%', $kernel->pub_redirect_for_form('gen_search_form'), $content);
        $props = CatalogCommons::get_props($groupid, true);
        $group = CatalogCommons::get_group($groupid);

        $templates = $this->get_template_file('modules/catalog/templates_user', true);
        $tpllines = "";
        foreach ($templates as $tpl)
        {
            $line = $this->get_template_block('template-line');
            $line = str_replace("%value%", $tpl, $line);
            $tpllines .= $line;
        }
        $content = str_replace("%template-lines%", $tpllines, $content);

        $content = str_replace("%group_name%", $group['name_full'], $content);
        $content = str_replace("%groupid%", $groupid, $content);
        $props_lines = "";
        foreach ($props as $prop)
        {
            switch ($prop['type'])
            {
                case 'string':
                case 'html':
                case 'text':
                    $pline = $this->get_template_block('string-prop');
                    break;
                case 'number':
                    $pline = $this->get_template_block('number-prop');
                    break;
                case 'enum':
                    $pline = $this->get_template_block('enum-prop');
                    break;
                case 'file':
                case 'pict':
                    $pline = $this->get_template_block('file-prop');
                    break;
                default:
                    continue 2;
            }
            $pline = str_replace("%propnamedb%", $prop['name_db'], $pline);
            $pline = str_replace("%propnamefull%", $prop['name_full'], $pline);
            $props_lines .= $pline;
        }
        $content = str_replace("%props_lines%", $props_lines, $content);
        return $content;
    }

    /**
     * Создаёт форму поиска
     * если не указаны свойства, значит это регенерация формы для поиска из админки, используем visible props тов. группы
     * @param integer $groupid
     * @param array $genprops
     * @return string
     */
    private function generate_search_form($groupid, $genprops)
    {
        global $kernel;
        if (!$genprops)
        {
            $genprops = array();
            $props = CatalogCommons::get_props($groupid, true);
            $visible_props = $this->get_group_visible_props($groupid);
            foreach ($props as $prop)
            {

                if ($prop['group_id'] == 0 && !array_key_exists($prop['name_db'], $visible_props))
                    continue;

                if ($prop['type'] == 'string' || $prop['type'] == 'text' || $prop['type'] == 'html')
                    $genprops[$prop['name_db']] = "like";
                elseif ($prop['type'] == 'number')
                    $genprops[$prop['name_db']] = "diapazon";
                elseif ($prop['type'] == 'enum')
                    $genprops[$prop['name_db']] = "select";
            }
        }

        $this->set_templates($kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'search_form_tpl.html'));
        $all_group_props = CatalogCommons::get_props2($groupid) + CatalogCommons::get_props2(0);
        $group = CatalogCommons::get_group($groupid);
        $content = $this->get_template_block("header");
        foreach ($genprops as $propname => $processtype)
        {
            if ($processtype == "ignore")
                continue;
            $dbtype = $all_group_props[$propname]['type'];
            switch ($dbtype)
            {
                case 'number':
                    if ($processtype == "accurate")
                        $block = $this->get_template_block("numberinput");
                    else
                    { //diapazon
                        $block = $this->get_template_block("diapazon");
                        $block = str_replace("%diapazons_from%", $this->get_template_block("diapazons_from"), $block);
                        $block = str_replace("%diapazons_to%", $this->get_template_block("diapazons_to"), $block);
                    }
                    break;
                case 'string':
                case 'html':
                case 'text':
                    $block = $this->get_template_block("textinput");
                    break;
                case 'enum':
                    //processtype = select | radio | checkbox
                    $block = $this->get_template_block($processtype);
                    $tinfo = $kernel->db_get_table_info('_catalog_items_'.$kernel->pub_module_id_get().'_'.$group['name_db']);
                    $tinfo = $tinfo + $kernel->db_get_table_info('_catalog_'.$kernel->pub_module_id_get().'_items');
                    if ($processtype == "checkbox")
                        $enum_vals = $this->get_enum_set_prop_values($tinfo[$all_group_props[$propname]['name_db']]['Type'], false);
                    else
                    {
                        $enum_vals = $this->get_enum_set_prop_values($tinfo[$all_group_props[$propname]['name_db']]['Type'], true);
                        if (!empty($enum_vals))
                            $enum_vals[0] = "";
                    }
                    $strings = "";
                    foreach ($enum_vals as $val)
                    {
                        $string = $this->get_template_block($processtype."s");
                        $string = str_replace("%value%", $val, $string);
                        $string = str_replace("%valuekey%", $val, $string);
                        $strings .= $string."\n";
                    }
                    $block = str_replace("%".$processtype."s%", $strings, $block);
                    break;
                case 'file':
                case 'pict':
                    //processtype = select | radio | checkbox
                    $block = $this->get_template_block($processtype);
                    if ($processtype == "checkbox")
                    { //чекбокс в данном случае один
                        $string = $this->get_template_block($processtype."s");
                        $string = str_replace("%valuekey%", $propname, $string);
                        $string = str_replace("%value%", $kernel->pub_page_textlabel_replace("[#catalog_gen_search_form_file_necessary_label#]"), $string);
                        $block = str_replace("%".$processtype."s%", $string, $block);
                    }
                    else
                    { //а селектов и радио - 2
                        $strings = "";
                        $string = $this->get_template_block($processtype."s");
                        $string = str_replace("%value%", $kernel->pub_page_textlabel_replace("[#catalog_gen_search_form_file_necessary_label#]"), $string);
                        $string = str_replace("%valuekey%", $propname, $string);
                        $strings .= $string;
                        $string = $this->get_template_block($processtype."s");
                        $string = str_replace("%value%", $kernel->pub_page_textlabel_replace("[#catalog_gen_search_form_file_notnecessary_label#]"), $string);
                        $string = str_replace("%valuekey%", "", $string);
                        $strings .= $string;
                        $block = str_replace("%".$processtype."s%", $strings, $block);
                    }
                    break;
                default:
                    $block = '';
                    break;
            }


            $block = str_replace("%prop_full_name%", $all_group_props[$propname]['name_full'], $block);
            $block = str_replace("%prop_db_name%", $all_group_props[$propname]['name_db'], $block);
            $content .= $block."\n";
        }
        $content .= $this->get_template_block("footer");
        return $content;
    }


    /**
     * Генерирует запрос для внутреннего фильтра
     *
     * @param integer $groupid
     * @param array $props
     * @return string
     */
    private function generate_inner_filter_query($groupid, $props)
    {
        $all_group_props = CatalogCommons::get_props2($groupid) + CatalogCommons::get_props2(0);
        $group = CatalogCommons::get_group($groupid);
        $conditions = array("true");
        $prfx = " AND ";

        $all_group_props['id'] = array('type' => 'number', 'group_id' => 0); //для ID-шника товара

        foreach ($props as $propname => $processtype)
        {
            if ($processtype == "ignore" || !isset($all_group_props[$propname]))
                continue;

            if ($all_group_props[$propname]['group_id'] != 0) //НЕ общее свойство
                $fieldname = "`".$group['name_db']."`.`".$propname."`";
            else
                $fieldname = "`items`.`".$propname."`";

            $dbtype = $all_group_props[$propname]['type'];
            if ($dbtype == "file" || $dbtype == "pict")
            {
                if ($all_group_props[$propname]['group_id'] != 0) //НЕ общее свойство
                    $conditions[] = "REMOVE_NOT_SET[".$prfx." `".$group['name_db']."`.`param[".$propname."]` IS NOT NULL]";
                else //общее
                    $conditions[] = "REMOVE_NOT_SET[".$prfx." `param[".$propname."]` IS NOT NULL]";
                continue;
            }
            switch ($processtype)
            {
                case 'accurate':
                    $conditions[] = "REMOVE_NOT_SET[".$prfx.$fieldname."='param[".$propname."]']";
                    break;
                case 'like':
                    $conditions[] = "REMOVE_NOT_SET[".$prfx.$fieldname." LIKE '%param[".$propname."]%']";
                    break;
                case 'diapazon':
                    $conditions[] = "REMOVE_NOT_SET[".$prfx.$fieldname.">=param[".$propname."_from]]";
                    $conditions[] = "REMOVE_NOT_SET[".$prfx.$fieldname."<=param[".$propname."_to]]";
                    break;
                case 'select':
                case 'radio':
                    $conditions[] = "REMOVE_NOT_SET[".$prfx.$fieldname."='param[".$propname."]']";
                    break;
                case 'checkbox':
                    $conditions[] = "REMOVE_NOT_SET[".$prfx.$fieldname." IN (param[".$propname."])]";
                    break;
            }
        }
        $query = implode("\n", $conditions);
        return $query;
    }

    /**
     * Генерирует внутренний фильтр и сохраняет в БД
     *
     * @param string $name
     * @param string $templatename
     * @param integer $groupid
     * @param array $props
     * @return void
     */
    private function generate_inner_filter($name, $templatename, $groupid, $props)
    {
        global $kernel;
        $stringid0 = $kernel->pub_translit_string($name);
        $stringid = $stringid0;
        $i = 1;
        while (CatalogCommons::get_inner_filter_by_stringid($stringid))
        {
            $stringid = $stringid0.++$i;
        }
        $query = $kernel->pub_str_prepare_set($this->generate_inner_filter_query($groupid, $props));
        $limit = "0";
        $perpage = "0";
        $maxpages = "0";
        $targetpage = "";
        $catids = "0";
        $iquery = 'INSERT INTO `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_inner_filters` '.
            "(`name`, `stringid`, `query`, `template`, `limit`, `perpage`, `maxpages`, `catids`, `targetpage`,`groupid`) VALUES ".
            "('".$name."','".$stringid."', '".$query."', '".$templatename."', ".$limit.",".$perpage.",".$maxpages.",'".$catids."','".$targetpage."','".$groupid."')";
        $kernel->runSQL($iquery);
    }



    /**
     * Публичный метод для вывода списка связанных товаров
     *
     * @param string $template
     * @param integer $limit
     * @return string
     */
    public function pub_catalog_show_linked_items($template, $limit, $header = '')
    {
        global $kernel;
        $itemid = intval($kernel->pub_httpget_get($this->frontend_param_item_id_name));
        if ($itemid < 1)
            return "";
        //если нет параметра - айдишника товара, нам нечего выводить

        $this->set_templates($kernel->pub_template_parse($template));

        $offset = $this->get_offset_user();
        $items = array_values($this->get_linked_items($itemid, true, $offset, $limit));

        $count = count($items);
        $total = $this->get_linked_items_count($itemid, true);

        if ($count == 0)
        {
            return $this->get_template_block('list_null');
            //return "Нет товаров";
        }
        $itemids = array();
        //проверим, принадлежат ли все товары к одной товарной группе
        //и сохраним id-шники
        $is_single_group = true;

        $groupid = $items[0]['group_id'];
        foreach ($items as $item)
        {
            $itemids[] = $item['ext_id'];
            if ($groupid != $item['group_id'])
            {
                $is_single_group = false;
                break;
            }
        }

        if ($is_single_group)
        {
            $group = CatalogCommons::get_group($groupid);
            $items2 = $this->get_group_items($group['name_db'], $itemids);
            $newitems = array();
            foreach ($items as $item)
            {
                $tmp = $item + $items2[$item['ext_id']];
                $tmp['id'] = $item['id'];
                $newitems[] = $tmp;
            }
            $items = $newitems;
            $props = CatalogCommons::get_props($groupid, true);
        }
        else
        {
            $props = CatalogCommons::get_common_props($kernel->pub_module_id_get(), false);
        }

        //при этом, надо пройтись по свойствам и если там есть
        //картинки, то нужно продублировать их свойствами большого
        //и маленького изображения

        for ($p = 0; $p < count($props); $p++)
        {
            if (isset($props[$p]['add_param']))
                $props[$p]['add_param'] = unserialize($props[$p]['add_param']);
        }

        //Сформируем сначала строки с товарами
        $rows = '';
        $curr = 1;
        foreach ($items as $item)
        {
            if ($curr % 2 == 0) //строка - чётная
                $odd_even = "even";
            else
                $odd_even = "odd";
            //Взяли блок строчки
            $block = $this->get_template_block('row_'.$odd_even);
            if (empty($block))
                $block = $this->get_template_block('row');
            $block = str_replace("%odd_even%", $odd_even, $block);
            //Теперь ищем переменные свойств и заменяем их
            $block = $this->process_item_props_out($item, $props, $block, CatalogCommons::get_group($groupid));

            $cat_id = CatalogCommons::get_item2cat_id($item['id']);
            if(!$cat_id)
                $cat_id = $this->get_current_catid(true);

            $curr_cat = CatalogCommons::get_category($cat_id);

            //также заменяем свойства категорий
            $block = $this->cats_props_out($curr_cat['id'], $block);

            if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                $block = str_replace("%link%", '/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($curr_cat['name']).'-c'.$curr_cat['id'].'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $block);
            else
                $block = str_replace("%link%", $kernel->pub_page_current_get().'.html?'.$this->frontend_param_item_id_name.'='.$item['id'], $block);

            $rows .= $block;
            $curr++;
        }

        $content = $this->get_template_block('list');
        $content = str_replace("%rows%", $rows, $content);
        $content = str_replace("%list_header%", $header, $content);
        $content = str_replace("%total_in_cat%", $total, $content);

        if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
            $purl = str_replace("%link%", '/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($curr_cat['name']).'-c'.$curr_cat['id'].'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html?'.$this->frontend_param_offset_name.'=', $block);
        else
            $purl = $kernel->pub_page_current_get().'.html?'.$this->frontend_param_item_id_name.'='.$itemid.'&'.$this->frontend_param_offset_name.'=';

        $content = str_replace('%pages%', $this->build_pages_nav($total, $offset, $limit, $purl, 0, 'link'), $content);
        $content = $this->process_variables_out($content);
        $content = $this->replace_current_page_url($content);
        //очистим оставшиеся метки
        $content = $this->clear_left_labels($content);
        return $content;
    }

    /**
     * Публичный метод для вывода оставшихся товаров в карточке товара
     *
     * @param string $template
     * @param integer $limit
     * @return string
     */
    public function pub_catalog_show_other_items($template, $limit, $header = '')
    {
        global $kernel;
        $catid = intval($kernel->pub_httpget_get($this->frontend_param_cat_id_name));
        $itemid = intval($kernel->pub_httpget_get($this->frontend_param_item_id_name));

        // если нет айдишника товара и айдишника категории - нам нечего выводить
        if ($catid < 1 && $itemid < 1)
            return "";

        $this->set_templates($kernel->pub_template_parse($template));
        $offset = $this->get_offset_user();
        //$items = array_values($this->get_linked_items($itemid, true, $offset, $limit));
        $total = $this->get_cat_items_count($catid, true);
        $items = $this->get_cat_items($catid, $offset, $limit, true);

        $count = count($items) - 1; // отнимем 1, т.к. предполагается, что у нас уже отображается один товар из категории
        if ($count == 0)
            return $this->get_template_block('list_null');

        //проверим, принадлежат ли все товары к одной товарной группе и сохраним id-шники
        $itemids = array();
        $is_single_group = true;
        $groupid = $items[0]['group_id'];

        foreach ($items as $item)
        {
            if($itemid !== $item['id']) {
                $itemids[] = $item['ext_id'];
                if ($groupid != $item['group_id'])
                {
                    $is_single_group = false;
                    break;
                }
            }
        }

        if ($is_single_group)
        {
            $group = CatalogCommons::get_group($groupid);
            $items2 = $this->get_group_items($group['name_db'], $itemids);
            $newitems = array();
            foreach ($items as $item)
            {
                if($itemid !== $item['id']) {
                    $tmp = $item + $items2[$item['ext_id']];
                    $tmp['id'] = $item['id'];
                    $newitems[] = $tmp;
                }
            }
            $items = $newitems;
            $props = CatalogCommons::get_props($groupid, true);
        }
        else
            $props = CatalogCommons::get_common_props($kernel->pub_module_id_get(), false);


        //при этом, надо пройтись по свойствам и если там есть
        //картинки, то нужно продублировать их свойствами большого
        //и маленького изображения

        for ($p = 0; $p < count($props); $p++)
        {
            if (isset($props[$p]['add_param']))
                $props[$p]['add_param'] = unserialize($props[$p]['add_param']);
        }

        //Сформируем сначала строки с товарами
        $rows = '';
        $curr = 1;
        foreach ($items as $item)
        {
            if($itemid == $item['id'])
                continue;

            if ($curr % 2 == 0) //строка - чётная
                $odd_even = "even";
            else
                $odd_even = "odd";
            //Взяли блок строчки
            $block = $this->get_template_block('row_'.$odd_even);
            if (empty($block))
                $block = $this->get_template_block('row');
            $block = str_replace("%odd_even%", $odd_even, $block);

            //Теперь ищем переменные свойств и заменяем их
            $block = $this->process_item_props_out($item, $props, $block, CatalogCommons::get_group($groupid));


            $cat_id = CatalogCommons::get_item2cat_id($item['id']);
            if(!$cat_id)
                $cat_id = $this->get_current_catid(true);

            $curr_cat = CatalogCommons::get_category($cat_id);

            //также заменяем свойства категорий
            $block = $this->cats_props_out($curr_cat['id'], $block);

            if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                $block = str_replace("%link%", '/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($curr_cat['name']).'-c'.$curr_cat['id'].'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $block);
            else
                $block = str_replace("%link%", $kernel->pub_page_current_get().'.html?'.$this->frontend_param_item_id_name.'='.$item['id'], $block);

            $rows .= $block;
            $curr++;
        }

        $content = $this->get_template_block('list');
        $content = str_replace("%row%", $rows, $content);
        $content = str_replace("%header%", $header, $content);
        $content = str_replace("%total_in_cat%", $total, $content);

        if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
            $purl = str_replace("%link%", '/'.$kernel->pub_page_current_get().'/'.$kernel->pub_pretty_url($curr_cat['name']).'-c'.$curr_cat['id'].'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html?'.$this->frontend_param_offset_name.'=', $block);
        else
            $purl = $kernel->pub_page_current_get().'.html?'.$this->frontend_param_item_id_name.'='.$itemid.'&'.$this->frontend_param_offset_name.'=';

        $content = str_replace('%pages%', $this->build_pages_nav($total, $offset, $limit, $purl, 0, 'link'), $content);
        $content = $this->process_variables_out($content);
        $content = $this->replace_current_page_url($content);
        //очистим оставшиеся метки
        $content = $this->clear_left_labels($content);
        return $content;
    }

    /**
     * Возвращает "основное" общее свойство
     *
     * @return string|boolean
     */
    private function get_common_main_prop()
    {
        global $kernel;
        $row = $kernel->db_get_record_simple('_catalog_item_props', '`module_id`="'.$kernel->pub_module_id_get().'" AND `group_id`=0 AND `ismain`=1', 'name_db');
        if ($row)
            return $row['name_db'];
        return false;
    }

    private function show_variables()
    {
        global $kernel;
        $items = CatalogCommons::get_variables();
        $this->set_templates($kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'variables.html'));
        $html = $this->get_template_block('header');
        $content = '';
        if (count($items) > 0)
        {
            $content .= $this->get_template_block('table_header');
            //$content     = str_replace('%form_action%', $kernel->pub_redirect_for_form('regen_tpls4groups'), $content);
            foreach ($items as $item)
            {
                $line = $this->get_template_block('table_body');
                $line = str_replace('%name_db%', $item['name_db'], $line);
                $line = str_replace('%name_full%', $item['name_full'], $line);
                $line = str_replace('%value%', $item['value'], $line);
                $content .= $line;
            }
            $content .= $this->get_template_block('table_footer');
        }
        else
            $content = $this->get_template_block('no_data');
        $html = str_replace("%table%", $content, $html);
        return $html;
    }

    private function show_variable_form($name_db)
    {
        global $kernel;
        $this->set_templates($kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'variable.html'));
        if (empty($name_db))
        {
            $form_header_txt = '[#catalog_edit_variable_header_label#]';
            $item = array("name_db" => "", "name_full" => "", "value" => "");
        }
        else
        {
            $form_header_txt = '[#catalog_edit_variable_header_label#]';
            $item = CatalogCommons::get_variable($name_db);
        }

        $content = $this->get_template_block('form');
        $content = str_replace('%form_header_txt%', $form_header_txt, $content);
        $content = str_replace('%value%', $item['value'], $content);
        $content = str_replace('%name_full%', $item['name_full'], $content);
        $content = str_replace('%name_db%', $item['name_db'], $content);
        $content = str_replace('%prev_name_db%', $item['name_db'], $content);
        $content = str_replace('%form_action%', $kernel->pub_redirect_for_form('variable_save'), $content);
        return $content;
    }

    /**
     *  сохраняет настройки для импорта commerceml
     *  пока в сессии, позже в БД
     * @param $settings array
     * @return void
     */
    private function save_import_commerceml_settings($settings)
    {
        $_SESSION['import_commerceml_settings'] = serialize($settings);
    }

    /**
     *  возвращает настройки для импорта commerceml
     *  пока в сессии, позже в БД
     * @return array
     */
    private function get_import_commerceml_settings()
    {
        /*
        global $kernel;
        $settings=$kernel->pub_session_get('import_commerceml_settings');
        if (!$settings)
        {
            //если нет сохранённых - создадим заглушки
            $settings=array('groupid'=>-1,'catid'=>-1,'assoc'=>array(),'priceField'=>'','pricePerField'=>'');
        }
        */
        if (isset($_SESSION['import_commerceml_settings']))
            return unserialize($_SESSION['import_commerceml_settings']);
        else
            return array('groupid' => -1, 'catid' => -1, 'assoc' => array(), 'priceField' => '', 'pricePerField' => '', 'priceType' => '', 'ID_field' => '', 'name_field' => '');
    }

    private function import_commerceml_buildprops_select($commonProps, $selected)
    {
        $options = '';
        foreach ($commonProps as $cprop)
        {
            if ($cprop['name_db'] == $selected)
                $line = $this->get_template_block('common_prop_option_selected');
            else
                $line = $this->get_template_block('common_prop_option');

            $line = str_replace('%name_db%', $cprop['name_db'], $line);
            $line = str_replace('%name_full%', $cprop['name_full'], $line);
            $options .= $line;
        }
        return $options;
    }

    private function import_commerceml()
    {
        global $kernel;
        $this->set_templates($kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'import_commerceml.html'));

        $msg = '';
        $settings = $this->get_import_commerceml_settings();
        if (isset($_POST['upload_import_file']))
        {
            //сначала заполнение всех настроек из POST, чтобы они сохранились даже в случае ошибки
            $settings['catid'] = intval($kernel->pub_httppost_get('catid'));
            $settings['groupid'] = intval($kernel->pub_httppost_get('groupid'));
            $settings['priceField'] = $kernel->pub_httppost_get('price_field', false);
            $settings['pricePerField'] = $kernel->pub_httppost_get('price_per_field', false);
            $settings['ID_field'] = $kernel->pub_httppost_get('ID_field', false);
            $settings['name_field'] = $kernel->pub_httppost_get('name_field', false);
            $settings['priceType'] = trim($kernel->pub_httppost_get('priceType', false));
            $settings['assoc'] = array(); //очистим, и заполним данными из POST
            if (isset($_POST['assocprops']))
            {
                foreach ($_POST['assocprops'] as $aprop)
                {
                    if (isset($aprop['name']) && isset($aprop['propid']) && !empty($aprop['name']) && !empty($aprop['propid']))
                        $settings['assoc'][trim($aprop['name'])] = $aprop['propid'];
                }
            }

            $this->save_import_commerceml_settings($settings);

            if ($kernel->pub_httppost_get('import_type', false) == 'import')
                $import_type = 'import'; //информация о товарах
            else
                $import_type = 'offers';
            //цены
            $shouldProcess = true;
            if (!isset($_FILES['commerceml_file']) || !is_uploaded_file($_FILES['commerceml_file']['tmp_name']))
            {
                $shouldProcess = false;
                $msg .= '[#catalog_commerceml_error_nofile#]';
            }
            elseif (!$settings['ID_field'])
            {
                $shouldProcess = false;
                $msg = '[#catalog_commerceml_error_reqfields_not_filled#]'; //не заполнены обязательные поля
            }
            else
            {
                //для разных типов импорта разные обязательные поля
                if ($import_type == 'import')
                {
                    if (!$settings['groupid'] || !$settings['name_field'])
                    {
                        $shouldProcess = false;
                        $msg = '[#catalog_commerceml_error_reqfields_not_filled#]'; //не заполнены обязательные поля
                    }
                }
                else
                {
                    if (!$settings['priceField'] || !$settings['priceType'])
                    {
                        $shouldProcess = false;
                        $msg = '[#catalog_commerceml_error_reqfields_not_filled#]'; //не заполнены обязательные поля
                    }
                }
            }

            if ($shouldProcess)
            {
                libxml_use_internal_errors(true); //чтобы не было ворнингов
                $xml = simplexml_load_file($_FILES['commerceml_file']['tmp_name']);

                $moduleid = $kernel->pub_module_id_get();
                if ($import_type == 'import')
                {
                    $group = CatalogCommons::get_group($settings['groupid']);
                    if (!$group)
                        $msg = '[#catalog_commerceml_error_no_group#]';
                    elseif (!$xml || !isset($xml->{'Классификатор'}->{'Свойства'}) || !isset($xml->{'Каталог'}->{'Товары'}))
                        $msg = '[#catalog_commerceml_error_malformed_xml#]';
                    else
                    {
                        //пройдём по свойствам, запомним ID
                        $assocs = array();
                        foreach ($xml->{'Классификатор'}->{'Свойства'}[0] as $prop)
                        {
                            $propID = (string)$prop->{'Ид'};
                            $propName = (string)$prop->{'Наименование'};
                            if (isset($settings['assoc'][$propName])) //
                                $assocs[$propID] = $propName;
                        }
                        $updated = 0;
                        $added = 0;
                        foreach ($xml->{'Каталог'}->{'Товары'}[0] as $item)
                        {
                            $item1Cid = (string)$item->{'Ид'};
                            $item1Cname = (string)$item->{'Наименование'};
                            $commonFields = array("`".$settings['name_field']."`" => "'".$kernel->pub_str_prepare_set($item1Cname)."'");
                            $groupFields = array();
                            if (isset($item->{'ЗначенияСвойств'}[0]))
                            {
                                foreach ($item->{'ЗначенияСвойств'}[0] as $fvalue)
                                {
                                    $propID = (string)$fvalue->{'Ид'};
                                    if (!isset($assocs[$propID]))
                                        continue;

                                    $propValue = (string)$fvalue->{'Значение'};
                                    $propDbName = $settings['assoc'][$assocs[$propID]];
                                    if (preg_match('~^group0_~', $propDbName))
                                    {
                                        $propDbName = substr($propDbName, 7);
                                        $commonFields["`".$propDbName."`"] = "'".$kernel->pub_str_prepare_set($propValue)."'";
                                    }
                                    else
                                        $groupFields["`".$propDbName."`"] = "'".$kernel->pub_str_prepare_set($propValue)."'";
                                }
                            }
                            $exRec = $kernel->db_get_record_simple('_catalog_'.$moduleid.'_items', "`".$settings['ID_field']."`='".$kernel->pub_str_prepare_set($item1Cid)."'");


                            if ($exRec)
                            {
                                if (count($groupFields))
                                {
                                    $q = "UPDATE `".$kernel->pub_prefix_get()."_catalog_items_".$moduleid."_".strtolower($group['name_db'])."`
                                    SET ";
                                    foreach ($groupFields as $k => $v)
                                    {
                                        $q .= $k."=".$v.", ";
                                    }
                                    $q .= " id=".$exRec['ext_id']." WHERE id='".$exRec['ext_id']."'";
                                    $kernel->runSQL($q);
                                }
                                //в $commonFields как минимум одно свойство из-за name
                                $q = "UPDATE `".$kernel->pub_prefix_get()."_catalog_".$moduleid."_items`
                                    SET ";
                                foreach ($commonFields as $k => $v)
                                {
                                    $q .= $k."=".$v.", ";
                                }
                                $q .= " id=".$exRec['id']." WHERE id='".$exRec['id']."'";
                                $kernel->runSQL($q);
                                $updated++;
                            }
                            else
                            {
                                $groupFields["`id`"] = "NULL";
                                $commonFields["`".$settings['ID_field']."`"] = "'".$kernel->pub_str_prepare_set($item1Cid)."'";
                                $query = "INSERT INTO `".$kernel->pub_prefix_get()."_catalog_items_".$moduleid."_".strtolower($group['name_db'])."`
                                          (".implode(",", array_keys($groupFields)).")
                                          VALUES
                                          (".implode(",", $groupFields).")";
                                $kernel->runSQL($query);
                                $ext_id = $kernel->db_insert_id();

                                //в $commonFields как минимум одно свойство из-за name и 1СID
                                $q = "INSERT INTO `".$kernel->pub_prefix_get()."_catalog_".$moduleid."_items`
                                    (".implode(",", array_keys($commonFields)).",`available`,`group_id`,`ext_id`)
                                    VALUES
                                    (".implode(",", $commonFields).",'1','".$settings['groupid']."','".$ext_id."')";
                                $kernel->runSQL($q);

                                if ($settings['catid'])
                                { //если надо - добавляем в категорию
                                    $newItemID = $kernel->db_insert_id();
                                    $newOrdr = $this->get_next_order_in_cat($settings['catid']);
                                    $query = "INSERT INTO `".$kernel->pub_prefix_get()."_catalog_".$moduleid."_item2cat` ".
                                        "(`cat_id`,`item_id`,`order`) VALUES ".
                                        "(".$settings['catid'].", ".$newItemID.", ".$newOrdr.")";
                                    $kernel->runSQL($query);
                                }
                                $added++;
                            }
                        }
                        $msg = $kernel->pub_page_textlabel_replace("[#catalog_commerceml_import_completed#]");
                        $msg = str_replace('%added%', $added, $msg);
                        $msg = str_replace('%updated%', $updated, $msg);

                    }
                }
                else
                { //offers.xml
                    if (!$xml || !isset($xml->{'ПакетПредложений'}->{'Предложения'}) || !isset($xml->{'ПакетПредложений'}->{'ТипыЦен'}))
                        $msg = '[#catalog_commerceml_error_malformed_xml#]';
                    else
                    {

                        $priceTypeID = false;
                        foreach ($xml->{'ПакетПредложений'}->{'ТипыЦен'}[0] as $priceType)
                        {
                            if ((string)$priceType->{'Наименование'} == $settings['priceType'])
                            {
                                $priceTypeID = (string)$priceType->{'Ид'};
                                break;
                            }
                        }
                        if (!$priceTypeID)
                            $msg = '[#catalog_commerceml_error_no_pricetype_found#]';
                        else
                        {
                            $updated = 0;
                            foreach ($xml->{'ПакетПредложений'}->{'Предложения'}[0] as $offer)
                            {
                                $offerID = (string)$offer->{'Ид'};


                                if (!isset($offer->{'Цены'}[0]))
                                    continue;

                                $oprice = false;
                                foreach ($offer->{'Цены'}[0] as $opriceElem)
                                {
                                    if ($priceTypeID == (string)$opriceElem->{'ИдТипаЦены'})
                                    {
                                        $oprice = $opriceElem;
                                        break;
                                    }
                                }
                                if (!$oprice)
                                    continue;

                                $price = $oprice->{'ЦенаЗаЕдиницу'};
                                $q = "UPDATE `".$kernel->pub_prefix_get()."_catalog_".$moduleid."_items`
                                SET `".$settings['priceField']."`='".$kernel->pub_str_prepare_set($price)."' ";
                                if ($settings['pricePerField'])
                                    $q .= ",`".$settings['pricePerField']."`='".(string)$oprice->{'Единица'}."'";
                                $q .= " WHERE `".$settings['ID_field']."`='".$kernel->pub_str_prepare_set($offerID)."'";
                                $kernel->runSQL($q);
                                $updated++;
                            }
                            $msg = $kernel->pub_page_textlabel_replace("[#catalog_commerceml_offers_completed#]");
                            $msg = str_replace('%updated%', $updated, $msg);
                        }
                    }
                }

            }

            $_SESSION['import_commerceml_msg'] = $msg;
            $kernel->pub_redirect_refresh_reload("import_commerceml");
        }

        if (isset($_SESSION['import_commerceml_msg']) && !empty($_SESSION['import_commerceml_msg']))
        {
            $msg = $_SESSION['import_commerceml_msg'];
            unset($_SESSION['import_commerceml_msg']);
        }

        $content = $this->get_template_block('content');

        $currGroupProps = array();
        $gprops = '';
        $groups = CatalogCommons::get_groups();
        $gblock = '';
        foreach ($groups as $group)
        {
            $props = CatalogCommons::get_props($group['id'], true);
            if ($group['id'] == $settings['groupid'])
            {
                foreach ($props as $prop)
                {
                    $opt_name = $prop['name_db'];
                    if ($prop['group_id'] == 0)
                        $opt_name = 'group0_'.$opt_name;
                    $currGroupProps[$opt_name] = $prop['name_full'];
                }
                $gblock .= $this->get_template_block('group_item_selected');

            }
            else
                $gblock .= $this->get_template_block('group_item');
            $gblock = str_replace('%group_id%', $group['id'], $gblock);
            $gblock = str_replace('%group_name%', htmlspecialchars($group['name_full']), $gblock);


            $thisGroupProps = array();
            foreach ($props as $prop)
            {
                if ($prop['type'] == 'file' || $prop['type'] == 'pict')
                    continue;
                $opt_name = $prop['name_db'];
                if ($prop['group_id'] == 0)
                    $opt_name = 'group0_'.$opt_name;
                $thisGroupProps[] = '{"name_db":"'.$opt_name.'","name_full":"'.$prop['name_full'].'"}';
            }
            $gprops .= '"'.$group['id'].'":['.implode(",", $thisGroupProps).'],'."\n";
        }

        $props_assoc_lines = '';
        $assocLinesCount = 0;
        foreach ($settings['assoc'] as $name1C => $nameDB)
        {
            if (!array_key_exists($nameDB, $currGroupProps))
                continue;
            $assocLinesCount++;
            $line = $this->get_template_block('props_assoc_line');

            $proplines = '';
            foreach ($currGroupProps as $propNameDB => $propNameFull)
            {
                if ($propNameDB == $nameDB)
                    $propline = $this->get_template_block('propline_selected');
                else
                    $propline = $this->get_template_block('propline');
                $propline = str_replace('%namedb%', $propNameDB, $propline);
                $propline = str_replace('%namefull%', $propNameFull, $propline);
                $proplines .= $propline;
            }
            $line = str_replace('%proplines%', $proplines, $line);
            $line = str_replace('%name1C%', htmlspecialchars($name1C), $line);

            $props_assoc_lines .= $line;
        }

        $commonProps = CatalogCommons::get_common_props($kernel->pub_module_id_get());

        $price_per_field_options = $this->import_commerceml_buildprops_select($commonProps, $settings['pricePerField']);
        $price_field_options = $this->import_commerceml_buildprops_select($commonProps, $settings['priceField']);
        $ID_field_options = $this->import_commerceml_buildprops_select($commonProps, $settings['ID_field']);
        $name_field_options = $this->import_commerceml_buildprops_select($commonProps, $settings['name_field']);

        $cats = $this->get_child_categories(0, 0, array());
        $options = '';
        $cat_shift = $this->get_template_block('cat_shift');
        foreach ($cats as $cat)
        {
            if ($cat['id'] == $settings['catid'])
                $option = $this->get_template_block('cat_option_selected');
            else
                $option = $this->get_template_block('cat_option');
            $option = str_replace('%cat_id%', $cat['id'], $option);
            $option = str_replace('%cat_name%', str_repeat($cat_shift, $cat['depth']).$cat['name'], $option);
            $options .= $option;
        }
        $content = str_replace('%cats_options%', $options, $content);
        $content = str_replace('var currAssocLines=0;', 'var currAssocLines='.$assocLinesCount.';', $content);
        $content = str_replace('%props_assoc_lines%', $props_assoc_lines, $content);
        $content = str_replace('%groups%', $gblock, $content);
        $content = str_replace('/*gprops*/', $gprops, $content);
        $content = str_replace('%price_field_options%', $price_field_options, $content);
        $content = str_replace('%price_per_field_options%', $price_per_field_options, $content);
        $content = str_replace('%ID_field_options%', $ID_field_options, $content);
        $content = str_replace('%name_field_options%', $name_field_options, $content);
        $content = str_replace('%priceType%', htmlspecialchars($settings['priceType']), $content);


        $content = str_replace('%form_action%', $kernel->pub_redirect_for_form('import_commerceml'), $content);
        $content = str_replace('%msg%', $msg, $content);
        return $content;
    }


    private function delete_group($groupid)
    {
        global $kernel;
        $group = CatalogCommons::get_group($groupid);
        if (!$group)
            return;
        $modid = $kernel->pub_module_id_get();
        $crec = $kernel->db_get_record_simple("_catalog_".$modid."_items", "group_id=".$group['id'], "COUNT(*) AS count");
        //не удаляем, если есть товары в группе
        if ($crec['count'] > 0)
            return;
        $prfx = $kernel->pub_prefix_get();
        $q = "DELETE FROM `".$prfx."_catalog_item_props` WHERE group_id='".$group['id']."' AND module_id='".$modid."'";
        $kernel->runSQL($q);
        $q = "DELETE FROM `".$prfx."_catalog_visible_gprops` WHERE group_id='".$group['id']."' AND module_id='".$modid."'";
        $kernel->runSQL($q);
        $q = "DELETE FROM `".$prfx."_catalog_item_groups` WHERE id='".$group['id']."'";
        $kernel->runSQL($q);
        $q = "DELETE FROM  `".$prfx."_catalog_".$modid."_inner_filters` WHERE groupid='".$group['id']."'";
        $kernel->runSQL($q);
        $q = "DROP TABLE `".$prfx."_catalog_items_".$modid."_".$group['name_db']."`";
        $kernel->runSQL($q);
        $this->generate_search_form($groupid, array());
    }

    /** Возвращает результаты быстрого поиска
     * @param string $term запрос
     * @param integer $catid ID категории
     * @param array $ignoreItemIds айдишники товаров, которые НЕ надо включать в результаты
     * @return array
     */
    public function get_quicksearch_results($term, $catid = null, $ignoreItemIds = array())
    {
        global $kernel;
        $terms = explode(" ", trim($term));
        if (!$terms)
            return array();
        $moduleid = $kernel->pub_module_id_get();
        $props = CatalogCommons::get_props(0, true);


        $addcond = "";
        if ($catid)
            $addcond .= " AND id IN (SELECT item_id FROM ".$kernel->pub_prefix_get()."_catalog_".$moduleid."_item2cat WHERE cat_id=".$catid.")";
        if ($ignoreItemIds)
            $addcond .= " AND id NOT IN (".implode(",", $ignoreItemIds).")";

        //сначала пробуем найти товары со ВСЕМИ словами из запроса (AND-логика)
        $cond = $this->get_like_condition_for_terms($terms, $props, "AND");
        $results = $kernel->db_get_list_simple("_catalog_".$moduleid."_items", $cond.$addcond, "*", 0, 100);
        if (!$results)
        { //если не нашли, пробуем найти хотя бы с одним словом из запроса (OR-логика)
            $cond = $this->get_like_condition_for_terms($terms, $props, "OR");
            $results = $kernel->db_get_list_simple("_catalog_".$moduleid."_items", $cond.$addcond, "*", 0, 100);
        }

        $sep = " | ";
        foreach ($results as &$r)
        {
            $stringBlocks = array();
            foreach ($props as $prop)
            {
                if (!$prop['showinlist'] || $prop['type'] == 'file' || $prop['type'] == 'pict')
                    continue;
                $strval = $r[$prop['name_db']];
                if (mb_strlen($strval) == 0)
                    continue;
                $stringBlocks[] = $strval;
            }
            $r['_string'] = strip_tags(implode($sep, $stringBlocks));
        }
        return $results;
    }


    private function get_like_condition_for_terms($terms, $props, $separator = "OR")
    {
        global $kernel;
        $tblocks = array();
        foreach ($terms as $t)
        {
            $t = trim($t);
            if (mb_strlen($t) == 0)
                continue;
            $propsblocks = array();
            foreach ($props as $prop)
            {
                if (!in_array($prop['type'], array('enum', 'string', 'number')))
                    continue;
                $propsblocks[] = "`".$prop['name_db']."` LIKE '%".$kernel->pub_str_prepare_set($t)."%'";
            }
            $tblocks[] = "(".implode(" OR ", $propsblocks).")";
        }
        return "(".implode(" ".$separator." ", $tblocks).")";
    }

    /**
     * Функция для отображения административного интерфейса
     *
     * @return string
     */
    public function start_admin()
    {
        global $kernel;
        $action = $kernel->pub_section_leftmenu_get();
        //если это не работа с деревом, "забудем" куку с выделенной нодой
        if (!in_array($action, array('category_items', 'category_move', 'category_items_save', 'save_selected_items', 'item_edit', 'item_save')))
            setcookie($this->structure_cookie_name, "");
        $moduleid = $kernel->pub_module_id_get();
        switch ($action)
        {
            case 'move_item2group':
                $itemid = intval($kernel->pub_httppost_get('itemid'));
                $groupid = intval($kernel->pub_httppost_get('groupid'));
                CatalogCommons::move_item2group($itemid,$groupid);;
                return $kernel->pub_httppost_response('[#catalog_item_moved_to_group#]','item_edit&id='.$itemid);

            case 'get_item_subcats_block':
                $this->set_templates($kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'items_edit.html'));
                $cid = intval($kernel->pub_httpget_get('cid'));
                $catsblock = $this->build_item_categories_block($cid);
                return "<ul>".$catsblock."</ul>";

            //ajax-поиск товаров
            case 'get_items_quicksearch_result':
                $term = $kernel->pub_httpget_get('term', false);
                $catid = intval($kernel->pub_httpget_get('catid', false));
                $ignoredid = intval($kernel->pub_httpget_get('ignored', false));
                if ($ignoredid)
                    $ignored = array($ignoredid);
                else
                    $ignored = array();
                $results = $this->get_quicksearch_results($term, $catid, $ignored);
                $jdata = array();
                foreach ($results as $result)
                {
                    $jdata[] = array("label" => $result['_string'], "value" => $result['id']);
                }
                return $kernel->pub_json_encode($jdata);
                break;

            //Удаление тов.группы
            case 'delete_group':
                $this->delete_group($kernel->pub_httpget_get('id'));
                $kernel->pub_redirect_refresh("show_groups");
                break;

            //импорт из 1С-формата CommerceML
            case 'import_commerceml':
                return $this->import_commerceml();

            case 'item_clone':
                $id = $kernel->pub_httpget_get('id');
                $cid = intval($kernel->pub_httpget_get('cid'));
                $newID = $this->item_clone($id);
                if ($newID)
                    return $this->show_item_form($newID, 0, $cid);
                else
                    return $this->show_item_form($id, 0, $cid);

            case 'show_variables':
                return $this->show_variables();

            case 'variable_delete':
                $namedb = $kernel->pub_httpget_get('name_db');
                $kernel->runSQL("DELETE FROM `".$kernel->pub_prefix_get()."_catalog_".$moduleid."_variables` WHERE `name_db`='".$namedb."'");
                $kernel->pub_redirect_refresh('show_variables');
                break;

            case 'show_variable_form':
                $name_db = $kernel->pub_httpget_get("name_db");
                return $this->show_variable_form($name_db);

            case 'variable_save':
                $name_full = trim($kernel->pub_httppost_get('name_full'));
                $name_db = trim($kernel->pub_httppost_get('name_db'));
                $prev_name_db = trim($kernel->pub_httppost_get('prev_name_db'));
                $value = trim($kernel->pub_httppost_get('value'));
                if (!preg_match("/^([0-9a-z_]+)$/i", $name_db))
                    return $kernel->pub_httppost_response("[#catalog_variable_save_msg_incorrect_namedb#]");
                elseif (empty($name_full))
                    return $kernel->pub_httppost_response("[#catalog_variable_save_msg_empty_name_full#]");
                elseif (empty($value))
                    return $kernel->pub_httppost_response("[#catalog_variable_save_msg_empty_value#]");
                else
                {
                    if (empty($prev_name_db))
                        $query = "INSERT INTO `".$kernel->pub_prefix_get()."_catalog_".$moduleid."_variables` ".
                            "(`name_db`,`name_full`,`value`) VALUES ".
                            "('".$name_db."','".$name_full."','".$value."')
                            ON DUPLICATE KEY UPDATE `name_full`='".$name_full."', `value`='".$value."'";
                    else
                        $query = "UPDATE `".$kernel->pub_prefix_get()."_catalog_".$moduleid."_variables` ".
                            "SET `name_db`='".$name_db."',`name_full`='".$name_full."',`value`='".$value."' ".
                            "WHERE `name_db`='".$prev_name_db."'";
                    $kernel->runSQL($query);
                    return $kernel->pub_httppost_response("[#catalog_variable_save_msg_ok#]", "show_variables");
                }

            case 'show_gen_search_form':
                $groupid = $kernel->pub_httpget_get('groupid', false);
                if (empty($groupid))
                    $groupid = $kernel->pub_httppost_get('groupid', false);
                return $this->show_gen_search_form($groupid);

            case 'gen_search_form':
                $groupid = $kernel->pub_httppost_get('id', false);
                $outfile = $kernel->pub_httppost_get('outfile');
                $filtername = $kernel->pub_httppost_get('filtername');
                $filtertpl = $kernel->pub_httppost_get('filtertpl');
                $props = $_POST['prop'];
                if (!empty($outfile))
                {
                    $content = $this->generate_search_form($groupid, $props);
                    $ext = mb_substr(mb_strtolower($outfile), -4);
                    if ($ext != "html" && $ext != ".htm")
                        $outfile .= ".html";
                    $kernel->pub_file_save("modules/catalog/templates_user/".$outfile, $content);
                }
                if (!empty($filtername) && !empty($filtertpl))
                {
                    $this->generate_inner_filter($filtername, $filtertpl, $groupid, $props);
                }
                $kernel->pub_redirect_refresh_reload("show_groups");
                return '';

            case 'show_csv_export':
                return $this->show_csv_export_form();

            case 'make_csv_export':
                $template = $kernel->pub_httppost_get('template', false);
                $filterid = intval($kernel->pub_httppost_get('filterid', false));
                $exported = $this->make_csv_export($template, $filterid);
                header('Content-type: application/vnd.ms-excel');
                header('Content-Disposition: attachment; filename=export.csv');
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                header('Pragma: public');
                header('Content-Length: '.mb_strlen($exported,'8bit'));
                //ob_clean();
                flush();
                print $exported;
                exit;

            case 'show_order_fields':
                return $this->show_order_fields();

            case 'save_order_fields_order':
                $this->save_order_fields_order();
                $kernel->pub_redirect_refresh_reload("show_order_fields");
                break;

            //Вызов формы редактирования добавления/конкретного поля корзины
            case 'order_field_edit':
                $id = $kernel->pub_httpget_get('id');
                return $this->show_order_field_form($id);

            case 'order_field_save':
                $id = $kernel->pub_httppost_get('id', false);
                $name_db = $kernel->pub_httppost_get('name_db', false);
                $name_full = $kernel->pub_httppost_get('name_full', false);
                $req = $kernel->pub_httppost_get('req', false);
                $regexp = $kernel->pub_httppost_get('regexp', false);
                $this->save_order_field($id, $name_full, $name_db, $regexp, $req);
                return $kernel->pub_httppost_response("[#catalog_order_field_saved_msg#]", "show_order_fields");

            case 'order_field_add':
                $name_db = $kernel->pub_httppost_get('name_db', false);
                $name_full = $kernel->pub_httppost_get('name_full', false);
                $req = $kernel->pub_httppost_get('req', false);
                $regexp = $kernel->pub_httppost_get('regexp', false);
                $ptype = $kernel->pub_httppost_get('ptype', false);
                $values = $kernel->pub_httppost_get('enum_values', false);
                $this->add_order_field($name_full, $name_db, $regexp, $req, $ptype, $values);
                return $kernel->pub_httppost_response("[#catalog_order_field_saved_msg#]", "show_order_fields");
                break;
            case 'order_field_delete':
                $this->delete_order_field($kernel->pub_httpget_get('id'));
                $kernel->pub_redirect_refresh("show_order_fields");
                break;

            case 'order_enum_field_delete':
                $id = $kernel->pub_httpget_get("id");
                $this->delete_order_enum_field($id);
                return $this->show_order_field_form($id);

            case 'order_enum_field_add':
                $id = $kernel->pub_httppost_get("id");
                $this->add_order_enum_field($id);
                $str = "order_field_edit&id=".$id;
                return $kernel->pub_httppost_response("[#catalog_edit_property_enum_add_msg#]", $str);

            case 'regenerate_order_tpls':
                $basket_items_tpl = $kernel->pub_httppost_get("basket_items_tpl");
                $basket_items_tpl_translit = $kernel->pub_translit_string($basket_items_tpl);
                $ext = strtolower(substr($basket_items_tpl_translit, -4));
                if ($ext != "html" && $ext != ".htm")
                    $basket_items_tpl_translit .= ".html";
                $basket_order_tpl = $kernel->pub_httppost_get("order_form_tpl");
                $basket_order_tpl_translit = $kernel->pub_translit_string($basket_order_tpl);
                $ext = strtolower(substr($basket_order_tpl_translit, -4));
                if ($ext != "html" && $ext != ".htm")
                    $basket_order_tpl_translit .= ".html";
                $this->regenerate_basket_items_tpl($basket_items_tpl_translit);
                $this->regenerate_basket_order_tpl($basket_order_tpl_translit);
                //значит была транслитерация
                if ($basket_order_tpl_translit != $basket_order_tpl || $basket_items_tpl_translit != $basket_items_tpl)
                    return $kernel->pub_httppost_response("[#catalog_order_tpls_regenerated_translit_msg#]", "show_order_fields");
                else
                    return $kernel->pub_httppost_response("[#catalog_order_tpls_regenerated_msg#]", "show_order_fields");

            case 'inner_filter_delete':
                $this->delete_inner_filter($kernel->pub_httpget_get("id"));
                $kernel->pub_redirect_refresh("show_inner_filters");
                break;

            case 'test_filter':
                return $this->test_filter($kernel->pub_httpget_get("id"));

            case 'show_inner_filters':
                return $this->show_inner_filters();

            case 'show_inner_filter_form':
                $id = $kernel->pub_httpget_get("id");
                return $this->show_inner_filter_form($id);

            case 'inner_filter_save':
                $id = $kernel->pub_httppost_get("id");
                $saveError = $this->save_inner_filter($id);
                if (!$saveError)
                    return $kernel->pub_httppost_response("[#catalog_edit_inner_filter_save_msg_ok#]", "show_inner_filters");
                else
                    return $kernel->pub_httppost_response("[#catalog_edit_inner_filter_save_msg_error#]", $saveError);

            case 'category_move':
                $cid = $kernel->pub_httppost_get("node");
                $parentNew = intval($kernel->pub_httppost_get("newParent"));
                $indexNew = $kernel->pub_httppost_get("index");
                $order2replace = $this->get_last_order_in_cat($parentNew, $indexNew);
                $catsTable='_catalog_'.$moduleid.'_cats';
                $query = "UPDATE `".$kernel->pub_prefix_get().$catsTable.'` SET `order`='.($order2replace + 1).' WHERE `parent_id`='.$parentNew.' AND `order`='.$order2replace;
                $kernel->runSQL($query);
                $query = "UPDATE `".$kernel->pub_prefix_get().$catsTable.'` SET `parent_id`='.$parentNew.', `order`='.$order2replace.' WHERE `id`='.$cid;
                $kernel->runSQL($query);
                $crecs=$kernel->db_get_list_simple($catsTable,"`parent_id`=".$parentNew,"id");
                $order = 0;
                foreach($crecs as $crec)
                {
                    $catid=$crec['id'];
                    $kernel->db_update_record($catsTable,array('order'=>$order+=$this->order_inc),"id=".$catid);
                }
                break;

            case 'save_selected_items':
                $group_id = $kernel->pub_httppost_get('group_id');
                $change_items = $kernel->pub_httppost_get('change_items');
                if (empty($change_items))
                    $this->save_selected_items();
                else
                    $this->change_selected_items();
                $kernel->pub_redirect_refresh_reload('show_items&group_id='.$group_id);
                break;
            case 'import_csv': //первый шаг - показываем форму для upload



                $newfilename = $kernel->pub_session_get('importcsv_filename');
                $group_id = $kernel->pub_session_get('importcsv_groupid');
                $separator = $kernel->pub_session_get('importcsv_separator');
                $break_files = $kernel->pub_session_get('break_files');
                $add_in_cat_parent = $kernel->pub_session_get('add_in_cat_parent');
                $category_auto = $kernel->pub_session_get('category_auto');

                //если в сессии есть значения для второго шага импорта, покажем таблицу с первыми 10 эл-тами
                if (!is_null($newfilename) && !is_null($group_id) && !is_null($separator))
                    return $this->show_import_csv_table($group_id, $newfilename, $separator, $break_files, $category_auto);
                else //иначе - начальную форму
                    return $this->show_import_csv_form();

            case 'import_csv2': //сохраняем в сессии данные для импорта
                $group_id 		= $kernel->pub_httppost_get('group');
                $separator 		= $kernel->pub_httppost_get('separator');
                $cat_id 		= intval($kernel->pub_httppost_get('cat'));
                $cat_id4new 	= intval($kernel->pub_httppost_get('cat4new'));
                $import_from 	= $kernel->pub_httppost_get("importfrom");
                $break_files 	= $kernel->pub_httppost_get("break_files");
                $add_in_cat_parent 	= $kernel->pub_httppost_get("add_in_cat_parent");
                $create_cat 	= $kernel->pub_httppost_get("create_cat");
                $category_auto 	= $kernel->pub_httppost_get("category_auto");
                $break_line 	= $kernel->pub_httppost_get("break_line");

                switch ($separator)
                {
                    case 'tab':
                        $sep =  "\t";
                        break;
                    case 'semicolon':
                        $sep = ';';
                        break;
                    case 'comma':
                    default:
                        $sep = ',';
                        break;
                }


                $need_save = false;
                $newfilename = '';
                if ($import_from == "textarea")
                {

                    //через буфер обмена
                    $buffer = trim($kernel->pub_httppost_get("textarea", false));
                    if (mb_strlen($buffer) > 0)
                    {
                        $buffer = str_replace('\r\n', "\n", $buffer);
                        $newfilename = 'content/files/'.$moduleid.'/buffer_'.time().'.txt';
                        $kernel->pub_file_save($newfilename, $buffer);
                        $need_save = true;
                    }
                }
                else if (isset($_FILES['importcsv']) && is_uploaded_file($_FILES['importcsv']['tmp_name']))
                {

                    //из файла, если он закачан
                    $newfilename = $this->process_file_upload($_FILES['importcsv']);
                    $need_save = true;
                }
                if ($need_save)
                {
                    $kernel->pub_session_set('importcsv_filename', $newfilename);
                    $kernel->pub_session_set('importcsv_groupid', $group_id);
                    $kernel->pub_session_set('importcsv_separator', $sep);
                    $kernel->pub_session_set('importcsv_catid', $cat_id);
                    $kernel->pub_session_set('importcsv_catid4new', $cat_id4new);
                    $kernel->pub_session_set('break_files', $break_files);
                    $kernel->pub_session_set('add_in_cat_parent', $add_in_cat_parent);
                    $kernel->pub_session_set('create_cat', $create_cat);
                    $kernel->pub_session_set('category_auto', $category_auto);
                    $kernel->pub_session_set('break_line', $break_line);
                }
                $kernel->pub_redirect_refresh_reload('import_csv');
                break;

            case 'import_csv3': //импортируем весь файл на основе настроек, выбранных админом
                $newfilename = $kernel->pub_session_get('importcsv_filename');
                $group_id = $kernel->pub_session_get('importcsv_groupid');
                $cat_id = $kernel->pub_session_get('importcsv_catid');
                $cat_id4new = $kernel->pub_session_get('importcsv_catid4new');
                $separator = $kernel->pub_session_get('importcsv_separator');

                $break_files 	= $kernel->pub_session_get("break_files");
                $add_in_cat_parent 	= $kernel->pub_session_get("add_in_cat_parent");
                $create_cat 	= $kernel->pub_session_get("create_cat");
                $category_auto 	= $kernel->pub_session_get("category_auto");

                $this->make_csv_import($group_id, $newfilename, $separator, $cat_id, $cat_id4new, $break_files, $category_auto, $add_in_cat_parent,$create_cat);
                $kernel->pub_session_unset('importcsv_filename');
                $kernel->pub_session_unset('importcsv_groupid');
                $kernel->pub_session_unset('importcsv_separator');
                $kernel->pub_session_unset('importcsv_catid');
                $kernel->pub_session_unset('importcsv_catid4new');
                $kernel->pub_session_unset('break_files');
                $kernel->pub_session_unset('add_in_cat_parent');
                $kernel->pub_session_unset('create_cat');
                $kernel->pub_session_unset('break_line');
                $kernel->pub_redirect_refresh_reload('show_items');
                break;


            //Формирование списка свойств товаров
            //как общего так и для товарной группы
            case 'show_group_props':
                $id = $kernel->pub_httpget_get('id');
                return $this->show_group_props($id);

            //Вызов формы редактирования добавления/конкртеного свойства
            case 'prop_edit':
                $id_group = $kernel->pub_httpget_get('id_group');
                $id_prop = $kernel->pub_httpget_get('id');
                //ID группы, из которой было вызвано редактирование, так как
                //из группы могут вызваны на редактирования и общие свойства
                $id_group_control = $kernel->pub_httpget_get('idg_control');
                return $this->show_prop_form($id_prop, $id_group, $id_group_control);

            //Сохраняем значения свойства
            case 'prop_save':
                $id = $kernel->pub_httppost_get('id');
                $name_db = $kernel->pub_httppost_get('name_db');
                $name_full = $kernel->pub_httppost_get('name_full');
                $inlist = $kernel->pub_httppost_get('inlist');
                $infilters = $kernel->pub_httppost_get('infilters');
                $sorted = intval($kernel->pub_httppost_get('sorted'));
                $ismain = intval($kernel->pub_httppost_get('ismain'));
                $this->save_prop($id, $name_full, $name_db, $inlist, $infilters, $sorted, $ismain);

                //Теперь опредилим, куда нужно вернуться
                $id_group_control = $kernel->pub_httpget_get('id_group_control');
                $id_group_control = intval($id_group_control);
                return $kernel->pub_httppost_response("[#catalog_prop_saved_msg#]", "show_group_props&id=".$id_group_control);

            //Сохраняем свойство КАТЕГОРИИ
            case 'cat_prop_save':
                $id = $kernel->pub_httppost_get('id');
                $name_db = $kernel->pub_httppost_get('name_db');
                $name_full = $kernel->pub_httppost_get('name_full');
                $this->save_cat_prop($id, $name_full, $name_db);
                return $kernel->pub_httppost_response("[#catalog_prop_saved_msg#]", "show_cat_props");

            //Добавляем новое свойтво и общее и в группу
            case 'prop_add':
                $this->add_prop_in_group();
                //Теперь определим, куда нужно вернуться
                $id_group_control = $kernel->pub_httpget_get('id_group_control');
                $id_group_control = intval($id_group_control);
                return $kernel->pub_httppost_response("[#catalog_edit_property_added_msg#]", "show_group_props&id=".$id_group_control);

            //Удаляем свойство товарной группы
            case 'prop_delete':
                $groupid = $kernel->pub_httpget_get("groupid");
                $propid = $kernel->pub_httpget_get("id");
                $this->delete_prop($propid, $groupid);

                //Теперь опрделим, куда нужно вернуться
                $id_group_control = $kernel->pub_httpget_get('idg_control');
                $id_group_control = intval($id_group_control);
                $kernel->pub_redirect_refresh("show_group_props&id=".$id_group_control);
                break;

            //Вызывается при добавлении к уже существующему перечислению нового значения
            case 'enum_prop_add':

                $enumval = $kernel->pub_httppost_get("enumval", false);
                $propid = $kernel->pub_httppost_get("id");
                $prop = CatalogCommons::get_prop($propid);
                if ($prop['group_id'] == 0)
                    $table = '_catalog_'.$kernel->pub_module_id_get().'_items';
                else
                {
                    $group = CatalogCommons::get_group($prop['group_id']);
                    $table = '_catalog_items_'.$kernel->pub_module_id_get().'_'.strtolower($group['name_db']);
                }
                $tinfo = $kernel->db_get_table_info($table);
                $evals = $this->get_enum_set_prop_values($tinfo[$prop['name_db']]['Type'], false);
                if (substr($tinfo[$prop['name_db']]['Type'], 0, 3) == 'set')
                {
                    $ptype = 'set';
                    if (count($evals) == 64)
                        return $kernel->pub_httppost_errore('[#catalog_set_type_64_max_error_msg#]', true);
                }
                else
                    $ptype = 'enum';

                $evals[] = $enumval;
                $query = 'ALTER TABLE `'.$kernel->pub_prefix_get().$table.'` CHANGE `'.$prop['name_db'].'` `'.$prop['name_db'].'` '.$this->convert_field_type_2_db($ptype, $evals);
                $kernel->runSQL($query);

                //ID группы, из которой было вызвано редактирование, так как
                //из группы могут вызваны на редактирования и общие свойства
                $group_id = $kernel->pub_httppost_get('group_id');
                $id_group_control = intval($kernel->pub_httpget_get('id_group_control'));

                $url = "prop_edit&id=".$propid."&id_group=".$group_id."&idg_control=$id_group_control";
                return $kernel->pub_httppost_response("[#catalog_edit_property_enum_add_msg#]", $url);

            case 'enum_prop_delete':
                $this->enum_set_prop_delete();
                $id_prop = $kernel->pub_httpget_get('propid');
                $group_id = $kernel->pub_httppost_get('group_id');
                $id_group_control = $kernel->pub_httpget_get('id_group_control');
                $id_group_control = intval($id_group_control);
                $str = "prop_edit&id=".$id_prop."&id_group=".$group_id."&idg_control=$id_group_control";
                $kernel->pub_redirect_refresh($str);
                break;

            //Работа с группами свойств
            //Выводит список доступных товарных групп
            case 'show_groups':
                return $this->show_groups();

            //Выводит форму редактирования/добавления товарной группы
            case 'show_group_form':
                $id = $kernel->pub_httpget_get('id');
                return $this->show_group_form($id);

            case 'group_save':
                $id = intval($kernel->pub_httppost_get('id'));
                $name = $kernel->pub_httppost_get('name');
                $namedb = $kernel->pub_httppost_get('namedb');
                if ($id == -1)
                    $groupid = $this->add_group($name, $namedb);
                else
                    $groupid = $this->save_group($id, $name, $namedb);
                if ($groupid > 0)
                    return $kernel->pub_httppost_response("[#catalog_edit_group_save_msg_ok#]", "show_groups");
                else
                    return $kernel->pub_httppost_response("[#catalog_edit_group_save_msg_err#]", "show_groups");
                break;
            case 'save_gprops_order':
                //$gid = $kernel->pub_httppost_get('group_id');
                $this->save_gprops_order();
                $kernel->pub_redirect_refresh_reload("show_groups");
                break;

            //Управление свойсвами всех категорий
            case 'show_cat_props':
                $id = $kernel->pub_httpget_get('id');
                return $this->show_cat_props($id);

            //Вызывает на редактирование свойство категории
            case 'cat_prop_edit':
                $id = $kernel->pub_httpget_get('id');
                return $this->show_cat_prop_form($id);



            //группы вариаций
            case 'option_group_edit':
                return $this->show_option_group_edit();



            case 'option_group_edit':


                //@todo подправить
                $group_option_id = $kernel->pub_httpget_get('group_id');

                $row = $kernel->db_get_record_simple('_catalog_'.$kernel->pub_module_id_get().'_item_props_groups', 'id='.$group_option_id);

                $source[]	= '%form_action%';
                $replace[]	= $kernel->pub_redirect_for_form('item_edit&id='.$kernel->pub_httpget_get('id').'&a=option_group_save');

                $source[]	= '%group_title%';
                $replace[]	= $row['title'];


                $content = $this->get_template_block('option_group_edit');

                $content = str_replace($source, $replace, $content);
                return $content;
                break;








            //Добавляет новое свойство к категории
            case 'cat_prop_add':
                $values = $kernel->pub_httppost_get('values', false);
                $pname = $kernel->pub_httppost_get('pname');
                $ptype = $kernel->pub_httppost_get('ptype');
                $pnamedb = $kernel->pub_httppost_get('pnamedb');
                $this->add_prop_in_cat($pname, $ptype, $values, $pnamedb);
                return $kernel->pub_httppost_response("[#catalog_edit_property_cat_add_new_prop_msg#]", "show_cat_props");

            case 'cat_prop_delete':
                $propid = $kernel->pub_httpget_get("id");
                $this->delete_cat_prop($propid);
                $kernel->pub_redirect_refresh("show_cat_props");
                break;

            case 'cat_enum_prop_add':
                $enumval = $kernel->pub_httppost_get("enumval", false);
                $propid = $kernel->pub_httppost_get("id");
                $prop = CatalogCommons::get_cat_prop($propid);
                $table = '_catalog_'.$moduleid.'_cats';
                $tinfo = $kernel->db_get_table_info($table);
                $evals = $this->get_enum_set_prop_values($tinfo[$prop['name_db']]['Type']);
                $evals[] = $enumval;
                $query = 'ALTER TABLE `'.$kernel->pub_prefix_get().$table.'` CHANGE `'.$prop['name_db'].'` `'.$prop['name_db'].'` '.$this->convert_field_type_2_db('enum', $evals);
                $kernel->runSQL($query);
                return $kernel->pub_httppost_response("[#catalog_edit_property_cat_enum_addnew_msg#]", "cat_prop_edit&id=".$propid);

            case 'cat_enum_prop_delete':
                $enumval = $kernel->pub_httpget_get("enumval", false);
                $propid = $kernel->pub_httpget_get("propid");
                $prop = CatalogCommons::get_cat_prop($propid);
                $table = '_catalog_'.$moduleid.'_cats';
                $tinfo = $kernel->db_get_table_info($table);
                $evals = $this->get_enum_set_prop_values($tinfo[$prop['name_db']]['Type']);
                $newevals = array();
                foreach ($evals as $eval)
                {
                    if ($eval != $enumval)
                        $newevals[] = $eval;
                }
                $query = 'UPDATE `'.$kernel->pub_prefix_get().$table.'` SET `'.$prop['name_db'].'`=NULL WHERE `'.$prop['name_db'].'`="'.$kernel->pub_str_prepare_set($enumval).'"';
                $kernel->runSQL($query);
                $query = 'ALTER TABLE `'.$kernel->pub_prefix_get().$table.'` CHANGE `'.$prop['name_db'].'` `'.$prop['name_db'].'` '.$this->convert_field_type_2_db('enum', $newevals);
                $kernel->runSQL($query);
                $kernel->pub_redirect_refresh("cat_prop_edit&id=".$propid);
                break;

            //Работа с категориями в дереве

            //Добавляем новую категорию через дерево
            case 'category_add':
                $pid = intval($kernel->pub_httppost_get("pid"));
                $name = $kernel->pub_httppost_get('name');
                if(mb_strlen($name)==0)
                    $name = $kernel->pub_str_prepare_set($kernel->pub_page_textlabel_replace('[#catalog_category_new_name#]'));
                $order = $this->get_last_order_in_cat($pid) + 2;
                $cid=$kernel->db_add_record('_catalog_'.$moduleid.'_cats',array('parent_id'=>$pid,'name'=>$name,'order'=>$order));
                $this->regenerate_all_groups_tpls(false);
                return $kernel->pub_httppost_response($cid);

            //Вызов формы редактирования категории
            case 'category_edit':
                $id = $kernel->pub_httpget_get("id");
                return $this->show_category_form($id);

            //Сохраняет параметры отредактированнной категории
            case 'category_save':
                return $this->save_category($kernel->pub_httppost_get('id'));

            //Удаление категории
            case 'category_delete':
                $cat = CatalogCommons::get_category(intval($kernel->pub_httppost_get('node')));
                if (!$cat)
                    $kernel->pub_redirect_refresh_reload('show_items');
                CatalogCommons::delete_category($cat);
                $this->regenerate_all_groups_tpls(false);
                $kernel->pub_redirect_refresh_reload('category_edit&id='.$cat['parent_id'].'&selectcat='.$cat['parent_id']);
                break;

            //Удаляет файл и очищает поле файл и изображения в категории
            case 'cat_clear_field':
                $id = intval($kernel->pub_httpget_get('id'));
                $dprop = $kernel->pub_httpget_get('field');
                $cat = CatalogCommons::get_category($id);
                $query = "UPDATE `".$kernel->pub_prefix_get().'_catalog_'.$moduleid.'_cats` SET `'.$dprop.'`=NULL WHERE `id`='.intval($id);
                $kernel->runSQL($query);
                $kernel->pub_file_delete($cat[$dprop]);

                // Также, если это изображение, необходимо удалить и его вариации
                $cats_props = CatalogCommons::get_cats_props();
                foreach ($cats_props as $cat_prop) {
                    if($cat_prop['name_db'] == $dprop && $cat_prop['type'] == 'pict') {
                        $path_parts = pathinfo($cat[$dprop]);

                        $path_small = $path_parts['dirname'].'/tn/'.$path_parts['basename'];
                        $kernel->pub_file_delete($path_small);

                        $path_source = $path_parts['dirname'].'/source/'.$path_parts['basename'];
                        $kernel->pub_file_delete($path_source);
                    }
                }

                $kernel->pub_redirect_refresh('category_edit&id='.$id.'&selectcat='.$id);
                break;

            //Список товаров в категории
            case 'category_items':
                return $this->show_category_items();

            case 'category_items_save':
                $catid = $kernel->pub_httppost_get("catid");
                $change_items = $kernel->pub_httppost_get('change_items');
                if (empty($change_items))
                    $this->save_category_items($catid);
                else
                    $this->change_selected_items();
                return $kernel->pub_httppost_response('[#catalog_variable_save_msg_ok#]', 'category_items&id='.$catid);

            //Выводит форму всех товаров, с фильтром по товарной группе
            case 'show_items':
                $group_id = 0;
                $gid_param = $kernel->pub_httpget_get('group_id');
                if (!empty($gid_param))
                    $group_id = intval($gid_param);
                else
                {
                    $gid_param = $kernel->pub_httppost_get('group_id');
                    if (!empty($gid_param))
                        $group_id = intval($gid_param);
                }
                $search_param = $kernel->pub_httppost_get('search');
                if (!empty($search_param))
                {
                    $groups = CatalogCommons::get_groups();
                    $postprops = $kernel->pub_httppost_get();
                    unset($postprops['group_id']);
                    unset($postprops['search']);
                    $search_props = array();
                    $matches = false;
                    foreach ($postprops as $ppname => $ppvalue)
                    {
                        if (preg_match("/([a-z0-9_-]+)_(to|from)$/", $ppname, $matches))
                        {
                            $search_props[$matches[1]] = "diapazon";
                            continue;
                        }
                        if (empty($ppvalue) /*|| !isset($all_group_props[$ppname])*/)
                            continue;
                        if ($ppname == "id")
                            $search_props[$ppname] = "accurate";
                        elseif (is_array($ppvalue))
                        { //enum-checkboxes
                            $search_props[$ppname] = "checkbox";
                        }
                        else
                        {
                            $search_props[$ppname] = "like";
                        }
                    }
                    $squery = $this->generate_inner_filter_query($group_id, $search_props);
                    $link = '';
                    $squery = $this->prepare_inner_filter_sql($squery, array(), $link);

                    $squery = "SELECT items.* FROM ".$kernel->pub_prefix_get()."_catalog_".$moduleid."_items AS items ".
                        "LEFT JOIN ".$kernel->pub_prefix_get()."_catalog_items_".$moduleid."_".strtolower($groups[$group_id]['name_db'])." AS `".strtolower($groups[$group_id]['name_db'])."` ON items.ext_id = `".strtolower($groups[$group_id]['name_db'])."`.id ".
                        "WHERE items.`group_id`=".$group_id." AND (".$squery.")";
                    $kernel->pub_session_set("search_items_query", $squery);
                    $kernel->pub_redirect_refresh_reload('show_items&search_results=1&group_id='.$group_id);
                    die();
                }
                return $this->show_items($group_id);

            //Форма редактирования товара
            case 'item_edit':
                $id = $kernel->pub_httpget_get('id');
                $removlinkedid = $kernel->pub_httpget_get("removlinkedid");
                if (!empty($removlinkedid))
                {
                    $query = 'DELETE FROM `'.$kernel->pub_prefix_get().'_catalog_'.$moduleid.'_items_links` WHERE '.
                        '(itemid1='.$id.' AND itemid2='.$removlinkedid.') OR '.
                        '(itemid2='.$id.' AND itemid1='.$removlinkedid.')';
                    $kernel->runSQL($query);
                }
                return $this->show_item_form($id, 0);

            //Добавление товара вызывает ту же строку с редактированием
            case 'item_add':
                $group_id = $kernel->pub_httppost_get('group_id');
                if (!$group_id)
                {
                    $group_id = $kernel->pub_httpget_get('group_id');
                    if (!$group_id)
                        return '';
                }
                $id_cat = $kernel->pub_httpget_get('id_cat');
                if (empty($id_cat))
                    $id_cat = $kernel->pub_httppost_get('id_cat');
                return $this->show_item_form(0, intval($group_id), intval($id_cat));

            //Сохраняет товар после редактирования или добавляет новый
            case 'item_save':
                return $this->save_item();

            //Очистка поля с файлом в товаре
            case 'item_clear_field':
                $id_tovar = $kernel->pub_httpget_get('id');
                $dprop = $kernel->pub_httpget_get('field');
                $id_tovar = intval($id_tovar);
                $item = CatalogCommons::get_item_full_data($id_tovar);
                //определяем, common-свойство или нет
                $tinfo = $kernel->db_get_table_info('_catalog_'.$moduleid.'_items');
                if (array_key_exists($dprop, $tinfo))
                    $query = "UPDATE `".$kernel->pub_prefix_get().'_catalog_'.$moduleid.'_items` SET `'.$dprop.'`=NULL WHERE `id`='.$id_tovar;
                else
                {
                    $group = CatalogCommons::get_group($item['group_id']);
                    $query = "UPDATE `".$kernel->pub_prefix_get().'_catalog_items_'.$moduleid.'_'.strtolower($group['name_db']).'` SET `'.$dprop.'`=NULL WHERE `id`='.$item['id'];
                }
                $kernel->runSQL($query);
                //Теперь удаяем само изображение
                //и его уменьшенную копию
                $path_parts = pathinfo($item[$dprop]);
                $kernel->pub_file_delete($item[$dprop]);
                $kernel->pub_file_delete($path_parts['dirname'].'/tn/'.$path_parts['basename']);
                $kernel->pub_file_delete($path_parts['dirname'].'/source/'.$path_parts['basename']);
                $kernel->pub_redirect_refresh("item_edit&id=".$id_tovar.'&redir2='.urlencode($kernel->pub_httpget_get('redir2')));
                break;

            //Удаление товара
            case 'item_delete':
                $groupid = $kernel->pub_httpget_get('group_id');
                $itemid = $kernel->pub_httpget_get('id');
                CatalogCommons::delete_item($itemid);
                //Если указана ID категории, то нужно вренуться в неё
                $id_cat = $kernel->pub_httpget_get('id_cat');
                $id_cat = intval($id_cat);
                if ($id_cat > 0)
                    $kernel->pub_redirect_refresh("category_items&id=".$id_cat);
                else
                    $kernel->pub_redirect_refresh("show_items&group_id=".$groupid);
                break;

            //Генерация шаблонов
            case 'regen_tpls4groups':
                $id_group = $kernel->pub_httpget_get("id_group");
                $id_group = intval($id_group);
                if ($id_group > 0)
                {
                    //Формируем шаблон списка товаров
                    $this->regenerate_group_tpls($id_group);
                    //Формируем шаблон карточки товара
                    $this->regenerate_group_tpls($id_group, true);
                }
                $kernel->pub_redirect_refresh("show_groups");
                break;

            case 'regen_tpl4itemlist':
                $kernel->pub_redirect_refresh_reload("show_groups");
                break;

            case 'option_group_save':
                return  $this->option_group_save();

            case 'option_group_delete':
                return  $this->option_group_delete();


            case 'show_orders':
                return $this->show_orders();

            case 'show_order':
                return $this->show_order();

            case 'show_cupons':
                return $this->show_cupons();

            case 'cupon_delete':
                $id = $kernel->pub_httpget_get('id');
                $kernel->runSQL("DELETE FROM `".$kernel->pub_prefix_get()."_catalog_".$moduleid."_cupons` WHERE `id`='".$id."'");
                $kernel->pub_redirect_refresh('show_cupons');
                break;

            case 'show_cupon_form':
                $id = $kernel->pub_httpget_get("id");
                return $this->show_cupon_form($id);

            case 'cupon_save':
                $cupon_id = trim($kernel->pub_httppost_get('cupon_id'));
                $cupon_textcode = trim($kernel->pub_httppost_get('cupon_textcode'));
                $cupon_created = strtotime(trim($kernel->pub_httppost_get('cupon_created')));
                $cupon_expiration = strtotime(trim($kernel->pub_httppost_get('cupon_expiration')));
                $cupon_use_count = trim($kernel->pub_httppost_get('cupon_use_count'));
                $cupon_type = trim($kernel->pub_httppost_get('cupon_type'));
                $cupon_value = trim($kernel->pub_httppost_get('cupon_value'));

                if (empty($cupon_created))
                    $cupon_created = strtotime(date("Y-m-d H:i:s"));

                if (!preg_match("/^([0-9A-Za-z\-_]+)$/i", $cupon_textcode))
                    return $kernel->pub_httppost_response("[#catalog_cupon_save_msg_incorrect_textcode#]");
                elseif (empty($cupon_textcode))
                    return $kernel->pub_httppost_response("[#catalog_cupon_save_msg_empty_textcode#]");
                elseif (!intval($cupon_created) > 0)
                    return $kernel->pub_httppost_response("[#catalog_cupon_save_msg_incorrect_value#]");
                elseif (empty($cupon_value))
                    return $kernel->pub_httppost_response("[#catalog_cupon_save_msg_empty_value#]");
                elseif (!preg_match("/^([0-9]+)$/i", $cupon_use_count))
                    return $kernel->pub_httppost_response("[#catalog_cupon_save_msg_incorrect_use_count#]");
                elseif ($cupon_use_count=='')
                    return $kernel->pub_httppost_response("[#catalog_cupon_save_msg_empty_use_count#]");
                elseif (empty($cupon_expiration))
                    return $kernel->pub_httppost_response("[#catalog_cupon_save_msg_empty_expiration#]");
                elseif (intval($cupon_expiration) <= intval($cupon_created))
                    return $kernel->pub_httppost_response("[#catalog_cupon_save_msg_incorrect_expiration#]");
                else
                {
                    if (empty($cupon_id)) {
                        $query = "INSERT INTO `".$kernel->pub_prefix_get()."_catalog_".$moduleid."_cupons` (`textcode`,`created`,`expiration`,`use_count`,`type`,`cupon_value`) VALUES ('".$cupon_textcode."','".date("Y-m-d H:i:s", $cupon_created)."','".date("Y-m-d H:i:s", $cupon_expiration)."','".$cupon_use_count."','".$cupon_type."','".$cupon_value."') ON DUPLICATE KEY UPDATE `textcode`='".$cupon_textcode."', `type`='".$cupon_type."', `expiration`='".date("Y-m-d H:i:s", $cupon_expiration)."', `cupon_value`='".$cupon_value."'";
                    } else {
                        $query = "UPDATE `".$kernel->pub_prefix_get()."_catalog_".$moduleid."_cupons` SET `textcode`='".$cupon_textcode."', `created`='".date("Y-m-d H:i:s", $cupon_created)."', `expiration`='".date("Y-m-d H:i:s", $cupon_expiration)."', `use_count`='".$cupon_use_count."', `type`='".$cupon_type."', `cupon_value`='".$cupon_value."', `textcode`='".$cupon_textcode."' WHERE `id`='".$cupon_id."'";
                    }

                    if($kernel->runSQL($query))
                        return $kernel->pub_httppost_response("[#catalog_cupon_save_msg_ok#]", "show_cupons");
                    else
                        return $kernel->pub_httppost_response("[#catalog_cupon_save_msg_error#]", "show_cupons");
                }

            case 'save_selected_orders':
                $exec = $kernel->pub_httppost_get('exec');
                if (!empty($exec))
                {

                    $this->save_selected_orders();
                }
                $kernel->pub_redirect_refresh_reload('show_orders');
                break;

            //Созданием админского шаблона редактирования товара
            /*
            case 'generate_admin_templ':
                $id = $kernel->pub_httpget_get("id");
                $this->regenerate_admin_template($id);

                return $kernel->pub_redirect_refresh("show_groups");
                break;
            */
        }

        return null;
    }



    /**
     * Возвращает максимальное кол-во терминов на страницу в админке
     *
     * @return integer
     */
    private function get_limit_admin()
    {
        global $kernel;
        $property = $kernel->pub_modul_properties_get('catalog_terms_per_page_admin');
        if ($property['isset'] && intval($property['value']) > 0)
            return intval($property['value']);
        else
            return 20;
    }


    /**
     * Возвращает текущий сдвиг во фронтэнде
     *
     * @return integer
     */
    private function get_offset_user()
    {
        global $kernel;

        $offset = intval($kernel->pub_httpget_get($this->frontend_param_offset_name));

        if ($offset < 0)
            $offset = 0;

        return $offset;
    }


    /**
     * Возвращает кол-во на страницу во фронтэнде
     *
     * @return integer
     */
    private function get_perpage_user()
    {
        global $kernel;
        $limit = intval($kernel->pub_httpget_get($this->frontend_param_perpage_name));

        if ($limit < 0)
            $limit = 0;

        return $limit;
    }

    /**
     * Возвращает текущий сдвиг  (для админки)
     *
     * @return integer
     */
    private function get_offset_admin()
    {
        global $kernel;
        $offset = intval($kernel->pub_httpget_get($this->admin_param_offset_name));
        if ($offset < 0)
            $offset = 0;
        return $offset;
    }


    /**
     * Удаляет одно из возможных значений поля типа enum (общее либо товарной группы)
     * @return void
     */
    private function enum_set_prop_delete()
    {
        global $kernel;
        $val2del = $kernel->pub_httpget_get("enumval", false);
        $propid = $kernel->pub_httpget_get("propid");
        $prop = CatalogCommons::get_prop($propid);
        if ($prop['group_id'] == 0)
            $table = '_catalog_'.$kernel->pub_module_id_get().'_items';
        else
        {
            $group = CatalogCommons::get_group($prop['group_id']);
            $table = '_catalog_items_'.$kernel->pub_module_id_get().'_'.strtolower($group['name_db']);
        }
        $tinfo = $kernel->db_get_table_info($table);
        $evals = $this->get_enum_set_prop_values($tinfo[$prop['name_db']]['Type'], false);
        $newevals = array();
        foreach ($evals as $eval)
        {
            if ($eval != $val2del)
                $newevals[] = $eval;
        }
        if (substr($tinfo[$prop['name_db']]['Type'], 0, 3) == 'set')
            $ptype = 'set';
        else
            $ptype = 'enum';
        if ($ptype == 'enum')
            $query = 'UPDATE `'.$kernel->pub_prefix_get().$table.'` SET `'.$prop['name_db'].'`=NULL WHERE `'.$prop['name_db'].'`="'.$kernel->pub_str_prepare_set($val2del).'"';
        else
            $query = "UPDATE `".$kernel->pub_prefix_get().$table."` SET `".$prop['name_db']."`=REPLACE(`".$prop['name_db']."`,'".$kernel->pub_str_prepare_set($val2del)."','') WHERE `".$prop['name_db']."` LIKE '%".$kernel->pub_str_prepare_set($val2del)."%' ";
        $kernel->runSQL($query);
        $query = 'ALTER TABLE `'.$kernel->pub_prefix_get().$table.'` CHANGE `'.$prop['name_db'].'` `'.$prop['name_db'].'` '.$this->convert_field_type_2_db($ptype, $newevals);
        $kernel->runSQL($query);
    }

    public static function clone_file_field($prop_type,$val)
    {
        global $kernel;

        $site_root = $kernel->pub_site_root_get();
        $full_orig_path=$site_root.'/'.$val;
        $origFilename=pathinfo($full_orig_path,PATHINFO_FILENAME);
        $ext='.'.pathinfo($full_orig_path,PATHINFO_EXTENSION);
        $save_path_rel = pathinfo($val,PATHINFO_DIRNAME);
        $save_path_full = $site_root.'/'.$save_path_rel;
        $newname=$origFilename.$ext;
        $n=1;
        while (file_exists($save_path_full."/".$newname) || file_exists($save_path_full."/tn/".$newname) || file_exists($save_path_full."/source/".$newname))
        {
            $n++;
            $newname=$origFilename.'_'.$n.$ext;
        }
        $newval = $save_path_rel.'/'.$newname;
        copy($full_orig_path,$save_path_full.'/'.$newname);
        if($prop_type=='pict')
        {
            $path_parts = pathinfo($val);
            $path_small = $path_parts['dirname'].'/tn/'.$path_parts['basename'];
            if(file_exists($path_small))
                copy($path_small,$save_path_full.'/tn/'.$newname);
            $path_source = $path_parts['dirname'].'/source/'.$path_parts['basename'];
            if(file_exists($path_source))
                copy($path_source,$save_path_full.'/source/'.$newname);
        }
        return $newval;
    }

    /**
     * Клонирует товар по переданному айдишнику
     * @param  integer $id
     * @return integer айдишник нового товара
     */
    private function item_clone($id)
    {
        global $kernel;
        //сначала клонируем запись в общей таблице товаров
        $olditem = CatalogCommons::get_item($id);
        if (!$olditem)
            return false;

        $props = CatalogCommons::get_props2(0);

        $newitem = array();
        foreach ($olditem as $k => $v)
        {
            if ($k == "id")
                continue;
            elseif ($k == "ext_id")
                $v = 0;
            elseif ($k == "name")
                $v .= " копия";
            if ($v && isset($props[$k]) && in_array($props[$k]['type'],array('file','pict')))
                $v=self::clone_file_field($props[$k]['type'],$v);

            if ($k != "ext_id" && mb_strlen($v) == 0)
                $newitem[$k] = null;
            else
                $newitem[$k] = $kernel->pub_str_prepare_set($v);
        }

        if (!$newitem)
            $newitem = array("id" => null);
        $newID = $kernel->db_add_record('_catalog_'.$kernel->pub_module_id_get().'_items', $newitem);

        $group_id=$olditem['group_id'];
        //теперь в таблице товарной группы
        $group = CatalogCommons::get_group($group_id);
        $olditem = CatalogCommons::get_item_group_fields($olditem['ext_id'], $group['name_db']);
        if (!$olditem)
            return false;

        $props = CatalogCommons::get_props2($group_id);
        $newitem = array();
        foreach ($olditem as $k => $v)
        {
            if ($k == "id")
                continue;
            if ($v && isset($props[$k]) && in_array($props[$k]['type'],array('file','pict')))
                $v=self::clone_file_field($props[$k]['type'],$v);

            if (mb_strlen($v) == 0)
                $newitem[$k] = null;
            else
                $newitem[$k] = $kernel->pub_str_prepare_set($v);
        }
        if (!$newitem)
            $newitem = array("id" => null);
        $newExtID = $kernel->db_add_record('_catalog_items_'.$kernel->pub_module_id_get().'_'.strtolower($group['name_db']), $newitem);

        $query = 'UPDATE `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_items` SET `ext_id`='.$newExtID.' WHERE id='.$newID;
        $kernel->runSQL($query);

        //скопируем принадлежность к категориям
        $catIDs = $this->get_item_catids_with_order($id);
        foreach ($catIDs as $cid => $order)
        {
            $query = 'INSERT INTO `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_item2cat` (`cat_id`,`item_id`,`order`) VALUES ('.$cid.','.$newID.','.$order.')';
            $kernel->runSQL($query);
        }

        //скопируем связанные товары
        $linked = $kernel->db_get_list_simple('_catalog_'.$kernel->pub_module_id_get().'_items_links','itemid1='.$id.' OR itemid2='.$id);
        foreach($linked as $row)
        {
            if ($row['itemid1'] == $id)
                $lid = $row['itemid2'];
            else
                $lid = $row['itemid1'];
            $this->add_items_link($newID, $lid);
        }
        return $newID;
    }

    /** Возвращает максимальную дорогу в категориях к товару
     * @param integer  $itemid айдишник товара
     * @param array $cached_cats массив всех категорий
     * @return array
     */
    private function get_max_catway2item($itemid, $cached_cats = null)
    {
        $catids = $this->get_item_catids($itemid);
        $max_length = 0;
        $max_way = array();
        foreach ($catids as $catid)
        {
            $way = $this->get_way2cat($catid, true, $cached_cats);
            if (count($way) > $max_length)
            {
                $max_way = $way;
                $max_length = count($way);
            }
        }
        return $max_way;
    }

    /**
     * Публичный метод для отображения экспорта
     *
     * @param string  $tpl   шаблон
     * @param string  $filterid   строковый ID-шник внутреннего фильтра
     * @param string  $mime_type  mime-type при выводе экспорта
     * @return string
     */
    public function pub_catalog_show_export($tpl, $filterid, $mime_type = false)
    {
        global $kernel;
        $categories = false;
        if ($filterid && $filterid != "null")
        {
            //если указан внутренний фильтр - возьмём товары из него
            //в данном случае будет использоваться шаблон, прописанный в фильтре
            $content = $this->pub_catalog_show_inner_selection_results($filterid, false, array(), 0, false, false);
            $this->set_templates($kernel->pub_template_parse($tpl));
        }
        else
        {
            $this->set_templates($kernel->pub_template_parse($tpl));
            $groups = CatalogCommons::get_groups();
            $groups_props = array(); //здесь будем кэшировать свойства групп, чтобы не повторять запросы к БД
            $items = $this->get_items(0, 0, 0, true);
            if (count($items) == 0)
                $content = $this->get_template_block('list_null');
            else
                $content = $this->get_template_block('list');

            $rows = "";
            if (!$categories)
                $categories = CatalogCommons::get_all_categories($kernel->pub_module_id_get());

            $catalog_page = $kernel->pub_modul_properties_get('catalog_property_catalog_page', $kernel->pub_module_id_get());
            if(empty($catalog_page['value']))
                $catalog_page = 'catalog';
            else
                $catalog_page = $catalog_page['value'];

            foreach ($items as $item)
            {
                $commonid = $item['id'];
                unset($item['id']);
                $item['commonid'] = $commonid;
                $igroup = $groups[$item['group_id']];
                if (isset($groups_props[$item['group_id']]))
                    $props = $groups_props[$item['group_id']];
                else
                {
                    $props = CatalogCommons::get_props($item['group_id'], true);
                    $groups_props[$item['group_id']] = $props;
                }

                $item2 = CatalogCommons::get_item_group_fields($item['ext_id'], $igroup['name_db']);
                if ($item2)
                    $item = $item + $item2;

                $block = $this->get_template_block('row');
                $max_way = $this->get_max_catway2item($item['id'], $categories);
                $category = $max_way[count($max_way) - 1];

                if (defined("USE_PRETTY_URL") && USE_PRETTY_URL) {
                    if(isset($category['name']) && isset($category['id']))
                        $block = str_replace("%link%", '/'.$catalog_page.'/'.$kernel->pub_pretty_url($category['name']).'-c'.$category['id'].'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $block);
                    else
                        $block = str_replace("%link%", '/'.$catalog_page.'/'.$kernel->pub_pretty_url($item['name']).'-i'.$item['id'].'.html', $block);
                } else {
                    $block = str_replace("%link%", $catalog_page.'.html?'.$this->frontend_param_item_id_name.'='.$item['id'], $block);
                }

                $block = $this->process_item_props_out($item, $props, $block, $igroup);
                $rows .= $block;
            }
            $content = str_replace("%row%", $rows, $content);
        }

        // добавим категории в шаблон, если надо
        if (mb_strpos($content, "%categories_flat_list%") !== false)
        {
            if (!$categories)
                $categories = CatalogCommons::get_all_categories($kernel->pub_module_id_get());
            if (count($categories) == 0)
                $block = $this->get_template_block('categories_flat_null');
            else
            {
                $block = $this->get_template_block('categories_flat_list');
                $rows = "";
                foreach ($categories as $cat)
                {
                    $row = $this->get_template_block('category_flat_row');
                    foreach ($cat as $ck => $cv)
                    {
                        $row = str_replace("%".$ck."%", $cv, $row);
                        $row = str_replace("%".$ck."_value%", $cv, $row);
                    }
                    $rows .= $row;
                }
                $block = str_replace("%category_row%", $rows, $block);
            }
            $content = str_replace("%categories_flat_list%", $block, $content);

        }

        if (preg_match_all("|\\%single_cat_id\\[(\\d+)\\]\\%|isU", $content, $matches, PREG_SET_ORDER))
        {
            if (!$categories)
                $categories = CatalogCommons::get_all_categories($kernel->pub_module_id_get());

            foreach ($matches as $match)
            {
                $itemid = intval($match[1]);
                $max_way = $this->get_max_catway2item($itemid, $categories);
                if (count($max_way) > 0)
                    $content = str_replace($match[0], $max_way[count($max_way) - 1]['id'], $content);
                else
                    $content = str_replace($match[0], "0", $content);
            }
        }

        $content = $this->process_variables_out($content);
        $content = $this->clear_left_labels($content);

        if($mime_type == 'xml') {
            header("Content-Type: text/xml");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $content;
            die;
        } else if($mime_type == 'csv') {
            header("Content-Type: text/csv");
            header("Content-Disposition: attachment; filename=export.csv");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo $content;
            die;
        }
        return $content;
    }


    private function get_current_catIDs()
    {
        if (isset($_GET[$this->frontend_param_cat_id_name]))
        {
            if (is_numeric($_GET[$this->frontend_param_cat_id_name]))
                return intval($_GET[$this->frontend_param_cat_id_name]);
            if (is_array($_GET[$this->frontend_param_cat_id_name]))
            {
                $arr = array();
                foreach (array_keys($_GET[$this->frontend_param_cat_id_name]) as $cid)
                {
                    $cid = intval($cid);
                    if ($cid > 0)
                        $arr[] = $cid;
                }
                if ($arr)
                    return $arr;
            }
        }
        if (isset($_POST[$this->frontend_param_cat_id_name]))
        {
            if (is_numeric($_POST[$this->frontend_param_cat_id_name]))
                return intval($_POST[$this->frontend_param_cat_id_name]);
            if (is_array($_POST[$this->frontend_param_cat_id_name]))
            {
                $arr = array();
                foreach (array_keys($_POST[$this->frontend_param_cat_id_name]) as $cid)
                {
                    $cid = intval($cid);
                    if ($cid > 0)
                        $arr[] = $cid;
                }
                if ($arr)
                    return $arr;
            }
        }
        return 0;
    }

    private function cats_props_out($catid, $content)
    {
        $cat = CatalogCommons::get_category($catid);
        if (!$cat)
            return $content;
        $content = str_replace("%catid%", $cat['id'], $content);
        $cats_props = CatalogCommons::get_cats_props();
        foreach ($cats_props as $cprop)
        {
            if (mb_strpos($content, '%category_'.$cprop['name_db'].'%') !== false)
            {
                $content = str_replace('%category_'.$cprop['name_db'].'_name%', $cprop['name_full'], $content);

                if(!empty($cat[$cprop['name_db']])) {
                    $content = str_replace('%category_'.$cprop['name_db'].'%', $this->get_template_block('category_'.$cprop['name_db']), $content);
                    //$content = str_replace('%category_'.$cprop['name_db'].'_value%', $cat[$cprop['name_db']], $content);
                    $content = str_replace('%category_'.$cprop['name_db'].'_value%', $this->format_value($cat[$cprop['name_db']], $cprop), $content);
                } else {
                    $prop_null = $this->get_template_block('category_'.$cprop['name_db'].'_null');
                    // Проверим, осталось ли в шаблоне чтото вида %category_aaaaa%, что может указывать на свойство категории
                    // и заменим если такое было обнаружено
                    if (preg_match("/\\%category_([a-z0-9_-]+)\\%/iU", $prop_null)) {
                        $prop_null = $this->cats_props_out($catid, $prop_null);
                    }
                    $content = str_replace('%category_'.$cprop['name_db'].'%', $prop_null, $content);
                }
            }
        }
        return $content;
    }




    private  function show_option_group_edit()
    {
        global $kernel;

        $this->set_templates($kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'group_options.html'));

        $group_id 			= $kernel->pub_httpget_get('group_id');

        $group_option_id	= $kernel->pub_httpget_get('group_option_id');

        if (!empty($group_option_id))
        {


            $row = $kernel->db_get_record_simple('_catalog_'.$kernel->pub_module_id_get().'_item_props_groups', 'group_id='.$group_id.' AND id ='.$group_option_id.'');


            $source[]	= '%group_title%';
            $replace[]	= $row['title'];

            $source[]	= '%form_action%';
            $replace[]	= $kernel->pub_redirect_for_form('option_group_save&group_id='.$kernel->pub_httpget_get('group_id')).'&group_options_id='.$group_option_id;


        }
        else
        {
            $end = '';

            $source[]	= '%group_title%';
            $replace[]	= '';

            $source[]	= '%form_action%';
            $replace[]	= $kernel->pub_redirect_for_form('option_group_save&group_id='.$kernel->pub_httpget_get('group_id'));


        }



        $content = $this->get_template_block('option_group_add');

        $content = str_replace($source, $replace, $content);
        return $content;
    }


    private function option_group_save()
    {
        global $kernel;


        if (!empty($_GET['group_options_id']))
        {

            $rec['title'] = $kernel->pub_httppost_get('group_title');
            $rec['group_id'] = $kernel->pub_httpget_get('group_id');

            $kernel->db_update_record('_catalog_'.$kernel->pub_module_id_get().'_item_props_groups', $rec, 'id='.$kernel->pub_httpget_get('group_options_id'));
        }
        else
        {
            $rec['title'] = $kernel->pub_httppost_get('group_title');
            $rec['group_id'] = $kernel->pub_httpget_get('group_id');

            $kernel->db_add_record('_catalog_'.$kernel->pub_module_id_get().'_item_props_groups', $rec);
        }



        return $kernel->pub_httppost_response('Группа опций успешно сохранилась', 'show_group_props&id='.$rec['group_id']);


    }


    private function option_group_delete()
    {
        global $kernel;

        $group_options_id = $kernel->pub_httpget_get('group_option_id');


        $sql = "DELETE FROM ".$kernel->pub_prefix_get()."_catalog_".$kernel->pub_module_id_get()."_item_props_groups WHERE id=".$group_options_id;

        $kernel->runSQL($sql);

        $sql = "DELETE  FROM ".$kernel->pub_prefix_get()."_catalog_".$kernel->pub_module_id_get()."_item_props_options WHERE group_option_id=".$group_options_id;

        $kernel->runSQL($sql);


        return $kernel->pub_redirect_refresh('show_group_props&id='.$kernel->pub_httpget_get('group_id'));

    }

    private function show_orders()
    {
        global $kernel;

        $offset		= (int) $kernel->pub_httpget_get($this->admin_param_offset_name);
        $op			= (int) $kernel->pub_httpget_get("op");
        $limit		= $this->get_limit_admin();

        $table_name = '_catalog_'.$kernel->pub_module_id_get().'_basket_orders';


        if (!empty($op))
        {
            $where = 'isprocessed = '.$op;
        }
        else
        {
            $op = 7;
            $where = 'isprocessed > 0';
        }

        $orders = $kernel->db_get_list_simple($table_name, $where.' ORDER BY lastaccess', '*', $offset, $limit);


        $sum = $kernel->db_get_record_simple($table_name, $where, 'COUNT(*) as sum', $offset, $limit);
        $total = $sum['sum'];


        $this->set_templates($kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'orders_list.html'));


        $states = range(0, 7);

        $states_names = array(
            '0'	=> 'Не оформлен',
            '1'	=> 'Оформлен, не подтвержден',
            '2'	=> 'Подтвержден, не оплачен',
            '3'	=> 'Оплачен, в обработке',
            '4'	=> 'В доставке',
            '5'	=> 'Доставлен',
            '6'	=> 'Отменен',
            '7'	=> 'Все'
        );


        $html  = $this->get_template_block('list');


        $order_fields	= CatalogCommons::get_order_fields();
        $tpl_header		= $this->get_template_block('header_row');

        foreach ($order_fields as $item)
        {
            $tpl_rows['fullname'] 	= $item['name_full'];

            $hlines[]	= $kernel->tpl_replace($tpl_rows, $tpl_header);
        }
        $hlines = join('', $hlines);

        unset($tpl_rows);

        $rows='';

        $row = $this->get_template_block('row');


        foreach ($orders as $order)
        {

            $oftd = array();
            foreach ($order_fields as $item)
            {
                $oftd[] = $kernel->tpl_replace(
                    array('value' => $order[$item['name_db']]),
                    $this->get_template_block('order_field_value')
                );
            }

            $oflines = join('', $oftd);

            $postid = ($order['isprocessed']>3) ? $order['sessionid'] : '';


            foreach($states as $key=> $state)
            {

                $tpl_rows['statuses'.$key.'_selected'] = ($key==$op) ? ' selected ' : '';

            }


            $tline = $kernel->tpl_replace(array(
                'order_fields'	=> $oflines,
                'lastaccess'	=> $order['lastaccess'],
                'id'			=> $order['id'],
                'postid'		=> $postid,
                'status'		=> $states_names[$order['isprocessed']]
            ), $row);

            $rows .= $tline;
        }


        //опции груповой обработки ордеров
        $order_process_options='';


        $order_process_options_tpl = $this->get_template_block('order_process_values');


        $tpl_rows['order_filter_values']	= '';
        $tpl_rows['form_action']			= $kernel->pub_redirect_for_form('save_selected_orders');
        $tpl_rows['header_lines']			= $hlines;
        $tpl_rows['rows']					= $rows;
        $tpl_rows['pages']					= $this->build_pages_nav($total, $offset, $limit,'show_orders&'.$this->admin_param_offset_name.'=',0,'url');
        $tpl_rows['change_status']			= $order_process_options;

        $html = $kernel->tpl_replace($tpl_rows, $html);

        return $html;
    }




    private function show_order()
    {
        global $kernel;

        $id = intval($kernel->pub_httpget_get('id'));

        $this->set_templates($kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'order_view.html'));

        $order = $kernel->db_get_record_simple('_catalog_'.$kernel->pub_module_id_get().'_basket_orders','id='.$id);

        if (!$order)
        {
            return $this->get_template_block('not_found');
        }


        $ofields = CatalogCommons::get_order_fields();


        $html=$this->get_template_block('content');



        $lines='';
        $tpl_line = $this->get_template_block('ofield');
        foreach ($ofields as $of)
        {
            $array['name_full'] = $of['name_full'];
            $array['value']		= isset($order[$of['name_db']]) ? $order[$of['name_db']] : '';
            $lines .= $kernel->tpl_replace($array, $tpl_line);
        }
        $html = $kernel->tpl_replace(array(
            'lastaccess'		=> $order['lastaccess'],
            'id'				=> $order['id'],
            'ofields'			=> $lines,
            'order_html_body'	=> $order['text']
        ), $html);

        return $html;
    }


    //груповая обработка ордеров
    private function save_selected_orders()
    {


        global $kernel;
        $vals = $kernel->pub_httppost_get("sso");
        if (!empty($vals))
        {
            $orderids = array();
            foreach ($vals as $orderid => $checked)
                $orderids[] = $orderid;
            $action = $kernel->pub_httppost_get('withselected');
            $manualstatuss=$kernel->pub_httppost_get("orderstatus");
            $postid = $kernel->pub_httppost_get("pid");
            if ($action=='delete_selected')
            {

                $this->delete_orders($orderids);
            }
            else
            {
                foreach ($orderids as $orderid)
                {
                    /*
					if (isset($manualstatuss[$orderid]) && ($manualstatuss[$orderid]==$action))
						$stat=$action;
					else
					{
						$stat=$manualstatuss[$orderid];
					}
				*/


                    $query = "UPDATE `".$kernel->pub_prefix_get()."_catalog_".$kernel->pub_module_id_get()."_basket_orders` SET `isprocessed`=".$action." WHERE `id`=".$orderid;
                    $kernel->runSQL($query);

                    if ($action==5)
                    {
                        $pid='не заполнено!';
                        if (!empty($postid[$orderid])) $pid=$postid[$orderid];
                        $query = "UPDATE `".$kernel->pub_prefix_get()."_catalog_".$kernel->pub_module_id_get()."_basket_orders` SET `sessionid`='". $pid."' WHERE `id`=".$orderid;
                        $kernel->runSQL($query);

                    }
                }

            }
        }
        //отправка уведомлений на email
        $mailflags=$kernel->pub_httppost_get("sma");
        if (!empty($mailflags))
        {
            foreach ($mailflags as $orderid => $checked)
            {
                $this->status_mail_sender($orderid);
            }
        }
    }

    private function status_mail_sender($orderid)
    {

        global $kernel;
        $order=$this->get_order($orderid);
        $email = $order['email'];
        $hostname = $_SERVER['HTTP_HOST'];

        if (empty($email)) return;

        switch (intval($order['isprocessed']))
        {
            case 1: // Оформлен, не подтвержден
                $key = CatalogCommons::generate_random_string(32, true);
                $query = "UPDATE `".$kernel->pub_prefix_get()."_catalog_".$kernel->pub_module_id_get()."_basket_orders` SET `sessionid`='". $key ."' WHERE `id`=".$orderid;
                $kernel->runSQL($query);

                $properties = $kernel->pub_modul_properties_get('catalog_property_order_email_confirm', $kernel->pub_module_id_get());
                if(empty($properties['value']))
                    $template = CatalogCommons::get_templates_user_prefix(). 'order_email_confirm.html';
                else
                    $template = $properties['value'];

                break;
            case 2: // Подтвержден, не оплачен
                $properties = $kernel->pub_modul_properties_get('catalog_property_order_email_confirmed', $kernel->pub_module_id_get());
                if(empty($properties['value']))
                    $template = CatalogCommons::get_templates_user_prefix(). 'order_email_confirmed.html';
                else
                    $template = $properties['value'];

                break;
            case 3: // Оплачен, в обработке
                $properties = $kernel->pub_modul_properties_get('catalog_property_order_email_payment', $kernel->pub_module_id_get());
                if(empty($properties['value']))
                    $template = CatalogCommons::get_templates_user_prefix(). 'order_email_payment.html';
                else
                    $template = $properties['value'];

                break;
            case 4: // В доставке
                $properties = $kernel->pub_modul_properties_get('catalog_property_order_email_shipping', $kernel->pub_module_id_get());
                if(empty($properties['value']))
                    $template = CatalogCommons::get_templates_user_prefix(). 'order_email_shipping.html';
                else
                    $template = $properties['value'];

                break;
            case 5: // Доставлен
                $properties = $kernel->pub_modul_properties_get('catalog_property_order_email_delivered', $kernel->pub_module_id_get());
                if(empty($properties['value']))
                    $template = CatalogCommons::get_templates_user_prefix(). 'order_email_delivered.html';
                else
                    $template = $properties['value'];

                break;
            case 6: // Отменен
                $properties = $kernel->pub_modul_properties_get('catalog_property_order_email_canceled', $kernel->pub_module_id_get());
                if(empty($properties['value']))
                    $template = CatalogCommons::get_templates_user_prefix(). 'order_email_canceled.html';
                else
                    $template = $properties['value'];

                break;
            default:
                return;
                break;
        }

        $this->set_templates($kernel->pub_template_parse($template));
        $mail_subj = $this->get_template_block('subj');
        $mail_subj = str_replace('%orderid%', $orderid, $mail_subj);
        $mail_subj = str_replace('%hostname%', $hostname, $mail_subj);
        $msg_body = $this->get_template_block('email');
        $from_name = $kernel->pub_http_host_get();
        $from_email = "noreply@".$kernel->pub_http_host_get();
        $kernel->pub_mail(array($email), array($email), $from_email, $from_name, $mail_subj, $msg_body, false, "", "", "");
    }

    public function pub_order_confirm($wrong_link_tpl, $confirmed_tpl)
    {
        global $kernel;
        $orderid=$kernel->pub_httpget_get('orderid');
        $key=$kernel->pub_httpget_get('key');

        if (!empty($orderid) && !empty($key))
        {
            $order=$this->get_order($orderid);
            if (!empty($order))
            {
                if ($key==$order['sessionid'] && '1'==$order['isprocessed'])
                {
                    $template = $confirmed_tpl;
                    $query = "UPDATE `".$kernel->pub_prefix_get()."_catalog_".$kernel->pub_module_id_get()."_basket_orders` SET `isprocessed`=2 WHERE `id`=".$orderid;
                    $kernel->runSQL($query);
                    $this->status_mail_sender($orderid);
                }
            }
        }

        if (!isset($template)) $template = $wrong_link_tpl;
        $template = file_get_contents($template);
        $template = str_replace('%orderid%', $orderid, $template);
        $template = $this->process_variables_out($template);
        //очистим оставшиеся метки
        $template = $this->clear_left_labels($template);
        return $template;
    }


    //возвращает ордер по id
    private function get_order($id)
    {
        global $kernel;
        $res    = false;
        $query  = 'SELECT * FROM `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_basket_orders` WHERE `id` ='.intval($id).' LIMIT 1';

        $result = $kernel->runSQL($query);
        if ($row = mysqli_fetch_assoc($result))
            $res = $row;
        mysqli_free_result($result);
        return $res;
    }

    //удаляет ордеры
    private function delete_orders($orderids)
    {
        foreach ($orderids as $orderid)
            $this->delete_order($orderid);
    }

    //удаляет 1 ордер
    private function delete_order($id)
    {
        global $kernel;
        $order = $this->get_order($id);
        if (!$order)
            return false;
        // удаляет basket_orders по id и basket_items по orderid
        $query = 'DELETE FROM `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_basket_orders` WHERE `id`='.$id;
        $kernel->runSQL($query);
        //удаляет товар из корзины если есть
        $query  = 'SELECT * FROM `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_basket_items` WHERE `orderid` ='.intval($id).' LIMIT 1';

        $result = $kernel->runSQL($query);
        if ($row = mysqli_fetch_assoc($result))
            $res = $row;
        mysqli_free_result($result);
        if (isset($res))
        {
            $query = 'DELETE FROM `'.$kernel->pub_prefix_get().'_catalog_'.$kernel->pub_module_id_get().'_basket_items` WHERE `orderid`='.$id;
            $kernel->runSQL($query);
        }

        return true;
    }

    /**
     * Выводит список доступных для редактирования купонов в АИ
     *
     * @return string
     */
    private function show_cupons()
    {
        global $kernel;
        $items = CatalogCommons::get_cupons();
        $this->set_templates($kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'cupons.html'));
        $html = $this->get_template_block('header');
        $content = '';
        if (count($items) > 0)
        {
            $content .= $this->get_template_block('table_header');
            foreach ($items as $item)
            {
                $line = $this->get_template_block('table_body');
                $line = str_replace('%cupon_id%', $item['id'], $line);
                $line = str_replace('%cupon_textcode%', $item['textcode'], $line);
                $line = str_replace('%cupon_created%', date('d.m.Y', strtotime($item['created'])), $line);
                $line = str_replace('%cupon_expiration%', date('d.m.Y', strtotime($item['expiration'])), $line);
                $line = str_replace('%cupon_use_count%', $item['use_count'], $line);
                $line = str_replace('%cupon_type%', $item['type'], $line);
                $line = str_replace('%cupon_value%', $item['cupon_value'], $line);
                $content .= $line;
            }
            $content .= $this->get_template_block('table_footer');
        }
        else
            $content = $this->get_template_block('no_data');
        $html = str_replace("%table%", $content, $html);
        return $html;
    }

    /**
     * Генерирует форму редактирования купона на скидку для АИ
     *
     * @param string  $id   ID купона
     * @return string
     */
    private function show_cupon_form($id)
    {
        global $kernel;
        $this->set_templates($kernel->pub_template_parse(CatalogCommons::get_templates_admin_prefix().'cupons.html'));
        if (empty($id))
        {
            $form_header = '[#catalog_new_cupon_header_label#]';
            $item = array("id" => "", "textcode" => "", "created" => date('d.m.Y'), "expiration" => date('d.m.Y', strtotime(date('d.m.Y') . ' +1 day')), "use_count" => "99", "type" => "lowprice", "cupon_value" => "10");
        }
        else
        {
            $form_header = '[#catalog_edit_cupon_header_label#]';
            $item = CatalogCommons::get_cupon($id);
        }

        $cupon_type_lowprice = '';
        $cupon_type_dropprice = '';
        if ($item['type']=='lowprice')
            $cupon_type_lowprice = ' selected="selected"';
        else if ($item['type']=='dropprice')
            $cupon_type_dropprice = ' selected="selected"';

        $cupon_type_list = '<option value="lowprice"'.$cupon_type_lowprice.'>[#catalog_cupon_type_lowprice#]</option>';
        $cupon_type_list .= '<option value="dropprice"'.$cupon_type_dropprice.'>[#catalog_cupon_type_dropprice#]</option>';

        $content = $this->get_template_block('form');
        $content = str_replace('%form_header%', $form_header, $content);
        $content = str_replace('%cupon_id%', $item['id'], $content);
        $content = str_replace('%cupon_textcode%', $item['textcode'], $content);
        $content = str_replace('%cupon_created%', date('d.m.Y', strtotime($item['created'])), $content);
        $content = str_replace('%cupon_expiration%', date('d.m.Y', strtotime($item['expiration'])), $content);
        $content = str_replace('%cupon_use_count%', $item['use_count'], $content);
        $content = str_replace('%cupon_type_list%', $cupon_type_list, $content);
        $content = str_replace('%cupon_value%', intval($item['cupon_value']), $content);
        $content = str_replace('%form_action%', $kernel->pub_redirect_for_form('cupon_save'), $content);
        return $content;
    }


}