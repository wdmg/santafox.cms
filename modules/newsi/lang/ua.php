<?php

$type_langauge = 'ua';

$il['news_base_name']         = 'Новини';
$il['newsi_modul_base_name1'] = 'Основні новини';

$il['news_property_img_big_width']      = 'Ширина великої картинки у пікселях';
$il['news_property_img_big_height']     = 'Висота великої картинки у пікселях';
$il['news_property_img_small_width']    = 'Ширина маленької картинки у пікселях';
$il['news_property_img_small_height']   = 'Висота маленької картинки у пікселях';
$il['news_property_deliver']            = 'Значення ознаки "У розсилку"';
$il['news_property_lenta']              = 'Значення ознаки "У стрічку"';
$il['news_property_rss']                = 'Значення ознаки "У RSS"';
$il['news_property_news_per_page']      = 'Новин на сторінку для АИ';
$il['news_property_page_for_lenta']     = 'Сторінка де формується архів (заповнюється у тому випадку, коли до новин модуля звертаються інші стрічки новин)';

$il['news_pub_show_lenta']              = 'Сформувати стрічку';
$il['news_pub_show_lenta_template']     = 'Шаблон стрічки';
$il['news_pub_show_lenta_limit']        = 'Кількість новин у стрічці';
$il['news_pub_show_lenta_type']         = 'Тип відбору';
$il['news_pub_show_lenta_type_default'] = 'За замовчуванням';
$il['news_pub_show_lenta_type_past']    = 'Від поточної дати в минуле';
$il['news_pub_show_lenta_type_future']  = 'Від поточної дати в майбутнє';
$il['news_pub_show_lenta_page']         = 'Сторінка, де формується архів';
$il['news_pub_show_lenta_id_modules']   = 'Ідентифікатори модулів (через кому), чиї новини також потраплять у стрічку';
$il['news_pub_show_lenta_header_list']  = 'Заголовок новостної стрічки';




$il['news_pub_show_archive']            = 'Сформувати архів';
$il['news_pub_show_archive_template']   = 'Шаблон';
$il['news_pub_show_archive_limit']      = 'Кількість новин на сторінку';
$il['news_pub_show_archive_type']       = 'Тип відбору';
$il['news_pub_show_archive_default']    = 'Незалежно від дати';
$il['news_pub_show_archive_past']       = 'Від поточної дати в майбутнє';
$il['news_pub_show_archive_future']     = 'Від поточної дати в минуле';

$il['news_pub_show_sorting']            = 'Показати сортування';
$il['news_pub_show_selection']          = 'Показати відбір';
$il['news_pub_show_selection_template'] = 'Шаблон';

$il['news_menu_label'] = 'Управління';
$il['news_menu_show_list'] = 'Переглянути';
$il['news_menu_between'] = 'За період';
$il['news_menu_add_new'] = 'Додати';
$il['news_menu_taxonomy'] = 'Таксономія';
$il['news_menu_cats_list'] = 'Рубрики записів';
$il['news_menu_tags_list'] = 'Список тегів';
$il['news_property_cats_list_label'] = 'Рубрика записи';
$il['news_property_tags_list_label'] = 'Теги записи';
$il['news_delete_taxonomy_alert'] = 'Ви дійсно бажаєте видалити тег із запису';
$il['news_delete_taxonomy_success_msg'] = 'Тег успішно видалений із запису';
$il['news_delete_taxonomy_error_msg'] = 'помилка видалення тега з записи';
$il['news_property_options_default'] = '- не вибрано -';

$il['news_menu_custom_fields'] = 'Довільні поля';
$il['news_custom_fields_empty'] = '-не вибрано-';
$il['custom_fields_field_title'] = 'Назва поля';
$il['custom_fields_field_name'] = 'Ідентифікатор';
$il['custom_fields_field_type'] = 'Тип поля';
$il['custom_fields_field_type_string'] = 'Рядок';
$il['custom_fields_field_type_pagesite'] = 'Сторінка';
$il['custom_fields_field_type_textarea'] = 'Текстове';
$il['custom_fields_field_type_select'] = 'Набір значень (ENUM)';
$il['custom_fields_field_type_checkbox'] = 'Набір значень (SET)';
$il['custom_fields_field_type_date'] = 'Дата і час';
$il['custom_fields_field_type_fileselect'] = 'Вибір файлу';
$il['custom_fields_field_type_imageselect'] = 'Вибір зображення';
$il['custom_fields_field_type_label'] = 'Кожне значення з нового рядка';
$il['custom_fields_field_new'] = 'Нове довільне поле';
$il['custom_fields_field_edit'] = 'Редагування довільного поля';
$il['custom_fields_field_order'] = 'Порядок сортування';
$il['custom_fields_order_delete'] = 'Видалити вибрані';
$il['news_custom_field_delete_confirm'] = 'Дійсно видалити довільне поле? Всі значення будуть втрачені. ';
$il['custom_fields_order_save'] = 'Зберегти порядок';
$il['custom_fields_order_add'] = 'Додати поле';
$il['news_save'] = 'Зберегти';
$il['news_property_seo_label'] = 'Мета-теги (SEO)';

$il['news_tags_header_title'] = 'Список тегів';
$il['news_add_tag_label'] = 'Додати тег';
$il['news_add_new_tag_label'] = 'Новий канал';
$il['news_edit_tag_label'] = 'Редагування тега';
$il['news_tag_id'] = 'ID';
$il['news_tag_name'] = 'Назва';
$il['news_tag_description'] = 'Опис';
$il['news_tag_use_count'] = 'Всього записів';
$il['news_tags_table_empty'] = 'У списку тегів порожньо';
$il['news_list_tag_action_edit'] = 'Редагувати';
$il['news_list_tag_action_del'] = 'Видалити';
$il['news_tag_edit_save_label'] = 'Зберегти';
$il['news_tag_del_alert'] = 'Ви дійсно бажаєте видалити тег з усіх записів !?';

$il['news_cats_header_title'] = 'Список рубрик';
$il['news_add_cat_label'] = 'Додати рубрику';
$il['news_add_new_cat_label'] = 'Нова рубрика';
$il['news_edit_cat_label'] = 'Редагування рубрики';
$il['news_cat_id'] = 'ID';
$il['news_cat_name'] = 'Назва';
$il['news_cat_description'] = 'Опис';
$il['news_cat_use_count'] = 'Всього записів';
$il['news_cats_table_empty'] = 'У списку рубрик порожньо';
$il['news_list_cat_action_edit'] = 'Редагувати';
$il['news_list_cat_action_del'] = 'Видалити';
$il['news_cat_edit_save_label'] = 'Зберегти';
$il['news_cat_del_alert'] = 'Ви дійсно бажаєте видалити рубрику з усіх записів !?';

// Публічний метод, щоб отримати перелік рубрик
$il['news_pub_show_cats'] = 'Показати список рубрик';
$il['news_pub_show_cats_template'] = 'Шаблон списку рубрик';
$il['news_pub_show_cats_header'] = 'Заголовок списку рубрик';
$il['news_pub_show_cats_ifempty'] = 'Виводити порожні рубрики';
$il['news_pub_show_tags'] = 'Показати список тегів';
$il['news_pub_show_tags_template'] = 'Шаблон списку тегів';
$il['news_pub_show_tags_header'] = 'Заголовок списку тегів';
$il['news_property_taxonomy']  = 'Використовувати таксономії рубрик та тегів';
$il['news_property_url_item_cats'] = 'Ідентифікатор рубрик в URL';
$il['news_property_url_item_tags'] = 'Ідентифікатор тегів в URL';
$il['news_pub_show_ifcount'] = 'Показувати лічильник записів';
$il['news_pub_show_infulltext']  = 'Показувати список при читанні';

$il['news_show_list_action_lenta_on']       = 'Відображати у стрічці';
$il['news_show_list_action_lenta_off']      = 'Не відображати у стрічці';
$il['news_show_list_action_available_on']   = 'Зробити видимим';
$il['news_show_list_action_available_off']  = 'Зробити невидимим';
$il['news_show_list_action_rss_on']         = 'Відображати у RSS';
$il['news_show_list_action_rss_off']        = 'Не відображати у RSS';
$il['news_show_list_action_delete']         = 'Видалити';
$il['news_show_list_action_move']           = 'Перемістити';

// Заголовки в show_list
$il['news_item_id']         = 'ID';
$il['news_item_date']       = 'Дата';
$il['news_item_datetime']   = 'Дата та час';
$il['news_item_header']     = 'Заголовок';
$il['news_item_available']  = 'Видимість';
$il['news_item_lenta']      = 'У стрічці';
$il['news_item_rss']        = 'У RSS';
$il['news_item_author']     = 'Автор';
$il['news_item_actions']    = 'Дія';

$il['news_property_date_label']                 = 'Дата публікації';
$il['news_property_time_label']                 = 'Час публікації';
$il['news_property_available_label']            = 'Новина активна';
$il['news_property_lenta_label']                = 'Відображати у стрічці';
$il['news_property_delivery_label']             = 'Відображати у розсилці';
$il['news_property_rss_label']                  = 'Відображати у RSS';
$il['news_property_header_label']               = 'Заголовок';
$il['news_property_description_short_label']    = 'Короткий опис';
$il['news_property_description_full_label']     = 'Повний опис';
$il['news_property_author_label']               = 'Автор';
$il['news_property_source_name_label']          = 'Ім’я джерела';
$il['news_property_source_url_label']           = 'Адреса URL';
$il['news_property_image_label']                = 'Зображення';
$il['news_property_news_date_format']           = 'Формат дати (d.m.Y)';

$il['news_item_action_edit']        = 'Редагувати';
$il['news_item_action_remove']      = 'Видалити';
$il['news_show_list_submit']        = 'ОК';
$il['news_actions_with_selected']   = 'Дія з відміченими:';
$il['news_submit_label']            = 'Зберегти';
$il['news_actions_simple']          = 'Дія';
$il['news_actions_advanced']        = 'Перемістити в:';
$il['news_item_number']             = 'Номер';
$il['news_menu_label1']             = 'Відбір за датою';
$il['news_select_between_label']    = 'Укажіть бажаний діапазон дат';
$il['news_start_date']              = 'Укажіть бажаний діапазон дат';
$il['news_end_date']                = 'Укажіть бажаний діапазон дат';
$il['news_button_show']             = 'Відобразити';

$il['news_property_pages_count']    = 'Кількість сторінок у блоці (N)';
$il['news_pub_pages_type']          = 'Вигляд посторінкової навігації';
$il['news_pub_pages_get_block']     = 'Блоками по N сторінок';
$il['news_pub_pages_get_float']     = 'Тек. сторінка завжди у центрі блоку з N сторінок';
$il['news_delete_confirm']			= 'Ви дійсно хочете видалити новину?';

$il['news_pub_show_html_title']       = 'Вивести заголовок';
$il['news_pub_show_meta_keywords']    = 'Вивести meta-keywords';
$il['news_pub_show_meta_description'] = 'Вивести meta-description';
$il['news_pub_pub_show_html_title_def'] = 'Заголовок за замовчуванням';
$il['news_pub_pub_show_meta_keywords_def'] = 'Meta-keywords за замовчуванням';
$il['news_pub_pub_show_meta_description_def'] = 'Meta-description за замовчуванням';
$il['news_property_html_title_label']       = 'SEO: Заголовок';
$il['news_property_meta_keywords_label']    = 'SEO: Meta-keywords';
$il['news_property_meta_description_label'] = 'SEO: Meta-description';

$il['news_error_incorrect_datetime'] 		= 'Некоректне значення дати або часу';
