<?php
/**
 * Модуль "Новости"
 *
 * @author Александр Ильин mecommayou@gmail.com, s@nchez s@nchez.me
 * @copyright ArtProm (с) 2001-2019
 * @name newsi
 * @contributors Oslix <oslix@yandex.ru>, Alexsander Vyshnyvetskyy (alex_wdmg) <wdmg.com.ua@gmail.com>, Rinat <mail.rinat@yandex.ru>, sanchez <sanchezby@gmail.com>, bubek <bvvtut@tut.by>, Alexander Merkulov <sasha@merqlove.ru>, VIA <ivan@vbsite.ru>
 * @version 1.2
 *
 */

require_once realpath(dirname(__FILE__)."/../../")."/include/basemodule.class.php";

class newsi extends basemodule
{
    /**
     * Действие по умолчанию
     *
     * @var string
     */
    protected $action_default = '';

    /**
     * Название перемнной в GET запросе определяющей действие
     *
     * @var string
     */
    protected $action_name = 'view';

    /**
     * Префикс путей к шаблонам административного интерфейса
     *
     * @var string
     */
    protected $templates_admin_prefix = '';

    /**
     * Префикс путей к шаблонам пользовательского интерфейса
     *
     * @var string
     */
    protected $templates_user_prefix = '';

    /**
     * Формат вывода даты
     *
     * @var string
     */
    protected $date_format = '';

    /**
     * Формат вывода рубрик в адресной строке
     *
     * @var string
     */
    protected $url_item_cats = 'category';

    /**
     * Формат вывода тегов в адресной строке
     *
     * @var string
     */
    protected $url_item_tags = 'tags';

    public function __construct()
    {
        global $kernel;
        if ($kernel->pub_httpget_get('flush'))
            $kernel->pub_session_unset();

        $value_date_format = $kernel->pub_modul_properties_get("news_date_format", $kernel->pub_module_id_get());
        $this->date_format = $value_date_format['value'];

        $value_url_item_cats = $kernel->pub_modul_properties_get("url_item_cats", $kernel->pub_module_id_get());
        if(is_array($value_url_item_cats))
            $this->url_item_cats = $value_url_item_cats['value'];

        $value_url_item_tags = $kernel->pub_modul_properties_get("url_item_tags", $kernel->pub_module_id_get());
        if(is_array($value_url_item_tags))
            $this->url_item_tags = $value_url_item_tags['value'];


    }


    /**
     * Возвращает имя переменной GET запроса определяющей действие
     *
     * @return string
     */
    protected function get_action_name()
    {
        return $this->action_name;
    }

    /**
     * Возвращает значение указанного действия, если установленно или значение по умолчанию
     *
     * @param string $action_name Имя параметра в GET запросе
     * @return string
     */
    protected function get_action_value($action_name)
    {
        global $kernel;

        if ($kernel->pub_httpget_get($action_name))
            return $kernel->pub_httpget_get($action_name);
        else
            return $this->action_default;
    }


    /**
     * Устанавливает действие по умолчанию
     *
     * @param string $value Имя GET параметра определяющего действие
     */
    protected function set_action_default($value)
    {
        $this->action_default = $value;
    }

    /**
     * Устанавливает имя переменной в GET запросе определяющей действие
     *
     * @param string $name
     */
    protected function set_action_name($name)
    {
        $this->action_name = $name;
    }

    /**
     * Возвращет префикс путей к шаблонам административного интерфейса
     *
     * @return string
     */
    protected function get_templates_admin_prefix()
    {
        return $this->templates_admin_prefix;
    }

    /**
     * Устанавливает префикс к шаблонам админки
     *
     * @param string $prefix
     */
    protected function set_templates_admin_prefix($prefix)
    {
        $this->templates_admin_prefix = $prefix;
    }

    /**
     * Возвращет префикс путей к шаблонам пользовательского интерфейса
     *
     * @return string
     */
    protected function get_templates_user_prefix()
    {
        return $this->templates_user_prefix;
    }

    /**
     * Устанавливает префикс путей к шаблонам пользовательского интерфейса
     *
     * @param string $prefix
     */
    protected function set_templates_user_prefix($prefix)
    {
        $this->templates_user_prefix = $prefix;
    }


    function pub_show_selection($template)
    {
        global $kernel;

        $this->set_templates($kernel->pub_template_parse($template));

        $content = $this->get_template_block('form');

        if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
            $content = str_replace('%url%', "/".$kernel->pub_page_current_get()."/", $content);
        else
            $content = str_replace('%url%', $kernel->pub_page_current_get() . '.html', $content);

        $content = str_replace('%date_alone_name%', 'date', $content);
        $content = str_replace('%date_start_name%', 'start', $content);
        $content = str_replace('%date_stop_name%', 'stop', $content);

        return $content;
    }

    /**
     * Публичное действие для отображения ленты новостей
     *
     * @param string $template Путь к файлу с шаблонами
     * @param integer $limit Количество выводимых новостей
     * @param string $type Тип отбора новостей для вывода
     * @param string $page ID странцы сайта с архивом новостей
     * @param string $id_modules IDшники модулей
     * @return string
     */
    public function pub_show_lenta($template, $header = '', $limit, $type, $page = '', $id_modules = '')
    {
        global $kernel;
        $limit = intval($limit);
        $offset = 0;
        $module_id = $kernel->pub_module_id_get();
        $items = $this->pub_items_get($limit, $offset, true, $type, $id_modules);
        $total = $this->pub_news_avaiable_get($type, null, null, null, $id_modules);
        $page_name = $this->get_page_name($page);
        $this->set_templates($kernel->pub_template_parse($template));
        if (empty($items))
            $content = $this->get_template_block('no_data');
        else
        {
            $lines = '';
            $fields = $this->get_custom_fields($module_id);
            foreach ($items as $item)
            {
                $line = $this->get_template_block('rows');

                if (empty($item['image']))
                    $line = str_replace('%image%', $this->get_template_block('no_images'), $line);
                else
                    $line = str_replace('%image%', str_replace(array('%image_source%', '%image_thumb%', '%image_big%'), array('/content/images/' . $module_id . '/source/' . $item['image'], '/content/images/' . $module_id . '/tn/' . $item['image'], '/content/images/' . $module_id . '/' . $item['image']), $this->get_template_block('image')), $line);

                if (empty($item['source_name']) && empty($item['source_url']))
                    $line = str_replace('%source%', '', $line);
                elseif (!empty($item['source_name']) && !empty($item['source_url']))
                    $line = str_replace('%source%', str_replace(array('%source_name%', '%source_url%'), array($item['source_name'], $item['source_url']), $this->get_template_block('source')), $line);
                elseif (!empty($item['source_name']) && empty($item['source_url']))
                    $line = str_replace('%source%', str_replace(array('%source_name%', '%source_url%'), array($item['source_name'], ''), $this->get_template_block('source_no_url')), $line);
                elseif (empty($item['source_name']) && !empty($item['source_url']))
                    $line = str_replace('%source%', str_replace(array('%source_name%', '%source_url%'), array($item['source_url']), $this->get_template_block('source')), $line);

                $page_ch = $page;
                $page_for_lenta = $kernel->pub_modul_properties_get("page_for_lenta", $module_id);
                if (!empty($page_for_lenta['value']))
                    $page_ch = $page_for_lenta['value'];

                $line = str_replace('%id%', $item['id'], $line);

                if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                    $line = str_replace('%url%', "/".$page_ch."/".$kernel->pub_pretty_url($item['header'])."-p".$item['id'].".html", $line);
                else
                    $line = str_replace('%url%', $page_ch . '.html?id=' . $item['id'], $line);

                if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                    $line = str_replace('%url_arhive%', "/".$page_ch."/", $line);
                else
                    $line = str_replace('%url_arhive%', $page_ch . '.html', $line);

                if (!empty($this->date_format))
                    $line = str_replace('%date%', date($this->date_format, strtotime($item['date'])), $line);
                else
                    $line = str_replace('%date%', $item['date'], $line);

                $line = $this->priv_custom_fields($fields, $item, $line);
                $line = str_replace('%time%', $item['time'], $line);
                $line = str_replace('%header%', $item['header'], $line);
                $line = str_replace('%description_short%', $item['description_short'], $line);
                $line = str_replace('%description_full%', $item['description_full'], $line);
                $line = str_replace('%author%', $item['author'], $line);

                $lines .= $line . "\r\n";
            }

            $content = $this->get_template_block('content');
            $content = str_replace('%rows%', $lines, $content);

            if(isset($page_name)){
                $content = str_replace('%page_name%', $page_name, $content);
            } else {
                $label = $this->get_module_label();
                $content = str_replace('%page_name%', $label, $content);
            }

            if (defined("USE_PRETTY_URL") && USE_PRETTY_URL) {
                $page_url = "/".$kernel->pub_page_current_get().'/page-';
            } else {
                $page_url = $kernel->pub_page_current_get().'.html?offset=';
            }

            $max_pages=10;
            $content = str_replace('%pages%', $this->build_pages_nav($total, $offset, $limit,$page_url,$max_pages,'url'), $content);
        }

        $content = str_replace('%header%', $header, $content);
        return $content;
    }

    protected function get_module_label()
    {
        global $kernel;
        $manager_modules = new manager_modules();
        $current_module = $manager_modules->return_info_modules($kernel->pub_module_id_get());
        return $kernel->pub_page_textlabel_replace($current_module['full_name']);
    }

    protected function get_page_name($page)
    {
        global $kernel;
        $title = $kernel->pub_page_property_get($page, 'name_title');
        return $title['value'];
    }

    protected function get_current_news_item()
    {
        global $kernel;
        $id=intval($kernel->pub_httpget_get('id'));
        if (!$id)
            return null;
        return $this->pub_item_get($id);
    }

    protected function get_current_news_item_field_encoded($fieldname)
    {
        global $kernel;
        $item = $this->get_current_news_item();
        if(!$item)
        {
            if($fieldname=='html_title')
                return $kernel->priv_page_title_get('');
            return '';
        }
        return htmlspecialchars($item[$fieldname]);
    }

    /**
     * Публичное действие для оотбражения `title` новости
     *
     * @param string $html_title_def Заголовок новости по умолчанию,
     * например для страниц с листингом новостей
     * @return string
     */
    public function pub_show_html_title($html_title_def)
    {
        global $kernel;
        $html_title = $this->get_current_news_item_field_encoded('html_title');

        if($html_title!='')
            return $html_title;
        else
            return trim($html_title_def);
    }

    /**
     * Публичное действие для оотбражения `meta-keywords` новости
     *
     * @param string $meta_keywords_def Ключевые слова новости по умолчанию,
     * например для страниц с листингом новостей
     * @return string
     */
    public function pub_show_meta_keywords($meta_keywords_def)
    {
        global $kernel;
        $meta_keywords = $this->get_current_news_item_field_encoded('meta_keywords');

        if($meta_keywords!='')
            return $meta_keywords;
        else
            return trim($meta_keywords_def);
    }

    /**
     * Публичное действие для оотбражения `meta-description` новости
     *
     * @param string $meta_description_def Описание новости по умолчанию,
     * например для страниц с листингом новостей
     * @return string
     */
    public function pub_show_meta_description($meta_description_def)
    {
        global $kernel;
        $meta_description = $this->get_current_news_item_field_encoded('meta_description');

        if($meta_description!='')
            return $meta_description;
        else
            return trim($meta_description_def);
    }

    /**
     * Публичное действие для оотбражения архива новостей
     *
     * @param string $template Путь к файлу с шаблонами
     * @param integer $limit Количество новостей на страницу
     * @param string $type Тип отбора новостей для вывода
     * @param string $pages_type Тип постраничной навигации (уже не используется)
     * @param integer $max_pages Страниц в блоке
     * @return string
     */
    public function pub_show_archive($template, $limit, $type, $pages_type, $max_pages)
    {
        global $kernel;
        $module_id = $kernel->pub_module_id_get();
        $modul_properties = $kernel->pub_modul_properties_get("taxonomy");
        $use_taxonomy = $modul_properties['value'];
        $fields = $this->get_custom_fields($module_id);
        $this->set_templates($kernel->pub_template_parse($template));

        // Отображение конретной новости
        if ($kernel->pub_httpget_get('id') && is_numeric($kernel->pub_httpget_get('id')))
        {
            $item = $this->pub_item_get($kernel->pub_httpget_get('id'));
            if (!$item)
                frontoffice_manager::throw_404_error();

            $content = $this->get_template_block('fulltext');

            if (empty($item['image']))
                $content = str_replace('%image%', $this->get_template_block('fulltext_no_images'), $content);
            else
            {
                $image = $this->get_template_block('fulltext_image');
                $image = str_replace('%image_source%', '/content/images/' . $module_id . '/source/' . $item['image'], $image);
                $image = str_replace('%image_thumb%', '/content/images/' . $module_id . '/tn/' . $item['image'], $image);
                $image = str_replace('%image_big%', '/content/images/' . $module_id . '/' . $item['image'], $image);
                $content = str_replace('%image%', $image, $content);
            }

            if (empty($item['source_name']) && empty($item['source_url']))
                $content = str_replace('%source%', '', $content);
            elseif (!empty($item['source_name']) && !empty($item['source_url']))
                $content = str_replace('%source%', str_replace(array('%source_name%', '%source_url%'), array($item['source_name'], $item['source_url']), $this->get_template_block('fulltext_source')), $content);
            elseif (!empty($item['source_name']) && empty($item['source_url']))
                $content = str_replace('%source%', str_replace(array('%source_name%', '%source_url%'), array($item['source_name'], ''), $this->get_template_block('fulltext_source_no_url')), $content);
            elseif (empty($item['source_name']) && !empty($item['source_url']))
                $content = str_replace('%source%', str_replace(array('%source_name%', '%source_url%'), array($item['source_url']), $this->get_template_block('fulltext_source')), $content);
            $kernel->pub_page_title_add($item['header']);

            if (!empty($this->date_format))
                $content = str_replace('%date%', date($this->date_format, strtotime($item['date'])), $content);
            else
                $content = str_replace('%date%', $item['date'], $content);

            $content = str_replace('%time%', $item['time'], $content);
            $content = str_replace('%header%', $item['header'], $content);
            $content = str_replace('%description_short%', $item['description_short'], $content);
            $content = str_replace('%description_full%', $item['description_full'], $content);

            // Рубрика записи
            if($use_taxonomy) {
                $category = $this->get_cat($item['cat_id']);
                if (is_array($category) && count($category) > 0) {
                    $cat_link = $this->get_template_block('fulltext_category');
                    $cat_link = str_replace('%id%', $category['id'], $cat_link);
                    $cat_link = str_replace('%name%', $category['name'], $cat_link);

                    if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                        $cat_link = str_replace('%link%', "/".$kernel->pub_page_current_get()."/".$this->url_item_cats."/".$kernel->pub_pretty_url($category['name'])."-i".$category['id']."/", $cat_link);
                    else
                        $cat_link = str_replace('%link%', $kernel->pub_page_current_get().'.html?'.$this->url_item_cats.'='.$category['id'], $cat_link);

                } else {
                    $cat_link = $this->get_template_block('fulltext_category_null');
                }
                $content = str_replace('%category%', $cat_link, $content);
            }

            // Список тегов записи
            if($use_taxonomy) {
                $tags = $this->get_taxonomy_list($item['id']);
                if (is_array($tags) && count($tags) > 0) {
                    $i = 0;
                    $item_tags = '';
                    $tags_list = $this->get_template_block('fulltext_tags');
                    foreach($tags as $tag_id => $tag_name) {
                        $link = $this->get_template_block('fulltext_tags_link');
                        $link  = str_replace('%id%', $tag_id, $link);
                        $link  = str_replace('%name%', $tag_name, $link);

                        if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                            $link = str_replace('%link%', "/".$kernel->pub_page_current_get()."/".$this->url_item_tags."/".$kernel->pub_pretty_url($tag_name)."-i".$tag_id."/", $link);
                        else
                            $link = str_replace('%link%', $kernel->pub_page_current_get().'.html?'.$this->url_item_tags.'='.$tag_id, $link);

                        $i++;
                        if(count($tags) > $i)
                            $item_tags .= $link.$this->get_template_block('fulltext_tags_delimiter');
                        else
                            $item_tags .= $link;
                    }
                    $tags_list = str_replace('%list%', $item_tags, $tags_list);
                } else {
                    $tags_list = $this->get_template_block('fulltext_tags_null');
                }
                $content = str_replace('%tags%', $tags_list, $content);
            }

            // Произвольные поля
            $content = $this->priv_custom_fields($fields, $item, $content);

            $author = $this->get_template_block('author');
            if (!empty($item['author']) && !empty($author))
                $content = str_replace('%author%', str_replace('%author%', $item['author'], $this->get_template_block('fulltext_author')), $content);
            else
                $content = str_replace('%author%', $item['author'], $content);

            $kernel->pub_waysite_set(array('url' => '#', 'caption' => $item['header'], 'properties'=> array('name_title' => $item['description_short'])));
        }
        else // Отображение списка
        {

            // Проверка при отборе за дату
            $date = $kernel->pub_httpget_get('date');
            if(empty($date))
                $date = $kernel->pub_httppost_get('date');

            if (preg_match('/\d{1,2}\-\d{1,2}\-\d{2,4}/', $date))
            {
                list($day, $month, $year) = explode("-", $date);
                if (checkdate($month, $day, $year))
                    $date = $year . '-' . $month . '-' . $day;

            }

            $start = $kernel->pub_httpget_get('start');
            if(empty($start))
                $start = $kernel->pub_httppost_get('start');

            if (preg_match('/\d{1,2}\-\d{1,2}\-\d{2,4}/', $start))
            {
                list($day, $month, $year) = explode("-", $start);
                if (checkdate($month, $day, $year))
                    $start = $year . '-' . $month . '-' . $day;
            }

            $stop = $kernel->pub_httpget_get('stop');
            if(empty($stop))
                $stop = $kernel->pub_httppost_get('stop');

            if (preg_match('/\d{1,2}\-\d{1,2}\-\d{2,4}/', $stop))
            {
                list($day, $month, $year) = explode("-", $stop);
                if (checkdate($month, $day, $year))
                    $stop = $year . '-' . $month . '-' . $day;
            }

            $offset = $this->pub_offset_get();
            $page = $kernel->pub_httpget_get('page');


            if(empty($offset) && !empty($page) && (defined("USE_PRETTY_URL") && USE_PRETTY_URL))
                $offset = ($page-1) * $limit;

            if ($this->pub_catid_get()) { // Отображение выборки по рубрике
                $catid = $this->pub_catid_get();
                $items = $this->pub_items_get($limit, $offset, false, $type, "", null, null, null, $catid);
            } else if ($this->pub_tagid_get()) { // Отображение выборки по тегу
                $tagid = $this->pub_tagid_get();
                $items = $this->pub_items_get_by_tag($limit, $offset, $type, $tagid);
            } else {
                if (empty($date) && empty($stop) && empty($start))
                    $items = $this->pub_items_get($limit, $offset, false, $type, "");
                elseif (!empty($date))
                    $items = $this->pub_items_get($limit, $offset, false, $type, "", $date);
                elseif (!empty($start) || !empty($stop))
                    $items = $this->pub_items_get($limit, $offset, false, $type, "", null, $start, $stop);
            }

            if (empty($items))
                $content = $this->get_template_block('no_data');
            else
            {
                $lines = '';
                $page_url = '';
                foreach ($items as $item)
                {
                    $line = $this->get_template_block('rows');

                    if (empty($item['image'])){
                        $no_images = $this->get_template_block('no_images');
                        $line = str_replace('%image%', $no_images, $line);
                    } else{
                        $line = str_replace('%image%', str_replace(array('%image_source%', '%image_thumb%', '%image_big%'), array('/content/images/' . $kernel->pub_module_id_get() . '/source/' . $item['image'], '/content/images/' . $kernel->pub_module_id_get() . '/tn/' . $item['image'], '/content/images/' . $kernel->pub_module_id_get() . '/' . $item['image']), $this->get_template_block('image')), $line);
                    }
                    if (empty($item['source_name']) && empty($item['source_url']))
                        $line = str_replace('%source%', '', $line);
                    elseif (!empty($item['source_name']) && !empty($item['source_url'])){
                        $source = $this->get_template_block('source');
                        $line = str_replace('%source%', str_replace(array('%source_name%', '%source_url%'), array($item['source_name'], $item['source_url']), $source), $line);
                    } elseif (!empty($item['source_name']) && empty($item['source_url'])){
                        $source_no_url = $this->get_template_block('source_no_url');
                        $line = str_replace('%source%', str_replace(array('%source_name%', '%source_url%'), array($item['source_name'], ''), $source_no_url), $line);
                    } elseif (empty($item['source_name']) && !empty($item['source_url'])){
                        $source = $this->get_template_block('source');
                        $line = str_replace('%source%', str_replace(array('%source_name%', '%source_url%'), array($item['source_url']), $source), $line);
                    }
                    $line = str_replace('%id%', $item['id'], $line);

                    // Рубрика записи
                    if($use_taxonomy) {
                        $category = $this->get_cat($item['cat_id']);
                        if (is_array($category) && count($category) > 0) {
                            $cat_link = $this->get_template_block('category');
                            $cat_link = str_replace('%id%', $category['id'], $cat_link);
                            $cat_link = str_replace('%name%', $category['name'], $cat_link);

                            if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                                $cat_link = str_replace('%link%', "/".$kernel->pub_page_current_get()."/".$this->url_item_cats."/".$kernel->pub_pretty_url($category['name'])."-i".$category['id']."/", $cat_link);
                            else
                                $cat_link = str_replace('%link%', $kernel->pub_page_current_get().'.html?'.$this->url_item_cats.'='.$category['id'], $cat_link);

                        } else {
                            $cat_link = $this->get_template_block('category_null');
                        }
                        $line = str_replace('%category%', $cat_link, $line);
                    }

                    // Список тегов записи
                    if($use_taxonomy) {
                        $tags = $this->get_taxonomy_list($item['id']);
                        if (is_array($tags) && count($tags) > 0) {
                            $i = 0;
                            $item_tags = '';
                            $tags_list = $this->get_template_block('tags');
                            foreach($tags as $tag_id => $tag_name) {
                                $link = $this->get_template_block('tags_link');
                                $link  = str_replace('%id%', $tag_id, $link);
                                $link  = str_replace('%name%', $tag_name, $link);

                                if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                                    $link = str_replace('%link%', "/".$kernel->pub_page_current_get()."/".$this->url_item_tags."/".$kernel->pub_pretty_url($tag_name)."-i".$tag_id."/", $link);
                                else
                                    $link = str_replace('%link%', $kernel->pub_page_current_get().'.html?'.$this->url_item_tags.'='.$tag_id, $link);

                                $i++;
                                if(count($tags) > $i)
                                    $item_tags .= $link.$this->get_template_block('tags_delimiter');
                                else
                                    $item_tags .= $link;
                            }
                            $tags_list = str_replace('%list%', $item_tags, $tags_list);
                        } else {
                            $tags_list = $this->get_template_block('tags_null');
                        }
                        $line = str_replace('%tags%', $tags_list, $line);
                    }

                    // Произвольные поля
                    $line = $this->priv_custom_fields($fields, $item, $line);

                    if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                        $line = str_replace('%url%', "/".$kernel->pub_page_current_get()."/".$kernel->pub_pretty_url($item['header'])."-p".$item['id'].".html", $line);
                    else
                        $line = str_replace('%url%', $kernel->pub_page_current_get().'.html?id=' . $item['id'], $line);

                    if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                        $line = str_replace('%url_arhive%', "/".$kernel->pub_page_current_get()."/", $line);
                    else
                        $line = str_replace('%url_arhive%', $kernel->pub_page_current_get().'.html', $line);

                    if (!empty($this->date_format))
                        $line = str_replace('%date%', date($this->date_format, strtotime($item['date'])), $line);
                    else
                        $line = str_replace('%date%', $item['date'], $line);

                    $line = str_replace('%time%', $item['time'], $line);
                    $line = str_replace('%header%', $item['header'], $line);
                    $line = str_replace('%description_short%', $item['description_short'], $line);
                    $line = str_replace('%description_full%', $item['description_full'], $line);

                    $author = $this->get_template_block('author');
                    if (!empty($item['author']) && !empty($author))
                        $line = str_replace('%author%', str_replace('%author%', $item['author'], $author), $line);
                    else
                        $line = str_replace('%author%', $item['author'], $line);

                    $lines .= $line;
                }

                $content = $this->get_template_block('content');
                $content = str_replace('%rows%', $lines, $content);

                if ($this->check_date($kernel->pub_httpget_get('date')) || $this->check_date($kernel->pub_httpget_get('start')) || $this->check_date($kernel->pub_httpget_get('stop')))
                    $total = $this->pub_news_avaiable_get($type, $kernel->pub_httpget_get('date'), $kernel->pub_httpget_get('start'), $kernel->pub_httpget_get('stop'));
                elseif ($this->check_date($kernel->pub_httppost_get('date')) || $this->check_date($kernel->pub_httppost_get('start')) || $this->check_date($kernel->pub_httppost_get('stop')))
                    $total = $this->pub_news_avaiable_get($type, $kernel->pub_httppost_get('date'), $kernel->pub_httppost_get('start'), $kernel->pub_httppost_get('stop'));
                else
                    $total = $this->pub_news_avaiable_get($type, null, null, null);

                if (defined("USE_PRETTY_URL") && USE_PRETTY_URL) {
                    $page_url = "/".$kernel->pub_page_current_get().'/';

                    if ($this->check_date($kernel->pub_httpget_get('date')) || $this->check_date($kernel->pub_httppost_get('date'))) {
                        if($this->check_date($kernel->pub_httpget_get('date')))
                            $page_url.= 'date-' . $kernel->pub_httpget_get('date').'/';
                        elseif($this->check_date($kernel->pub_httppost_get('date')))
                            $page_url.= 'date-' . $kernel->pub_httppost_get('date').'/';
                    }
                    if ($this->check_date($kernel->pub_httpget_get('start')) || $this->check_date($kernel->pub_httppost_get('start'))) {
                        if($this->check_date($kernel->pub_httpget_get('start')))
                            $page_url.= 'start-' . $kernel->pub_httpget_get('start').'/';
                        elseif($this->check_date($kernel->pub_httppost_get('start')))
                            $page_url.= 'start-' . $kernel->pub_httppost_get('start').'/';
                    }
                    if ($this->check_date($kernel->pub_httpget_get('stop')) || $this->check_date($kernel->pub_httppost_get('stop'))) {
                        if($this->check_date($kernel->pub_httpget_get('stop')))
                            $page_url.= 'stop-' . $kernel->pub_httpget_get('stop').'/';
                        elseif($this->check_date($kernel->pub_httppost_get('stop')))
                            $page_url.= 'stop-' . $kernel->pub_httppost_get('stop').'/';
                    }

                    $page_url.='page-';

                } else {
                    $page_url = $kernel->pub_page_current_get().'.html?';

                    if ($this->check_date($kernel->pub_httpget_get('date')) || $this->check_date($kernel->pub_httppost_get('date'))) {
                        if($this->check_date($kernel->pub_httpget_get('date')))
                            $page_url.= 'date=' . $kernel->pub_httpget_get('date').'&';
                        elseif($this->check_date($kernel->pub_httppost_get('date')))
                            $page_url.= 'date=' . $kernel->pub_httppost_get('date').'&';
                    }
                    if ($this->check_date($kernel->pub_httpget_get('start')) || $this->check_date($kernel->pub_httppost_get('start'))) {
                        if($this->check_date($kernel->pub_httpget_get('start')))
                            $page_url.= 'start=' . $kernel->pub_httpget_get('start').'&';
                        elseif($this->check_date($kernel->pub_httppost_get('start')))
                            $page_url.= 'start=' . $kernel->pub_httppost_get('start').'&';
                    }
                    if ($this->check_date($kernel->pub_httpget_get('stop')) || $this->check_date($kernel->pub_httppost_get('stop'))) {
                        if($this->check_date($kernel->pub_httpget_get('stop')))
                            $page_url.= 'stop=' . $kernel->pub_httpget_get('stop').'&';
                        elseif($this->check_date($kernel->pub_httppost_get('stop')))
                            $page_url.= 'stop=' . $kernel->pub_httppost_get('stop').'&';
                    }

                    $page_url.='offset=';
                }

                $content = str_replace('%pages%', $this->build_pages_nav($total, $offset, $limit,$page_url,$max_pages,'url'), $content);

                $page_name = $this->get_page_name($kernel->pub_page_current_get());
                if(isset($page_name)){
                    $content = str_replace('%page_name%', $page_name, $content);
                } else {
                    $label = $this->get_module_label();
                    $content = str_replace('%page_name%', $label, $content);
                }
            }
        }

        $content = str_replace('%url_back%', ((isset($_SERVER['HTTP_REFERER'])) ? ($_SERVER['HTTP_REFERER']) : ('/')), $content);

        return $content;
    }

    public function pub_show_cats($template, $header, $ifempty, $ifcounter = false, $for_fulltext = true) {
        global $kernel;
        $modul_properties = $kernel->pub_modul_properties_get("taxonomy");

        if(!$modul_properties['value'])
            return;

        if(!$for_fulltext && $this->get_current_news_item())
            return;

        $this->set_templates($kernel->pub_template_parse($template));
        $cats = $this->get_cats();

        if (is_array($cats) && count($cats) > 0) {
            $cats_list = '';
            $content = $this->get_template_block('content');

            if (!$this->pub_catid_get())
                $def_link = $this->get_template_block('rows_all_active');
            else
                $def_link = $this->get_template_block('rows_all');

            if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                $def_link = str_replace('%url%', "/".$kernel->pub_page_current_get()."/", $def_link);
            else
                $def_link = str_replace('%url%', $kernel->pub_page_current_get().'.html', $def_link);

            if($ifcounter) {
                $count = $this->get_categories_count(false);

                if($count > 0)
                    $counter = $this->get_template_block('counter');
                else
                    $counter = $this->get_template_block('counter_null');

                $counter = str_replace('%count%', $count, $counter);
                $def_link = str_replace('%counter%', $counter, $def_link);
            } else {
                $def_link = str_replace('%counter%', '', $def_link);
            }

            foreach($cats as $category) {

                if ($this->pub_catid_get() == $category['id'])
                    $cat_link = $this->get_template_block('rows_active');
                else
                    $cat_link = $this->get_template_block('rows');

                $cat_link = str_replace('%id%', $category['id'], $cat_link);
                $cat_link = str_replace('%name%', $category['name'], $cat_link);

                if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                    $cat_link = str_replace('%url%', "/".$kernel->pub_page_current_get()."/".$this->url_item_cats."/".$kernel->pub_pretty_url($category['name'])."-i".$category['id']."/", $cat_link);
                else
                    $cat_link = str_replace('%url%', $kernel->pub_page_current_get().'.html?'.$this->url_item_cats.'='.$category['id'], $cat_link);

                if($ifcounter) {
                    //get_taxonomy_count($tag_id) { //get_categories_count
                    $count = $this->get_categories_count($category['id']);
                    if($count > 0) {
                        $counter = $this->get_template_block('counter');
                        $counter = str_replace('%count%', $count, $counter);
                        $cat_link = str_replace('%counter%', $counter, $cat_link);
                        $cats_list .= $cat_link;
                    } else if($ifempty) {
                        $counter = $this->get_template_block('counter_null');
                        $cat_link = str_replace('%counter%', $counter, $cat_link);
                        $cats_list .= $cat_link;
                    }
                } else {
                    $cat_link = str_replace('%counter%', '', $cat_link);
                    $cats_list .= $cat_link;
                }

            }
            $content = str_replace('%rows%', $def_link.$cats_list, $content);
        } else {
            $content = $this->get_template_block('no_data');
        }

        $content = str_replace('%header%', $header, $content);
        return $content;
    }

    public function pub_show_tags($template, $header, $ifcounter = false, $for_fulltext = true) {
        global $kernel;
        $modul_properties = $kernel->pub_modul_properties_get("taxonomy");

        if(!$modul_properties['value'])
            return;

        if(!$for_fulltext && $this->get_current_news_item())
            return;

        $this->set_templates($kernel->pub_template_parse($template));
        $tags = $this->get_tags();

        if (is_array($tags) && count($tags) > 0) {
            $tags_list = '';
            $content = $this->get_template_block('content');

            foreach($tags as $tag) {

                if ($this->pub_tagid_get() == $tag['id'])
                    $tag_link = $this->get_template_block('rows_active');
                else
                    $tag_link = $this->get_template_block('rows');

                $tag_link = str_replace('%id%', $tag['id'], $tag_link);
                $tag_link = str_replace('%name%', $tag['name'], $tag_link);

                if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                    $tag_link = str_replace('%url%', "/".$kernel->pub_page_current_get()."/".$this->url_item_tags."/".$kernel->pub_pretty_url($tag['name'])."-i".$tag['id']."/", $tag_link);
                else
                    $tag_link = str_replace('%url%', $kernel->pub_page_current_get().'.html?'.$this->url_item_tags.'='.$tag['id'], $tag_link);

                if($ifcounter) {
                    $count = $this->get_taxonomy_count($tag['id']);
                    if($count > 0) {
                        $counter = $this->get_template_block('counter');
                        $counter = str_replace('%count%', $count, $counter);
                        $tag_link = str_replace('%counter%', $counter, $tag_link);
                        $tags_list .= $tag_link;
                    } else {
                        $counter = $this->get_template_block('counter_null');
                        $tag_link = str_replace('%counter%', $counter, $tag_link);
                        $tags_list .= $tag_link;
                    }
                } else {
                    $tag_link = str_replace('%counter%', '', $tag_link);
                    $tags_list .= $tag_link;
                }

            }
            $content = str_replace('%rows%', $tags_list, $content);
        } else {
            $content = $this->get_template_block('no_data');
        }

        $content = str_replace('%header%', $header, $content);
        return $content;
    }

    function pub_item_get($item_id)
    {
        global $kernel;
        $moduleid = $kernel->pub_module_id_get();
        return $kernel->db_get_record_simple("_".$moduleid, '`available` AND `id` ="'.intval($item_id).'"', '*,DATE_FORMAT(`date`, "%d-%m-%Y") AS `date`');
    }

    function pub_items_get_by_tag($limit, $offset = 0, $type, $tagid) {
        global $kernel;
        $module_id = $kernel->pub_module_id_get();

        switch ($type) {
            case 'past':
                $order = ' ORDER by items.date DESC, items.time DESC ';
                $where = ' AND items.date <= DATE(NOW()) ';
                break;
            case 'future':
                $order = ' ORDER by items.date ASC, items.time ASC ';
                $where = ' AND items.date >= DATE(NOW()) ';
                break;
            case 'random':
                $order = ' ORDER by RAND() ';
                break;
            default:
                $order = ' ORDER by items.date DESC, items.time DESC ';
                break;
        }

        if($tagid && is_numeric($tagid)) {

            $items = array();
            $query = "SELECT items.* FROM ".$kernel->pub_prefix_get()."_".$module_id." AS items
                      LEFT JOIN ".$kernel->pub_prefix_get()."_newsi_taxonomy AS taxonomy ON items.id = taxonomy.pub_id
                      WHERE items.available = '1' AND taxonomy.module_id = '".$module_id."' AND taxonomy.tag_id = '".$tagid."' ".$where.$order.
                "LIMIT ".$offset.", ".$limit;

            $res = $kernel->runSQL($query);

            while ($row = mysqli_fetch_assoc($res)) {
                $items[] = $row;
            }
            mysqli_free_result($res);
            if (!count($items) > 0)
                return array();
            return $items;
        }
        return false;

    }

    function pub_items_get($limit, $offset = 0, $lenta = false, $type = null, $modules = '', $date = null, $start = null, $stop = null, $catid = null)
    {
        global $kernel;
        $module_id = $kernel->pub_module_id_get();
        $where = array();
        $order = array();

        $tableName = $kernel->pub_prefix_get()."_".$module_id;

        /* @TODO
        //Если дополнительные модули не передаются, значит запрашиваем новости только тек моудля
        if (empty($modules))
        $where[] = '`module_id` = "' . $module_id . '"';
        else
        {
        $tmp = '"' . str_replace(",", '","', $modules) . '"';
        $where[] = '`module_id` IN ("' . $module_id . '",' . $tmp . ')';
        }
         */

        $where[] = '`available` = 1';
        if ($lenta)
            $where[] = '`lenta` = 1';
        switch ($type)
        {
            case 'past':
                $order[] = $tableName.'.`date` DESC';
                $order[] = '`time` DESC';
                $where[] = '`date` <= DATE(NOW())';
                break;
            case 'future':
                $order[] = $tableName.'.`date` ASC';
                $order[] = '`time` ASC';
                $where[] = $tableName.'.`date` >= DATE(NOW())';
                break;
            case 'random':
                $order[] = 'RAND()';
                break;
            default:
                $order[] = $tableName.'.`date` DESC';
                $order[] = '`time` DESC';
                break;
        }

        if (!is_null($date))
            $where[] = $tableName.'.`date` = "' . $date . '"';

        if (!is_null($catid))
            $where[] = $tableName.'.`cat_id` = "' . $catid . '"';

        if (!empty($start) && empty($stop))
            $where[] = $tableName.'.`date` >= "' . $start . '"';
        elseif (empty($start) && !empty($stop))
            $where[] = $tableName.'.`date` <= "' . $stop . '"';
        elseif (!empty($start) && !empty($stop))
            $where[] = $tableName.'.`date` BETWEEN "' . $start . '" AND "' . $stop . '"';

        return $kernel->db_get_list_simple("_".$module_id, implode(' AND ', $where) . ' ORDER BY ' . implode(', ', $order), '*, DATE_FORMAT(`date`, "%d-%m-%Y") AS `date`', $offset, $limit);
    }

    function pub_offset_get()
    {
        global $kernel;
        $offset = intval($kernel->pub_httpget_get('offset'));
        return $offset;
    }

    function pub_catid_get()
    {
        global $kernel;
        $catid = intval($kernel->pub_httpget_get($this->url_item_cats));
        return $catid;
    }

    function pub_tagid_get()
    {
        global $kernel;
        $tagid = intval($kernel->pub_httpget_get($this->url_item_tags));
        return $tagid;
    }

    function pub_news_avaiable_get($type = null, $date = null, $start = null, $stop = null,$module_ids='')
    {
        global $kernel;
        $moduleid = $kernel->pub_module_id_get();

        if (preg_match('/\d{1,2}\-\d{1,2}\-\d{2,4}/', $date))
        {
            list($day, $month, $year) = explode("-", $date);
            if (checkdate($month, $day, $year))
                $date = $year . '-' . $month . '-' . $day;
            else
                $date = null;
        }
        else
            $date = null;

        if (preg_match('/\d{1,2}\-\d{1,2}\-\d{2,4}/', $start))
        {
            list($day, $month, $year) = explode("-", $start);
            if (checkdate($month, $day, $year))
                $start = $year . '-' . $month . '-' . $day;
            else
                $start = null;
        }
        else
            $start = null;

        if (preg_match('/\d{1,2}\-\d{1,2}\-\d{2,4}/', $stop))
        {
            list($day, $month, $year) = explode("-", $stop);
            if (checkdate($month, $day, $year))
                $stop = $year . '-' . $month . '-' . $day;
            else
                $stop = null;
        }
        else
            $stop = null;

        $where = array();


        /* @TODO
        if (!empty("_".$module_ids))
        {
        $tmp = '"' . str_replace(",", '","', $module_ids) . '"';
        $where[] = '`module_id` IN ("' . $moduleid . '",' . $tmp . ')';
        }
        else
        $where[] = '`module_id`="' . $moduleid . '"';
         */

        $where[] = '`available` = 1';

        switch ($type)
        {
            case 'past':
                $where[] = '`date` <= DATE(NOW())';
                break;
            case 'future':
                $where[] = '`date` >= DATE(NOW())';
                break;
            case 'default':
            default:
                break;
        }

        if (!empty($date))
            $where[] = '`date` = "' . $date . '"';

        if (!empty($start) && empty($stop))
            $where[] = '`date` >= "' . $start . '"';
        elseif (empty($start) && !empty($stop))
            $where[] = '`date` <= "' . $stop . '"';
        elseif (!empty($start) && !empty($stop))
            $where[] = '`date` BETWEEN "' . $start . '" AND "' . $stop . '"';

        $total = 0;

        $trec = $kernel->db_get_record_simple("_".$moduleid, implode(' AND ', $where), 'COUNT(*) AS `count`');
        if ($trec)
            $total=$trec['count'];

        return $total;
    }

    /**
     * Функция для построения меню для административного интерфейса
     *
     * @param pub_interface $menu Обьект класса для управления помтроеним меню
     * @return boolean
     */
    public function interface_get_menu($menu)
    {
        global $kernel;
        $modul_properties = $kernel->pub_modul_properties_get("taxonomy");
        $use_taxonomy = $modul_properties['value'];

        $menu->set_menu_block('[#news_menu_label#]');
        $menu->set_menu("[#news_menu_show_list#]", "show_list", array('flush' => 1));
        $menu->set_menu("[#news_menu_add_new#]", "show_add", array('flush' => 1));
        $menu->set_menu("[#news_menu_between#]", "select_between", array('flush' => 1));
        $menu->set_menu("[#news_menu_custom_fields#]", "custom_fields");

        if($use_taxonomy) {
            $menu->set_menu_block('[#news_menu_taxonomy#]');
            $menu->set_menu("[#news_menu_cats_list#]", "cats_list", array('flush' => 1));
            $menu->set_menu("[#news_menu_tags_list#]", "tags_list", array('flush' => 1));
        }

        $menu->set_menu_block('[#news_menu_label1#]');
        $menu->set_menu_plain($this->priv_show_date_picker());
        //$this->priv_show_date_picker();
        $menu->set_menu_default('show_list');
        return true;
    }

    function priv_show_date_picker()
    {
        global $kernel;

        $this->set_templates_admin_prefix('modules/newsi/templates_admin/');
        $this->set_templates($kernel->pub_template_parse($this->get_templates_admin_prefix() . 'date_picker.html'));
        $content = $this->get_template_block('date_picker');
        return $content;
    }

    /**
     * Функция для отображаения административного интерфейса
     *
     * @return string
     */
    public function start_admin()
    {
        global $kernel;
        $moduleid = $kernel->pub_module_id_get();
        $this->set_templates_admin_prefix('modules/newsi/templates_admin/');
        switch ($kernel->pub_section_leftmenu_get())
        {
            default:
            case 'show_list':
                return $this->priv_show_list($this->priv_get_limit(), $this->priv_get_offset(), $this->priv_get_field(), $this->priv_get_direction(), $this->priv_get_start(), $this->priv_get_stop(), $this->priv_get_date());
                break;

            case 'cats_list':
                return $this->show_cats_list();
                break;
            case 'cat_delete':
                $id = $kernel->pub_httpget_get('id');
                $this->delete_cat(intval($id));
                return $this->show_cats_list();
                break;
            case 'cat_form':
                $id = $kernel->pub_httpget_get('id');
                return $this->show_cat_form(intval($id));
                break;
            case 'save_cat':
                $id = $kernel->pub_httppost_get('cat_id');
                $name = $kernel->pub_httppost_get('cat_name');
                $description = $kernel->pub_httppost_get('cat_description');
                $this->save_cat(intval($id), $name, $description);
                return $kernel->pub_redirect_refresh_reload('cats_list');
                break;
            case 'tags_list':
                return $this->show_tags_list();
                break;
            case 'add_tag':
                $id = $kernel->pub_httppost_get('id');
                $tag = $kernel->pub_httppost_get('tag');
                return $this->add_tag(intval($id), $tag);
                break;
            case 'save_tag':
                $id = $kernel->pub_httppost_get('tag_id');
                $tag = $kernel->pub_httppost_get('tag_name');
                $description = $kernel->pub_httppost_get('tag_description');
                $this->save_tag(intval($id), $tag, $description);
                return $kernel->pub_redirect_refresh_reload('tags_list');
                break;
            case 'tag_form':
                $id = $kernel->pub_httpget_get('id');
                return $this->show_tag_form(intval($id));
                break;
            case 'tag_delete':
                $id = $kernel->pub_httpget_get('id');
                $this->delete_tag(intval($id));
                return $this->show_tags_list();
                break;
            case 'delete_taxonomy':
                $id = $kernel->pub_httpget_get('id');
                $tag_id = $kernel->pub_httpget_get('tag_id');
                $this->delete_taxonomy(intval($id), $tag_id);
                return $this->show_item_form($id);
                break;

            case 'quicksearch_tags':
                $term = $kernel->pub_httpget_get('term', false);
                $results = $this->get_quicksearch_tags($term);
                $jdata = array();
                foreach ($results as $result) {
                    $jdata[] = array("label" => $result['name'], "value" => $result['id']);
                }
                return $kernel->pub_json_encode($jdata);
                break;

            case 'select_between';
                $this->set_templates($kernel->pub_template_parse($this->get_templates_admin_prefix() . 'select_between.html'));
                $content = $this->get_template_block('form');
                $content = str_replace('%form_action%', $kernel->pub_redirect_for_form('test_select_between'), $content);
                $content = str_replace('%form_action_sucsess%', 'admin/index.php?action=set_left_menu&leftmenu=show_list', $content);
                return $content;
                break;

            case 'test_select_between':
                return '{success: true}';
                break;

            case 'show_edit':
                return $this->show_item_form(intval($kernel->pub_httpget_get('id')));
                break;

            case 'show_add':
                return $this->show_item_form();
                break;

            case 'item_save':
                $values = $kernel->pub_httppost_get('values');
                $values['description_full'] = $kernel->pub_httppost_get('content_html');
                $this->priv_item_save($values, $_FILES['image']);
                $kernel->pub_redirect_refresh_reload('show_list');
                break;

            case 'item_remove':
                $this->priv_item_delete(intval($kernel->pub_httpget_get('id')));
                $kernel->pub_redirect_refresh('show_list');
                break;

            case 'list_actions':
                $this->priv_items_do_action($kernel->pub_httppost_get('action'), $kernel->pub_httppost_get('items'));
                $kernel->pub_redirect_refresh_reload('show_list');
                break;

            case 'show_between':
                break;

            case 'custom_fields': // выводим список произвольных полей
                $this->set_templates($kernel->pub_template_parse($this->get_templates_admin_prefix() . 'fields.html'));
                $content = $this->get_template_block('custom_fields_list');
                $rows = '';
                $fields = $this->get_custom_fields($moduleid);
                $field_order = 0;
                foreach ($fields as $field) {
                    $row = $this->get_template_block('custom_fields_row');
                    $row = str_replace('%id%', $field['id'], $row);
                    $row = str_replace('%field_name%', $field['field_name'], $row);
                    $row = str_replace('%field_title%', $field['field_title'], $row);
                    $row = str_replace('%field_type%', $field['field_type'], $row);
                    $row = str_replace('%field_order%', $field_order, $row);
                    $field_order = $field_order+5;
                    $rows .= $row;
                }
                $content = str_replace('%action%', $kernel->pub_redirect_for_form("custom_fields_save"), $content);
                $content = str_replace('%rows%', $rows, $content);
                break;

            case "custom_fields_edit": // выводим форму для добавления или редактирования произвольного поля
                $this->set_templates($kernel->pub_template_parse($this->get_templates_admin_prefix() . 'fields.html'));
                $content = $this->get_template_block('custom_fields_edit');
                $id = intval($kernel->pub_httpget_get('id'));

                if($id==0) {
                    $content = str_replace('%form_title%', $this->get_template_block('custom_fields_title_new'), $content);
                    $content = str_replace('%field_title%', '', $content);
                    $content = str_replace('%field_name%', '', $content);
                    $content = str_replace('%field_type%', '', $content);
                    $content = str_replace('%disabled%', '', $content);
                    $content = str_replace('%string_selected%', '', $content);
                    $content = str_replace('%pagesite_selected%', '', $content);
                    $content = str_replace('%select_selected%', '', $content);
                    $content = str_replace('%textarea_selected%', '', $content);
                    $content = str_replace('%checkbox_selected%', '', $content);
                    $content = str_replace('%date_selected%', '', $content);
                    $content = str_replace('%fileselect_selected%', '', $content);
                    $content = str_replace('%imageselect_selected%', '', $content);
                    $content = str_replace("%field_params%", '', $content);
                } else {
                    $field = $this->get_custom_field($moduleid, $id);
                    $content = str_replace('%form_title%', $this->get_template_block('custom_fields_title_edit'), $content);
                    $content = str_replace('%field_title%', $field['field_title'], $content);
                    $content = str_replace('%field_name%', $field['field_name'], $content);

                    if($field['field_type']=='string')
                        $content = str_replace('%string_selected%', 'selected="selected"', $content);
                    elseif($field['field_type']=='pagesite')
                        $content = str_replace('%pagesite_selected%', 'selected="selected"', $content);
                    elseif($field['field_type']=='select')
                        $content = str_replace('%select_selected%', 'selected="selected"', $content);
                    elseif($field['field_type']=='textarea')
                        $content = str_replace('%textarea_selected%', 'selected="selected"', $content);
                    elseif($field['field_type']=='checkbox')
                        $content = str_replace('%checkbox_selected%', 'selected="selected"', $content);
                    elseif($field['field_type']=='date')
                        $content = str_replace('%date_selected%', 'selected="selected"', $content);
                    elseif($field['field_type']=='fileselect')
                        $content = str_replace('%fileselect_selected%', 'selected="selected"', $content);
                    elseif($field['field_type']=='imageselect')
                        $content = str_replace('%imageselect_selected%', 'selected="selected"', $content);

                    $content = str_replace("%field_params%", $field['field_params'], $content);

                    $content = str_replace('%disabled%', 'disabled="disabled"', $content);
                    $content = str_replace('%string_selected%', '', $content);
                    $content = str_replace('%pagesite_selected%', '', $content);
                    $content = str_replace('%select_selected%', '', $content);
                    $content = str_replace('%textarea_selected%', '', $content);
                    $content = str_replace('%checkbox_selected%', '', $content);
                }

                $content = str_replace('%action%', $kernel->pub_redirect_for_form("custom_fields_save"), $content);
                $content = str_replace('%id%', $id, $content);
                break;

            case 'custom_fields_save': // сохраняем/обновляем соотвествующее произвольное поле
                $this->set_templates($kernel->pub_template_parse($this->get_templates_admin_prefix() . 'fields.html'));
                $id = intval($kernel->pub_httppost_get('id'));
                $save_order = $kernel->pub_httppost_get('save_order');
                $fields_delete = $kernel->pub_httppost_get('fields_delete');
                $field_title = trim($kernel->pub_httppost_get('newsi_field_title'));
                $field_name = trim($kernel->pub_httppost_get('newsi_field_name'));

                if($fields_delete) {
                    $field_id = $kernel->pub_httppost_get('field_id');
                    foreach($field_id as $id => $value) {
                        if(is_numeric($id));
                        $this->delete_custom_field($moduleid, $id);
                    }
                    $kernel->pub_redirect_refresh_reload("custom_fields");
                    break;
                }

                if($save_order) {
                    $field_order = $kernel->pub_httppost_get('field_order');
                    foreach($field_order as $id => $order) {
                        $query = 'UPDATE `'.$kernel->pub_prefix_get().'_newsi_fields` SET `field_order`="'.$order.'" WHERE `id`="'.$id.'" AND `module_id`="'.$moduleid.'";';
                        $kernel->runSQL($query);
                    }
                    $kernel->pub_redirect_refresh_reload("custom_fields");
                    break;
                }

                if(!empty($field_name))
                    $field_name = $this->translate_string2db($field_name);
                else
                    $field_name = $this->translate_string2db($field_title);

                $field_type = $kernel->pub_httppost_get('newsi_field_type');
                $field_params = $kernel->pub_httppost_get('newsi_field_params');

                if($id==0 && !$save_order) {
                    $field_name = $this->gen_field_name($moduleid, $field_name);
                    $query = "ALTER TABLE `".$kernel->pub_prefix_get()."_".$moduleid."` ADD `".$field_name."` VARCHAR(255) NOT NULL;";
                    if($kernel->runSQL($query)) {
                        $query = 'INSERT INTO `'.$kernel->pub_prefix_get().'_newsi_fields` (`module_id`, `field_type`, `field_title`, `field_name`, `field_params`, `field_order`) VALUES ("'.$moduleid.'", "'.$field_type.'", "'.$field_title.'","'.$field_name.'", "'.$field_params.'", "0");';
                        $kernel->runSQL($query);
                    }
                } elseif($id > 0) {
                    $field = $this->get_custom_field($moduleid, $id);
                    $field_name_old = $field['field_name'];
                    $query = 'UPDATE `'.$kernel->pub_prefix_get().'_newsi_fields` SET `field_type`="'.$field_type.'", `field_title`="'.$field_title.'", `field_name`="'.$field_name.'", `field_params`="'.$field_params.'" WHERE `id`="'.$id.'" AND `module_id`="'.$moduleid.'";';
                    if($kernel->runSQL($query)) {
                        $query = "ALTER TABLE `".$kernel->pub_prefix_get()."_".$moduleid."` CHANGE `".$field_name_old."` `".$field_name."` VARCHAR(255) NOT NULL;";
                        $kernel->runSQL($query);
                    }
                }

                $kernel->pub_redirect_refresh_reload("custom_fields");
                break;

            case 'custom_fields_delete': // удаляем соотвествующее произвольное поле
                $getid = intval($kernel->pub_httpget_get('id'));
                if(!empty($getid))
                    $this->delete_custom_field($moduleid, $getid);

                $kernel->pub_redirect_refresh_reload("custom_fields");
                break;
        }

        return ((isset($content)) ? ($content) : (null));
    }

    function priv_items_do_action($action, $items)
    {
        global $kernel;
        $moduleid = $kernel->pub_module_id_get();

        if (empty($items))
            return false;

        switch ($action)
        {
            case 'lenta_on':
                $query = 'UPDATE `' . $kernel->pub_prefix_get() . '_' . $moduleid . '` SET `lenta` = "1" WHERE `id` IN (' . implode(', ', $items) . ')';
                $kernel->runSQL($query);
                break;
            case 'lenta_off':
                $query = 'UPDATE `' . $kernel->pub_prefix_get() . '_' . $moduleid . '` SET `lenta` = "0" WHERE `id` IN (' . implode(', ', $items) . ')';
                $kernel->runSQL($query);
                break;
            case 'available_on':
                $query = 'UPDATE `' . $kernel->pub_prefix_get() . '_' . $moduleid . '` SET `available` = "1" WHERE `id` IN (' . implode(', ', $items) . ')';
                $kernel->runSQL($query);
                break;
            case 'available_off':
                $query = 'UPDATE `' . $kernel->pub_prefix_get() . '_' . $moduleid . '` SET `available` = "0" WHERE `id` IN (' . implode(', ', $items) . ')';
                $kernel->runSQL($query);
                break;
            case 'rss_on':
                $query = 'UPDATE `' . $kernel->pub_prefix_get() . '_' . $moduleid . '` SET `rss` = "1" WHERE `id` IN (' . implode(', ', $items) . ')';
                $kernel->runSQL($query);
                break;
            case 'rss_off':
                $query = 'UPDATE `' . $kernel->pub_prefix_get() . '_' . $moduleid . '` SET `rss` = "0" WHERE `id` IN (' . implode(', ', $items) . ')';
                $kernel->runSQL($query);
                break;
            case 'delete':
                $query = 'DELETE FROM `' . $kernel->pub_prefix_get() . '_' . $moduleid . '` WHERE `id` IN (' . implode(', ', $items) . ')';
                $kernel->runSQL($query);
                break;
            default:
                $query = 'SELECT FROM `' . $kernel->pub_prefix_get() . '_' . $moduleid . '` WHERE `id` IN (' . implode(', ', $items) . ')';
                $kernel->runSQL($query);
                break;
        }

        return $kernel->db_affected_rows();
    }

    function priv_item_delete($item_id)
    {
        global $kernel;
        $moduleid = $kernel->pub_module_id_get();
        $query = 'DELETE FROM `' . $kernel->pub_prefix_get() . '_' . $moduleid . '` WHERE `id` = ' . $item_id . '';
        $kernel->runSQL($query);
    }

    private function priv_item_save($item_data, $file)
    {
        global $kernel;
        $moduleid = $kernel->pub_module_id_get();
        $modul_properties = $kernel->pub_modul_properties_get("taxonomy");
        $use_taxonomy = $modul_properties['value'];
        $fields = $this->get_custom_fields($moduleid);
        list($day, $month, $year) = explode('.', $item_data['date']);

        $cat_id = 0;
        $tags_list = false;
        $field_names = '';
        $field_values = '';
        if($use_taxonomy) {
            if(intval($item_data['cat']) > 0)
                $cat_id = intval($item_data['cat']);

            if(isset($item_data['tags']))
                $tags_list = explode(',', $item_data['tags']);
        }

        foreach ($fields as $field) {
            $custom_field = $kernel->pub_httppost_get($field['field_name']);
            $field_names .= ', `'.$field['field_name'].'`';
            if(!empty($custom_field)) {
                if($field['field_type']=='checkbox')
                    $field_values .= ", '".serialize($custom_field)."'";
                else
                    $field_values .= ', "'.$custom_field.'"';
            } else {
                $field_values .= ', ""';
            }
        }

        $query = 'REPLACE `' . $kernel->pub_prefix_get() . '_' . $moduleid . '`
            (`id`, `cat_id`, `date`, `time`, `available`, `lenta`, `delivery`, `rss`, `header`,
             `description_short`, `description_full`, `author`, `source_name`, `source_url`, `image`,`html_title`,`meta_keywords`,`meta_description`' . $field_names . ') '
            . ' VALUES (' . $item_data['id'] . '
            ,"' . $cat_id . '"
            ,"' . $year . '-' . $month . '-' . $day . '"
            ,"' . $item_data['time'] . '"
            ,"' . ((isset($item_data['available'])) ? (1) : (0)) . '"
            ,"' . ((isset($item_data['lenta'])) ? (1) : (0)) . '"
            ,"' . ((isset($item_data['delivery'])) ? (1) : (0)) . '"
            ,"' . ((isset($item_data['rss'])) ? (1) : (0)) . '"
            ,"' . $kernel->pub_str_prepare_set($item_data['header']) . '"
            ,"' . $kernel->pub_str_prepare_set($item_data['description_short']) . '"
            ,"' . $item_data['description_full'] . '"
            ,"' . $kernel->pub_str_prepare_set($item_data['author']) . '"
            ,"' . $kernel->pub_str_prepare_set($item_data['source_name']) . '"
            ,"' . $item_data['source_url'] . '"
            ,"' . $this->priv_get_image_filename($file, ((isset($item_data['remove_image'])) ? (true) : (false)), $item_data['id']) . '"
             ,"' . $kernel->pub_str_prepare_set($item_data['html_title']) . '"
             ,"' . $kernel->pub_str_prepare_set($item_data['meta_keywords']) . '"
             ,"' . $kernel->pub_str_prepare_set($item_data['meta_description']) . '"'
            . $field_values . ')';

        $kernel->runSQL($query);

        if (isset($item_data['id']) && is_array($tags_list) && count($tags_list) > 0) {
            foreach($tags_list as $tag) {
                $this->add_tag($item_data['id'], $tag);
            }
        }

    }


    /**
     * Обработка изображения
     *
     */
    function process_image($image_path)
    {
        global $kernel;
        $img_source_width    = $kernel->pub_modul_properties_get('img_source_width');
        $img_source_height   = $kernel->pub_modul_properties_get('img_source_height');
        $img_big_width    = $kernel->pub_modul_properties_get('img_big_width');
        $img_big_height   = $kernel->pub_modul_properties_get('img_big_height');
        $img_small_width  = $kernel->pub_modul_properties_get('img_small_width');
        $img_small_height = $kernel->pub_modul_properties_get('img_small_height');

        $watermark_path   = $kernel->pub_modul_properties_get('path_to_copyright_file');
        $watermark_place  = $kernel->pub_modul_properties_get('copyright_position');
        $watermark_transparency = $kernel->pub_modul_properties_get('copyright_transparency');

        $source_image = array('width' => $img_source_width['value'],
            'height' => $img_source_height['value']);
        $big_image = array('width' => $img_big_width['value'],
            'height' => $img_big_height['value']);
        $thumb_image = array('width' => $img_small_width['value'],
            'height' => $img_small_height['value']);

        $watermark = $kernel->pub_modul_properties_get('watermark');

        if($watermark['value'])
            $watermark_image = array('path' => $watermark_path['value'],
                'place' => $watermark_place['value'],
                'transparency' => $watermark_transparency['value']);
        else
            $watermark_image = 0;

        $path_to_save = 'content/images/' . $kernel->pub_module_id_get();

        //Имя файла пропустим через транслит, что бы исключить руcские буквы
        //отделив сначала расширение
        $file_ext = pathinfo($image_path['name'], PATHINFO_EXTENSION);
        $filename = basename($image_path['name'], ".".$file_ext);
        $only_name = $kernel->pub_translit_string($filename);
        $filename = $only_name;
        $filename = $kernel->pub_image_save($image_path['tmp_name'], $filename.'_'.rand(1000,9999), $path_to_save, $big_image, $thumb_image, $watermark_image, $source_image, $watermark_image);
        return $filename;
    }

    private function priv_get_image_filename($data, $remove = false, $item_id = null)
    {
        global $kernel;
        $moduleid = $kernel->pub_module_id_get();
        $filename = '';
        $old_filename = '';
        if (is_numeric($item_id))
        {
            $query = 'SELECT `image` FROM `' . $kernel->pub_prefix_get() . '_' . $moduleid . '` WHERE `id` = ' . $item_id . ' LIMIT 1';
            $result = $kernel->runSQL($query);

            if (mysqli_num_rows($result))
            {
                $old_filename = mysqli_fetch_array($result);
                $old_filename = $old_filename['image'];
            }
        }

        if (is_array($data) && $data['error'] == 0)
        {

            $filename = $this->process_image($data);

            if (!empty($old_filename))
            {
                $kernel->pub_file_delete('content/images/' . $kernel->pub_module_id_get() . '/' . $old_filename);
                $kernel->pub_file_delete('content/images/' . $kernel->pub_module_id_get() . '/tn/' . $old_filename);
                $kernel->pub_file_delete('content/images/' . $kernel->pub_module_id_get() . '/source/' . $old_filename);
            }

        }
        elseif ($remove)
        {
            $kernel->pub_file_delete('content/images/' . $kernel->pub_module_id_get() . '/' . $old_filename);
            $kernel->pub_file_delete('content/images/' . $kernel->pub_module_id_get() . '/tn/' . $old_filename);
            $kernel->pub_file_delete('content/images/' . $kernel->pub_module_id_get() . '/source/' . $old_filename);
            $filename = '';
        }
        elseif (!empty($old_filename))
            return $old_filename;
        return $filename;
    }

    private function show_item_form($item_id = null)
    {
        global $kernel;
        $module_id = $kernel->pub_module_id_get();
        $modul_properties = $kernel->pub_modul_properties_get("taxonomy");
        $use_taxonomy = $modul_properties['value'];

        $this->set_templates($kernel->pub_template_parse($this->get_templates_admin_prefix() . 'item_form.html'));

        $content = $this->get_template_block('form');

        if($use_taxonomy) {
            $content = str_replace('%taxonomy_cats%', $this->get_template_block('taxonomy_cats'), $content);
            $content = str_replace('%taxonomy_tags%', $this->get_template_block('taxonomy_tags'), $content);
        } else {
            $content = str_replace('%taxonomy_cats%', '', $content);
            $content = str_replace('%taxonomy_tags%', '', $content);
        }

        $content = str_replace('%form_action%', $kernel->pub_redirect_for_form('item_save'), $content);
        $content = str_replace('%id%', ((is_numeric($item_id)) ? ($item_id) : ('NULL')), $content);

        $item_data = $this->get_item_data($item_id);
        if (!empty($item_data['image']))
            $content = str_replace('%image_block%', ((is_numeric($item_id)) ? ($this->get_template_block('image')) : ('&nbsp;')), $content);
        else
            $content = str_replace('%image_block%', ('&nbsp;'), $content);

        //Если это ввод новой новости, то надо добавить значение текущего времени и даты
        if (is_null($item_id))
        {
            if (!empty($this->date_format))
                $content = str_replace('%date%', date($this->date_format), $content);
            else
                $content = str_replace('%date%', date('Y-m-d'), $content);

            $content = str_replace('%time%', date('H:i:s'), $content);

            $content = str_replace('%author%', $kernel->priv_admin_name_current_get(), $content);
        }

        // Вывод доступных произвольных полей
        $fields_list = '';
        $fields = $this->get_custom_fields($module_id);
        foreach ($fields as $field) {

            $fieldset = '';
            $field_value = '';

            if(!empty($item_data[$field['field_name']]))
                $field_value = $item_data[$field['field_name']];

            if($field['field_type']=='string') {
                $fieldset .= $this->get_template_block('field_type_string');
                $fieldset = str_replace("%field_id%", $field['id'], $fieldset);
                $fieldset = str_replace("%field_title%", $field['field_title'], $fieldset);
                $fieldset = str_replace("%field_name%", $field['field_name'], $fieldset);
                $fieldset = str_replace("%field_value%", $field_value, $fieldset);
            } elseif($field['field_type']=='pagesite') {
                $fieldset .= $this->get_template_block('field_type_pagesite');
                $fieldset = str_replace("%field_id%", $field['id'], $fieldset);
                $fieldset = str_replace("%field_title%", $field['field_title'], $fieldset);
                $fieldset = str_replace("%field_name%", $field['field_name'], $fieldset);
                $fieldset = str_replace("%field_value%", $field_value, $fieldset);
            }  elseif($field['field_type']=='textarea') {
                $fieldset .= $this->get_template_block('field_type_textarea');
                $fieldset = str_replace("%field_id%", $field['id'], $fieldset);
                $fieldset = str_replace("%field_title%", $field['field_title'], $fieldset);
                $fieldset = str_replace("%field_name%", $field['field_name'], $fieldset);
                $fieldset = str_replace("%field_value%", $field_value, $fieldset);
            } elseif($field['field_type']=='select') {
                $fieldset .= $this->get_template_block('field_type_select');
                $fieldset = str_replace("%field_id%", $field['id'], $fieldset);
                $fieldset = str_replace("%field_title%", $field['field_title'], $fieldset);
                $fieldset = str_replace("%field_name%", $field['field_name'], $fieldset);
                $options = '';
                $values = explode("\n", trim($field['field_params']));
                foreach ($values as $value) {
                    $option = $this->get_template_block('field_type_select_options');
                    $option = str_replace("%field_value%", $value, $option);
                    if(!empty($field_value)) {
                        if($field_value == trim($value))
                            $option = str_replace("%selected%", ' selected="selected"', $option);
                        else
                            $option = str_replace("%selected%", '', $option);
                    } else {
                        $option = str_replace("%selected%", '', $option);
                    }
                    $options .= $option;
                }
                $fieldset = str_replace("%options%", $options, $fieldset);
            } elseif($field['field_type']=='checkbox') {
                $fieldset .= $this->get_template_block('field_type_checkbox');
                $fieldset = str_replace("%field_id%", $field['id'], $fieldset);
                $fieldset = str_replace("%field_title%", $field['field_title'], $fieldset);
                $fieldset = str_replace("%field_name%", $field['field_name'], $fieldset);
                $options = '';
                $values = explode("\n", $field['field_params']);
                $setvalues = unserialize($field_value);
                foreach ($values as $value) {
                    $option = $this->get_template_block('field_type_checkbox_options');
                    $option = str_replace("%field_name%", trim($field['field_name']), $option);
                    $option = str_replace("%field_value%", trim($value), $option);
                    if(!empty($setvalues[trim($value)])) {
                        if($setvalues[trim($value)] == 'on')
                            $option = str_replace("%checked%", ' checked="checked"', $option);
                        else
                            $option = str_replace("%checked%", '', $option);
                    } else {
                        $option = str_replace("%checked%", '', $option);
                    }
                    $options .= $option;
                }
                $fieldset = str_replace("%options%", $options, $fieldset);
            } elseif($field['field_type']=='date') {
                $fieldset .= $this->get_template_block('field_type_date');
                $fieldset = str_replace("%field_id%", $field['id'], $fieldset);
                $fieldset = str_replace("%field_title%", $field['field_title'], $fieldset);
                $fieldset = str_replace("%field_name%", $field['field_name'], $fieldset);
                $fieldset = str_replace("%field_value%", $field_value, $fieldset);
            } elseif($field['field_type']=='fileselect') {
                $fieldset .= $this->get_template_block('field_type_fileselect');
                $fieldset = str_replace("%field_id%", $field['id'], $fieldset);
                $fieldset = str_replace("%field_title%", $field['field_title'], $fieldset);
                $fieldset = str_replace("%field_name%", $field['field_name'], $fieldset);
                $fieldset = str_replace("%field_value%", $field_value, $fieldset);
            } elseif($field['field_type']=='imageselect') {
                $fieldset .= $this->get_template_block('field_type_imageselect');
                $fieldset = str_replace("%field_id%", $field['id'], $fieldset);
                $fieldset = str_replace("%field_title%", $field['field_title'], $fieldset);
                $fieldset = str_replace("%field_name%", $field['field_name'], $fieldset);
                $fieldset = str_replace("%field_value%", $field_value, $fieldset);
            };
            $fields_list .= $fieldset;
        }
        if(!empty($fields_list)) {
            $custom_fields = $this->get_template_block('custom_fields');
            $custom_fields = str_replace("%fields_list%", $fields_list, $custom_fields);
            $content = str_replace("%custom_fields%", $custom_fields, $content);
        } else {
            $content = str_replace("%custom_fields%", '', $content);
        }

        $search = array(
            '%date%',
            '%time%',
            '%available%',
            '%lenta%',
            '%delivery%',
            '%rss%',
            '%header%',
            '%description_short%',
            '%description_full%',
            '%cats_list%',
            '%tags_list%',
            '%author%',
            '%source_name%',
            '%source_url%',
            '%image_url%',
            '%html_title%',
            '%meta_keywords%',
            '%meta_description%',
        );
        $content = str_replace($search, $this->priv_get_item_data_replace($item_id), $content);
        return $content;
    }

    function priv_get_item_data_replace($item_id)
    {
        global $kernel;
        $item_data = $this->get_item_data($item_id);

        $editor = new edit_content();
        $editor->set_edit_name('content_html');
        $editor->set_simple_theme(true);

        if (empty($item_data))
        {
            $deliver = $kernel->pub_modul_properties_get('deliver');
            $lenta = $kernel->pub_modul_properties_get('lenta');
            $rss = $kernel->pub_modul_properties_get('rss');

            $cats_list = '';
            $cats = $this->get_cats();
            if (is_array($cats) && count($cats) > 0) {
                foreach($cats as $key) {
                    $cats_list .= '<option value="'.$key['id'].'">'.$key['name'].'</option>';
                }
            }
            return array(
                '',//date
                '',//time
                'checked',//available
                (($lenta['value'] == 'on' || $lenta['value'] == 'true') ? ('checked') : ('')), //lenta
                (($deliver['value'] == 'on' || $deliver['value'] == 'true') ? ('checked') : ('')), //delivery
                (($rss['value'] == 'on' || $rss['value'] == 'true') ? ('checked') : ('')),
                '',
                '',
                $editor->create(),
                $cats_list,//cats_list
                '',//tags_list
                '',
                '',
                '',
                '',
                '',//html_title
                '',//meta_keywords
                '',//meta_description
            );
        }
        else
        {
            $editor->set_content($item_data['description_full']);

            $cats_list = '';
            $cats = $this->get_cats();
            if (is_array($cats) && count($cats) > 0) {
                foreach($cats as $key) {
                    if($key['id'] == $item_data['cat_id'])
                        $cats_list .= '<option value="'.$key['id'].'" selected="selected">'.$key['name'].'</option>';
                    else
                        $cats_list .= '<option value="'.$key['id'].'">'.$key['name'].'</option>';
                }
            }

            $item_tags = '';
            $tags = $this->get_taxonomy_list($item_id);
            if (is_array($tags) && count($tags) > 0) {
                foreach($tags as $key => $value) {
                    $item_tags .= '<li>'.$value.'<a href="#" onclick="jspub_click(\'delete_taxonomy&amp;id='.$item_id.'&amp;tag_id='.$key.'\')" title="Удалить"><img src="/admin/templates/default/images/icon_delet.gif" alt="Удалить" /></a></li>';
                }
            }

            if (!empty($this->date_format))
                $date = date($this->date_format, strtotime($item_data['date']));
            else
                $date = date('Y-m-d', strtotime($item_data['date']));

            return array(
                $date,
                $item_data['time'],
                ($item_data['lenta'] == 1) ? ('checked') : (''),
                ($item_data['available'] == 1) ? ('checked') : (''),
                ($item_data['delivery'] == 1) ? ('checked') : (''),
                ($item_data['rss'] == 1) ? ('checked') : (''),
                htmlspecialchars($item_data['header']),
                htmlspecialchars($item_data['description_short']),
                $editor->create(),
                $cats_list,
                $item_tags,
                htmlspecialchars($item_data['author']),
                htmlspecialchars($item_data['source_name']),
                $item_data['source_url'],
                '/content/images/' . $kernel->pub_module_id_get() . '/tn/' . $item_data['image'],
                htmlspecialchars($item_data['html_title']),
                htmlspecialchars($item_data['meta_keywords']),
                htmlspecialchars($item_data['meta_description']),
            );
        }
    }


    /**
     * Возвращает данные по указанному ID
     *
     * @param integer|null $item_id
     * @return array
     */
    private function get_item_data($item_id)
    {
        global $kernel;
        $moduleid = $kernel->pub_module_id_get();

        if (!is_numeric($item_id))
            return array();

        $item=$kernel->db_get_record_simple("_".$moduleid, "`id` = '".$item_id."'", "*, DATE_FORMAT(`date`,'%d-%m-%Y') AS date");
        return $item;
    }


    private function priv_get_limit()
    {
        global $kernel;
        $property = $kernel->pub_modul_properties_get('news_per_page');
        if ($property['isset'] && is_numeric($property['value']))
            return $property['value'];
        else
            return 10;
    }

    /**
     * Возвращает текущий сдвиг
     *
     * @return integer
     */
    private function priv_get_offset()
    {
        global $kernel;
        $offset = $kernel->pub_httpget_get('offset');
        if (trim($offset) == '')
            $offset = $kernel->pub_session_get('offset');
        if (!is_numeric($offset))
            $offset = 0;
        $kernel->pub_session_set('offset', $offset);
        return $offset;
    }

    private function priv_get_direction()
    {
        global $kernel;
        $direction = $kernel->pub_httpget_get('direction');
        if (empty($direction))
            $direction = $kernel->pub_session_get('direction');
        if (!in_array(strtoupper($direction), array('ASC', 'DESC')))
            $direction = 'DESC';
        $kernel->pub_session_set('direction', $direction);
        return $direction;
    }

    private function priv_get_field()
    {
        global $kernel;
        $moduleid = $kernel->pub_module_id_get();
        $query = 'SHOW COLUMNS FROM `' . $kernel->pub_prefix_get() . '_'.$moduleid.'`';
        $result = $kernel->runSQL($query);
        $fields = array();
        while ($row = mysqli_fetch_assoc($result))
        {
            $fields[] = $row['Field'];
        }
        mysqli_free_result($result);
        $field = $kernel->pub_httpget_get('field');
        if (empty($field))
            $field = $kernel->pub_session_get('field');
        if (!in_array($field, $fields))
            $field = 'date';
        $kernel->pub_session_set('field', $field);
        return $field;
    }

    private function priv_get_start()
    {
        global $kernel;
        $start = $kernel->pub_httpget_get('start');
        if (empty($start))
            $start = $kernel->pub_session_get('start');
        $kernel->pub_session_set('start', $start);
        return $start;
    }

    private function priv_get_stop()
    {
        global $kernel;
        $stop = $kernel->pub_httpget_get('stop');
        if (empty($stop))
            $stop = $kernel->pub_session_get('stop');
        $kernel->pub_session_set('stop', $stop);
        return $stop;
    }

    private function priv_get_date()
    {
        global $kernel;
        $date = $kernel->pub_httpget_get('date');
        if (empty($date))
            $date = $kernel->pub_session_get('date');
        $kernel->pub_session_set('date', $date);
        return $date;
    }


    /**
     * Отображает список новостей
     *
     * @param integer $limit Лимит новостей
     * @param integer $offset Сдвиг
     * @param string $field Поле для сортировки
     * @param string $direction НАправление сортировки
     * @param string $start
     * @param string $stop
     * @param string $date
     * @return string
     */
    private function priv_show_list($limit, $offset, $field, $direction, $start = null, $stop = null, $date = null)
    {
        global $kernel;
        $moduleid = $kernel->pub_module_id_get();
        $this->set_templates($kernel->pub_template_parse($this->get_templates_admin_prefix() . 'show_list.html'));

        if (!is_null($start) && !is_null($stop))
            $cond='`date` BETWEEN "'.$start.'" AND "'.$stop.'" ORDER BY `'.$field.'` '.$direction;
        elseif (!is_null($date))
            $cond='`date` = "' . $date . '" ORDER BY `'.$field.'` '.$direction;
        else
            $cond='`id` != 0 ORDER BY `'.$field.'` '.$direction;

        if (!empty($this->date_format)) {
            $date_format = $this->date_format;
            $date_format = str_ireplace('d', '%d', $date_format);
            $date_format = str_ireplace('m', '%m', $date_format);
            $date_format = str_ireplace('Y', '%Y', $date_format);
            $rows = $kernel->db_get_list_simple("_".$moduleid, $cond, '*, CONCAT(DATE_FORMAT(`date`, "'.$date_format.'"), \' \', `time`) as `date_time`', $offset, $limit);
        } else {
            $rows = $kernel->db_get_list_simple("_".$moduleid, $cond, '*, CONCAT(DATE_FORMAT(`date`, "%Y-%m-%d"), \' \', `time`) as `date_time`', $offset, $limit);
        }

        if (count($rows)==0)
            return $this->get_template_block('no_data');

        $lines = array();
        $first_element_number = $offset + 1;
        foreach($rows as $row)
        {
            $line = $this->get_template_block('table_body');
            $line = str_replace('%number%', $first_element_number++, $line);
            $line = str_replace('%id%', $row['id'], $line);
            $line = str_replace('%date%', $row['date_time'], $line);
            $line = str_replace('%header%', $row['header'], $line);
            $line = str_replace('%available%', (($row['available']) ? ($this->get_template_block('on')) : ($this->get_template_block('off'))), $line);
            $line = str_replace('%lenta%', (($row['lenta']) ? ($this->get_template_block('on')) : ($this->get_template_block('off'))), $line);
            $line = str_replace('%rss%', (($row['rss']) ? ($this->get_template_block('on')) : ($this->get_template_block('off'))), $line);
            $line = str_replace('%author%', $row['author'], $line);
            $line = str_replace('%action_edit%', 'show_edit', $line);
            $line = str_replace('%action_remove%', 'item_remove', $line);
            $lines[] = $line;
        }

        $header = $this->get_template_block('table_header');
        $header = str_replace('%img_sort_' . $field . '%', (($direction == 'ASC') ? ($this->get_template_block('img_sort_asc')) : ($this->get_template_block('img_sort_desc'))), $header);
        $header = preg_replace('/\%img_sort_\w+%/', '', $header);

        $content = $header . implode("\n", $lines) . $this->get_template_block('table_footer');
        $content = str_replace('%form_action%', $kernel->pub_redirect_for_form('list_actions'), $content);

        $modules = $kernel->pub_modules_get('newsi');
        $array = array();
        foreach ($modules as $module_id => $properties)
        {
            if ("_".$module_id != $kernel->pub_module_id_get())
                $array[$module_id] = $properties['caption'];
        }
        if (count($modules) > 1)
        {
            $actions = array(
                '[#news_actions_simple#]' => array(
                    'lenta_on' => '[#news_show_list_action_lenta_on#]',
                    'lenta_off' => '[#news_show_list_action_lenta_off#]',
                    'available_on' => '[#news_show_list_action_available_on#]',
                    'available_off' => '[#news_show_list_action_available_off#]',
                    'rss_on' => '[#news_show_list_action_rss_on#]',
                    'rss_off' => '[#news_show_list_action_rss_off#]',
                    'delete' => '[#news_show_list_action_delete#]'
//                    'move' => '[#news_show_list_action_move#]'
                ),
                '[#news_actions_advanced#]' => $array
            );
            $content = str_replace('%actions%', $this->priv_show_html_select('action', $actions, array(), true), $content);
        }
        else
        {
            $actions = array(
                'lenta_on' => '[#news_show_list_action_lenta_on#]',
                'lenta_off' => '[#news_show_list_action_lenta_off#]',
                'available_on' => '[#news_show_list_action_available_on#]',
                'available_off' => '[#news_show_list_action_available_off#]',
                'rss_on' => '[#news_show_list_action_rss_on#]',
                'rss_off' => '[#news_show_list_action_rss_off#]',
                'delete' => '[#news_show_list_action_delete#]',
                'move' => '[#news_show_list_action_move#]'
            );
            $content = str_replace('%actions%', $this->priv_show_html_select('action', $actions), $content);
        }

        $content = str_replace('%pages%', (is_null($date) ? ($this->priv_show_pages($offset, $limit, $field, $direction, $date, $start, $stop)) : ('')), $content);
        $sort_headers = $this->priv_get_sort_headers($field, $direction, $kernel->pub_httpget_get('date'), $start, $stop);
        $content = str_replace(array_keys($sort_headers), $sort_headers, $content);

        return $content;
    }

    private function priv_get_sort_headers($field, $direction, $date = null, $start = null, $stop = null)
    {
        $url = 'show_list&offset=0&field=%field%&direction=%direction%';

        if (!empty($date))
            $url .= '&date=' . $date;
        elseif (!empty($start) && !empty($stop))
            $url .= '&start=' . $start . '&stop=' . $stop;

        $array = array( //
            '%url_sort_id%' => (($field == 'id') ? (str_replace(array('%direction%', '%field%'), array((strtoupper($direction) == 'ASC') ? ('DESC') : ('ASC'), 'id'), $url)) : (str_replace(array('%direction%', '%field%'), array((strtoupper($direction) == 'ASC') ? ('DESC') : ('ASC'), 'id'), $url))),
            '%url_sort_date%' => (($field == 'date') ? (str_replace(array('%direction%', '%field%'), array((strtoupper($direction) == 'ASC') ? ('DESC') : ('ASC'), 'date'), $url)) : (str_replace(array('%direction%', '%field%'), array('ASC', 'date'), $url))),
            '%url_sort_header%' => (($field == 'header') ? (str_replace(array('%direction%', '%field%'), array((strtoupper($direction) == 'ASC') ? ('DESC') : ('ASC'), 'header'), $url)) : (str_replace(array('%direction%', '%field%'), array('ASC', 'header'), $url))),
            '%url_sort_available%' => (($field == 'available') ? (str_replace(array('%direction%', '%field%'), array((strtoupper($direction) == 'ASC') ? ('DESC') : ('ASC'), 'available'), $url)) : (str_replace(array('%direction%', '%field%'), array('ASC', 'available'), $url))),
            '%url_sort_lenta%' => (($field == 'lenta') ? (str_replace(array('%direction%', '%field%'), array((strtoupper($direction) == 'ASC') ? ('DESC') : ('ASC'), 'lenta'), $url)) : (str_replace(array('%direction%', '%field%'), array('ASC', 'lenta'), $url))),
            '%url_sort_rss%' => (($field == 'rss') ? (str_replace(array('%direction%', '%field%'), array((strtoupper($direction) == 'ASC') ? ('DESC') : ('ASC'), 'rss'), $url)) : (str_replace(array('%direction%', '%field%'), array('ASC', 'rss'), $url))),
            '%url_sort_author%' => (($field == 'author') ? (str_replace(array('%direction%', '%field%'), array((strtoupper($direction) == 'ASC') ? ('DESC') : ('ASC'), 'author'), $url)) : (str_replace(array('%direction%', '%field%'), array('ASC', 'author'), $url))),
        );
        return $array;
    }

    private function priv_show_pages($offset, $limit, $field, $direction, $date = null, $start = null, $stop = null)
    {
        global $kernel;
        $moduleid = $kernel->pub_module_id_get();

        $this->set_templates($kernel->pub_template_parse($this->get_templates_admin_prefix() . 'pages.html'));

        if (!empty($date))
        {
            $query = 'SELECT COUNT(*) AS totalCount FROM `' . $kernel->pub_prefix_get() . '_' . $moduleid . '` WHERE `date` = "' . $date . '"';
        }
        elseif (!empty($start) && !empty($stop))
        {
            $query = 'SELECT COUNT(*) AS totalCount FROM `' . $kernel->pub_prefix_get() . '_' . $moduleid . '` WHERE `date` BETWEEN "' . $start . '" AND "' . $stop . '"';
        }
        else
        {
            $query = 'SELECT COUNT(*) AS totalCount FROM `' . $kernel->pub_prefix_get() . '_' . $moduleid . '`';
        }



        $res=$kernel->runSQL($query);

        $total = mysqli_fetch_array($res);
        $total = $total['totalCount'];

        mysqli_free_result($res);

        $pages = ceil($total / $limit);

        if ($pages == 1)
        {
            return '';
        }

        $content = array();
        for ($page = 0; $page < $pages; $page++)
        {
            $url = 'show_list&offset=' . $limit * $page . '&field=' . $field . '&direction=' . $direction;

            if (!empty($date))
            {
                $url .= '&date=' . $date;
            }
            elseif (!empty($start) && !empty($stop))
            {
                $url .= '&start=' . $start . '&stop=' . $stop;
            }
            $content[] = str_replace(array('%url%', '%page%'), array($url, ($page + 1)), (($limit * $page == $offset) ? ($this->get_template_block('page_passive')) : ($this->get_template_block('page'))));
        }

        $content = implode($this->get_template_block('delimeter'), $content);

        return $content;
    }

    /**
     * Возвращает html код select'a
     *
     * @param string $name
     * @param array $array
     * @param array $selected
     * @param boolean $optgruop
     * @param string $style
     * @param boolean $multiple
     * @param integer $size
     * @param boolean $disabled
     * @param string $adds
     * @return string
     */
    private function priv_show_html_select($name, $array, $selected = array(), $optgruop = false, $style = "", $multiple = false, $size = 1, $disabled = false, $adds = '')
    {
        $html_select = '<select id="' . $name . '" ' . ($multiple ? 'multiple="multiple"' : '') . ' size="' . $size . '" name="' . $name . '" style="' . $style . '"' . ($disabled ? 'disabled="disabled"' : '') . ' class="text" ' . $adds . '>' . "\n";

        switch ($optgruop)
        {
            case false:
                foreach ($array as $option => $label)
                {
                    if (!is_null($selected) && in_array($option, $selected))
                    {
                        $html_select .= '<option value="' . $option . '" selected="selected"">' . htmlspecialchars($label) . '</option>' . "\n";
                    }
                    else
                    {
                        $html_select .= '<option value="' . $option . '">' . $label . '</option>' . "\n";
                    }
                }
                break;

            case true:
                foreach ($array as $key => $value)
                {
                    $html_select .= '<optgroup label="' . $key . '">' . "\n";
                    foreach ($value as $option => $label)
                    {
                        if (!is_null($selected) && in_array($option, $selected))
                        {
                            $html_select .= '<option value="' . $option . '" selected="selected" style="background-color: white;">' . htmlspecialchars($label) . '</option>' . "\n";
                        }
                        else
                        {
                            $html_select .= '<option value="' . $option . '">' . $label . '</option>' . "\n";
                        }
                    }
                    $html_select .= '</optgroup>' . "\n";
                }
                break;
        }
        $html_select .= '</select>' . "\n";
        return $html_select;
    }

    function check_date($date)
    {
        if (preg_match('/(\d{1,2})\-(\d{1,2})\-(\d{2,4})/', $date, $subpatterns) && checkdate($subpatterns[2], $subpatterns[1], $subpatterns[3]))
            return true;
        else
            return false;
    }

    public function get_news_for_submit($moduleid)
    {
        global $kernel;

        if(!$moduleid)
            $moduleid = $kernel->pub_module_id_get();

        $rows = $kernel->db_get_list_simple("_".$moduleid, '`delivery` = 1 AND `post_date` IS NULL');
        $arr = array();
        foreach ($rows as $row)
        {
            $arr[$row['id']]['time'] = $row['date'] . ' ' . $row['time'];
            $arr[$row['id']]['header'] = $row['header'];
            $arr[$row['id']]['announce'] = $row['description_short'];
        }
        return $arr;
    }

    /**
     * Возвращает все поля новости и записывает информацию о том что новость отослана
     *
     * @param integer $id
     * @param string $time
     * @param boolean $is_test
     * @return array
     */
    function get_full_info_and_submit($id, $time, $is_test = true)
    {
        global $kernel;
        $moduleid = $kernel->pub_module_id_get();

        if (!$is_test)
        {
            //Сначала напишем что новость отослана
            $query = "UPDATE " . $kernel->pub_prefix_get() . "_".$moduleid."
                        SET post_date = '$time'
                        WHERE id = $id
                        LIMIT 1";

            $kernel->runSQL($query);
        }

        //А теперь вернём полную инфу по новости
        $row = $kernel->db_get_record_simple("_".$moduleid, "id='".$id."'");

        if (!$row)
            return array();
        return $row;

    }


    /**
     * Возвращает все категории записей
     *
     * @return mixed (boolean/array)
     */
    private function get_cats() {
        global $kernel;
        $module_id = $kernel->pub_module_id_get();
        $rows = $kernel->db_get_list_simple("_newsi_cats", '`module_id` = "'.$module_id.'"');

        if($rows)
            return $rows;
        else
            return false;
    }

    /**
     * Возвращает категорию записи
     *
     * $cat_id ID рубрики
     *
     * @return mixed (boolean/array)
     */
    private function get_cat($cat_id) {
        global $kernel;
        $module_id = $kernel->pub_module_id_get();
        $rows = $kernel->db_get_record_simple("_newsi_cats", '`module_id` = "'.$module_id.'" AND `id` = "'.$cat_id.'"');

        if($rows)
            return $rows;
        else
            return false;
    }

    /**
     * Удаляет рубрику
     *
     * $cat_id ID рубрики
     *
     * @return mixed (boolean/string)
     */
    private function delete_cat($cat_id) {
        global $kernel;
        $module_id = $kernel->pub_module_id_get();
        if (is_numeric($cat_id)) {
            $query = "DELETE FROM `" . $kernel->pub_prefix_get() . "_newsi_cats` WHERE `id` = '".$cat_id."' LIMIT 1";
            if ($kernel->runSQL($query))
                $kernel->db_update_record("_".$module_id, array('cat_id'=>'0'), "`cat_id` = '".$cat_id."'");
            else
                return false;
        } else {
            return false;
        }
    }

    /**
     * Возвращает форму редактирования рубрики
     *
     * $cat_id ID рубрики
     *
     * @return string
     */
    private function show_cat_form($cat_id) {

        global $kernel;
        $module_id = $kernel->pub_module_id_get();
        $this->set_templates($kernel->pub_template_parse($this->get_templates_admin_prefix().'cats_list.html'));
        if (!is_numeric($cat_id)) {
            $form_header = '[#news_add_new_cat_label#]';
            $item = array("id" => "", "name" => "", "description" => "");
        } else {
            $form_header = '[#news_edit_cat_label#]';
            $item = $kernel->db_get_record_simple('_newsi_cats', "`module_id`='".$module_id."' AND `id`='".$cat_id."'");
        }

        $content = $this->get_template_block('form');
        $content = str_replace('%form_header%', $form_header, $content);

        $editor = new edit_content();
        $editor->set_edit_name('cat_description');
        $editor->set_simple_theme(true);
        $editor->set_content($item['description']);

        $content = str_replace('%cat_id%', $item['id'], $content);
        $content = str_replace('%cat_name%', $item['name'], $content);
        $content = str_replace('%cat_description%', $editor->create(), $content);
        $content = str_replace('%form_action%', $kernel->pub_redirect_for_form('save_cat'), $content);
        return $content;
    }

    /**
     * Сохраняет/добавляет новую рубрику без прикрепления к записи
     *
     * $cat_id ID рубрики (при редактировании)
     * $name ID заголовка рубрики
     * $description описание рубрики
     *
     * @return mixed (boolean/string)
     */
    private function save_cat($cat_id, $name, $description) {
        global $kernel;
        $module_id = $kernel->pub_module_id_get();

        if(!empty($cat_id))
            $kernel->db_update_record('_newsi_cats', array('module_id'=>$module_id, 'name'=>$name, 'description'=>$description), "`id`='".$cat_id."' AND `module_id`='".$module_id."'");
        else
            $kernel->db_add_record('_newsi_cats', array('module_id'=>$module_id, 'name'=>$name, 'description'=>$description), "INSERT");
    }


    /**
     * Добавляет тег с прикриплением к записи
     *
     * $pub_id ID публикации
     * $string текст тега
     *
     * @return mixed (boolean/string)
     */
    private function add_tag($pub_id, $string) {
        global $kernel;
        $module_id = $kernel->pub_module_id_get();
        $row = $kernel->db_get_record_simple('_newsi_tags', "`module_id`='".$module_id."' AND `name`='".$string."'");
        if (!$row)
            $tag_id = $kernel->db_add_record('_newsi_tags', array('module_id'=>$module_id, 'name'=>$string), "INSERT");
        else
            $tag_id = $row['id'];

        if(!empty($tag_id) && !empty($pub_id))
            return $this->add_taxonomy($pub_id, $tag_id, $string);
        else
            return $tag_id;
    }

    /**
     * Сохраняет/добавляет новый тег без прикрепления к записи
     *
     * $tag_id ID тега (при редактировании)
     * $name ID текст тега
     * $description описание тега
     *
     * @return mixed (boolean/string)
     */
    private function save_tag($tag_id, $name, $description) {
        global $kernel;
        $module_id = $kernel->pub_module_id_get();

        if(!empty($tag_id))
            $kernel->db_update_record('_newsi_tags', array('module_id'=>$module_id, 'name'=>$name, 'description'=>$description), "`id` = '".$tag_id."' AND `module_id` = '".$module_id."'");
        else
            $kernel->db_add_record('_newsi_tags', array('module_id'=>$module_id, 'name'=>$name, 'description'=>$description), "INSERT");
    }


    /**
     * Удаляет тег
     *
     * $pub_id ID тега
     *
     * @return mixed (boolean/string)
     */
    private function delete_tag($tag_id) {
        global $kernel;
        $module_id = $kernel->pub_module_id_get();
        if (is_numeric($tag_id)) {
            $query = "DELETE FROM `" . $kernel->pub_prefix_get() . "_newsi_tags` WHERE `id` = '" . $tag_id . "' LIMIT 1";
            if ($kernel->runSQL($query))
                $this->delete_taxonomy(false, $tag_id);
            else
                return false;
        } else {
            return false;
        }
    }


    /**
     * Добавляет тег в таксономию публикации и возвращает ссылку для АИ
     *
     * $pub_id ID публикации
     * $tag_id ID тега
     * $string текст тега
     *
     * @return string json
     */
    private function add_taxonomy($pub_id, $tag_id, $string) {
        global $kernel;
        $module_id = $kernel->pub_module_id_get();

        $query = 'INSERT INTO `'.$kernel->pub_prefix_get().'_newsi_taxonomy` (`module_id`, `pub_id`, `tag_id`) VALUES ("'.$module_id.'", "'.$pub_id.'", "'.$tag_id.'");';

        $link = '<li>'.$string.'<a href="#" onclick="jspub_click(\'delete_taxonomy&amp;id='.$pub_id.'&amp;tag_id='.$tag_id.'\')" title="Удалить"><img src="/admin/templates/default/images/icon_delet.gif" alt="Удалить" /></a></li>';

        if($kernel->runSQL($query))
            return $link;
        else
            return false;
    }

    /**
     * Удаляет один или несколько тегов из таксономии публикации
     *
     * $pub_id ID публикации
     * $tag_id ID тега/ов
     *
     * @return boolean
     */
    private function delete_taxonomy($pub_id, $tags_id) {
        global $kernel;
        $module_id = $kernel->pub_module_id_get();

        if (is_numeric($pub_id)) {
            if (!empty($tags_id) && is_array($tags_id)) {
                $query = 'DELETE FROM `' . $kernel->pub_prefix_get() . '_newsi_taxonomy` WHERE `module_id`="'.$module_id.'" AND `pub_id`="'.$pub_id.'" AND `tag_id` IN (' . implode(', ', $tags_id) . ')';
            } else if (!empty($tags_id)) {
                $query = 'DELETE FROM `' . $kernel->pub_prefix_get() . '_newsi_taxonomy` WHERE `module_id`="'.$module_id.'" AND `pub_id`="'.$pub_id.'" AND `tag_id`="'.$tags_id.'"';
            } else {
                return '{success: false}';
            }

            if($kernel->runSQL($query))
                return true;
            else
                return false;

        } else {
            $query = 'DELETE FROM `' . $kernel->pub_prefix_get() . '_newsi_taxonomy` WHERE `module_id`="'.$module_id.'" AND `tag_id`="'.$tags_id.'"';

            if($kernel->runSQL($query))
                return true;
            else
                return false;
        }
    }

    /**
     * Возвращает все теги записей
     *
     * @return mixed (boolean/array)
     */
    private function get_tags() {
        global $kernel;
        $module_id = $kernel->pub_module_id_get();
        $rows = $kernel->db_get_list_simple("_newsi_tags", '`module_id` = "'.$module_id.'"');

        if($rows)
            return $rows;
        else
            return false;
    }

    /**
     * Возвращает таксономию записи
     *
     * $pub_id ID записи
     * $type тип таксономии (tags/cats)
     *
     * @return mixed (boolean/array)
     */
    private function get_taxonomy_list($pub_id) {
        global $kernel;
        $module_id = $kernel->pub_module_id_get();

        if($pub_id && is_numeric($pub_id)) {
            $items = array();
            $query = "SELECT tags.*, taxonomy.pub_id AS pub_id FROM ".$kernel->pub_prefix_get()."_newsi_tags AS tags
                          LEFT JOIN ".$kernel->pub_prefix_get()."_newsi_taxonomy AS taxonomy ON taxonomy.tag_id = tags.id
                          WHERE tags.module_id='".$module_id."' AND taxonomy.pub_id='".$pub_id."' GROUP BY tags.id";
            $res = $kernel->runSQL($query);
            while ($row = mysqli_fetch_assoc($res)) {
                $items[$row['id']] = $row['name'];
            }
            mysqli_free_result($res);
            if (!count($items) > 0)
                return array();
            return $items;
        }
        return false;
    }

    /**
     * Возвращает количество записей в рубрике
     *
     * $cat_id ID категории
     *
     * @return mixed (boolean/array)
     */
    private function get_categories_count($cat_id) {

        global $kernel;
        $module_id = $kernel->pub_module_id_get();

        if($cat_id)
            $rows = $kernel->db_get_list_simple("_".$module_id, '`cat_id` = "'.$cat_id.'"');
        else
            $rows = $kernel->db_get_list_simple("_".$module_id);

        if($rows)
            return count($rows);
        else
            return 0;
    }

    /**
     * Возвращает количество записей с указанным тегом
     *
     * $tag_id ID тега
     *
     * @return mixed (boolean/array)
     */
    private function get_taxonomy_count($tag_id) {

        global $kernel;
        $module_id = $kernel->pub_module_id_get();
        $rows = $kernel->db_get_list_simple("_newsi_taxonomy", '`module_id` = "'.$module_id.'" AND `tag_id` = "'.$tag_id.'"');
        if($rows)
            return count($rows);
        else
            return 0;
    }

    /**
     * Возвращает список рубрик записей
     *
     * @return string
     */
    private function show_cats_list() {
        global $kernel;
        $items = $this->get_cats();
        $this->set_templates($kernel->pub_template_parse($this->get_templates_admin_prefix().'cats_list.html'));
        $html = $this->get_template_block('header');
        $content = '';
        if ($items)
        {
            $content .= $this->get_template_block('table_header');
            foreach ($items as $item)
            {
                $line = $this->get_template_block('table_body');
                $line = str_replace('%cat_id%', $item['id'], $line);
                $line = str_replace('%cat_name%', $item['name'], $line);
                $line = str_replace('%cat_description%', substr($item['description'], 0, 255).'...', $line);
                $content .= $line;
            }
            $content .= $this->get_template_block('table_footer');
        }
        else
            $content = $this->get_template_block('no_data');
        $html = str_replace("%table%", $content, $html);
        return $html;
    }


    /**
     * Возвращает список тегов записей
     *
     * @return string
     */
    private function show_tags_list() {
        global $kernel;
        $items = $this->get_tags();
        $this->set_templates($kernel->pub_template_parse($this->get_templates_admin_prefix().'tags_list.html'));
        $html = $this->get_template_block('header');
        $content = '';
        if ($items)
        {
            $content .= $this->get_template_block('table_header');
            foreach ($items as $item)
            {
                $line = $this->get_template_block('table_body');
                $line = str_replace('%tag_id%', $item['id'], $line);
                $line = str_replace('%tag_name%', $item['name'], $line);
                $line = str_replace('%tag_description%', substr($item['description'], 0, 255).'...', $line);
                $line = str_replace('%tag_use_count%', $this->get_taxonomy_count($item['id']), $line);
                $content .= $line;
            }
            $content .= $this->get_template_block('table_footer');
        }
        else
            $content = $this->get_template_block('no_data');
        $html = str_replace("%table%", $content, $html);
        return $html;
    }

    /**
     * Возвращает форму редактирования тега
     *
     * $tag_id ID тега
     *
     * @return string
     */
    private function show_tag_form($tag_id) {

        global $kernel;
        $module_id = $kernel->pub_module_id_get();
        $this->set_templates($kernel->pub_template_parse($this->get_templates_admin_prefix().'tags_list.html'));
        if (!is_numeric($tag_id)) {
            $form_header = '[#news_add_new_tag_label#]';
            $item = array("tag_id" => "", "tag_name" => "", "tag_description" => "");
        } else {
            $form_header = '[#news_edit_tag_label#]';
            $item = $kernel->db_get_record_simple('_newsi_tags', "module_id='".$module_id."' AND id='".$tag_id."'");
        }

        $content = $this->get_template_block('form');
        $content = str_replace('%form_header%', $form_header, $content);

        $editor = new edit_content();
        $editor->set_edit_name('tag_description');
        $editor->set_simple_theme(true);
        $editor->set_content($item['description']);

        $content = str_replace('%tag_id%', $item['id'], $content);
        $content = str_replace('%tag_name%', $item['name'], $content);
        $content = str_replace('%tag_description%', $editor->create(), $content);
        $content = str_replace('%form_action%', $kernel->pub_redirect_for_form('save_tag'), $content);
        return $content;
    }


    /**
     * Осуществляет быстрый поиск тегов и возвращает рузельтат
     *
     * @return string
     */
    private function get_quicksearch_tags($string) {
        global $kernel;
        $module_id = $kernel->pub_module_id_get();
        $rows = $kernel->db_get_list_simple("_newsi_tags", '`module_id` = "'.$module_id.'" AND `name` LIKE "%'.$string.'%"', "*", 0, 10);

        if($rows)
            return $rows;
        else
            return false;
    }


    /**
     * Возвращает список произвольных полей модуля по ИД
     *
     * $moduleid ID модуля
     *
     * @return array
     */
    private function get_custom_fields($moduleid)
    {
        global $kernel;
        $cond = "module_id='".$moduleid."' ORDER BY `field_order` ASC";
        return $kernel->db_get_list_simple("_newsi_fields", $cond);
    }

    /**
     * Возвращает произвольное поле по ИД
     *
     * $moduleid ID модуля
     * $id ID произвольного поля
     *
     * @return array
     */
    private function get_custom_field($moduleid, $id)
    {
        global $kernel;
        $cond = "module_id = '".$moduleid."' AND id = '".$id."'";
        return $kernel->db_get_record_simple("_newsi_fields", $cond);
    }

    /**
     * Удаляет произвольное поле по ИД
     *
     * $moduleid ID модуля
     * $id ID произвольного поля
     *
     * @return boolean
     */
    private function delete_custom_field($moduleid, $id)
    {
        global $kernel;
        $field = $this->get_custom_field($moduleid, $id);

        if(!empty($field["field_name"]))
            $kernel->runSQL('ALTER TABLE `'.$kernel->pub_prefix_get().'_'.$moduleid.'` DROP `'.$field["field_name"].'`');

        if($kernel->runSQL('DELETE FROM `'.$kernel->pub_prefix_get().'_newsi_fields` WHERE `id`="'.$id.'" AND `module_id`="'.$moduleid.'"'))
            return true;
        else
            return false;

    }

    /**
     * Генерирует новое имя произвольного поля с учётом дублей
     *
     * $moduleid ID модуля
     * $field_name желаемое имя поля
     *
     * @return string
     */
    private function gen_field_name($moduleid, $field_name)
    {
        global $kernel;
        $newfield_name = $field_name;
        $cond = "`field_name` = '".$newfield_name."'";
        $fields = $kernel->db_get_list_simple("_newsi_fields", $cond);
        $res = $kernel->runSQL("SHOW COLUMNS FROM `".$kernel->pub_prefix_get()."_".$moduleid."` WHERE `Field` = '".$newfield_name."'");
        if(count($fields)>0 || count($res)>0) {
            for($i=1;$i<=10;$i++) {
                $res = $kernel->runSQL("SHOW COLUMNS FROM `".$kernel->pub_prefix_get()."_".$moduleid."` WHERE `Field` = '".$newfield_name.$i."'");
                if(count($res->fetch_assoc())==0) {
                    $newfield_name = $newfield_name.$i;
                    break;
                }
            }
        }
        return $newfield_name;
    }


    /**
     * Транслитерирует имя произвольного поля
     *
     * $res желаемое имя поля
     *
     * @return string
     */
    private function translate_string2db($res)
    {
        global $kernel;
        $res = $kernel->pub_translit_string($res);
        $res = preg_replace("/[^0-9a-z_]/i", '', $res);
        return strtolower($res);
    }


    /**
     * Производит поиск и замену меток на значения произвольных полей
     * в зависимости от набора для данного модуля и типа данных
     *
     * $fields список доступных произвольных полей
     * $data значения произвольных полей
     * $content html-контент
     *
     * @return string
     */
    private function priv_custom_fields($fields, $data, $content)
    {
        global $kernel;

        if ($kernel->pub_https_scheme_get())
            $host = 'https://'.$kernel->pub_http_host_get();
        else
            $host = 'http://'.$kernel->pub_http_host_get();

        foreach($fields as $field) {
            if(!empty($data[$field['field_name']])) {
                if($field['field_type']=='checkbox') {
                    $setvalue = unserialize($data[$field['field_name']]);
                    $custom_field = $this->get_template_block($field['field_name']);
                    if(!empty($setvalue)) {
                        $custom_field_vals = '';
                        $custom_field_val = $this->get_template_block($field['field_name'].'_val');
                        foreach($setvalue as $key=>$value) {
                            if($value=='on')
                                $custom_field_vals .= str_replace('%setvalue%', $key, $custom_field_val);
                        }
                        $custom_field_val = $custom_field_vals;
                    } else {
                        $custom_field_val = $this->get_template_block($field['field_name'].'_null');
                    }
                    $custom_field = str_replace('%'.$field['field_name'].'_value%', $custom_field_val, $custom_field);
                    $content = str_replace('%'.$field['field_name'].'%', $custom_field, $content);
                } else {

                    if($field['field_type'] == 'textarea')
                        $value = nl2br($data[$field['field_name']]);
                    elseif($field['field_type'] == 'pagesite')
                        $value = $host.'/'.$data[$field['field_name']];
                    else
                        $value = $data[$field['field_name']];

                    $custom_field = $this->get_template_block($field['field_name']);
                    if(!empty($custom_field)) {
                        $custom_field = str_replace('%'.$field['field_name'].'_value%', $value, $custom_field);
                        $content = str_replace('%'.$field['field_name'].'%', $custom_field, $content);
                    } else {
                        $content = str_replace('%'.$field['field_name'].'%', $value, $content);
                    }

                    $content = str_replace('%'.$field['field_name'].'_value%', $value, $content);

                }

            } else {
                $custom_field_null = $this->get_template_block($field['field_name'].'_null');
                if(!empty($custom_field_null))
                    $content = str_replace('%'.$field['field_name'].'%', $custom_field_null, $content);
                else
                    $content = str_replace('%'.$field['field_name'].'%', '', $content);
            }
        }
        return $content;
    }

}
