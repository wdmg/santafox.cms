<?php
// ©SantaFox 2010г.
//
//Assenbled by Ronin
//Crazy_Cat@pop3.ru
//fixes by s@nchez

//Без параметров выводит RSS всех новостных лент
//Вызов с параметром rss.php?id=newsi1 - выдает RSS новостной ленты с ID newsi1


$doc_root = $_SERVER['DOCUMENT_ROOT']."/";
$script = $_SERVER['SCRIPT_NAME'];
$query_id = "";

include ($doc_root."ini.php"); // Файл с настройками

if (SHOW_INT_ERRORE_MESSAGE)
    error_reporting(E_ALL^E_NOTICE);
else
    error_reporting(0);

include ($doc_root."include/kernel.class.php"); //Ядро
include ($doc_root."include/security.class.php"); //Безопасность
include ($doc_root."include/log.class.php"); //Логирование
include ($doc_root."include/pub_interface.class.php");
include ($doc_root."include/frontoffice_manager.class.php"); //управление фронт офисом
include ($doc_root."admin/manager_modules.class.php"); //Менеджер управления модулями
include ($doc_root."admin/manager_users.class.php"); //Менеджер управления модулями
include ($doc_root."admin/manager_stat.class.php");

$kernel = new kernel(PREFIX);
$host_root = $kernel->pub_http_host_get();
$module_id = $kernel->pub_httpget_get("id", true, false);
$modules = $kernel->pub_modules_get("newsi");

define('DATE_FORMAT_RFC822','r');
header("Content-type: text/xml; charset=utf-8");

$last_date = date(DATE_FORMAT_RFC822);

if ($module_id) {
	if(array_key_exists($modules[$module_id]))
		$modules = $modules[$module_id];
}

/*
Поиск имени страницы для формирования новости по номеру новости в таблице 'prefix'_newsi
Сначала по таблице новостей выбираем модуль, который отвечает за вывод
новости под номером $index. Далее по 'prefix'_structure получаем имя страницы для формирования
корректного URL на новость.
*/
function page_by_id($index, $header)
{
	global $kernel;
	global $modules;
	
	$current_id = null;
	foreach($modules as $module => $params) {
		$result = $kernel->runSQL('SELECT `id` FROM `'.$kernel->pub_prefix_get().'_'.$module.'` WHERE `id`= "'.$index.'" AND `header`= "'.$header.'"');
		$id = mysqli_fetch_row($result);		
		if($id) {
			$current_id = $module;
			break;
		}
	}
	
	$pageid = $kernel->pub_modul_properties_get("page_for_lenta", $current_id);
	if(!empty($pageid["value"]))
		return $pageid["value"]."/";
	else
		return "";
};


/*
Шапка RSS
*/
echo '<?xml version="1.0" encoding="utf-8"?>
	<rss version="2.0" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:content="http://purl.org/rss/1.0/modules/content/">
	<channel>
	<title>Новостная лента</title>
    <link>http://'.$host_root.$script.'</link>
    <description>Последние новости на '.$host_root.'</description>
    <pubDate>'.$last_date.'</pubDate>
    <lastBuildDate>'.$last_date.'</lastBuildDate>
    <language>ru</language>
	<copyright></copyright>
	<image>
		<url>http://'.$host_root.'/rss_logo.png</url>
		<title>Новостная лента</title>
		<link>http://'.$host_root.'/</link>
	</image>
    ';

foreach($modules as $module => $params) {
	
	$query = "SELECT `header`,`description_short`,`id`, CAST(concat(`date`,' ',`time`) AS DATETIME) AS data
			  FROM `".$kernel->pub_prefix_get().'_'.$module.'`
			  WHERE (`date`<CURDATE() OR (`date`=CURDATE() AND `time`<=CURTIME())) AND `rss`=1
			  ORDER by `date` desc, `time` desc';
	$result = $kernel->runSQL($query);

	while ($row = $kernel->db_fetch_array($result))
	{
		$title   = strip_tags(trim($row['header']));
		$text    = $row['description_short'];
		$news_id = $row['id'];
		$pub_date = date(DATE_FORMAT_RFC822,strtotime($row['data']));
		$author = $row['author'];
		$page_url = page_by_id($news_id, $row['header']);

		if (defined("USE_PRETTY_URL") && USE_PRETTY_URL) {
			echo '<item>
			  <title>'.$title.'</title>
			  <link>http://'.$host_root.'/'.$page_url.$kernel->pub_pretty_url($row['header']).'-p'.$news_id.'.html</link>
			  <pubDate>'.$pub_date.'</pubDate>
			  <guid>http://'.$host_root.'/'.$page_url.$kernel->pub_pretty_url($row['header']).'-p'.$news_id.'.html</guid>
			  <dc:creator>'.$author.'</dc:creator>'.
			  '<description><![CDATA['.$text.']]></description>'.
			  '</item>';
		} else {
			if(empty($page_url))
				$page_url = "index.html";
			else
				$page_url = $page_url.".html";
			
			echo '<item>
			  <title>'.$title.'</title>
			  <link>http://'.$host_root.'/'.$page_url.'?id='.$news_id.'</link>
			  <pubDate>'.$pub_date.'</pubDate>
			  <guid>http://'.$host_root.'/'.$page_url.'?id='.$news_id.'</guid>
			  <dc:creator>'.$author.'</dc:creator>'.
			  '<description><![CDATA['.$text.']]></description>'.
			  '</item>';
		}

	}
}
/*
Подвал RSS
*/
echo '</channel>
</rss>';
?>
